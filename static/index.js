// Offline plugin (for progressive web app)

require('offline-plugin/runtime').install();


// pull in desired CSS/SASS files
// Note: index.scss is already imported by bootstrap-loader (see .bootstraprc).
// require('./css/index.scss');


// Inject bundled Elm app into div#main.

var Elm = require('../src/Main');
var flags = {
};
var main = Elm.Main.embed(document.getElementById('main'), flags);


// Ports

main.ports.setDocumentMetatags.subscribe(function (metatags) {
  if (metatags.hasOwnProperty('description')) {
    var element = document.head.querySelector('meta[property="og:description"]');
    if (element) {
      element.setAttribute('content', metatags.description);
    }
  }

  if (metatags.hasOwnProperty('imageUrl')) {
    var element = document.head.querySelector('meta[property="og:image"]');
    if (element) {
      element.setAttribute('content', metatags.imageUrl);
    }
  }

  if (metatags.hasOwnProperty('title')) {
    var genericTitle = "Assemblée";
    var title = metatags.title + " – " + genericTitle;

    var element = document.head.querySelector('meta[property="og:title"]');
    if (element) {
      element.setAttribute('content', title);
    }

    var elements = document.head.getElementsByTagName('title');
    if (elements.length) {
      var element = elements[0];
      element.innerText = title;
    }
  }

  var element = document.head.querySelector('meta[property="og:url"]');
  if (element) {
    element.setAttribute('content', window.location.href);
  }

  if (metatags.hasOwnProperty('twitterName')) {
    var element = document.head.querySelector('meta[property="twitter:site"]');
    if (element) {
      element.setAttribute('content', metatags.twitterName);
    }
  }
});
