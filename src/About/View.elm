module About.View exposing (..)

import Data exposing (deputeBySlug)
import About.Types exposing (..)
import Deputes.ViewsHelpers exposing (viewDepute)
import Dict exposing (Dict)
import Html exposing (..)
import Html.Attributes exposing (..)


view : Model -> Html Msg
view model =
    case Dict.get "paula-forteza" deputeBySlug of
        Just paula ->
            div []
                [ div [ class "jumbotron" ]
                    [ h1 [ class "display-3" ]
                        [ text "À l'Assemblée "
                        , small [ class "text-muted" ] [ text "(À l'AN)" ]
                        ]
                    , p
                        []
                        [ text "Un trombinoscope de l'Assemblée nationale" ]
                    ]
                , p [ class "mb-5 mt-5" ]
                    [ text "Ce site web est un logiciel libre. Vous trouverez plus d'informations et vous pouvez aussi y contribuer sur "
                    , a [ href "https://framagit.org/paula.forteza/alan", target "_blank" ] [ text "FramaGit" ]
                    , text "."
                    ]
                , p [] [ text "Ce site vous est proposé par :" ]
                , div [ class "mx-auto w-75" ] [ viewDepute paula ]
                ]

        Nothing ->
            text ""
