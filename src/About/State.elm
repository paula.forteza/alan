module About.State exposing (..)

import About.Types exposing (..)
import Navigation
import Ports
import Types exposing (Depute)
import Urls


init : Model
init =
    {}


update : InternalMsg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )


urlUpdate : Navigation.Location -> Model -> ( Model, Cmd Msg )
urlUpdate location model =
    ( model
    , Ports.setDocumentMetadata
        { description = "Tout savoir sur ce site"
        , imageUrl = Urls.appLogoFullUrl
        , title = "À propos"
        }
    )
