module Types exposing (..)


type alias Collaborateur =
    { deputes : List String
    , courriels : List String
    , enCirconscription : Bool
    , nom : String
    , nomComplet : String
    , particule : Maybe String
    , prenom : String
    , slug : String
    }


type alias Collaborateurs =
    List Collaborateur


type alias Depute =
    { circonscription : String
    , code : String
    , collaborateurs : List String
    , commission : String
    , courriels : List String
    , fonctionCommission : String
    , groupe : String
    , nom : String
    , nomComplet : String
    , particule : Maybe String
    , photo : String
    , prenom : String
    , slug : String
    , sitesWeb : List String
    , titre : String
    }


type alias Deputes =
    List Depute
