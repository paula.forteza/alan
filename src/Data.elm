module Data exposing (..)

import Dict exposing (Dict)
import ElmTextSearch
import List.Extra
import Types exposing (Collaborateur, Collaborateurs, Depute, Deputes)


collaborateurs0 : Collaborateurs
collaborateurs0 =
    [ { courriels = [ "louize.aelbrecht@gmail.com" ]
      , deputes = [ "anne-laure-cattelot" ]
      , enCirconscription = False
      , nom = "Aelbrecht"
      , nomComplet = "Louise Aelbrecht"
      , particule = Nothing
      , prenom = "Louise"
      , slug = "louise-aelbrecht"
      }
    , { courriels = [ "depute.zulesi@gmail.com" ]
      , deputes = [ "jean-marc-zulesi" ]
      , enCirconscription = False
      , nom = "Affes"
      , nomComplet = "Mehdi-Thibaut Affes"
      , particule = Nothing
      , prenom = "Mehdi-Thibaut"
      , slug = "mehdi-thibaut-affes"
      }
    , { courriels = [ "aalliot.an70@gmail.com" ]
      , deputes = [ "barbara-bessot-ballot" ]
      , enCirconscription = False
      , nom = "Alliot"
      , nomComplet = "Antoine Alliot"
      , particule = Nothing
      , prenom = "Antoine"
      , slug = "antoine-alliot"
      }
    , { courriels = [ "melodie.ambroise@gmail.com" ]
      , deputes = [ "pierre-person" ]
      , enCirconscription = False
      , nom = "Ambroise"
      , nomComplet = "Mélodie Ambroise"
      , particule = Nothing
      , prenom = "Mélodie"
      , slug = "melodie-ambroise"
      }
    , { courriels = [ "claire.b.andre@gmail.com" ]
      , deputes = [ "emilie-guerel" ]
      , enCirconscription = False
      , nom = "Andre"
      , nomComplet = "Claire Andre"
      , particule = Nothing
      , prenom = "Claire"
      , slug = "claire-andre"
      }
    , { courriels = [ "margot.antoniazzi91@gmail.com" ]
      , deputes = [ "manuel-valls" ]
      , enCirconscription = False
      , nom = "Antoniazzi"
      , nomComplet = "Margot Antoniazzi"
      , particule = Nothing
      , prenom = "Margot"
      , slug = "margot-antoniazzi"
      }
    , { courriels = [ "severinearbautrabasse@gmail.com" ]
      , deputes = [ "perrine-goulet" ]
      , enCirconscription = False
      , nom = "Arbaut"
      , nomComplet = "Séverine Arbaut"
      , particule = Nothing
      , prenom = "Séverine"
      , slug = "severine-arbaut"
      }
    , { courriels = [ "joelle.argot@clb.an.fr" ]
      , deputes = [ "jean-luc-fugit" ]
      , enCirconscription = False
      , nom = "Argot"
      , nomComplet = "Joëlle Argot"
      , particule = Nothing
      , prenom = "Joëlle"
      , slug = "joelle-argot"
      }
    , { courriels = []
      , deputes = [ "laurence-gayte" ]
      , enCirconscription = False
      , nom = "Aspar"
      , nomComplet = "Camille Aspar"
      , particule = Nothing
      , prenom = "Camille"
      , slug = "camille-aspar"
      }
    , { courriels = []
      , deputes = [ "aurelien-tache" ]
      , enCirconscription = False
      , nom = "Asseraf"
      , nomComplet = "Deborah Asseraf"
      , particule = Nothing
      , prenom = "Deborah"
      , slug = "deborah-asseraf"
      }
    , { courriels = [ "mathilde.ayral@clb-an.fr" ]
      , deputes = [ "fabienne-colboc" ]
      , enCirconscription = False
      , nom = "Ayral"
      , nomComplet = "Mathilde Ayral"
      , particule = Nothing
      , prenom = "Mathilde"
      , slug = "mathilde-ayral"
      }
    , { courriels = [ "bailleulentreprise@gmail.com" ]
      , deputes = [ "bertrand-sorre" ]
      , enCirconscription = False
      , nom = "Bailleul"
      , nomComplet = "Benjamin Bailleul"
      , particule = Nothing
      , prenom = "Benjamin"
      , slug = "benjamin-bailleul"
      }
    , { courriels = [ "ludovic.marnay@yahoo.fr" ]
      , deputes = [ "amelie-de-montchalin" ]
      , enCirconscription = False
      , nom = "Balzeau"
      , nomComplet = "Ludovic Balzeau"
      , particule = Nothing
      , prenom = "Ludovic"
      , slug = "ludovic-balzeau"
      }
    , { courriels = []
      , deputes = [ "gregory-besson-moreau" ]
      , enCirconscription = False
      , nom = "Barnier"
      , nomComplet = "Nicolas Barnier"
      , particule = Nothing
      , prenom = "Nicolas"
      , slug = "nicolas-barnier"
      }
    , { courriels = []
      , deputes = [ "laurent-saint-martin" ]
      , enCirconscription = False
      , nom = "Barraud"
      , nomComplet = "Etienne Barraud"
      , particule = Nothing
      , prenom = "Etienne"
      , slug = "etienne-barraud"
      }
    , { courriels = [ "hugo.basset@gmail.com" ]
      , deputes = [ "celia-de-lavergne" ]
      , enCirconscription = False
      , nom = "Basset"
      , nomComplet = "Hugo Basset"
      , particule = Nothing
      , prenom = "Hugo"
      , slug = "hugo-basset"
      }
    , { courriels = [ "Baudry.benjamin@gmail.com" ]
      , deputes = [ "delphine-o" ]
      , enCirconscription = False
      , nom = "Baudry"
      , nomComplet = "Benjamin Baudry"
      , particule = Nothing
      , prenom = "Benjamin"
      , slug = "benjamin-baudry"
      }
    , { courriels = [ "kevin.baudy@clb-an.fr" ]
      , deputes = [ "raphael-gerard" ]
      , enCirconscription = False
      , nom = "Baudy"
      , nomComplet = "Kevin Baudy"
      , particule = Nothing
      , prenom = "Kevin"
      , slug = "kevin-baudy"
      }
    , { courriels = []
      , deputes = [ "audrey-dufeu-schubert" ]
      , enCirconscription = False
      , nom = "Benaissa"
      , nomComplet = "William Benaissa"
      , particule = Nothing
      , prenom = "William"
      , slug = "william-benaissa"
      }
    , { courriels = [ "sacha.benhamou@clb-an.fr" ]
      , deputes = [ "olivier-veran" ]
      , enCirconscription = False
      , nom = "Benhamou"
      , nomComplet = "Sacha Benhamou"
      , particule = Nothing
      , prenom = "Sacha"
      , slug = "sacha-benhamou"
      }
    , { courriels = [ "alexandre.bianco@clb-an.fr" ]
      , deputes = [ "pacome-rupin" ]
      , enCirconscription = False
      , nom = "Bianco"
      , nomComplet = "Alexandre Bianco"
      , particule = Nothing
      , prenom = "Alexandre"
      , slug = "alexandre-bianco"
      }
    , { courriels = [ "bianquis.lionel@gmail.com" ]
      , deputes = [ "coralie-dubost" ]
      , enCirconscription = False
      , nom = "Bianquis"
      , nomComplet = "Lionel Bianquis"
      , particule = Nothing
      , prenom = "Lionel"
      , slug = "lionel-bianquis"
      }
    , { courriels = []
      , deputes = [ "paula-forteza" ]
      , enCirconscription = False
      , nom = "Billard"
      , nomComplet = "Marianne Billard"
      , particule = Nothing
      , prenom = "Marianne"
      , slug = "marianne-billard"
      }
    , { courriels = [ "clemencebillot@gmail.com" ]
      , deputes = [ "laetitia-saint-paul" ]
      , enCirconscription = False
      , nom = "Billot"
      , nomComplet = "Clémence Billot"
      , particule = Nothing
      , prenom = "Clémence"
      , slug = "clemence-billot"
      }
    , { courriels = [ "charline.birault@clb-an.fr" ]
      , deputes = [ "herve-pellois" ]
      , enCirconscription = False
      , nom = "Birault"
      , nomComplet = "Charline Birault"
      , particule = Nothing
      , prenom = "Charline"
      , slug = "charline-birault"
      }
    , { courriels = [ "blondel_sabrina@yahoo.fr" ]
      , deputes = [ "jennifer-de-temmerman" ]
      , enCirconscription = False
      , nom = "Blondel"
      , nomComplet = "Sabrina Blondel"
      , particule = Nothing
      , prenom = "Sabrina"
      , slug = "sabrina-blondel"
      }
    , { courriels = [ "bodoignet.emmanuel@gmail.com" ]
      , deputes = [ "celine-calvez" ]
      , enCirconscription = False
      , nom = "Bodoignet"
      , nomComplet = "Emannuel Bodoignet"
      , particule = Nothing
      , prenom = "Emannuel"
      , slug = "emannuel-bodoignet"
      }
    , { courriels = [ "bonfils.dylan@gmail.com" ]
      , deputes = [ "laurence-maillart-mehaignerie" ]
      , enCirconscription = False
      , nom = "Bonfils"
      , nomComplet = "Dylan Bonfils"
      , particule = Nothing
      , prenom = "Dylan"
      , slug = "dylan-bonfils"
      }
    , { courriels = []
      , deputes = [ "denis-masseglia" ]
      , enCirconscription = False
      , nom = "Bossard-Gautier"
      , nomComplet = "Carole Bossard-Gautier"
      , particule = Nothing
      , prenom = "Carole"
      , slug = "carole-bossard-gautier"
      }
    , { courriels = [ "r.bouachra@gmail.com" ]
      , deputes = [ "delphine-o" ]
      , enCirconscription = False
      , nom = "Bouachra"
      , nomComplet = "Rhizlane Bouachra"
      , particule = Nothing
      , prenom = "Rhizlane"
      , slug = "rhizlane-bouachra"
      }
    , { courriels = [ "olivier.boudet@clb-an.fr" ]
      , deputes = [ "nicolas-demoulin" ]
      , enCirconscription = False
      , nom = "Boudet"
      , nomComplet = "Olivier Boudet"
      , particule = Nothing
      , prenom = "Olivier"
      , slug = "olivier-boudet"
      }
    , { courriels = [ "yann-z@hotmail.fr" ]
      , deputes = [ "philippe-chalumeau" ]
      , enCirconscription = False
      , nom = "Boulay"
      , nomComplet = "Yann Boulay"
      , particule = Nothing
      , prenom = "Yann"
      , slug = "yann-boulay"
      }
    , { courriels = [ "karim.boulkhoubz@gmail.com" ]
      , deputes = [ "cecile-rilhac" ]
      , enCirconscription = False
      , nom = "Boulkhoubz"
      , nomComplet = "Karim Boulkhoubz"
      , particule = Nothing
      , prenom = "Karim"
      , slug = "karim-boulkhoubz"
      }
    , { courriels = [ "rohman.boumlih14@gmail.com" ]
      , deputes = [ "fiona-lazaar" ]
      , enCirconscription = False
      , nom = "Boumlih"
      , nomComplet = "Roman Boumlih"
      , particule = Nothing
      , prenom = "Roman"
      , slug = "roman-boumlih"
      }
    , { courriels = [ "laure.bourdon@clb-an.fr" ]
      , deputes = [ "hugues-renson" ]
      , enCirconscription = False
      , nom = "Bourdon"
      , nomComplet = "Laure Bourdon"
      , particule = Nothing
      , prenom = "Laure"
      , slug = "laure-bourdon"
      }
    , { courriels = []
      , deputes = [ "sabine-thillaye" ]
      , enCirconscription = False
      , nom = "Bousquet"
      , nomComplet = "Benjamin Bousquet"
      , particule = Nothing
      , prenom = "Benjamin"
      , slug = "benjamin-bousquet"
      }
    , { courriels = [ "s.boussion@orange.fr" ]
      , deputes = [ "stella-dupont" ]
      , enCirconscription = False
      , nom = "Boussion"
      , nomComplet = "Sebastien Boussion"
      , particule = Nothing
      , prenom = "Sebastien"
      , slug = "sebastien-boussion"
      }
    , { courriels = [ "eleonorebranchy@gmail.com" ]
      , deputes = [ "blandine-brocard" ]
      , enCirconscription = False
      , nom = "Branchy"
      , nomComplet = "Eléonore Branchy"
      , particule = Nothing
      , prenom = "Eléonore"
      , slug = "eleonore-branchy"
      }
    , { courriels = [ "antoine.brandalac@gmail.com" ]
      , deputes = [ "aude-amadou" ]
      , enCirconscription = False
      , nom = "Brandalac"
      , nomComplet = "Antoine Brandalac"
      , particule = Nothing
      , prenom = "Antoine"
      , slug = "antoine-brandalac"
      }
    , { courriels = [ "gaspard.brochard@gmail.com" ]
      , deputes = [ "olivia-gregoire" ]
      , enCirconscription = False
      , nom = "Brochard"
      , nomComplet = "Gaspard Brochard"
      , particule = Nothing
      , prenom = "Gaspard"
      , slug = "gaspard-brochard"
      }
    , { courriels = []
      , deputes = [ "carole-grandjean" ]
      , enCirconscription = False
      , nom = "Brockly"
      , nomComplet = "Céline Brockly"
      , particule = Nothing
      , prenom = "Céline"
      , slug = "celine-brockly"
      }
    , { courriels = [ "broutcedric@gmail.com" ]
      , deputes = [ "fabien-gouttefarde" ]
      , enCirconscription = False
      , nom = "Brout"
      , nomComplet = "Cédric Brout"
      , particule = Nothing
      , prenom = "Cédric"
      , slug = "cedric-brout"
      }
    , { courriels = [ "leonor.brucker@clb-an.fr" ]
      , deputes = [ "albane-gaillot" ]
      , enCirconscription = False
      , nom = "Brucker"
      , nomComplet = "Léonor Brucker"
      , particule = Nothing
      , prenom = "Léonor"
      , slug = "leonor-brucker"
      }
    , { courriels = [ "jimmybrumant@gmail.com" ]
      , deputes = [ "bruno-bonnell" ]
      , enCirconscription = False
      , nom = "Brumant"
      , nomComplet = "Jimmy Brumant"
      , particule = Nothing
      , prenom = "Jimmy"
      , slug = "jimmy-brumant"
      }
    , { courriels = [ "chrbrunel@gmail.com" ]
      , deputes = [ "martine-wonner" ]
      , enCirconscription = False
      , nom = "Brunelle"
      , nomComplet = "Christophe Brunelle"
      , particule = Nothing
      , prenom = "Christophe"
      , slug = "christophe-brunelle"
      }
    , { courriels = [ "tanguy.buche@yaelbraunpivet.fr" ]
      , deputes = [ "yael-braun-pivet" ]
      , enCirconscription = False
      , nom = "Buche"
      , nomComplet = "Tanguy Buche"
      , particule = Nothing
      , prenom = "Tanguy"
      , slug = "tanguy-buche"
      }
    , { courriels = [ "collaborateur.thomasmesnier@gmail.com" ]
      , deputes = [ "thomas-mesnier" ]
      , enCirconscription = False
      , nom = "Buffeteau"
      , nomComplet = "Fabien Buffeteau"
      , particule = Nothing
      , prenom = "Fabien"
      , slug = "fabien-buffeteau"
      }
    , { courriels = [ "mickael.burlot.an@gmail.com" ]
      , deputes = [ "frederique-dumas" ]
      , enCirconscription = False
      , nom = "Burlot"
      , nomComplet = "Mickael Burlot"
      , particule = Nothing
      , prenom = "Mickael"
      , slug = "mickael-burlot"
      }
    , { courriels = []
      , deputes = [ "carole-grandjean" ]
      , enCirconscription = False
      , nom = "Butault"
      , nomComplet = "Anne-Pauline Butault"
      , particule = Nothing
      , prenom = "Anne-Pauline"
      , slug = "anne-pauline-butault"
      }
    , { courriels = [ "jpcalmus@yahoo.fr" ]
      , deputes = [ "mustapha-laabid" ]
      , enCirconscription = False
      , nom = "Calmus"
      , nomComplet = "Jean-Philippe Calmus"
      , particule = Nothing
      , prenom = "Jean-Philippe"
      , slug = "jean-philippe-calmus"
      }
    , { courriels = [ "joan.carbuccia@gmail.com" ]
      , deputes = [ "delphine-bagarry" ]
      , enCirconscription = False
      , nom = "Carbuccia"
      , nomComplet = "Joan Carbuccia"
      , particule = Nothing
      , prenom = "Joan"
      , slug = "joan-carbuccia"
      }
    , { courriels = [ "Mjid.elguerrab@assemblee-nationale.fr" ]
      , deputes = [ "m-jid-el-guerrab" ]
      , enCirconscription = False
      , nom = "Carneiro"
      , nomComplet = "Elsa Carneiro"
      , particule = Nothing
      , prenom = "Elsa"
      , slug = "elsa-carneiro"
      }
    , { courriels = [ "eleonore.cartillier@gmail.com" ]
      , deputes = [ "matthieu-orphelin" ]
      , enCirconscription = False
      , nom = "Cartillier"
      , nomComplet = "Eleonore Cartillier"
      , particule = Nothing
      , prenom = "Eleonore"
      , slug = "eleonore-cartillier"
      }
    , { courriels = []
      , deputes = [ "bruno-questel" ]
      , enCirconscription = False
      , nom = "Celik"
      , nomComplet = "Jacob Celik"
      , particule = Nothing
      , prenom = "Jacob"
      , slug = "jacob-celik"
      }
    , { courriels = [ "ceylan.mehmetc@gmail.com" ]
      , deputes = [ "frederic-descrozaille" ]
      , enCirconscription = False
      , nom = "Ceylan"
      , nomComplet = "Mehmet Ceylan"
      , particule = Nothing
      , prenom = "Mehmet"
      , slug = "mehmet-ceylan"
      }
    , { courriels = [ "kchafi78@yahoo.fr" ]
      , deputes = [ "marc-delatte" ]
      , enCirconscription = False
      , nom = "Chafi"
      , nomComplet = "Karim Chafi"
      , particule = Nothing
      , prenom = "Karim"
      , slug = "karim-chafi"
      }
    , { courriels = [ "agathe.chamfeuil@clb-an.fr" ]
      , deputes = [ "laetitia-avia" ]
      , enCirconscription = False
      , nom = "Chamfeuil"
      , nomComplet = "Agathe Chamfeuil"
      , particule = Nothing
      , prenom = "Agathe"
      , slug = "agathe-chamfeuil"
      }
    , { courriels = [ "Barbara.chancrin@clb-an.fr" ]
      , deputes = [ "stephanie-do" ]
      , enCirconscription = False
      , nom = "Chancrin"
      , nomComplet = "Barbara Chancrin"
      , particule = Nothing
      , prenom = "Barbara"
      , slug = "barbara-chancrin"
      }
    , { courriels = [ "maelle.charreau@sciencespo.fr" ]
      , deputes = [ "stanislas-guerini" ]
      , enCirconscription = False
      , nom = "Charreau"
      , nomComplet = "Maëlle Charreau"
      , particule = Nothing
      , prenom = "Maëlle"
      , slug = "maelle-charreau"
      }
    , { courriels = [ "kchatouani@yahoo.com" ]
      , deputes = [ "frederic-descrozaille" ]
      , enCirconscription = False
      , nom = "Chatouani"
      , nomComplet = "Khadija Chatouani"
      , particule = Nothing
      , prenom = "Khadija"
      , slug = "khadija-chatouani"
      }
    , { courriels = [ "emmanuel@descrozaille.fr" ]
      , deputes = [ "frederic-descrozaille" ]
      , enCirconscription = False
      , nom = "Chaumery"
      , nomComplet = "Emmanuel Chaumery"
      , particule = Nothing
      , prenom = "Emmanuel"
      , slug = "emmanuel-chaumery"
      }
    , { courriels = [ "Chris.chenebault@gmail.com" ]
      , deputes = [ "marie-guevenoux" ]
      , enCirconscription = False
      , nom = "Chenebault"
      , nomComplet = "Chris Chenebault"
      , particule = Nothing
      , prenom = "Chris"
      , slug = "chris-chenebault"
      }
    , { courriels = [ "philippecheron.em@gmail.com" ]
      , deputes = [ "coralie-dubost" ]
      , enCirconscription = False
      , nom = "Cheron"
      , nomComplet = "Philippe Cheron"
      , particule = Nothing
      , prenom = "Philippe"
      , slug = "philippe-cheron"
      }
    , { courriels = [ "arthur.cheul@clb-an.fr" ]
      , deputes = [ "olga-givernet" ]
      , enCirconscription = False
      , nom = "Cheul"
      , nomComplet = "Arthur Cheul"
      , particule = Nothing
      , prenom = "Arthur"
      , slug = "arthur-cheul"
      }
    , { courriels = [ "romain.chevrey@gmail.com" ]
      , deputes = [ "aurelien-tache" ]
      , enCirconscription = False
      , nom = "Chevrey"
      , nomComplet = "Romain Chevrey"
      , particule = Nothing
      , prenom = "Romain"
      , slug = "romain-chevrey"
      }
    , { courriels = [ "alexis.cintrat@clb-an.fr" ]
      , deputes = [ "veronique-riotton" ]
      , enCirconscription = False
      , nom = "Cintrat"
      , nomComplet = "Alexis Cintrat"
      , particule = Nothing
      , prenom = "Alexis"
      , slug = "alexis-cintrat"
      }
    , { courriels = [ "Nicole.lepeih@assemblee-nationale.fr" ]
      , deputes = [ "nicole-le-peih" ]
      , enCirconscription = False
      , nom = "Cloe-Sola"
      , nomComplet = "Paola Cloe-Sola"
      , particule = Nothing
      , prenom = "Paola"
      , slug = "paola-cloe-sola"
      }
    , { courriels = [ "mcconstans@gmail.com" ]
      , deputes = [ "mickael-nogal" ]
      , enCirconscription = False
      , nom = "Constans"
      , nomComplet = "Marie-Claire Constans"
      , particule = Nothing
      , prenom = "Marie-Claire"
      , slug = "marie-claire-constans"
      }
    , { courriels = [ "julien.coquenas@gmail.com" ]
      , deputes = [ "corinne-vignon" ]
      , enCirconscription = False
      , nom = "Coquenas"
      , nomComplet = "Julien Coquenas"
      , particule = Nothing
      , prenom = "Julien"
      , slug = "julien-coquenas"
      }
    , { courriels = [ "Cordon.valerie@neuf.fr" ]
      , deputes = [ "jacques-marilossian" ]
      , enCirconscription = False
      , nom = "Cordon"
      , nomComplet = "Valérie Cordon"
      , particule = Nothing
      , prenom = "Valérie"
      , slug = "valerie-cordon"
      }
    , { courriels = [ "corone.alphonse@gmail.com" ]
      , deputes = [ "guillaume-kasbarian" ]
      , enCirconscription = False
      , nom = "Corone"
      , nomComplet = "Alphonse Corone"
      , particule = Nothing
      , prenom = "Alphonse"
      , slug = "alphonse-corone"
      }
    , { courriels = [ "axelle.corosine@hotmail.fr" ]
      , deputes = [ "carole-grandjean" ]
      , enCirconscription = False
      , nom = "Corosine"
      , nomComplet = "Axelle Corosine"
      , particule = Nothing
      , prenom = "Axelle"
      , slug = "axelle-corosine"
      }
    , { courriels = [ "jaimecostacentena@gmail.com" ]
      , deputes = [ "jean-noel-barrot" ]
      , enCirconscription = False
      , nom = "Costa Centena"
      , nomComplet = "Jaime Costa Centena"
      , particule = Nothing
      , prenom = "Jaime"
      , slug = "jaime-costa-centena"
      }
    , { courriels = [ "stephanie.cotrel@orange.fr" ]
      , deputes = [ "aude-amadou" ]
      , enCirconscription = False
      , nom = "Cotrel"
      , nomComplet = "Stéphanie Cotrel"
      , particule = Nothing
      , prenom = "Stéphanie"
      , slug = "stephanie-cotrel"
      }
    , { courriels = []
      , deputes = [ "lionel-causse" ]
      , enCirconscription = False
      , nom = "Courant"
      , nomComplet = "Antoine Courant"
      , particule = Nothing
      , prenom = "Antoine"
      , slug = "antoine-courant"
      }
    , { courriels = [ "thibaut.coussens@hotmail.fr" ]
      , deputes = [ "christophe-euzet" ]
      , enCirconscription = False
      , nom = "Coussens"
      , nomComplet = "Thibaut Coussens"
      , particule = Nothing
      , prenom = "Thibaut"
      , slug = "thibaut-coussens"
      }
    , { courriels = [ "jean-michel.fauvergue@assemblee-nationale.fr" ]
      , deputes = [ "jean-michel-fauvergue" ]
      , enCirconscription = False
      , nom = "Coutant"
      , nomComplet = "Antoine Coutant"
      , particule = Nothing
      , prenom = "Antoine"
      , slug = "antoine-coutant"
      }
    , { courriels = []
      , deputes = [ "herve-berville" ]
      , enCirconscription = False
      , nom = "D'Yvoire"
      , nomComplet = "Thomas D'Yvoire"
      , particule = Nothing
      , prenom = "Thomas"
      , slug = "thomas-d-yvoire"
      }
    , { courriels = [ "theo.dardonville@clb-an.fr" ]
      , deputes = [ "gilles-le-gendre" ]
      , enCirconscription = False
      , nom = "Dardonville"
      , nomComplet = "Théo Dardonville"
      , particule = Nothing
      , prenom = "Théo"
      , slug = "theo-dardonville"
      }
    , { courriels = [ "Maeva.dargaud-tarquin@clb-an.fr" ]
      , deputes = [ "raphael-gerard" ]
      , enCirconscription = False
      , nom = "Dargaud-Tarquin"
      , nomComplet = "Maeva Dargaud-Tarquin"
      , particule = Nothing
      , prenom = "Maeva"
      , slug = "maeva-dargaud-tarquin"
      }
    , { courriels = [ "Sam.Dautrevaux@clb-an.fr" ]
      , deputes = [ "sylvie-charriere" ]
      , enCirconscription = False
      , nom = "Dautrevaux"
      , nomComplet = "Sam Dautrevaux"
      , particule = Nothing
      , prenom = "Sam"
      , slug = "sam-dautrevaux"
      }
    , { courriels = [ "Florence.David@clb-an.fr" ]
      , deputes = [ "caroline-abadie" ]
      , enCirconscription = False
      , nom = "David"
      , nomComplet = "Florence David"
      , particule = Nothing
      , prenom = "Florence"
      , slug = "florence-david"
      }
    , { courriels = [ "vincent.debas@gmail.com" ]
      , deputes = [ "fabien-gouttefarde" ]
      , enCirconscription = False
      , nom = "Debas"
      , nomComplet = "Vincent Debas"
      , particule = Nothing
      , prenom = "Vincent"
      , slug = "vincent-debas"
      }
    , { courriels = [ "eleann@hotmail.fr" ]
      , deputes = [ "jean-francois-mbaye" ]
      , enCirconscription = False
      , nom = "Delannoy"
      , nomComplet = "Eleonore Delannoy"
      , particule = Nothing
      , prenom = "Eleonore"
      , slug = "eleonore-delannoy"
      }
    , { courriels = [ "Nicolas.c.delmas@gmail.com" ]
      , deputes = [ "sebastien-cazenove" ]
      , enCirconscription = False
      , nom = "Delmas"
      , nomComplet = "Nicolas Delmas"
      , particule = Nothing
      , prenom = "Nicolas"
      , slug = "nicolas-delmas"
      }
    , { courriels = [ "selim.denoyelle@gmail.com" ]
      , deputes = [ "christine-hennion" ]
      , enCirconscription = False
      , nom = "Denoyelle"
      , nomComplet = "Selim Denoyelle"
      , particule = Nothing
      , prenom = "Selim"
      , slug = "selim-denoyelle"
      }
    , { courriels = [ "laurederoche@orange.fr" ]
      , deputes = [ "nathalie-sarles" ]
      , enCirconscription = False
      , nom = "Deroche"
      , nomComplet = "Laure Deroche"
      , particule = Nothing
      , prenom = "Laure"
      , slug = "laure-deroche"
      }
    , { courriels = [ "nadiadesbois82@gmail.com" ]
      , deputes = [ "laurianne-rossi" ]
      , enCirconscription = False
      , nom = "Desbois"
      , nomComplet = "Nadia Desbois"
      , particule = Nothing
      , prenom = "Nadia"
      , slug = "nadia-desbois"
      }
    , { courriels = [ "celine.devoise@clb-an.fr" ]
      , deputes = [ "perrine-goulet" ]
      , enCirconscription = False
      , nom = "Devoise"
      , nomComplet = "Céline Devoise"
      , particule = Nothing
      , prenom = "Céline"
      , slug = "celine-devoise"
      }
    , { courriels = [ "charlotte.dewitte@clb-an.fr" ]
      , deputes = [ "xavier-roseren" ]
      , enCirconscription = False
      , nom = "Dewitte"
      , nomComplet = "Charlotte Dewitte"
      , particule = Nothing
      , prenom = "Charlotte"
      , slug = "charlotte-dewitte"
      }
    , { courriels = [ "alizata.diallo@clb-an.fr" ]
      , deputes = [ "stephanie-do" ]
      , enCirconscription = False
      , nom = "Diallo"
      , nomComplet = "Alizata Diallo"
      , particule = Nothing
      , prenom = "Alizata"
      , slug = "alizata-diallo"
      }
    , { courriels = [ "juan.diazdelcano@clb-an.fr" ]
      , deputes = [ "naima-moutchou" ]
      , enCirconscription = False
      , nom = "Diaz Del Cano"
      , nomComplet = "Juan Diaz Del Cano"
      , particule = Nothing
      , prenom = "Juan"
      , slug = "juan-diaz-del-cano"
      }
    , { courriels = [ "maxime.donadille@clb-an.fr" ]
      , deputes = [ "eric-bothorel" ]
      , enCirconscription = False
      , nom = "Donadille"
      , nomComplet = "Maxime Donadille"
      , particule = Nothing
      , prenom = "Maxime"
      , slug = "maxime-donadille"
      }
    , { courriels = [ "raphael.dorgans@gmail.com" ]
      , deputes = [ "pieyre-alexandre-anglade" ]
      , enCirconscription = False
      , nom = "Dorgans"
      , nomComplet = "Raphaël Dorgans"
      , particule = Nothing
      , prenom = "Raphaël"
      , slug = "raphael-dorgans"
      }
    , { courriels = [ "paula.doumerg@gmail.com" ]
      , deputes = [ "guillaume-gouffier-cha" ]
      , enCirconscription = False
      , nom = "Doumerg"
      , nomComplet = "Paula Doumerg"
      , particule = Nothing
      , prenom = "Paula"
      , slug = "paula-doumerg"
      }
    , { courriels = [ "juliedulcire@outlook.com" ]
      , deputes = [ "olivier-veran" ]
      , enCirconscription = False
      , nom = "Dulcire"
      , nomComplet = "Julie Dulcire"
      , particule = Nothing
      , prenom = "Julie"
      , slug = "julie-dulcire"
      }
    , { courriels = [ "gabriel.dumont@clb-an.fr" ]
      , deputes = [ "amal-amelia-lakrafi" ]
      , enCirconscription = False
      , nom = "Dumont"
      , nomComplet = "Gabriel Dumont"
      , particule = Nothing
      , prenom = "Gabriel"
      , slug = "gabriel-dumont"
      }
    , { courriels = [ "salmaelayyadi@gmail.com" ]
      , deputes = [ "caroline-abadie" ]
      , enCirconscription = False
      , nom = "El Ayyadi"
      , nomComplet = "Salma El Ayyadi"
      , particule = Nothing
      , prenom = "Salma"
      , slug = "salma-el-ayyadi"
      }
    , { courriels = [ "ijja.elghoul@clb-an.fr" ]
      , deputes = [ "anne-laure-cattelot" ]
      , enCirconscription = False
      , nom = "El Ghoul"
      , nomComplet = "Ijja El Ghoul"
      , particule = Nothing
      , prenom = "Ijja"
      , slug = "ijja-el-ghoul"
      }
    , { courriels = [ "lebohec.depute@gmail.com" ]
      , deputes = [ "gael-le-bohec" ]
      , enCirconscription = False
      , nom = "Elleouet"
      , nomComplet = "Nicolas Elleouet"
      , particule = Nothing
      , prenom = "Nicolas"
      , slug = "nicolas-elleouet"
      }
    ]


collaborateurs1 : Collaborateurs
collaborateurs1 =
    [ { courriels = [ "aida.epelbaum@clb-an.fr" ]
      , deputes = [ "christophe-arend" ]
      , enCirconscription = False
      , nom = "Epelbaum"
      , nomComplet = "Aïda Epelbaum"
      , particule = Nothing
      , prenom = "Aïda"
      , slug = "aida-epelbaum"
      }
    , { courriels = [ "emma.ettlinger@clb-an.fr" ]
      , deputes = [ "sacha-houlie" ]
      , enCirconscription = False
      , nom = "Ettlinger"
      , nomComplet = "Emma Ettlinger"
      , particule = Nothing
      , prenom = "Emma"
      , slug = "emma-ettlinger"
      }
    , { courriels = [ "jacqueline.durler@orange.fr" ]
      , deputes = [ "jean-francois-mbaye" ]
      , enCirconscription = False
      , nom = "Eude-Dürler"
      , nomComplet = "Jacqueline Eude-Dürler"
      , particule = Nothing
      , prenom = "Jacqueline"
      , slug = "jacqueline-eude-durler"
      }
    , { courriels = []
      , deputes = [ "helene-zannier" ]
      , enCirconscription = False
      , nom = "Fargeaud"
      , nomComplet = "Benjamin Fargeaud"
      , particule = Nothing
      , prenom = "Benjamin"
      , slug = "benjamin-fargeaud"
      }
    , { courriels = [ "alexandre.fernandez@sciencespo-lille.eu" ]
      , deputes = [ "laurianne-rossi" ]
      , enCirconscription = False
      , nom = "Fernandez"
      , nomComplet = "Alexandre Fernandez"
      , particule = Nothing
      , prenom = "Alexandre"
      , slug = "alexandre-fernandez"
      }
    , { courriels = [ "aurore.feuer@clb-an.fr" ]
      , deputes = [ "celia-de-lavergne" ]
      , enCirconscription = False
      , nom = "Feuer"
      , nomComplet = "Aurore Feuer"
      , particule = Nothing
      , prenom = "Aurore"
      , slug = "aurore-feuer"
      }
    , { courriels = [ "feugnet@outlook.fr" ]
      , deputes = [ "veronique-riotton" ]
      , enCirconscription = False
      , nom = "Feugnet"
      , nomComplet = "Francis Feugnet"
      , particule = Nothing
      , prenom = "Francis"
      , slug = "francis-feugnet"
      }
    , { courriels = [ "paul.fleurance@en-marche.fr" ]
      , deputes = [ "stanislas-guerini" ]
      , enCirconscription = False
      , nom = "Fleurance"
      , nomComplet = "Paul Fleurance"
      , particule = Nothing
      , prenom = "Paul"
      , slug = "paul-fleurance"
      }
    , { courriels = [ "candice.foehrenbach@gmail.com" ]
      , deputes = [ "cedric-villani" ]
      , enCirconscription = False
      , nom = "Foehrenbach"
      , nomComplet = "Candice Foehrenbach"
      , particule = Nothing
      , prenom = "Candice"
      , slug = "candice-foehrenbach"
      }
    , { courriels = [ "framont.v@hotmail.fr" ]
      , deputes = [ "benedicte-peyrol" ]
      , enCirconscription = False
      , nom = "Framont"
      , nomComplet = "Valentin Framont"
      , particule = Nothing
      , prenom = "Valentin"
      , slug = "valentin-framont"
      }
    , { courriels = [ "thomas.friang@gmail.com" ]
      , deputes = [ "cedric-villani" ]
      , enCirconscription = False
      , nom = "Friang"
      , nomComplet = "Thomas Friang"
      , particule = Nothing
      , prenom = "Thomas"
      , slug = "thomas-friang"
      }
    , { courriels = [ "jean-bernard.gaillot@clb-an.fr" ]
      , deputes = [ "romain-grau" ]
      , enCirconscription = False
      , nom = "Gaillot"
      , nomComplet = "Jean-Bernard Gaillot"
      , particule = Nothing
      , prenom = "Jean-Bernard"
      , slug = "jean-bernard-gaillot"
      }
    , { courriels = [ "gall.florence@orange.fr" ]
      , deputes = [ "guillaume-gouffier-cha" ]
      , enCirconscription = False
      , nom = "Gall"
      , nomComplet = "Florence Gall"
      , particule = Nothing
      , prenom = "Florence"
      , slug = "florence-gall"
      }
    , { courriels = [ "emilie_gapaillard@msn.com" ]
      , deputes = [ "alice-thourot" ]
      , enCirconscription = False
      , nom = "Gapaillard"
      , nomComplet = "Emilie Gapaillard"
      , particule = Nothing
      , prenom = "Emilie"
      , slug = "emilie-gapaillard"
      }
    , { courriels = [ "fabrice.garau@clb-an.fr" ]
      , deputes = [ "marie-pierre-rixain" ]
      , enCirconscription = False
      , nom = "Garau"
      , nomComplet = "Fabrice Garau"
      , particule = Nothing
      , prenom = "Fabrice"
      , slug = "fabrice-garau"
      }
    , { courriels = [ "sebastien.garnault@clb-an.fr" ]
      , deputes = [ "florian-bachelier" ]
      , enCirconscription = False
      , nom = "Garnault"
      , nomComplet = "Sébastien Garnault"
      , particule = Nothing
      , prenom = "Sébastien"
      , slug = "sebastien-garnault"
      }
    , { courriels = [ "gael.garreau@clb-an.fr" ]
      , deputes = [ "guillaume-kasbarian" ]
      , enCirconscription = False
      , nom = "Garreau"
      , nomComplet = "Gaël Garreau"
      , particule = Nothing
      , prenom = "Gaël"
      , slug = "gael-garreau"
      }
    , { courriels = [ "Loris.gaudin@gmail.com" ]
      , deputes = [ "valerie-gomez-bassac" ]
      , enCirconscription = False
      , nom = "Gaudin"
      , nomComplet = "Loris Gaudin"
      , particule = Nothing
      , prenom = "Loris"
      , slug = "loris-gaudin"
      }
    , { courriels = []
      , deputes = [ "remy-rebeyrotte" ]
      , enCirconscription = False
      , nom = "Gauthey"
      , nomComplet = "Benjamin Gauthey"
      , particule = Nothing
      , prenom = "Benjamin"
      , slug = "benjamin-gauthey"
      }
    , { courriels = [ "gencoalice@gmail.com" ]
      , deputes = [ "ludovic-mendes" ]
      , enCirconscription = False
      , nom = "Genco"
      , nomComplet = "Alice Genco"
      , particule = Nothing
      , prenom = "Alice"
      , slug = "alice-genco"
      }
    , { courriels = [ "geney.fre@gmail.com" ]
      , deputes = [ "laurent-saint-martin" ]
      , enCirconscription = False
      , nom = "Geney"
      , nomComplet = "Frédéric Geney"
      , particule = Nothing
      , prenom = "Frédéric"
      , slug = "frederic-geney"
      }
    , { courriels = [ "romain.gerardi.sgl@gmail.com" ]
      , deputes = [ "thomas-gassilloud" ]
      , enCirconscription = False
      , nom = "Gerardi"
      , nomComplet = "Romain Gerardi"
      , particule = Nothing
      , prenom = "Romain"
      , slug = "romain-gerardi"
      }
    , { courriels = [ "aliciagerbercirco1@gmail.com" ]
      , deputes = [ "fabrice-le-vigoureux" ]
      , enCirconscription = False
      , nom = "Gerber"
      , nomComplet = "Alicia Gerber"
      , particule = Nothing
      , prenom = "Alicia"
      , slug = "alicia-gerber"
      }
    , { courriels = [ "okan.germiyan@clb-an.fr" ]
      , deputes = [ "jacques-marilossian" ]
      , enCirconscription = False
      , nom = "Germiyan"
      , nomComplet = "Okan Germiyan"
      , particule = Nothing
      , prenom = "Okan"
      , slug = "okan-germiyan"
      }
    , { courriels = []
      , deputes = [ "jacqueline-dubois" ]
      , enCirconscription = False
      , nom = "Ghirardelli"
      , nomComplet = "Jacqueline Ghirardelli"
      , particule = Nothing
      , prenom = "Jacqueline"
      , slug = "jacqueline-ghirardelli"
      }
    , { courriels = [ "florent.degigord@clb-an.fr" ]
      , deputes = [ "laetitia-romeiro-dias" ]
      , enCirconscription = False
      , nom = "Gigord"
      , nomComplet = "Florent de Gigord"
      , particule = Just "de"
      , prenom = "Florent"
      , slug = "florent-de-gigord"
      }
    , { courriels = [ "assistantparlementaire33@christelledubos.fr" ]
      , deputes = [ "christelle-dubos" ]
      , enCirconscription = False
      , nom = "Giraud"
      , nomComplet = "Xavier Giraud"
      , particule = Nothing
      , prenom = "Xavier"
      , slug = "xavier-giraud"
      }
    , { courriels = [ "barbara.gombert@enmarche28.fr" ]
      , deputes = [ "guillaume-kasbarian" ]
      , enCirconscription = False
      , nom = "Gombert"
      , nomComplet = "Barbara Gombert"
      , particule = Nothing
      , prenom = "Barbara"
      , slug = "barbara-gombert"
      }
    , { courriels = [ "marine.gossa@gmail.com" ]
      , deputes = [ "laurianne-rossi" ]
      , enCirconscription = False
      , nom = "Gossa"
      , nomComplet = "Marine Gossa"
      , particule = Nothing
      , prenom = "Marine"
      , slug = "marine-gossa"
      }
    , { courriels = [ "a.gourbeyre@hotmail.fr" ]
      , deputes = [ "julien-borowczyk" ]
      , enCirconscription = False
      , nom = "Gourbeyre"
      , nomComplet = "Auriane Gourbeyre"
      , particule = Nothing
      , prenom = "Auriane"
      , slug = "auriane-gourbeyre"
      }
    , { courriels = [ "tony.granzotto@clb-an.fr" ]
      , deputes = [ "carole-bureau-bonnard" ]
      , enCirconscription = False
      , nom = "Granzotto"
      , nomComplet = "Tony Granzotto"
      , particule = Nothing
      , prenom = "Tony"
      , slug = "tony-granzotto"
      }
    , { courriels = [ "ines.guennouni@hotmail.fr" ]
      , deputes = [ "eric-bothorel" ]
      , enCirconscription = False
      , nom = "Guennouni"
      , nomComplet = "Ines Guennouni"
      , particule = Nothing
      , prenom = "Ines"
      , slug = "ines-guennouni"
      }
    , { courriels = [ "Benjamin.Guichard@clb-an.fr" ]
      , deputes = [ "danielle-brulebois" ]
      , enCirconscription = False
      , nom = "Guichard"
      , nomComplet = "Benjamin Guichard"
      , particule = Nothing
      , prenom = "Benjamin"
      , slug = "benjamin-guichard"
      }
    , { courriels = [ "julie.guyot7@gmail.com" ]
      , deputes = [ "yannick-kerlogot" ]
      , enCirconscription = False
      , nom = "Guyot"
      , nomComplet = "Julie Guyot"
      , particule = Nothing
      , prenom = "Julie"
      , slug = "julie-guyot"
      }
    , { courriels = [ "jhaddad@philippe-folliot.fr" ]
      , deputes = [ "philippe-folliot" ]
      , enCirconscription = False
      , nom = "Haddad"
      , nomComplet = "Jeremy Haddad"
      , particule = Nothing
      , prenom = "Jeremy"
      , slug = "jeremy-haddad"
      }
    , { courriels = [ "X" ]
      , deputes = [ "thomas-mesnier" ]
      , enCirconscription = False
      , nom = "Haffen"
      , nomComplet = "Elise Haffen"
      , particule = Nothing
      , prenom = "Elise"
      , slug = "elise-haffen"
      }
    , { courriels = [ "ninhalimi@gmail.com" ]
      , deputes = [ "veronique-hammerer" ]
      , enCirconscription = False
      , nom = "Halimi"
      , nomComplet = "Nina Halimi"
      , particule = Nothing
      , prenom = "Nina"
      , slug = "nina-halimi"
      }
    , { courriels = [ "jerome.hebert@clb-an.fr" ]
      , deputes = [ "didier-le-gac" ]
      , enCirconscription = False
      , nom = "Hebert"
      , nomComplet = "Jérôme Hebert"
      , particule = Nothing
      , prenom = "Jérôme"
      , slug = "jerome-hebert"
      }
    , { courriels = [ "flore.hennion@gmail.com" ]
      , deputes = [ "monica-michel" ]
      , enCirconscription = False
      , nom = "Hennion"
      , nomComplet = "Flore Hennion"
      , particule = Nothing
      , prenom = "Flore"
      , slug = "flore-hennion"
      }
    , { courriels = [ "jeanpierreheranval@gmail.com" ]
      , deputes = [ "damien-adam" ]
      , enCirconscription = False
      , nom = "Heranval"
      , nomComplet = "Jean-Pierre Heranval"
      , particule = Nothing
      , prenom = "Jean-Pierre"
      , slug = "jean-pierre-heranval"
      }
    , { courriels = [ "anne-marie.hodinh@clb-an.fr" ]
      , deputes = [ "sandrine-morch" ]
      , enCirconscription = False
      , nom = "Ho Dinh"
      , nomComplet = "Anne-Marie Ho Dinh"
      , particule = Nothing
      , prenom = "Anne-Marie"
      , slug = "anne-marie-ho-dinh"
      }
    , { courriels = [ "nam.hoang@hec.edu" ]
      , deputes = [ "pierre-person" ]
      , enCirconscription = False
      , nom = "Hoang"
      , nomComplet = "Nam Hoang"
      , particule = Nothing
      , prenom = "Nam"
      , slug = "nam-hoang"
      }
    , { courriels = [ "dtlhorn.pro@gmail.com" ]
      , deputes = [ "fiona-lazaar" ]
      , enCirconscription = False
      , nom = "Horn"
      , nomComplet = "Damien Horn"
      , particule = Nothing
      , prenom = "Damien"
      , slug = "damien-horn"
      }
    , { courriels = []
      , deputes = [ "loic-dombreval" ]
      , enCirconscription = False
      , nom = "Hubert"
      , nomComplet = "Aline Hubert"
      , particule = Nothing
      , prenom = "Aline"
      , slug = "aline-hubert"
      }
    , { courriels = [ "beatrice.piron@assemblee-nationale.fr" ]
      , deputes = [ "beatrice-piron" ]
      , enCirconscription = False
      , nom = "Hugo Provost"
      , nomComplet = "Isadora Hugo Provost"
      , particule = Nothing
      , prenom = "Isadora"
      , slug = "isadora-hugo-provost"
      }
    , { courriels = [ "marion.hugues@clb-an.fr" ]
      , deputes = [ "amal-amelia-lakrafi" ]
      , enCirconscription = False
      , nom = "Hugues"
      , nomComplet = "Marion Hugues"
      , particule = Nothing
      , prenom = "Marion"
      , slug = "marion-hugues"
      }
    , { courriels = [ "catherinehurstel@gmail.com" ]
      , deputes = [ "isabelle-rauch" ]
      , enCirconscription = False
      , nom = "Hurstel"
      , nomComplet = "Catherine Hurstel"
      , particule = Nothing
      , prenom = "Catherine"
      , slug = "catherine-hurstel"
      }
    , { courriels = [ "laylaimoula@hotmail.fr" ]
      , deputes = [ "christophe-di-pompeo" ]
      , enCirconscription = False
      , nom = "Imoula"
      , nomComplet = "Layla Imoula"
      , particule = Nothing
      , prenom = "Layla"
      , slug = "layla-imoula"
      }
    , { courriels = [ "tess.indycki@gmail.com" ]
      , deputes = [ "gabriel-attal" ]
      , enCirconscription = False
      , nom = "Indycki"
      , nomComplet = "Tess Indycki"
      , particule = Nothing
      , prenom = "Tess"
      , slug = "tess-indycki"
      }
    , { courriels = [ "Jalade.sarah@gmail.com" ]
      , deputes = [ "valeria-faure-muntian" ]
      , enCirconscription = False
      , nom = "Jalade"
      , nomComplet = "Sarah Jalade"
      , particule = Nothing
      , prenom = "Sarah"
      , slug = "sarah-jalade"
      }
    , { courriels = [ "victoria.jolly@clb-an.fr" ]
      , deputes = [ "philippe-latombe" ]
      , enCirconscription = False
      , nom = "Jolly"
      , nomComplet = "Victoria Jolly"
      , particule = Nothing
      , prenom = "Victoria"
      , slug = "victoria-jolly"
      }
    , { courriels = [ "aminekaabeche.01@gmail.com" ]
      , deputes = [ "stephane-trompille" ]
      , enCirconscription = False
      , nom = "Kaabeche"
      , nomComplet = "Amine Kaabeche"
      , particule = Nothing
      , prenom = "Amine"
      , slug = "amine-kaabeche"
      }
    , { courriels = [ "lorene.kloster@clb-an.fr" ]
      , deputes = [ "laurence-maillart-mehaignerie" ]
      , enCirconscription = True
      , nom = "Kloster"
      , nomComplet = "Lorène Kloster"
      , particule = Nothing
      , prenom = "Lorène"
      , slug = "lorene-kloster"
      }
    , { courriels = [ "arnaud.knobloch@clb-an.fr" ]
      , deputes = [ "nicolas-demoulin" ]
      , enCirconscription = False
      , nom = "Knobloch"
      , nomComplet = "Arnaud Knobloch"
      , particule = Nothing
      , prenom = "Arnaud"
      , slug = "arnaud-knobloch"
      }
    , { courriels = [ "fadila.khattabi@assemblee-nationale.fr" ]
      , deputes = [ "fadila-khattabi" ]
      , enCirconscription = False
      , nom = "Koshkhou"
      , nomComplet = "Andrea Koshkhou"
      , particule = Nothing
      , prenom = "Andrea"
      , slug = "andrea-koshkhou"
      }
    , { courriels = [ "pierre@delatouche.eu" ]
      , deputes = [ "emilie-guerel" ]
      , enCirconscription = False
      , nom = "La Touche"
      , nomComplet = "Pierre de La Touche"
      , particule = Just "de"
      , prenom = "Pierre"
      , slug = "pierre-de-la-touche"
      }
    , { courriels = [ "amel.labbas@hotmail.fr" ]
      , deputes = [ "amal-amelia-lakrafi" ]
      , enCirconscription = False
      , nom = "Labbas"
      , nomComplet = "Amel Labbas"
      , particule = Nothing
      , prenom = "Amel"
      , slug = "amel-labbas"
      }
    , { courriels = [ "victorien.Lachas@clb-an.fr" ]
      , deputes = [ "danielle-brulebois" ]
      , enCirconscription = False
      , nom = "Lachas"
      , nomComplet = "Victorien Lachas"
      , particule = Nothing
      , prenom = "Victorien"
      , slug = "victorien-lachas"
      }
    , { courriels = [ "brice.lacourieux@gmail.com" ]
      , deputes = [ "stephanie-rist" ]
      , enCirconscription = False
      , nom = "Lacourieux"
      , nomComplet = "Brice Lacourieux"
      , particule = Nothing
      , prenom = "Brice"
      , slug = "brice-lacourieux"
      }
    , { courriels = [ "victorlailler49@gmail.com" ]
      , deputes = [ "pierre-person" ]
      , enCirconscription = False
      , nom = "Lailler"
      , nomComplet = "Victor Lailler"
      , particule = Nothing
      , prenom = "Victor"
      , slug = "victor-lailler"
      }
    , { courriels = [ "rlapin01@hotmail.fr" ]
      , deputes = [ "olivier-serva" ]
      , enCirconscription = False
      , nom = "Lapin"
      , nomComplet = "Raphaël Lapin"
      , particule = Nothing
      , prenom = "Raphaël"
      , slug = "raphael-lapin"
      }
    , { courriels = [ "Mathildelaurentassnat@gmail.com" ]
      , deputes = [ "adrien-taquet" ]
      , enCirconscription = False
      , nom = "Laurent"
      , nomComplet = "Mathilde Laurent"
      , particule = Nothing
      , prenom = "Mathilde"
      , slug = "mathilde-laurent"
      }
    , { courriels = [ "Mat.laurin@gmail.com" ]
      , deputes = [ "aurelien-tache" ]
      , enCirconscription = False
      , nom = "Laurin"
      , nomComplet = "Mathurin Laurin"
      , particule = Nothing
      , prenom = "Mathurin"
      , slug = "mathurin-laurin"
      }
    , { courriels = [ "sebastien.lavandon@gmail.com" ]
      , deputes = [ "albane-gaillot" ]
      , enCirconscription = False
      , nom = "Lavandon"
      , nomComplet = "Sebastien Lavandon"
      , particule = Nothing
      , prenom = "Sebastien"
      , slug = "sebastien-lavandon"
      }
    , { courriels = [ "elenorcy.assemblee@gmail.com" ]
      , deputes = [ "valerie-thomas" ]
      , enCirconscription = False
      , nom = "Le Norcy"
      , nomComplet = "Etienne Le Norcy"
      , particule = Nothing
      , prenom = "Etienne"
      , slug = "etienne-le-norcy"
      }
    , { courriels = [ "gregoire.letaillandier@gmail.com" ]
      , deputes = [ "buon-tan" ]
      , enCirconscription = False
      , nom = "Le Taillandier"
      , nomComplet = "Grégoire Le Taillandier"
      , particule = Nothing
      , prenom = "Grégoire"
      , slug = "gregoire-le-taillandier"
      }
    , { courriels = [ "le-vay.celine@orange.fr" ]
      , deputes = [ "eric-bothorel" ]
      , enCirconscription = False
      , nom = "Le Vay"
      , nomComplet = "Céline Le Vay"
      , particule = Nothing
      , prenom = "Céline"
      , slug = "celine-le-vay"
      }
    , { courriels = [ "legros.bertrand@gmail.com" ]
      , deputes = [ "emilie-cariou" ]
      , enCirconscription = False
      , nom = "Legros"
      , nomComplet = "Bertrand Legros"
      , particule = Nothing
      , prenom = "Bertrand"
      , slug = "bertrand-legros"
      }
    , { courriels = [ "maxime.lelievre@hotmail.fr" ]
      , deputes = [ "benoit-potterie" ]
      , enCirconscription = False
      , nom = "Lelievre"
      , nomComplet = "Maxime Lelievre"
      , particule = Nothing
      , prenom = "Maxime"
      , slug = "maxime-lelievre"
      }
    , { courriels = [ "samy.lemedy@gmail.com" ]
      , deputes = [ "delphine-o" ]
      , enCirconscription = False
      , nom = "Lemedy"
      , nomComplet = "Samy Lemedy"
      , particule = Nothing
      , prenom = "Samy"
      , slug = "samy-lemedy"
      }
    , { courriels = [ "nicolas.leron@clb-an.fr" ]
      , deputes = [ "didier-baichere" ]
      , enCirconscription = False
      , nom = "Leron"
      , nomComplet = "Nicolas Leron"
      , particule = Nothing
      , prenom = "Nicolas"
      , slug = "nicolas-leron"
      }
    , { courriels = [ "marc.lerouge@yahoo.fr" ]
      , deputes = [ "denis-masseglia" ]
      , enCirconscription = False
      , nom = "Lerouge"
      , nomComplet = "Marc Lerouge"
      , particule = Nothing
      , prenom = "Marc"
      , slug = "marc-lerouge"
      }
    , { courriels = [ "etienne.lesoeur@gmail.com" ]
      , deputes = [ "frederique-dumas" ]
      , enCirconscription = False
      , nom = "Lesoeur"
      , nomComplet = "Etienne Lesoeur"
      , particule = Nothing
      , prenom = "Etienne"
      , slug = "etienne-lesoeur"
      }
    , { courriels = [ "remi.loison@clb-an.fr" ]
      , deputes = [ "jennifer-de-temmerman" ]
      , enCirconscription = False
      , nom = "Loison"
      , nomComplet = "Rémi Loison"
      , particule = Nothing
      , prenom = "Rémi"
      , slug = "remi-loison"
      }
    , { courriels = [ "lucie.loncle-duda@clb-an.fr" ]
      , deputes = [ "annie-vidal" ]
      , enCirconscription = False
      , nom = "Loncle Duda"
      , nomComplet = "Lucie Loncle Duda"
      , particule = Nothing
      , prenom = "Lucie"
      , slug = "lucie-loncle-duda"
      }
    , { courriels = [ "yannick.loustau@cendramotin.fr" ]
      , deputes = [ "cendra-motin" ]
      , enCirconscription = False
      , nom = "Loustau"
      , nomComplet = "Yannick Loustau"
      , particule = Nothing
      , prenom = "Yannick"
      , slug = "yannick-loustau"
      }
    , { courriels = [ "margauxlyprendi@hotmail.com" ]
      , deputes = [ "catherine-fabre" ]
      , enCirconscription = False
      , nom = "Lyprendi"
      , nomComplet = "Margaux Lyprendi"
      , particule = Nothing
      , prenom = "Margaux"
      , slug = "margaux-lyprendi"
      }
    , { courriels = [ "amandinemck@gmail.com" ]
      , deputes = [ "laurent-saint-martin" ]
      , enCirconscription = False
      , nom = "Mackako"
      , nomComplet = "Amandine Mackako"
      , particule = Nothing
      , prenom = "Amandine"
      , slug = "amandine-mackako"
      }
    , { courriels = [ "sonia.demaigret@clb-an.fr" ]
      , deputes = [ "gilles-le-gendre" ]
      , enCirconscription = False
      , nom = "Maigret"
      , nomComplet = "Sonia de Maigret"
      , particule = Just "de"
      , prenom = "Sonia"
      , slug = "sonia-de-maigret"
      }
    , { courriels = [ "catherine.maligne@clb-an.fr" ]
      , deputes = [ "patrice-perrot" ]
      , enCirconscription = False
      , nom = "Maligne"
      , nomComplet = "Catherine Maligne"
      , particule = Nothing
      , prenom = "Catherine"
      , slug = "catherine-maligne"
      }
    , { courriels = [ "pierre.manenti2@gmail.com" ]
      , deputes = [ "aurore-berge" ]
      , enCirconscription = False
      , nom = "Manenti"
      , nomComplet = "Pierre Manenti"
      , particule = Nothing
      , prenom = "Pierre"
      , slug = "pierre-manenti"
      }
    , { courriels = [ "juliette.marceaux@gmail.com" ]
      , deputes = [ "cedric-villani" ]
      , enCirconscription = False
      , nom = "Marceaux"
      , nomComplet = "Juliette Marceaux"
      , particule = Nothing
      , prenom = "Juliette"
      , slug = "juliette-marceaux"
      }
    , { courriels = [ "jpmariot.an70@gmail.com" ]
      , deputes = [ "barbara-bessot-ballot" ]
      , enCirconscription = False
      , nom = "Mariot"
      , nomComplet = "Jean-Pascal Mariot"
      , particule = Nothing
      , prenom = "Jean-Pascal"
      , slug = "jean-pascal-mariot"
      }
    , { courriels = [ "amelie.marques@sciencespo.fr" ]
      , deputes = [ "marie-pierre-rixain" ]
      , enCirconscription = False
      , nom = "Marques"
      , nomComplet = "Amélie Marques"
      , particule = Nothing
      , prenom = "Amélie"
      , slug = "amelie-marques"
      }
    , { courriels = [ "paul.marsauche@clb-an.fr" ]
      , deputes = [ "florian-bachelier" ]
      , enCirconscription = False
      , nom = "Marsauche"
      , nomComplet = "Paul Marsauche"
      , particule = Nothing
      , prenom = "Paul"
      , slug = "paul-marsauche"
      }
    , { courriels = []
      , deputes = [ "valerie-petit" ]
      , enCirconscription = False
      , nom = "Martinez"
      , nomComplet = "Raphael Martinez"
      , particule = Nothing
      , prenom = "Raphael"
      , slug = "raphael-martinez"
      }
    , { courriels = [ "mail d'Amélie de Montchalin" ]
      , deputes = [ "amelie-de-montchalin" ]
      , enCirconscription = False
      , nom = "Marty"
      , nomComplet = "Laurence Marty"
      , particule = Nothing
      , prenom = "Laurence"
      , slug = "laurence-marty"
      }
    , { courriels = [ "sebastien.masteau@gmail.com" ]
      , deputes = [ "sereine-mauborgne" ]
      , enCirconscription = False
      , nom = "Masteau"
      , nomComplet = "Sébstien Masteau"
      , particule = Nothing
      , prenom = "Sébstien"
      , slug = "sebstien-masteau"
      }
    , { courriels = [ "michel.mazars@clb-an.fr" ]
      , deputes = [ "yael-braun-pivet" ]
      , enCirconscription = False
      , nom = "Mazars"
      , nomComplet = "Michel Mazars"
      , particule = Nothing
      , prenom = "Michel"
      , slug = "michel-mazars"
      }
    , { courriels = [ "stephane-mb@orange.fr" ]
      , deputes = [ "olivier-damaisin" ]
      , enCirconscription = False
      , nom = "Mazeaufroid-Boulestin"
      , nomComplet = "Stéphane Mazeaufroid-Boulestin"
      , particule = Nothing
      , prenom = "Stéphane"
      , slug = "stephane-mazeaufroid-boulestin"
      }
    , { courriels = [ "yass.medjani@gmail.com" ]
      , deputes = [ "alexandra-louis" ]
      , enCirconscription = False
      , nom = "Medjani"
      , nomComplet = "Yassine Medjani"
      , particule = Nothing
      , prenom = "Yassine"
      , slug = "yassine-medjani"
      }
    , { courriels = []
      , deputes = [ "paula-forteza" ]
      , enCirconscription = False
      , nom = "Meija"
      , nomComplet = "Mauricio Meija"
      , particule = Nothing
      , prenom = "Mauricio"
      , slug = "mauricio-meija"
      }
    , { courriels = [ "meiller.charles@outlook.com" ]
      , deputes = [ "agnes-thill" ]
      , enCirconscription = False
      , nom = "Meiller"
      , nomComplet = "Charles Meiller"
      , particule = Nothing
      , prenom = "Charles"
      , slug = "charles-meiller"
      }
    , { courriels = [ "baltis.mejanes@gmail.com" ]
      , deputes = [ "olivia-gregoire" ]
      , enCirconscription = False
      , nom = "Mejanes"
      , nomComplet = "Baltis Mejanes"
      , particule = Nothing
      , prenom = "Baltis"
      , slug = "baltis-mejanes"
      }
    , { courriels = [ "samuel.menager@clb-an.fr" ]
      , deputes = [ "stephanie-kerbarh" ]
      , enCirconscription = False
      , nom = "Menager"
      , nomComplet = "Samuel Menager"
      , particule = Nothing
      , prenom = "Samuel"
      , slug = "samuel-menager"
      }
    , { courriels = [ "s.merceron@wanadoo.fr" ]
      , deputes = [ "daniel-labaronne" ]
      , enCirconscription = False
      , nom = "Merceron"
      , nomComplet = "Stéphane Merceron"
      , particule = Nothing
      , prenom = "Stéphane"
      , slug = "stephane-merceron"
      }
    , { courriels = [ "imercier@assemblee-nationale.fr" ]
      , deputes = [ "guillaume-vuilletet" ]
      , enCirconscription = False
      , nom = "Mercier"
      , nomComplet = "Isabelle Mercier"
      , particule = Nothing
      , prenom = "Isabelle"
      , slug = "isabelle-mercier"
      }
    , { courriels = [ "cedric.merlaud@clb-an.fr" ]
      , deputes = [ "laetitia-avia" ]
      , enCirconscription = False
      , nom = "Merlaud"
      , nomComplet = "Cédric Merlaud"
      , particule = Nothing
      , prenom = "Cédric"
      , slug = "cedric-merlaud"
      }
    , { courriels = [ "jer.meunier@gmail.com" ]
      , deputes = [ "stephane-teste" ]
      , enCirconscription = False
      , nom = "Meunier"
      , nomComplet = "Jérôme Meunier"
      , particule = Nothing
      , prenom = "Jérôme"
      , slug = "jerome-meunier"
      }
    , { courriels = [ "fbarbier@assemblee-nationale.fr" ]
      , deputes = [ "frederic-barbier" ]
      , enCirconscription = False
      , nom = "Meyssonnier"
      , nomComplet = "Trixie Meyssonnier"
      , particule = Nothing
      , prenom = "Trixie"
      , slug = "trixie-meyssonnier"
      }
    ]


collaborateurs2 : Collaborateurs
collaborateurs2 =
    [ { courriels = [ "quentin.michael@ntymail.com" ]
      , deputes = [ "jacques-maire" ]
      , enCirconscription = False
      , nom = "Michael"
      , nomComplet = "Quentin Michael"
      , particule = Nothing
      , prenom = "Quentin"
      , slug = "quentin-michael"
      }
    , { courriels = [ "permanence@brunobonnell-villeurbanne.fr" ]
      , deputes = [ "bruno-bonnell" ]
      , enCirconscription = True
      , nom = "Monne"
      , nomComplet = "Anne-Sophie Monne"
      , particule = Nothing
      , prenom = "Anne-Sophie"
      , slug = "anne-sophie-monne"
      }
    , { courriels = [ "celinemontaner@yahoo.fr" ]
      , deputes = [ "aurore-berge" ]
      , enCirconscription = False
      , nom = "Montaner"
      , nomComplet = "Céline Montaner"
      , particule = Nothing
      , prenom = "Céline"
      , slug = "celine-montaner"
      }
    , { courriels = [ "t.montmessin@gmail.com" ]
      , deputes = [ "thomas-rudigoz" ]
      , enCirconscription = False
      , nom = "Montmessin"
      , nomComplet = "Thomas Montmessin"
      , particule = Nothing
      , prenom = "Thomas"
      , slug = "thomas-montmessin"
      }
    , { courriels = [ "leila.morch@gmail.com" ]
      , deputes = [ "jean-bernard-sempastous" ]
      , enCirconscription = False
      , nom = "Morch"
      , nomComplet = "Leila Morch"
      , particule = Nothing
      , prenom = "Leila"
      , slug = "leila-morch"
      }
    , { courriels = [ "berenice.mottelay@clb-an.fr" ]
      , deputes = [ "raphael-gauvain" ]
      , enCirconscription = False
      , nom = "Mottelay"
      , nomComplet = "Bérénice Mottelay"
      , particule = Nothing
      , prenom = "Bérénice"
      , slug = "berenice-mottelay"
      }
    , { courriels = [ "nadege.mourygadiot@clb-an.fr" ]
      , deputes = [ "sandrine-josso" ]
      , enCirconscription = False
      , nom = "Moury Gadiot"
      , nomComplet = "Nadège Moury Gadiot"
      , particule = Nothing
      , prenom = "Nadège"
      , slug = "nadege-moury-gadiot"
      }
    , { courriels = []
      , deputes = [ "adrien-morenas" ]
      , enCirconscription = False
      , nom = "Moïse Houzard"
      , nomComplet = "Maurice Moïse Houzard"
      , particule = Nothing
      , prenom = "Maurice"
      , slug = "maurice-moise-houzard"
      }
    , { courriels = [ "laurence.muguet@clb-an.fr" ]
      , deputes = [ "sebastien-cazenove" ]
      , enCirconscription = False
      , nom = "Muguet"
      , nomComplet = "Laurence Muguet"
      , particule = Nothing
      , prenom = "Laurence"
      , slug = "laurence-muguet"
      }
    , { courriels = []
      , deputes = [ "anne-france-brunet" ]
      , enCirconscription = False
      , nom = "Nicoud"
      , nomComplet = "Anthony Nicoud"
      , particule = Nothing
      , prenom = "Anthony"
      , slug = "anthony-nicoud"
      }
    , { courriels = [ "arthurc.nowicki@gmail.com" ]
      , deputes = [ "guillaume-vuilletet" ]
      , enCirconscription = False
      , nom = "Nowicki"
      , nomComplet = "Arthur Nowicki"
      , particule = Nothing
      , prenom = "Arthur"
      , slug = "arthur-nowicki"
      }
    , { courriels = [ "jnuti.mvalls@gmail.com" ]
      , deputes = [ "manuel-valls" ]
      , enCirconscription = False
      , nom = "Nuti"
      , nomComplet = "Juliette Nuti"
      , particule = Nothing
      , prenom = "Juliette"
      , slug = "juliette-nuti"
      }
    , { courriels = [ "p.obrecht@yvesblein.fr" ]
      , deputes = [ "yves-blein" ]
      , enCirconscription = False
      , nom = "Obrecht"
      , nomComplet = "Pierre Obrecht"
      , particule = Nothing
      , prenom = "Pierre"
      , slug = "pierre-obrecht"
      }
    , { courriels = [ "jb.ogoundele@gmail.com" ]
      , deputes = [ "marie-lebec" ]
      , enCirconscription = False
      , nom = "Ogoundele"
      , nomComplet = "Jean-Baptiste Ogoundele"
      , particule = Nothing
      , prenom = "Jean-Baptiste"
      , slug = "jean-baptiste-ogoundele"
      }
    , { courriels = [ "ouarouss@rifco.fr" ]
      , deputes = [ "nadia-hai" ]
      , enCirconscription = False
      , nom = "Ouarouss"
      , nomComplet = "Moussa Ouarouss"
      , particule = Nothing
      , prenom = "Moussa"
      , slug = "moussa-ouarouss"
      }
    , { courriels = [ "paulineparisot.an@gmail.com" ]
      , deputes = [ "dominique-da-silva" ]
      , enCirconscription = False
      , nom = "Parisot"
      , nomComplet = "Pauline Parisot"
      , particule = Nothing
      , prenom = "Pauline"
      , slug = "pauline-parisot"
      }
    , { courriels = [ "amandine.pasquier@clb-an.fr" ]
      , deputes = [ "mickael-nogal" ]
      , enCirconscription = False
      , nom = "Pasquier"
      , nomComplet = "Amandine Pasquier"
      , particule = Nothing
      , prenom = "Amandine"
      , slug = "amandine-pasquier"
      }
    , { courriels = [ "c.pautet4702lrem@gmail.com" ]
      , deputes = [ "alexandre-freschi" ]
      , enCirconscription = False
      , nom = "Pautet"
      , nomComplet = "Charlotte Pautet"
      , particule = Nothing
      , prenom = "Charlotte"
      , slug = "charlotte-pautet"
      }
    , { courriels = [ "pier_slyboy@hotmail.fr" ]
      , deputes = [ "dimitri-houbron" ]
      , enCirconscription = False
      , nom = "Pavy"
      , nomComplet = "Pierre Pavy"
      , particule = Nothing
      , prenom = "Pierre"
      , slug = "pierre-pavy"
      }
    , { courriels = []
      , deputes = [ "lionel-causse" ]
      , enCirconscription = False
      , nom = "Pecastaings"
      , nomComplet = "Pierre Pecastaings"
      , particule = Nothing
      , prenom = "Pierre"
      , slug = "pierre-pecastaings"
      }
    , { courriels = [ "ph.pierson@free.fr" ]
      , deputes = [ "valerie-petit" ]
      , enCirconscription = False
      , nom = "Person Fauqueu"
      , nomComplet = "Philippe-Henry Person Fauqueu"
      , particule = Nothing
      , prenom = "Philippe-Henry"
      , slug = "philippe-henry-person-fauqueu"
      }
    , { courriels = [ "david.petit@clb-an.fr" ]
      , deputes = [ "typhanie-degois" ]
      , enCirconscription = False
      , nom = "Petit"
      , nomComplet = "David Petit"
      , particule = Nothing
      , prenom = "David"
      , slug = "david-petit"
      }
    , { courriels = [ "flavie.philipon@gmail.com" ]
      , deputes = [ "sacha-houlie" ]
      , enCirconscription = False
      , nom = "Philipon"
      , nomComplet = "Flavie Philipon"
      , particule = Nothing
      , prenom = "Flavie"
      , slug = "flavie-philipon"
      }
    , { courriels = [ "tp.thomas.philippe@gmail.com" ]
      , deputes = [ "marie-lebec" ]
      , enCirconscription = False
      , nom = "Philippe"
      , nomComplet = "Thomas Philippe"
      , particule = Nothing
      , prenom = "Thomas"
      , slug = "thomas-philippe"
      }
    , { courriels = []
      , deputes = [ "adrien-taquet" ]
      , enCirconscription = False
      , nom = "Pierre"
      , nomComplet = "Gwennaëlle Pierre"
      , particule = Nothing
      , prenom = "Gwennaëlle"
      , slug = "gwennaelle-pierre"
      }
    , { courriels = []
      , deputes = [ "berangere-couillard" ]
      , enCirconscription = False
      , nom = "Pinte"
      , nomComplet = "Clément Pinte"
      , particule = Nothing
      , prenom = "Clément"
      , slug = "clement-pinte"
      }
    , { courriels = [ "francois.Plaine@clb-an.fr" ]
      , deputes = [ "xavier-paluszkiewicz" ]
      , enCirconscription = False
      , nom = "Plaine"
      , nomComplet = "François Plaine"
      , particule = Nothing
      , prenom = "François"
      , slug = "francois-plaine"
      }
    , { courriels = [ "maxim.plat@clb-an.fr" ]
      , deputes = [ "benjamin-dirx" ]
      , enCirconscription = False
      , nom = "Plat"
      , nomComplet = "Maxim Plat"
      , particule = Nothing
      , prenom = "Maxim"
      , slug = "maxim-plat"
      }
    , { courriels = [ "florent.poindron@clb-an.fr" ]
      , deputes = [ "xavier-paluszkiewicz" ]
      , enCirconscription = False
      , nom = "Poindron"
      , nomComplet = "Florent Poindron"
      , particule = Nothing
      , prenom = "Florent"
      , slug = "florent-poindron"
      }
    , { courriels = [ "gabrielle.pollet@clb-an.fr" ]
      , deputes = [ "alain-perea" ]
      , enCirconscription = False
      , nom = "Pollet"
      , nomComplet = "Gabrielle Pollet"
      , particule = Nothing
      , prenom = "Gabrielle"
      , slug = "gabrielle-pollet"
      }
    , { courriels = [ "bprevost.clb@gmail.com" ]
      , deputes = [ "bruno-bonnell" ]
      , enCirconscription = False
      , nom = "Prevost"
      , nomComplet = "Bertrand Prevost"
      , particule = Nothing
      , prenom = "Bertrand"
      , slug = "bertrand-prevost"
      }
    , { courriels = []
      , deputes = [ "matthieu-orphelin" ]
      , enCirconscription = False
      , nom = "Prime"
      , nomComplet = "Isabelle Prime"
      , particule = Nothing
      , prenom = "Isabelle"
      , slug = "isabelle-prime"
      }
    , { courriels = [ "prudhomme.chloe@gmail.com" ]
      , deputes = [ "hubert-julien-laferriere" ]
      , enCirconscription = False
      , nom = "Prudhomme"
      , nomComplet = "Chloé Prudhomme"
      , particule = Nothing
      , prenom = "Chloé"
      , slug = "chloe-prudhomme"
      }
    , { courriels = [ "virginie.prufer@clb-an.fr" ]
      , deputes = [ "lenaick-adam" ]
      , enCirconscription = False
      , nom = "Prufer"
      , nomComplet = "Virginie Prufer"
      , particule = Nothing
      , prenom = "Virginie"
      , slug = "virginie-prufer"
      }
    , { courriels = []
      , deputes = [ "valerie-petit" ]
      , enCirconscription = False
      , nom = "Pruvost"
      , nomComplet = "Cédric Pruvost"
      , particule = Nothing
      , prenom = "Cédric"
      , slug = "cedric-pruvost"
      }
    , { courriels = [ "julien.ranc@laposte.net" ]
      , deputes = [ "valeria-faure-muntian" ]
      , enCirconscription = False
      , nom = "Ranc"
      , nomComplet = "Julien Ranc"
      , particule = Nothing
      , prenom = "Julien"
      , slug = "julien-ranc"
      }
    , { courriels = [ "eraviart@forteza.fr" ]
      , deputes = [ "paula-forteza" ]
      , enCirconscription = False
      , nom = "Raviart"
      , nomComplet = "Emmanuel Raviart"
      , particule = Nothing
      , prenom = "Emmanuel"
      , slug = "emmanuel-raviart"
      }
    , { courriels = [ "camille.reboul@clb-an.fr" ]
      , deputes = [ "benoit-simian" ]
      , enCirconscription = False
      , nom = "Reboul"
      , nomComplet = "Camille Reboul"
      , particule = Nothing
      , prenom = "Camille"
      , slug = "camille-reboul"
      }
    , { courriels = [ "boris.restier@gmail.com" ]
      , deputes = [ "sonia-krimi" ]
      , enCirconscription = False
      , nom = "Restier"
      , nomComplet = "Boris Restier"
      , particule = Nothing
      , prenom = "Boris"
      , slug = "boris-restier"
      }
    , { courriels = [ "rey.basile@gmail.com" ]
      , deputes = [ "damien-adam" ]
      , enCirconscription = False
      , nom = "Rey"
      , nomComplet = "Basile Rey"
      , particule = Nothing
      , prenom = "Basile"
      , slug = "basile-rey"
      }
    , { courriels = [ "jennifer.richard@clb-an.fr" ]
      , deputes = [ "patrice-anato" ]
      , enCirconscription = False
      , nom = "Richard"
      , nomComplet = "Jennifer Richard"
      , particule = Nothing
      , prenom = "Jennifer"
      , slug = "jennifer-richard"
      }
    , { courriels = [ "mickael.rigault86@gmail.com" ]
      , deputes = [ "patrice-anato" ]
      , enCirconscription = False
      , nom = "Rigault"
      , nomComplet = "Mickael Rigault"
      , particule = Nothing
      , prenom = "Mickael"
      , slug = "mickael-rigault"
      }
    , { courriels = [ "friondel.an70@gmail.com" ]
      , deputes = [ "barbara-bessot-ballot" ]
      , enCirconscription = False
      , nom = "Riondel"
      , nomComplet = "Francine Riondel"
      , particule = Nothing
      , prenom = "Francine"
      , slug = "francine-riondel"
      }
    , { courriels = [ "karin_riviere@hotmail.com" ]
      , deputes = [ "fiona-lazaar" ]
      , enCirconscription = False
      , nom = "Riviere"
      , nomComplet = "Karin Riviere"
      , particule = Nothing
      , prenom = "Karin"
      , slug = "karin-riviere"
      }
    , { courriels = [ "emeline.robert@clb-an.fr" ]
      , deputes = [ "matthieu-orphelin" ]
      , enCirconscription = False
      , nom = "Robert"
      , nomComplet = "Emeline Robert"
      , particule = Nothing
      , prenom = "Emeline"
      , slug = "emeline-robert"
      }
    , { courriels = [ "thibaud_roblin@live.fr" ]
      , deputes = [ "benedicte-peyrol" ]
      , enCirconscription = False
      , nom = "Roblin"
      , nomComplet = "Thibaud Roblin"
      , particule = Nothing
      , prenom = "Thibaud"
      , slug = "thibaud-roblin"
      }
    , { courriels = [ "jeremy.rojon@clb-an.fr" ]
      , deputes = [ "jacques-marilossian" ]
      , enCirconscription = False
      , nom = "Rojon"
      , nomComplet = "Jérémy Rojon"
      , particule = Nothing
      , prenom = "Jérémy"
      , slug = "jeremy-rojon"
      }
    , { courriels = [ "marie-ange.rousselot@clb-an.fr" ]
      , deputes = [ "joachim-son-forget" ]
      , enCirconscription = False
      , nom = "Rousselot"
      , nomComplet = "Marie-Ange Rousselot"
      , particule = Nothing
      , prenom = "Marie-Ange"
      , slug = "marie-ange-rousselot"
      }
    , { courriels = [ "coline.roux@gmail.com" ]
      , deputes = [ "daniel-labaronne" ]
      , enCirconscription = False
      , nom = "Roux"
      , nomComplet = "Coline Roux"
      , particule = Nothing
      , prenom = "Coline"
      , slug = "coline-roux"
      }
    , { courriels = [ "assnat.1circo37@gmail.com" ]
      , deputes = [ "philippe-chalumeau" ]
      , enCirconscription = False
      , nom = "Roy"
      , nomComplet = "Arnaud Roy"
      , particule = Nothing
      , prenom = "Arnaud"
      , slug = "arnaud-roy"
      }
    , { courriels = [ "eric.rucklin@clb-an.fr" ]
      , deputes = [ "xavier-batut" ]
      , enCirconscription = False
      , nom = "Rucklin"
      , nomComplet = "Eric Rucklin"
      , particule = Nothing
      , prenom = "Eric"
      , slug = "eric-rucklin"
      }
    , { courriels = [ "benoit.rudinger@gmail.com" ]
      , deputes = [ "dominique-david" ]
      , enCirconscription = False
      , nom = "Rudinger"
      , nomComplet = "Benoit Rudinger"
      , particule = Nothing
      , prenom = "Benoit"
      , slug = "benoit-rudinger"
      }
    , { courriels = [ "raphaelsaidi@gmail.com" ]
      , deputes = [ "perrine-goulet" ]
      , enCirconscription = False
      , nom = "Saidi"
      , nomComplet = "Raphaël Saidi"
      , particule = Nothing
      , prenom = "Raphaël"
      , slug = "raphael-saidi"
      }
    , { courriels = [ "c.salas@yvesblein.fr" ]
      , deputes = [ "yves-blein" ]
      , enCirconscription = False
      , nom = "Salas"
      , nomComplet = "Christelle Salas"
      , particule = Nothing
      , prenom = "Christelle"
      , slug = "christelle-salas"
      }
    , { courriels = [ "constantin.de.salvatore@gmail.com" ]
      , deputes = [ "elise-fajgeles" ]
      , enCirconscription = False
      , nom = "Salvatore"
      , nomComplet = "Constantin de Salvatore"
      , particule = Just "de"
      , prenom = "Constantin"
      , slug = "constantin-de-salvatore"
      }
    , { courriels = [ "samingo.john@gmail.com" ]
      , deputes = [ "anne-christine-lang" ]
      , enCirconscription = False
      , nom = "Samingo"
      , nomComplet = "John Samingo"
      , particule = Nothing
      , prenom = "John"
      , slug = "john-samingo"
      }
    , { courriels = [ "sardinfred@yahoo.fr" ]
      , deputes = [ "francois-cormier-bouligeon" ]
      , enCirconscription = False
      , nom = "Sardin"
      , nomComplet = "Frédéric Sardin"
      , particule = Nothing
      , prenom = "Frédéric"
      , slug = "frederic-sardin"
      }
    , { courriels = [ "sabrine.sassi@hotmail.fr" ]
      , deputes = [ "marie-christine-verdier-jouclas" ]
      , enCirconscription = False
      , nom = "Sassi"
      , nomComplet = "Sabrine Sassi"
      , particule = Nothing
      , prenom = "Sabrine"
      , slug = "sabrine-sassi"
      }
    , { courriels = [ "dsaussol.apcv@gmail.com" ]
      , deputes = [ "cedric-villani" ]
      , enCirconscription = False
      , nom = "Saussol"
      , nomComplet = "David Saussol"
      , particule = Nothing
      , prenom = "David"
      , slug = "david-saussol"
      }
    , { courriels = [ "anthony.cellier@assemblee-nationale.fr" ]
      , deputes = [ "anthony-cellier" ]
      , enCirconscription = False
      , nom = "Savary"
      , nomComplet = "Laurent Savary"
      , particule = Nothing
      , prenom = "Laurent"
      , slug = "laurent-savary"
      }
    , { courriels = [ "Benjamin.sayagsg@gmail.com" ]
      , deputes = [ "jean-marie-fievet" ]
      , enCirconscription = False
      , nom = "Sayag"
      , nomComplet = "Benjamin Sayag"
      , particule = Nothing
      , prenom = "Benjamin"
      , slug = "benjamin-sayag"
      }
    , { courriels = [ "delphine.scaini@clb-an.fr" ]
      , deputes = [ "jean-noel-barrot" ]
      , enCirconscription = False
      , nom = "Scaini"
      , nomComplet = "Delphine Scaini"
      , particule = Nothing
      , prenom = "Delphine"
      , slug = "delphine-scaini"
      }
    , { courriels = []
      , deputes = [ "celine-calvez" ]
      , enCirconscription = False
      , nom = "Sebaux"
      , nomComplet = "Catherine Sebaux"
      , particule = Nothing
      , prenom = "Catherine"
      , slug = "catherine-sebaux"
      }
    , { courriels = [ "s.seridji75@gmail.com" ]
      , deputes = [ "marie-guevenoux" ]
      , enCirconscription = False
      , nom = "Seridji"
      , nomComplet = "Sofiane Seridji"
      , particule = Nothing
      , prenom = "Sofiane"
      , slug = "sofiane-seridji"
      }
    , { courriels = [ "simonmorgann@yahoo.fr" ]
      , deputes = [ "valerie-oppelt" ]
      , enCirconscription = False
      , nom = "Simon"
      , nomComplet = "Morgan Simon"
      , particule = Nothing
      , prenom = "Morgan"
      , slug = "morgan-simon"
      }
    , { courriels = [ "valerie.singer@yaelbraunpivet.fr" ]
      , deputes = [ "yael-braun-pivet" ]
      , enCirconscription = False
      , nom = "Singer"
      , nomComplet = "Valérie Singer"
      , particule = Nothing
      , prenom = "Valérie"
      , slug = "valerie-singer"
      }
    , { courriels = [ "aminata.sissoko@clb-an.fr" ]
      , deputes = [ "patrice-anato" ]
      , enCirconscription = False
      , nom = "Sissoko"
      , nomComplet = "Aminata Sissoko"
      , particule = Nothing
      , prenom = "Aminata"
      , slug = "aminata-sissoko"
      }
    , { courriels = [ "Louise.sita.clb.an@gmail.com" ]
      , deputes = [ "aina-kuric" ]
      , enCirconscription = False
      , nom = "Sita"
      , nomComplet = "Louise Sita"
      , particule = Nothing
      , prenom = "Louise"
      , slug = "louise-sita"
      }
    , { courriels = [ "lea.soldermann@clb-an.fr" ]
      , deputes = [ "stanislas-guerini" ]
      , enCirconscription = False
      , nom = "Soldermann"
      , nomComplet = "Léa Soldermann"
      , particule = Nothing
      , prenom = "Léa"
      , slug = "lea-soldermann"
      }
    , { courriels = [ "sottoisabelle5@gmail.com" ]
      , deputes = [ "jacques-maire" ]
      , enCirconscription = False
      , nom = "Sotto"
      , nomComplet = "Isabelle Sotto"
      , particule = Nothing
      , prenom = "Isabelle"
      , slug = "isabelle-sotto"
      }
    , { courriels = [ "myriamsouami.an@gmail.com" ]
      , deputes = [ "damien-pichereau" ]
      , enCirconscription = False
      , nom = "Souami"
      , nomComplet = "Myriam Souami"
      , particule = Nothing
      , prenom = "Myriam"
      , slug = "myriam-souami"
      }
    , { courriels = [ "alexia.soucaze@gmail.com" ]
      , deputes = [ "jean-baptiste-djebbari" ]
      , enCirconscription = False
      , nom = "Soucaze"
      , nomComplet = "Alexia Soucaze"
      , particule = Nothing
      , prenom = "Alexia"
      , slug = "alexia-soucaze"
      }
    , { courriels = [ "nadia-souffoy@live.fr" ]
      , deputes = [ "jean-michel-jacques" ]
      , enCirconscription = False
      , nom = "Souffoy"
      , nomComplet = "Nadia Souffoy"
      , particule = Nothing
      , prenom = "Nadia"
      , slug = "nadia-souffoy"
      }
    , { courriels = [ "quentin.spooner@clb-an.fr" ]
      , deputes = [ "aina-kuric" ]
      , enCirconscription = False
      , nom = "Spooner"
      , nomComplet = "Quentin Spooner"
      , particule = Nothing
      , prenom = "Quentin"
      , slug = "quentin-spooner"
      }
    , { courriels = [ "Barbara.Stanisavljevic@clb-an.fr" ]
      , deputes = [ "sylvie-charriere" ]
      , enCirconscription = False
      , nom = "Stanisavljevic"
      , nomComplet = "Barbara Stanisavljevic"
      , particule = Nothing
      , prenom = "Barbara"
      , slug = "barbara-stanisavljevic"
      }
    , { courriels = [ "emile.stefani@gmail.com" ]
      , deputes = [ "guillaume-gouffier-cha" ]
      , enCirconscription = False
      , nom = "Stefani"
      , nomComplet = "Emile Stefani"
      , particule = Nothing
      , prenom = "Emile"
      , slug = "emile-stefani"
      }
    , { courriels = [ "clemence.taillan@clb-an.fr" ]
      , deputes = [ "nicole-trisse" ]
      , enCirconscription = False
      , nom = "Taillan"
      , nomComplet = "Clémence Taillan"
      , particule = Nothing
      , prenom = "Clémence"
      , slug = "clemence-taillan"
      }
    , { courriels = [ "theotedeschi2@gmail.com" ]
      , deputes = [ "anne-christine-lang" ]
      , enCirconscription = False
      , nom = "Tedeschi"
      , nomComplet = "Théo Tedeschi"
      , particule = Nothing
      , prenom = "Théo"
      , slug = "theo-tedeschi"
      }
    , { courriels = [ "severine.templet@clb-an.fr" ]
      , deputes = [ "hugues-renson" ]
      , enCirconscription = False
      , nom = "Templet"
      , nomComplet = "Severine Templet"
      , particule = Nothing
      , prenom = "Severine"
      , slug = "severine-templet"
      }
    , { courriels = [ "loic.terrenes@clb-an.fr" ]
      , deputes = [ "olivier-veran" ]
      , enCirconscription = False
      , nom = "Terrenes"
      , nomComplet = "Loic Terrenes"
      , particule = Nothing
      , prenom = "Loic"
      , slug = "loic-terrenes"
      }
    , { courriels = [ "monalea12@hotmail.com." ]
      , deputes = [ "frederique-lardet" ]
      , enCirconscription = False
      , nom = "Thabot"
      , nomComplet = "Léa Thabot"
      , particule = Nothing
      , prenom = "Léa"
      , slug = "lea-thabot"
      }
    , { courriels = [ "charlene.then@hotmail.fr" ]
      , deputes = [ "jean-bernard-sempastous" ]
      , enCirconscription = False
      , nom = "Then"
      , nomComplet = "Charlene Then"
      , particule = Nothing
      , prenom = "Charlene"
      , slug = "charlene-then"
      }
    , { courriels = [ "ctholoniat@gmail.com" ]
      , deputes = [ "julien-borowczyk" ]
      , enCirconscription = False
      , nom = "Tholoniat"
      , nomComplet = "Corentin Tholoniat"
      , particule = Nothing
      , prenom = "Corentin"
      , slug = "corentin-tholoniat"
      }
    , { courriels = [ "sylvie.thouvenin.b@gmail.com" ]
      , deputes = [ "thomas-gassilloud" ]
      , enCirconscription = False
      , nom = "Thouvenin"
      , nomComplet = "Sylvie Thouvenin"
      , particule = Nothing
      , prenom = "Sylvie"
      , slug = "sylvie-thouvenin"
      }
    , { courriels = [ "edgar.tilly@clb-an.fr" ]
      , deputes = [ "jean-noel-barrot" ]
      , enCirconscription = False
      , nom = "Tilly"
      , nomComplet = "Edgar Tilly"
      , particule = Nothing
      , prenom = "Edgar"
      , slug = "edgar-tilly"
      }
    , { courriels = [ "williamtissandier@hotmail.com" ]
      , deputes = [ "jean-rene-cazeneuve" ]
      , enCirconscription = False
      , nom = "Tissandier"
      , nomComplet = "William Tissandier"
      , particule = Nothing
      , prenom = "William"
      , slug = "william-tissandier"
      }
    , { courriels = [ "pierre.tiss@laposte.net" ]
      , deputes = [ "jean-charles-larsonneur" ]
      , enCirconscription = False
      , nom = "Tissier"
      , nomComplet = "Pierre Tissier"
      , particule = Nothing
      , prenom = "Pierre"
      , slug = "pierre-tissier"
      }
    , { courriels = [ "r.troutot@gmail.com" ]
      , deputes = [ "caroline-janvier" ]
      , enCirconscription = False
      , nom = "Troutot"
      , nomComplet = "Robin Troutot"
      , particule = Nothing
      , prenom = "Robin"
      , slug = "robin-troutot"
      }
    , { courriels = [ "gwenaelle.tschudin@en-marche.fr" ]
      , deputes = [ "perrine-goulet" ]
      , enCirconscription = False
      , nom = "Tschudin"
      , nomComplet = "Gwenaëlle Tschudin"
      , particule = Nothing
      , prenom = "Gwenaëlle"
      , slug = "gwenaelle-tschudin"
      }
    , { courriels = [ "nicolas.turmeau@outloo.fr" ]
      , deputes = [ "natalia-pouzyreff" ]
      , enCirconscription = False
      , nom = "Turmeau"
      , nomComplet = "Nicolas Turmeau"
      , particule = Nothing
      , prenom = "Nicolas"
      , slug = "nicolas-turmeau"
      }
    , { courriels = [ "emeric.vallespi@clb-an.fr" ]
      , deputes = [ "florence-granjus" ]
      , enCirconscription = False
      , nom = "Vallespi"
      , nomComplet = "Emeric Vallespi"
      , particule = Nothing
      , prenom = "Emeric"
      , slug = "emeric-vallespi"
      }
    , { courriels = [ "ericvanroy@yahoo.fr" ]
      , deputes = [ "laurent-pietraszewski" ]
      , enCirconscription = False
      , nom = "Van Roy"
      , nomComplet = "Eric Van Roy"
      , particule = Nothing
      , prenom = "Eric"
      , slug = "eric-van-roy"
      }
    , { courriels = [ "marie.vanderchmitt@clb-an.fr" ]
      , deputes = [ "monique-iborra" ]
      , enCirconscription = False
      , nom = "Vanderchmitt"
      , nomComplet = "Marie Vanderchmitt"
      , particule = Nothing
      , prenom = "Marie"
      , slug = "marie-vanderchmitt"
      }
    , { courriels = [ "romainvariot@gmail.com" ]
      , deputes = [ "nicole-dubre-chirat" ]
      , enCirconscription = False
      , nom = "Variot"
      , nomComplet = "Romain Variot"
      , particule = Nothing
      , prenom = "Romain"
      , slug = "romain-variot"
      }
    , { courriels = [ "baptiste.vaugoyeau@gmail.com" ]
      , deputes = [ "pacome-rupin" ]
      , enCirconscription = False
      , nom = "Vaugoyeau"
      , nomComplet = "Baptiste Vaugoyeau"
      , particule = Nothing
      , prenom = "Baptiste"
      , slug = "baptiste-vaugoyeau"
      }
    , { courriels = []
      , deputes = [ "graziella-melchior" ]
      , enCirconscription = False
      , nom = "Vernerey"
      , nomComplet = "Gautier Vernerey"
      , particule = Nothing
      , prenom = "Gautier"
      , slug = "gautier-vernerey"
      }
    , { courriels = []
      , deputes = [ "bruno-questel" ]
      , enCirconscription = False
      , nom = "Verneuil"
      , nomComplet = "Irving Verneuil"
      , particule = Nothing
      , prenom = "Irving"
      , slug = "irving-verneuil"
      }
    , { courriels = [ "isa.deputation@orange.fr" ]
      , deputes = [ "belkhir-belhaddad" ]
      , enCirconscription = False
      , nom = "Verot-Besnault"
      , nomComplet = "Isabelle Verot-Besnault"
      , particule = Nothing
      , prenom = "Isabelle"
      , slug = "isabelle-verot-besnault"
      }
    , { courriels = [ "Maxime.Vesselinoff@clb-an.fr" ]
      , deputes = [ "sylvie-charriere" ]
      , enCirconscription = False
      , nom = "Vesselinoff"
      , nomComplet = "Maxime Vesselinoff"
      , particule = Nothing
      , prenom = "Maxime"
      , slug = "maxime-vesselinoff"
      }
    , { courriels = []
      , deputes = [ "marie-tamarelle-verhaeghe" ]
      , enCirconscription = True
      , nom = "Vidal"
      , nomComplet = "Arnaud Vidal"
      , particule = Nothing
      , prenom = "Arnaud"
      , slug = "arnaud-vidal"
      }
    ]


collaborateurs3 : Collaborateurs
collaborateurs3 =
    [ { courriels = []
      , deputes = [ "ludovic-mendes" ]
      , enCirconscription = False
      , nom = "Vidal"
      , nomComplet = "Arthur Vidal"
      , particule = Nothing
      , prenom = "Arthur"
      , slug = "arthur-vidal"
      }
    , { courriels = [ "cvieville@assemblee-nationale.fr" ]
      , deputes = [ "jean-jacques-bridey" ]
      , enCirconscription = False
      , nom = "Vieville"
      , nomComplet = "Caroline Vieville"
      , particule = Nothing
      , prenom = "Caroline"
      , slug = "caroline-vieville"
      }
    , { courriels = [ "antonin.violette@clb-an.fr" ]
      , deputes = [ "marie-tamarelle-verhaeghe" ]
      , enCirconscription = False
      , nom = "Violette"
      , nomComplet = "Antonin Violette"
      , particule = Nothing
      , prenom = "Antonin"
      , slug = "antonin-violette"
      }
    , { courriels = [ "simonvirlogeux2017@gmail.com" ]
      , deputes = [ "hubert-julien-laferriere" ]
      , enCirconscription = False
      , nom = "Virlogeux"
      , nomComplet = "Simon Virlogeux"
      , particule = Nothing
      , prenom = "Simon"
      , slug = "simon-virlogeux"
      }
    , { courriels = [ "lvoisin.an@gmail.com" ]
      , deputes = [ "yannick-haury" ]
      , enCirconscription = False
      , nom = "Voisin"
      , nomComplet = "Lucie Voisin"
      , particule = Nothing
      , prenom = "Lucie"
      , slug = "lucie-voisin"
      }
    , { courriels = [ "vrilletaude@gmail.com" ]
      , deputes = [ "sylvain-maillard" ]
      , enCirconscription = False
      , nom = "Vrillet"
      , nomComplet = "Aude Vrillet"
      , particule = Nothing
      , prenom = "Aude"
      , slug = "aude-vrillet"
      }
    , { courriels = [ "gwaller.an70@gmail.com" ]
      , deputes = [ "barbara-bessot-ballot" ]
      , enCirconscription = False
      , nom = "Waller"
      , nomComplet = "Gaelle Waller"
      , particule = Nothing
      , prenom = "Gaelle"
      , slug = "gaelle-waller"
      }
    , { courriels = [ "barbara.wallon@clb-an.fr" ]
      , deputes = [ "jacques-marilossian" ]
      , enCirconscription = False
      , nom = "Wallon"
      , nomComplet = "Barbara Wallon"
      , particule = Nothing
      , prenom = "Barbara"
      , slug = "barbara-wallon"
      }
    , { courriels = [ "patrick.wei@clb-an.fr" ]
      , deputes = [ "laetitia-avia" ]
      , enCirconscription = False
      , nom = "Wei"
      , nomComplet = "Patrick Wei"
      , particule = Nothing
      , prenom = "Patrick"
      , slug = "patrick-wei"
      }
    , { courriels = [ "bweinberger.enmarche@gmail.com" ]
      , deputes = [ "anne-christine-lang" ]
      , enCirconscription = False
      , nom = "Weinberger"
      , nomComplet = "Benjamin Weinberger"
      , particule = Nothing
      , prenom = "Benjamin"
      , slug = "benjamin-weinberger"
      }
    , { courriels = [ "christopher.weissberg@clb-an.fr" ]
      , deputes = [ "roland-lescure" ]
      , enCirconscription = False
      , nom = "Weissberg"
      , nomComplet = "Christophe Weissberg"
      , particule = Nothing
      , prenom = "Christophe"
      , slug = "christophe-weissberg"
      }
    , { courriels = []
      , deputes = [ "frederique-lardet" ]
      , enCirconscription = False
      , nom = "Wodli"
      , nomComplet = "Jordane Wodli"
      , particule = Nothing
      , prenom = "Jordane"
      , slug = "jordane-wodli"
      }
    , { courriels = [ "1circo03@en-marche.fr" ]
      , deputes = [ "olga-givernet" ]
      , enCirconscription = True
      , nom = "Yavanovitch"
      , nomComplet = "Yaël Yavanovitch"
      , particule = Nothing
      , prenom = "Yaël"
      , slug = "yael-yavanovitch"
      }
    , { courriels = []
      , deputes = [ "helene-zannier" ]
      , enCirconscription = False
      , nom = "Yildrim"
      , nomComplet = "Umit Yildrim"
      , particule = Nothing
      , prenom = "Umit"
      , slug = "umit-yildrim"
      }
    ]


collaborateurs : Collaborateurs
collaborateurs =
    collaborateurs0 ++ collaborateurs1 ++ collaborateurs2 ++ collaborateurs3


deputes0 : Deputes
deputes0 =
    [ { circonscription = "Ain (5e circonscription)"
      , code = "OMC_PA605036"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "damien.abad@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Abad"
      , nomComplet = "Damien Abad"
      , particule = Nothing
      , photo = "/img/photos/605036.jpg"
      , prenom = "Damien"
      , sitesWeb = []
      , slug = "damien-abad"
      , titre = "M."
      }
    , { circonscription = "Isère (8e circonscription)"
      , code = "OMC_PA719866"
      , collaborateurs =
            [ "salma-el-ayyadi"
            , "florence-david"
            ]
      , commission = "Commission des lois"
      , courriels = [ "caroline.abadie@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Abadie"
      , nomComplet = "Caroline Abadie"
      , particule = Nothing
      , photo = "/img/photos/719866.jpg"
      , prenom = "Caroline"
      , sitesWeb = []
      , slug = "caroline-abadie"
      , titre = "Mme"
      }
    , { circonscription = "Haute-Marne (1re circonscription)"
      , code = "OMC_PA720242"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "berangere.abba@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Abba"
      , nomComplet = "Bérangère Abba"
      , particule = Nothing
      , photo = "/img/photos/720242.jpg"
      , prenom = "Bérangère"
      , sitesWeb = []
      , slug = "berangere-abba"
      , titre = "Mme"
      }
    , { circonscription = "Haute-Corse (2e circonscription)"
      , code = "OMC_PA719146"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "jean-felix.acquaviva@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Non inscrit"
      , nom = "Acquaviva"
      , nomComplet = "Jean-Félix Acquaviva"
      , particule = Nothing
      , photo = "/img/photos/719146.jpg"
      , prenom = "Jean-Félix"
      , sitesWeb = []
      , slug = "jean-felix-acquaviva"
      , titre = "M."
      }
    , { circonscription = "Seine-Maritime (1re circonscription)"
      , code = "OMC_PA722038"
      , collaborateurs =
            [ "jean-pierre-heranval"
            , "basile-rey"
            ]
      , commission = "Commission des affaires économiques"
      , courriels = [ "damien.adam@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Adam"
      , nomComplet = "Damien Adam"
      , particule = Nothing
      , photo = "/img/photos/722038.jpg"
      , prenom = "Damien"
      , sitesWeb = [ "http://www.damienadam2017.fr" ]
      , slug = "damien-adam"
      , titre = "M."
      }
    , { circonscription = "Guyane (2e circonscription)"
      , code = "OMC_PA721036"
      , collaborateurs = [ "virginie-prufer" ]
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "lenaick.adam@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Adam"
      , nomComplet = "Lénaïck Adam"
      , particule = Nothing
      , photo = "/img/photos/721036.jpg"
      , prenom = "Lénaïck"
      , sitesWeb = []
      , slug = "lenaick-adam"
      , titre = "M."
      }
    , { circonscription = "Bouches-du-Rhône (7e circonscription)"
      , code = "OMC_PA718954"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "said.ahamada@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Ahamada"
      , nomComplet = "Saïd Ahamada"
      , particule = Nothing
      , photo = "/img/photos/718954.jpg"
      , prenom = "Saïd"
      , sitesWeb = []
      , slug = "said-ahamada"
      , titre = "M."
      }
    , { circonscription = "Doubs (2e circonscription)"
      , code = "OMC_PA605963"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "ealauzet@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Alauzet"
      , nomComplet = "Éric Alauzet"
      , particule = Nothing
      , photo = "/img/photos/605963.jpg"
      , prenom = "Éric"
      , sitesWeb = [ "http://www.alauzet.net/" ]
      , slug = "eric-alauzet"
      , titre = "M."
      }
    , { circonscription = "Mayotte (1re circonscription)"
      , code = "OMC_PA724827"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "ramlati.ali@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Ali"
      , nomComplet = "Ramlati Ali"
      , particule = Nothing
      , photo = "/img/photos/724827.jpg"
      , prenom = "Ramlati"
      , sitesWeb = []
      , slug = "ramlati-ali"
      , titre = "Mme"
      }
    , { circonscription = "Pyrénées-Orientales (2e circonscription)"
      , code = "OMC_PA720798"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "louis.aliot@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Non inscrit"
      , nom = "Aliot"
      , nomComplet = "Louis Aliot"
      , particule = Nothing
      , photo = "/img/photos/720798.jpg"
      , prenom = "Louis"
      , sitesWeb = []
      , slug = "louis-aliot"
      , titre = "M."
      }
    , { circonscription = "Loire-Atlantique (4e circonscription)"
      , code = "OMC_PA720124"
      , collaborateurs =
            [ "stephanie-cotrel"
            , "antoine-brandalac"
            ]
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "aude.amadou@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Amadou"
      , nomComplet = "Aude Amadou"
      , particule = Nothing
      , photo = "/img/photos/720124.jpg"
      , prenom = "Aude"
      , sitesWeb = []
      , slug = "aude-amadou"
      , titre = "Mme"
      }
    , { circonscription = "Seine-Saint-Denis (3e circonscription)"
      , code = "OMC_PA721278"
      , collaborateurs =
            [ "mickael-rigault"
            , "jennifer-richard"
            , "aminata-sissoko"
            ]
      , commission = "Commission des affaires économiques"
      , courriels = [ "patrice.anato@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Anato"
      , nomComplet = "Patrice Anato"
      , particule = Nothing
      , photo = "/img/photos/721278.jpg"
      , prenom = "Patrice"
      , sitesWeb = []
      , slug = "patrice-anato"
      , titre = "M."
      }
    , { circonscription = "Ille-et-Vilaine (3e circonscription)"
      , code = "OMC_PA606675"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "francois.andre@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "André"
      , nomComplet = "François André"
      , particule = Nothing
      , photo = "/img/photos/606675.jpg"
      , prenom = "François"
      , sitesWeb = [ "http://www.francoisandre.fr" ]
      , slug = "francois-andre"
      , titre = "M."
      }
    , { circonscription = "Français établis hors de France (4e circonscription)"
      , code = "OMC_PA721158"
      , collaborateurs = [ "raphael-dorgans" ]
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "pieyre-alexandre.anglade@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Anglade"
      , nomComplet = "Pieyre-Alexandre Anglade"
      , particule = Nothing
      , photo = "/img/photos/721158.jpg"
      , prenom = "Pieyre-Alexandre"
      , sitesWeb = []
      , slug = "pieyre-alexandre-anglade"
      , titre = "M."
      }
    , { circonscription = "Drôme (4e circonscription)"
      , code = "OMC_PA719318"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "emmanuelle.anthoine@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Anthoine"
      , nomComplet = "Emmanuelle Anthoine"
      , particule = Nothing
      , photo = "/img/photos/719318.jpg"
      , prenom = "Emmanuelle"
      , sitesWeb = []
      , slug = "emmanuelle-anthoine"
      , titre = "Mme"
      }
    , { circonscription = "Charente-Maritime (3e circonscription)"
      , code = "OMC_PA719100"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "jean-philippe.ardouin@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Ardouin"
      , nomComplet = "Jean-Philippe Ardouin"
      , particule = Nothing
      , photo = "/img/photos/719100.jpg"
      , prenom = "Jean-Philippe"
      , sitesWeb = []
      , slug = "jean-philippe-ardouin"
      , titre = "M."
      }
    , { circonscription = "Moselle (6e circonscription)"
      , code = "OMC_PA720310"
      , collaborateurs = [ "aida-epelbaum" ]
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "christophe.arend@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Arend"
      , nomComplet = "Christophe Arend"
      , particule = Nothing
      , photo = "/img/photos/720310.jpg"
      , prenom = "Christophe"
      , sitesWeb = []
      , slug = "christophe-arend"
      , titre = "M."
      }
    , { circonscription = "Hauts-de-Seine (10e circonscription)"
      , code = "OMC_PA722190"
      , collaborateurs = [ "tess-indycki" ]
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "gabriel.attal@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Attal"
      , nomComplet = "Gabriel Attal"
      , particule = Nothing
      , photo = "/img/photos/722190.jpg"
      , prenom = "Gabriel"
      , sitesWeb = [ "http://www.gabrielattal.fr" ]
      , slug = "gabriel-attal"
      , titre = "M."
      }
    , { circonscription = "Vaucluse (5e circonscription)"
      , code = "OMC_PA609726"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "julien.aubert@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Aubert"
      , nomComplet = "Julien Aubert"
      , particule = Nothing
      , photo = "/img/photos/609726.jpg"
      , prenom = "Julien"
      , sitesWeb = [ "http://www.julienaubert.fr" ]
      , slug = "julien-aubert"
      , titre = "M."
      }
    , { circonscription = "Indre-et-Loire (3e circonscription)"
      , code = "OMC_PA719806"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "sophie.auconie@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Auconie"
      , nomComplet = "Sophie Auconie"
      , particule = Nothing
      , photo = "/img/photos/719806.jpg"
      , prenom = "Sophie"
      , sitesWeb = [ "http://www.sophieauconie.fr" ]
      , slug = "sophie-auconie"
      , titre = "Mme"
      }
    , { circonscription = "Seine-Saint-Denis (11e circonscription)"
      , code = "OMC_PA588884"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "clementine.autain@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La France insoumise"
      , nom = "Autain"
      , nomComplet = "Clémentine Autain"
      , particule = Nothing
      , photo = "/img/photos/588884.jpg"
      , prenom = "Clémentine"
      , sitesWeb = []
      , slug = "clementine-autain"
      , titre = "Mme"
      }
    , { circonscription = "Paris (8e circonscription)"
      , code = "OMC_PA721624"
      , collaborateurs =
            [ "agathe-chamfeuil"
            , "patrick-wei"
            , "cedric-merlaud"
            ]
      , commission = "Commission des lois"
      , courriels = [ "laetitia.avia@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Avia"
      , nomComplet = "Laetitia Avia"
      , particule = Nothing
      , photo = "/img/photos/721624.jpg"
      , prenom = "Laetitia"
      , sitesWeb = [ "http://www.laetitia-deputee.com" ]
      , slug = "laetitia-avia"
      , titre = "Mme"
      }
    , { circonscription = "Haute-Garonne (8e circonscription)"
      , code = "OMC_PA642764"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "joel.aviragnet@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Nouvelle Gauche"
      , nom = "Aviragnet"
      , nomComplet = "Joël Aviragnet"
      , particule = Nothing
      , photo = "/img/photos/642764.jpg"
      , prenom = "Joël"
      , sitesWeb = [ "http://www.joelaviragnet.com" ]
      , slug = "joel-aviragnet"
      , titre = "M."
      }
    , { circonscription = "Ille-et-Vilaine (8e circonscription)"
      , code = "OMC_PA719770"
      , collaborateurs =
            [ "sebastien-garnault"
            , "paul-marsauche"
            ]
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "florian.bachelier@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Bachelier"
      , nomComplet = "Florian Bachelier"
      , particule = Nothing
      , photo = "/img/photos/719770.jpg"
      , prenom = "Florian"
      , sitesWeb = [ "http://florianbachelier.en-marche.fr" ]
      , slug = "florian-bachelier"
      , titre = "M."
      }
    , { circonscription = "Alpes-de-Haute-Provence (1re circonscription)"
      , code = "OMC_PA718744"
      , collaborateurs = [ "joan-carbuccia" ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "delphine.bagarry@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Bagarry"
      , nomComplet = "Delphine Bagarry"
      , particule = Nothing
      , photo = "/img/photos/718744.jpg"
      , prenom = "Delphine"
      , sitesWeb = [ "http://www.delphinebagarry.fr" ]
      , slug = "delphine-bagarry"
      , titre = "Mme"
      }
    , { circonscription = "Yvelines (1re circonscription)"
      , code = "OMC_PA721776"
      , collaborateurs = [ "nicolas-leron" ]
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "didier.baichere@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Baichère"
      , nomComplet = "Didier Baichère"
      , particule = Nothing
      , photo = "/img/photos/721776.jpg"
      , prenom = "Didier"
      , sitesWeb = [ "http://www.didierbaichere.fr" ]
      , slug = "didier-baichere"
      , titre = "M."
      }
    , { circonscription = "Finistère (8e circonscription)"
      , code = "OMC_PA719558"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "erwan.balanant@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Balanant"
      , nomComplet = "Erwan Balanant"
      , particule = Nothing
      , photo = "/img/photos/719558.jpg"
      , prenom = "Erwan"
      , sitesWeb = []
      , slug = "erwan-balanant"
      , titre = "M."
      }
    , { circonscription = "Mayenne (2e circonscription)"
      , code = "OMC_PA720256"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "geraldine.bannier@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Bannier"
      , nomComplet = "Géraldine Bannier"
      , particule = Nothing
      , photo = "/img/photos/720256.jpg"
      , prenom = "Géraldine"
      , sitesWeb = []
      , slug = "geraldine-bannier"
      , titre = "Mme"
      }
    , { circonscription = "Doubs (4e circonscription)"
      , code = "OMC_PA642724"
      , collaborateurs = [ "trixie-meyssonnier" ]
      , commission = "Commission des affaires étrangères"
      , courriels = [ "frederic.barbier@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Barbier"
      , nomComplet = "Frédéric Barbier"
      , particule = Nothing
      , photo = "/img/photos/642724.jpg"
      , prenom = "Frédéric"
      , sitesWeb = []
      , slug = "frederic-barbier"
      , titre = "M."
      }
    , { circonscription = "Réunion (1re circonscription)"
      , code = "OMC_PA610681"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "ericka.bareigts@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Nouvelle Gauche"
      , nom = "Bareigts"
      , nomComplet = "Ericka Bareigts"
      , particule = Nothing
      , photo = "/img/photos/610681.jpg"
      , prenom = "Ericka"
      , sitesWeb = []
      , slug = "ericka-bareigts"
      , titre = "Mme"
      }
    , { circonscription = "Yvelines (2e circonscription)"
      , code = "OMC_PA721836"
      , collaborateurs =
            [ "edgar-tilly"
            , "delphine-scaini"
            , "jaime-costa-centena"
            ]
      , commission = "Commission des finances"
      , courriels = [ "jean-noel.barrot@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Barrot"
      , nomComplet = "Jean-Noël Barrot"
      , particule = Nothing
      , photo = "/img/photos/721836.jpg"
      , prenom = "Jean-Noël"
      , sitesWeb = []
      , slug = "jean-noel-barrot"
      , titre = "M."
      }
    , { circonscription = "Réunion (3e circonscription)"
      , code = "OMC_PA721046"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "nathalie.bassire@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Bassire"
      , nomComplet = "Nathalie Bassire"
      , particule = Nothing
      , photo = "/img/photos/721046.jpg"
      , prenom = "Nathalie"
      , sitesWeb = [ "http://www.nathaliebassire.re" ]
      , slug = "nathalie-bassire"
      , titre = "Mme"
      }
    , { circonscription = "Deux-Sèvres (2e circonscription)"
      , code = "OMC_PA335999"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "delphine.batho@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Nouvelle Gauche"
      , nom = "Batho"
      , nomComplet = "Delphine Batho"
      , particule = Nothing
      , photo = "/img/photos/335999.jpg"
      , prenom = "Delphine"
      , sitesWeb = []
      , slug = "delphine-batho"
      , titre = "Mme"
      }
    , { circonscription = "Isère (4e circonscription)"
      , code = "OMC_PA342415"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "marie-noelle.battistel@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "Nouvelle Gauche"
      , nom = "Battistel"
      , nomComplet = "Marie-Noëlle Battistel"
      , particule = Nothing
      , photo = "/img/photos/342415.jpg"
      , prenom = "Marie-Noëlle"
      , sitesWeb = []
      , slug = "marie-noelle-battistel"
      , titre = "Mme"
      }
    , { circonscription = "Seine-Maritime (10e circonscription)"
      , code = "OMC_PA721522"
      , collaborateurs = [ "eric-rucklin" ]
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "xavier.batut@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Batut"
      , nomComplet = "Xavier Batut"
      , particule = Nothing
      , photo = "/img/photos/721522.jpg"
      , prenom = "Xavier"
      , sitesWeb = []
      , slug = "xavier-batut"
      , titre = "M."
      }
    , { circonscription = "Meurthe-et-Moselle (4e circonscription)"
      , code = "OMC_PA642847"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "thibault.bazin@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Bazin"
      , nomComplet = "Thibault Bazin"
      , particule = Nothing
      , photo = "/img/photos/642847.jpg"
      , prenom = "Thibault"
      , sitesWeb = [ "http://www.thibaultbazin.fr" ]
      , slug = "thibault-bazin"
      , titre = "M."
      }
    , { circonscription = "Aube (2e circonscription)"
      , code = "OMC_PA718884"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "valerie.bazin-malgras@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Bazin-Malgras"
      , nomComplet = "Valérie Bazin-Malgras"
      , particule = Nothing
      , photo = "/img/photos/718884.jpg"
      , prenom = "Valérie"
      , sitesWeb = []
      , slug = "valerie-bazin-malgras"
      , titre = "Mme"
      }
    , { circonscription = "Haute-Vienne (1re circonscription)"
      , code = "OMC_PA722170"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "sophie.beaudouin-hubiere@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Beaudouin-Hubiere"
      , nomComplet = "Sophie Beaudouin-Hubiere"
      , particule = Nothing
      , photo = "/img/photos/722170.jpg"
      , prenom = "Sophie"
      , sitesWeb = [ "http://sophiebeaudouinhubiere.en-marche.fr" ]
      , slug = "sophie-beaudouin-hubiere"
      , titre = "Mme"
      }
    , { circonscription = "Marne (1re circonscription)"
      , code = "OMC_PA720210"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "valerie.beauvais@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Beauvais"
      , nomComplet = "Valérie Beauvais"
      , particule = Nothing
      , photo = "/img/photos/720210.jpg"
      , prenom = "Valérie"
      , sitesWeb = []
      , slug = "valerie-beauvais"
      , titre = "Mme"
      }
    , { circonscription = "Haut-Rhin (5e circonscription)"
      , code = "OMC_PA642935"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "olivier.becht@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Becht"
      , nomComplet = "Olivier Becht"
      , particule = Nothing
      , photo = "/img/photos/642935.jpg"
      , prenom = "Olivier"
      , sitesWeb = [ "http://www.becht-groff2017.fr" ]
      , slug = "olivier-becht"
      , titre = "M."
      }
    , { circonscription = "Moselle (1re circonscription)"
      , code = "OMC_PA720362"
      , collaborateurs = [ "isabelle-verot-besnault" ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "belkhir.belhaddad@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Belhaddad"
      , nomComplet = "Belkhir Belhaddad"
      , particule = Nothing
      , photo = "/img/photos/720362.jpg"
      , prenom = "Belkhir"
      , sitesWeb = []
      , slug = "belkhir-belhaddad"
      , titre = "M."
      }
    , { circonscription = "Réunion (2e circonscription)"
      , code = "OMC_PA441"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "huguette.bello@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Gauche démocrate et républicaine"
      , nom = "Bello"
      , nomComplet = "Huguette Bello"
      , particule = Nothing
      , photo = "/img/photos/441.jpg"
      , prenom = "Huguette"
      , sitesWeb = []
      , slug = "huguette-bello"
      , titre = "Mme"
      }
    , { circonscription = "Guadeloupe (2e circonscription)"
      , code = "OMC_PA720968"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "justine.benin@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Benin"
      , nomComplet = "Justine Benin"
      , particule = Nothing
      , photo = "/img/photos/720968.jpg"
      , prenom = "Justine"
      , sitesWeb = []
      , slug = "justine-benin"
      , titre = "Mme"
      }
    , { circonscription = "Ille-et-Vilaine (6e circonscription)"
      , code = "OMC_PA332228"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "thierry.benoit@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Benoit"
      , nomComplet = "Thierry Benoit"
      , particule = Nothing
      , photo = "/img/photos/332228.jpg"
      , prenom = "Thierry"
      , sitesWeb = [ "http://www.thierry-benoit.fr" ]
      , slug = "thierry-benoit"
      , titre = "M."
      }
    , { circonscription = "Yvelines (10e circonscription)"
      , code = "OMC_PA722046"
      , collaborateurs =
            [ "celine-montaner"
            , "pierre-manenti"
            ]
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "aurore.berge@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Bergé"
      , nomComplet = "Aurore Bergé"
      , particule = Nothing
      , photo = "/img/photos/722046.jpg"
      , prenom = "Aurore"
      , sitesWeb = [ "http://auroreberge2017.fr" ]
      , slug = "aurore-berge"
      , titre = "Mme"
      }
    , { circonscription = "Nord (2e circonscription)"
      , code = "OMC_PA720430"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "ugo.bernalicis@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La France insoumise"
      , nom = "Bernalicis"
      , nomComplet = "Ugo Bernalicis"
      , particule = Nothing
      , photo = "/img/photos/720430.jpg"
      , prenom = "Ugo"
      , sitesWeb = [ "http://www.ugobernalicis.fr" ]
      , slug = "ugo-bernalicis"
      , titre = "M."
      }
    , { circonscription = "Gard (6e circonscription)"
      , code = "OMC_PA719488"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "philippe.berta@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Berta"
      , nomComplet = "Philippe Berta"
      , particule = Nothing
      , photo = "/img/photos/719488.jpg"
      , prenom = "Philippe"
      , sitesWeb = []
      , slug = "philippe-berta"
      , titre = "M."
      }
    , { circonscription = "Côtes-d'Armor (2e circonscription)"
      , code = "OMC_PA719218"
      , collaborateurs = [ "thomas-d-yvoire" ]
      , commission = "Commission des affaires étrangères"
      , courriels = [ "herve.berville@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Berville"
      , nomComplet = "Hervé Berville"
      , particule = Nothing
      , photo = "/img/photos/719218.jpg"
      , prenom = "Hervé"
      , sitesWeb = []
      , slug = "herve-berville"
      , titre = "M."
      }
    , { circonscription = "Aube (1re circonscription)"
      , code = "OMC_PA718876"
      , collaborateurs = [ "nicolas-barnier" ]
      , commission = "Commission des affaires économiques"
      , courriels = [ "gregory.besson-moreau@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Besson-Moreau"
      , nomComplet = "Grégory Besson-Moreau"
      , particule = Nothing
      , photo = "/img/photos/718876.jpg"
      , prenom = "Grégory"
      , sitesWeb = []
      , slug = "gregory-besson-moreau"
      , titre = "M."
      }
    , { circonscription = "Haute-Saône (1re circonscription)"
      , code = "OMC_PA722312"
      , collaborateurs =
            [ "francine-riondel"
            , "antoine-alliot"
            , "gaelle-waller"
            , "jean-pascal-mariot"
            ]
      , commission = "Commission des affaires économiques"
      , courriels = [ "barbara.bessotballot@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Bessot Ballot"
      , nomComplet = "Barbara Bessot Ballot"
      , particule = Nothing
      , photo = "/img/photos/722312.jpg"
      , prenom = "Barbara"
      , sitesWeb = []
      , slug = "barbara-bessot-ballot"
      , titre = "Mme"
      }
    , { circonscription = "Pas-de-Calais (12e circonscription)"
      , code = "OMC_PA720822"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "bruno.bilde@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Non inscrit"
      , nom = "Bilde"
      , nomComplet = "Bruno Bilde"
      , particule = Nothing
      , photo = "/img/photos/720822.jpg"
      , prenom = "Bruno"
      , sitesWeb = []
      , slug = "bruno-bilde"
      , titre = "M."
      }
    , { circonscription = "Gers (2e circonscription)"
      , code = "OMC_PA508"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "gisele.biemouret@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Nouvelle Gauche"
      , nom = "Biémouret"
      , nomComplet = "Gisèle Biémouret"
      , particule = Nothing
      , photo = "/img/photos/508.jpg"
      , prenom = "Gisèle"
      , sitesWeb = []
      , slug = "gisele-biemouret"
      , titre = "Mme"
      }
    , { circonscription = "Aveyron (2e circonscription)"
      , code = "OMC_PA718990"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "anne.blanc@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Blanc"
      , nomComplet = "Anne Blanc"
      , particule = Nothing
      , photo = "/img/photos/718990.jpg"
      , prenom = "Anne"
      , sitesWeb = []
      , slug = "anne-blanc"
      , titre = "Mme"
      }
    , { circonscription = "Calvados (4e circonscription)"
      , code = "OMC_PA719024"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "christophe.blanchet@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Blanchet"
      , nomComplet = "Christophe Blanchet"
      , particule = Nothing
      , photo = "/img/photos/719024.jpg"
      , prenom = "Christophe"
      , sitesWeb = [ "http://www.christopheblanchet2017.fr" ]
      , slug = "christophe-blanchet"
      , titre = "M."
      }
    , { circonscription = "Rhône (14e circonscription)"
      , code = "OMC_PA608641"
      , collaborateurs =
            [ "pierre-obrecht"
            , "christelle-salas"
            ]
      , commission = "Commission des affaires économiques"
      , courriels = [ "yves.blein@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Blein"
      , nomComplet = "Yves Blein"
      , particule = Nothing
      , photo = "/img/photos/608641.jpg"
      , prenom = "Yves"
      , sitesWeb = [ "http://www.yvesblein.fr" ]
      , slug = "yves-blein"
      , titre = "M."
      }
    , { circonscription = "Oise (3e circonscription)"
      , code = "OMC_PA720576"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "pascal.bois@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "La République en Marche"
      , nom = "Bois"
      , nomComplet = "Pascal Bois"
      , particule = Nothing
      , photo = "/img/photos/720576.jpg"
      , prenom = "Pascal"
      , sitesWeb = [ "http://www.pascalbois.fr" ]
      , slug = "pascal-bois"
      , titre = "M."
      }
    , { circonscription = "Maine-et-Loire (7e circonscription)"
      , code = "OMC_PA720162"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "philippe.bolo@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Bolo"
      , nomComplet = "Philippe Bolo"
      , particule = Nothing
      , photo = "/img/photos/720162.jpg"
      , prenom = "Philippe"
      , sitesWeb = []
      , slug = "philippe-bolo"
      , titre = "M."
      }
    , { circonscription = "Vaucluse (4e circonscription)"
      , code = "OMC_PA563"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "jacques.bompard@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Non inscrit"
      , nom = "Bompard"
      , nomComplet = "Jacques Bompard"
      , particule = Nothing
      , photo = "/img/photos/563.jpg"
      , prenom = "Jacques"
      , sitesWeb = [ "http://www.jacquesbompard.fr" ]
      , slug = "jacques-bompard"
      , titre = "M."
      }
    , { circonscription = "Rhône (6e circonscription)"
      , code = "OMC_PA721344"
      , collaborateurs =
            [ "bertrand-prevost"
            , "jimmy-brumant"
            , "anne-sophie-monne"
            ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "bruno.bonnell@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Bonnell"
      , nomComplet = "Bruno Bonnell"
      , particule = Nothing
      , photo = "/img/photos/721344.jpg"
      , prenom = "Bruno"
      , sitesWeb = [ "http://www.brunobonnell-villeurbanne.fr" ]
      , slug = "bruno-bonnell"
      , titre = "M."
      }
    , { circonscription = "Savoie (3e circonscription)"
      , code = "OMC_PA721410"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "emilie.bonnivard@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Bonnivard"
      , nomComplet = "Émilie Bonnivard"
      , particule = Nothing
      , photo = "/img/photos/721410.jpg"
      , prenom = "Émilie"
      , sitesWeb = [ "http://emiliebonnivard.fr" ]
      , slug = "emilie-bonnivard"
      , titre = "Mme"
      }
    , { circonscription = "Aisne (1re circonscription)"
      , code = "OMC_PA718756"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "aude.bono-vandorme@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Bono-Vandorme"
      , nomComplet = "Aude Bono-Vandorme"
      , particule = Nothing
      , photo = "/img/photos/718756.jpg"
      , prenom = "Aude"
      , sitesWeb = []
      , slug = "aude-bono-vandorme"
      , titre = "Mme"
      }
    , { circonscription = "Cantal (2e circonscription)"
      , code = "OMC_PA266793"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "jean-yves.bony@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Bony"
      , nomComplet = "Jean-Yves Bony"
      , particule = Nothing
      , photo = "/img/photos/266793.jpg"
      , prenom = "Jean-Yves"
      , sitesWeb = []
      , slug = "jean-yves-bony"
      , titre = "M."
      }
    , { circonscription = "Loire (6e circonscription)"
      , code = "OMC_PA719980"
      , collaborateurs =
            [ "corentin-tholoniat"
            , "auriane-gourbeyre"
            ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "julien.borowczyk@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "La République en Marche"
      , nom = "Borowczyk"
      , nomComplet = "Julien Borowczyk"
      , particule = Nothing
      , photo = "/img/photos/719980.jpg"
      , prenom = "Julien"
      , sitesWeb = [ "http://www.julienborowczyk.fr" ]
      , slug = "julien-borowczyk"
      , titre = "M."
      }
    , { circonscription = "Côtes-d'Armor (5e circonscription)"
      , code = "OMC_PA642695"
      , collaborateurs =
            [ "maxime-donadille"
            , "celine-le-vay"
            , "ines-guennouni"
            ]
      , commission = "Commission des affaires économiques"
      , courriels = [ "eric.bothorel@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Bothorel"
      , nomComplet = "Éric Bothorel"
      , particule = Nothing
      , photo = "/img/photos/642695.jpg"
      , prenom = "Éric"
      , sitesWeb = []
      , slug = "eric-bothorel"
      , titre = "M."
      }
    , { circonscription = "Territoire de Belfort (1re circonscription)"
      , code = "OMC_PA721816"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "ian.boucard@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Boucard"
      , nomComplet = "Ian Boucard"
      , particule = Nothing
      , photo = "/img/photos/721816.jpg"
      , prenom = "Ian"
      , sitesWeb = [ "http://boucard2017.fr/" ]
      , slug = "ian-boucard"
      , titre = "M."
      }
    , { circonscription = "Vaucluse (2e circonscription)"
      , code = "OMC_PA336316"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "jean-claude.bouchet@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Bouchet"
      , nomComplet = "Jean-Claude Bouchet"
      , particule = Nothing
      , photo = "/img/photos/336316.jpg"
      , prenom = "Jean-Claude"
      , sitesWeb = []
      , slug = "jean-claude-bouchet"
      , titre = "M."
      }
    , { circonscription = "Gironde (10e circonscription)"
      , code = "OMC_PA606507"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "florent.boudie@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Boudié"
      , nomComplet = "Florent Boudié"
      , particule = Nothing
      , photo = "/img/photos/606507.jpg"
      , prenom = "Florent"
      , sitesWeb = [ "http://www.florentboudie.fr" ]
      , slug = "florent-boudie"
      , titre = "M."
      }
    , { circonscription = "Seine-Maritime (5e circonscription)"
      , code = "OMC_PA267233"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "christophe.bouillon@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Nouvelle Gauche"
      , nom = "Bouillon"
      , nomComplet = "Christophe Bouillon"
      , particule = Nothing
      , photo = "/img/photos/267233.jpg"
      , prenom = "Christophe"
      , sitesWeb = []
      , slug = "christophe-bouillon"
      , titre = "M."
      }
    , { circonscription = "Pas-de-Calais (6e circonscription)"
      , code = "OMC_PA608083"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "brigitte.bourguignon@assemblee-nationale.fr" ]
      , fonctionCommission = "Président"
      , groupe = "La République en Marche"
      , nom = "Bourguignon"
      , nomComplet = "Brigitte Bourguignon"
      , particule = Nothing
      , photo = "/img/photos/608083.jpg"
      , prenom = "Brigitte"
      , sitesWeb = []
      , slug = "brigitte-bourguignon"
      , titre = "Mme"
      }
    , { circonscription = "Hauts-de-Seine (12e circonscription)"
      , code = "OMC_PA721608"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "jean-louis.bourlanges@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Bourlanges"
      , nomComplet = "Jean-Louis Bourlanges"
      , particule = Nothing
      , photo = "/img/photos/721608.jpg"
      , prenom = "Jean-Louis"
      , sitesWeb = []
      , slug = "jean-louis-bourlanges"
      , titre = "M."
      }
    , { circonscription = "Paris (18e circonscription)"
      , code = "OMC_PA722030"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "pierre-yves.bournazel@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Bournazel"
      , nomComplet = "Pierre-Yves Bournazel"
      , particule = Nothing
      , photo = "/img/photos/722030.jpg"
      , prenom = "Pierre-Yves"
      , sitesWeb = []
      , slug = "pierre-yves-bournazel"
      , titre = "M."
      }
    , { circonscription = "Calvados (5e circonscription)"
      , code = "OMC_PA719032"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "bertrand.bouyx@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Bouyx"
      , nomComplet = "Bertrand Bouyx"
      , particule = Nothing
      , photo = "/img/photos/719032.jpg"
      , prenom = "Bertrand"
      , sitesWeb = []
      , slug = "bertrand-bouyx"
      , titre = "M."
      }
    , { circonscription = "Hautes-Alpes (1re circonscription)"
      , code = "OMC_PA718710"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "pascale.boyer@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Boyer"
      , nomComplet = "Pascale Boyer"
      , particule = Nothing
      , photo = "/img/photos/718710.jpg"
      , prenom = "Pascale"
      , sitesWeb = []
      , slug = "pascale-boyer"
      , titre = "Mme"
      }
    , { circonscription = "Bouches-du-Rhône (1re circonscription)"
      , code = "OMC_PA330684"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "valerie.boyer@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Boyer"
      , nomComplet = "Valérie Boyer"
      , particule = Nothing
      , photo = "/img/photos/330684.jpg"
      , prenom = "Valérie"
      , sitesWeb = []
      , slug = "valerie-boyer"
      , titre = "Mme"
      }
    , { circonscription = "Yvelines (5e circonscription)"
      , code = "OMC_PA721908"
      , collaborateurs =
            [ "tanguy-buche"
            , "michel-mazars"
            , "valerie-singer"
            ]
      , commission = "Commission des lois"
      , courriels = [ "yael.braun-pivet@assemblee-nationale.fr" ]
      , fonctionCommission = "Président"
      , groupe = "La République en Marche"
      , nom = "Braun-Pivet"
      , nomComplet = "Yaël Braun-Pivet"
      , particule = Nothing
      , photo = "/img/photos/721908.jpg"
      , prenom = "Yaël"
      , sitesWeb = []
      , slug = "yael-braun-pivet"
      , titre = "Mme"
      }
    , { circonscription = "Alpes-Maritimes (5e circonscription)"
      , code = "OMC_PA713448"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "marine.brenier@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Brenier"
      , nomComplet = "Marine Brenier"
      , particule = Nothing
      , photo = "/img/photos/713448.jpg"
      , prenom = "Marine"
      , sitesWeb = [ "http://www.marinebrenier.fr" ]
      , slug = "marine-brenier"
      , titre = "Mme"
      }
    , { circonscription = "Ain (1re circonscription)"
      , code = "OMC_PA330008"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "xavier.breton@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "Les Républicains"
      , nom = "Breton"
      , nomComplet = "Xavier Breton"
      , particule = Nothing
      , photo = "/img/photos/330008.jpg"
      , prenom = "Xavier"
      , sitesWeb = [ "http://www.xavierbreton.fr" ]
      , slug = "xavier-breton"
      , titre = "M."
      }
    , { circonscription = "Nord (18e circonscription)"
      , code = "OMC_PA267766"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "guy.bricout@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Bricout"
      , nomComplet = "Guy Bricout"
      , particule = Nothing
      , photo = "/img/photos/267766.jpg"
      , prenom = "Guy"
      , sitesWeb = [ "http://guybricout.org" ]
      , slug = "guy-bricout"
      , titre = "M."
      }
    , { circonscription = "Aisne (3e circonscription)"
      , code = "OMC_PA605069"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "jean-louis.bricout@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Nouvelle Gauche"
      , nom = "Bricout"
      , nomComplet = "Jean-Louis Bricout"
      , particule = Nothing
      , photo = "/img/photos/605069.jpg"
      , prenom = "Jean-Louis"
      , sitesWeb = [ "http://www.jeanlouisbricout.fr" ]
      , slug = "jean-louis-bricout"
      , titre = "M."
      }
    , { circonscription = "Val-de-Marne (7e circonscription)"
      , code = "OMC_PA672"
      , collaborateurs = [ "caroline-vieville" ]
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "jean-jacques.bridey@assemblee-nationale.fr" ]
      , fonctionCommission = "Président"
      , groupe = "La République en Marche"
      , nom = "Bridey"
      , nomComplet = "Jean-Jacques Bridey"
      , particule = Nothing
      , photo = "/img/photos/672.jpg"
      , prenom = "Jean-Jacques"
      , sitesWeb = []
      , slug = "jean-jacques-bridey"
      , titre = "M."
      }
    , { circonscription = "Rhône (5e circonscription)"
      , code = "OMC_PA721336"
      , collaborateurs = [ "eleonore-branchy" ]
      , commission = "Commission des affaires étrangères"
      , courriels = [ "blandine.brocard@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Brocard"
      , nomComplet = "Blandine Brocard"
      , particule = Nothing
      , photo = "/img/photos/721336.jpg"
      , prenom = "Blandine"
      , sitesWeb = []
      , slug = "blandine-brocard"
      , titre = "Mme"
      }
    , { circonscription = "Alpes-Maritimes (8e circonscription)"
      , code = "OMC_PA223837"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "bernard.brochand@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Brochand"
      , nomComplet = "Bernard Brochand"
      , particule = Nothing
      , photo = "/img/photos/223837.jpg"
      , prenom = "Bernard"
      , sitesWeb = []
      , slug = "bernard-brochand"
      , titre = "M."
      }
    , { circonscription = "Polynésie Française (3e circonscription)"
      , code = "OMC_PA721118"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "moetai.brotherson@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Gauche démocrate et républicaine"
      , nom = "Brotherson"
      , nomComplet = "Moetai Brotherson"
      , particule = Nothing
      , photo = "/img/photos/721118.jpg"
      , prenom = "Moetai"
      , sitesWeb = [ "http://www.moetai.brotherson.org" ]
      , slug = "moetai-brotherson"
      , titre = "M."
      }
    , { circonscription = "Pyrénées-Atlantiques (6e circonscription)"
      , code = "OMC_PA720772"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "vincent.bru@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Bru"
      , nomComplet = "Vincent Bru"
      , particule = Nothing
      , photo = "/img/photos/720772.jpg"
      , prenom = "Vincent"
      , sitesWeb = [ "http://vincentbru.en-marche.fr" ]
      , slug = "vincent-bru"
      , titre = "M."
      }
    , { circonscription = "Rhône (4e circonscription)"
      , code = "OMC_PA721328"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "anne.brugnera@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Brugnera"
      , nomComplet = "Anne Brugnera"
      , particule = Nothing
      , photo = "/img/photos/721328.jpg"
      , prenom = "Anne"
      , sitesWeb = [ "http://www.annebrugnera.fr" ]
      , slug = "anne-brugnera"
      , titre = "Mme"
      }
    , { circonscription = "Jura (1re circonscription)"
      , code = "OMC_PA719890"
      , collaborateurs =
            [ "victorien-lachas"
            , "benjamin-guichard"
            ]
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "danielle.brulebois@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Brulebois"
      , nomComplet = "Danielle Brulebois"
      , particule = Nothing
      , photo = "/img/photos/719890.jpg"
      , prenom = "Danielle"
      , sitesWeb = []
      , slug = "danielle-brulebois"
      , titre = "Mme"
      }
    , { circonscription = "Ardèche (3e circonscription)"
      , code = "OMC_PA718838"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "fabrice.brun@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Brun"
      , nomComplet = "Fabrice Brun"
      , particule = Nothing
      , photo = "/img/photos/718838.jpg"
      , prenom = "Fabrice"
      , sitesWeb = [ "http://fabricebrun.fr" ]
      , slug = "fabrice-brun"
      , titre = "M."
      }
    , { circonscription = "Nord (16e circonscription)"
      , code = "OMC_PA720546"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "alain.bruneel@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Gauche démocrate et républicaine"
      , nom = "Bruneel"
      , nomComplet = "Alain Bruneel"
      , particule = Nothing
      , photo = "/img/photos/720546.jpg"
      , prenom = "Alain"
      , sitesWeb = []
      , slug = "alain-bruneel"
      , titre = "M."
      }
    , { circonscription = "Loire-Atlantique (3e circonscription)"
      , code = "OMC_PA720116"
      , collaborateurs = [ "anthony-nicoud" ]
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "anne-france.brunet@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Brunet"
      , nomComplet = "Anne-France Brunet"
      , particule = Nothing
      , photo = "/img/photos/720116.jpg"
      , prenom = "Anne-France"
      , sitesWeb = []
      , slug = "anne-france-brunet"
      , titre = "Mme"
      }
    , { circonscription = "Vendée (3e circonscription)"
      , code = "OMC_PA722000"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "stephane.buchou@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Buchou"
      , nomComplet = "Stéphane Buchou"
      , particule = Nothing
      , photo = "/img/photos/722000.jpg"
      , prenom = "Stéphane"
      , sitesWeb = []
      , slug = "stephane-buchou"
      , titre = "M."
      }
    , { circonscription = "Seine-Saint-Denis (4e circonscription)"
      , code = "OMC_PA689"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "marie-george.buffet@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Gauche démocrate et républicaine"
      , nom = "Buffet"
      , nomComplet = "Marie-George Buffet"
      , particule = Nothing
      , photo = "/img/photos/689.jpg"
      , prenom = "Marie-George"
      , sitesWeb = []
      , slug = "marie-george-buffet"
      , titre = "Mme"
      }
    , { circonscription = "Oise (6e circonscription)"
      , code = "OMC_PA720622"
      , collaborateurs = [ "tony-granzotto" ]
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "carole.bureau-bonnard@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Bureau-Bonnard"
      , nomComplet = "Carole Bureau-Bonnard"
      , particule = Nothing
      , photo = "/img/photos/720622.jpg"
      , prenom = "Carole"
      , sitesWeb = []
      , slug = "carole-bureau-bonnard"
      , titre = "Mme"
      }
    , { circonscription = "Haute-Garonne (1re circonscription)"
      , code = "OMC_PA719496"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "pierre.cabare@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Cabaré"
      , nomComplet = "Pierre Cabaré"
      , particule = Nothing
      , photo = "/img/photos/719496.jpg"
      , prenom = "Pierre"
      , sitesWeb = []
      , slug = "pierre-cabare"
      , titre = "M."
      }
    , { circonscription = "Hauts-de-Seine (5e circonscription)"
      , code = "OMC_PA721750"
      , collaborateurs =
            [ "emannuel-bodoignet"
            , "catherine-sebaux"
            ]
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "celine.calvez@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Calvez"
      , nomComplet = "Céline Calvez"
      , particule = Nothing
      , photo = "/img/photos/721750.jpg"
      , prenom = "Céline"
      , sitesWeb = []
      , slug = "celine-calvez"
      , titre = "Mme"
      }
    , { circonscription = "Meuse (2e circonscription)"
      , code = "OMC_PA720298"
      , collaborateurs = [ "bertrand-legros" ]
      , commission = "Commission des finances"
      , courriels = [ "emilie.cariou@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "La République en Marche"
      , nom = "Cariou"
      , nomComplet = "Émilie Cariou"
      , particule = Nothing
      , photo = "/img/photos/720298.jpg"
      , prenom = "Émilie"
      , sitesWeb = []
      , slug = "emilie-cariou"
      , titre = "Mme"
      }
    , { circonscription = "Val-de-Marne (5e circonscription)"
      , code = "OMC_PA746"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "gilles.carrez@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Carrez"
      , nomComplet = "Gilles Carrez"
      , particule = Nothing
      , photo = "/img/photos/746.jpg"
      , prenom = "Gilles"
      , sitesWeb = []
      , slug = "gilles-carrez"
      , titre = "M."
      }
    , { circonscription = "Val-de-Marne (9e circonscription)"
      , code = "OMC_PA429893"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "luc.carvounas@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Nouvelle Gauche"
      , nom = "Carvounas"
      , nomComplet = "Luc Carvounas"
      , particule = Nothing
      , photo = "/img/photos/429893.jpg"
      , prenom = "Luc"
      , sitesWeb = [ "http://www.luccarvounas.fr" ]
      , slug = "luc-carvounas"
      , titre = "M."
      }
    , { circonscription = "Haute-Corse (1re circonscription)"
      , code = "OMC_PA719138"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "michel.castellani@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Non inscrit"
      , nom = "Castellani"
      , nomComplet = "Michel Castellani"
      , particule = Nothing
      , photo = "/img/photos/719138.jpg"
      , prenom = "Michel"
      , sitesWeb = []
      , slug = "michel-castellani"
      , titre = "M."
      }
    , { circonscription = "Nord (12e circonscription)"
      , code = "OMC_PA720520"
      , collaborateurs =
            [ "ijja-el-ghoul"
            , "louise-aelbrecht"
            ]
      , commission = "Commission des finances"
      , courriels = [ "anne-laure.cattelot@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Cattelot"
      , nomComplet = "Anne-Laure Cattelot"
      , particule = Nothing
      , photo = "/img/photos/720520.jpg"
      , prenom = "Anne-Laure"
      , sitesWeb = []
      , slug = "anne-laure-cattelot"
      , titre = "Mme"
      }
    , { circonscription = "Haut-Rhin (2e circonscription)"
      , code = "OMC_PA767"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "jacques.cattin@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Cattin"
      , nomComplet = "Jacques Cattin"
      , particule = Nothing
      , photo = "/img/photos/767.jpg"
      , prenom = "Jacques"
      , sitesWeb = []
      , slug = "jacques-cattin"
      , titre = "M."
      }
    ]


deputes1 : Deputes
deputes1 =
    [ { circonscription = "Landes (2e circonscription)"
      , code = "OMC_PA719922"
      , collaborateurs =
            [ "antoine-courant"
            , "pierre-pecastaings"
            ]
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "lionel.causse@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Causse"
      , nomComplet = "Lionel Causse"
      , particule = Nothing
      , photo = "/img/photos/719922.jpg"
      , prenom = "Lionel"
      , sitesWeb = []
      , slug = "lionel-causse"
      , titre = "M."
      }
    , { circonscription = "Rhône (13e circonscription)"
      , code = "OMC_PA721352"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "daniele.cazarian@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Cazarian"
      , nomComplet = "Danièle Cazarian"
      , particule = Nothing
      , photo = "/img/photos/721352.jpg"
      , prenom = "Danièle"
      , sitesWeb = []
      , slug = "daniele-cazarian"
      , titre = "Mme"
      }
    , { circonscription = "Français établis hors de France (5e circonscription)"
      , code = "OMC_PA721166"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "samantha.cazebonne@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Cazebonne"
      , nomComplet = "Samantha Cazebonne"
      , particule = Nothing
      , photo = "/img/photos/721166.jpg"
      , prenom = "Samantha"
      , sitesWeb = []
      , slug = "samantha-cazebonne"
      , titre = "Mme"
      }
    , { circonscription = "Gers (1re circonscription)"
      , code = "OMC_PA719472"
      , collaborateurs = [ "william-tissandier" ]
      , commission = "Commission des finances"
      , courriels = [ "jean-rene.cazeneuve@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Cazeneuve"
      , nomComplet = "Jean-René Cazeneuve"
      , particule = Nothing
      , photo = "/img/photos/719472.jpg"
      , prenom = "Jean-René"
      , sitesWeb = []
      , slug = "jean-rene-cazeneuve"
      , titre = "M."
      }
    , { circonscription = "Pyrénées-Orientales (4e circonscription)"
      , code = "OMC_PA720814"
      , collaborateurs =
            [ "nicolas-delmas"
            , "laurence-muguet"
            ]
      , commission = "Commission des affaires économiques"
      , courriels = [ "sebastien.cazenove@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Cazenove"
      , nomComplet = "Sébastien Cazenove"
      , particule = Nothing
      , photo = "/img/photos/720814.jpg"
      , prenom = "Sébastien"
      , sitesWeb = []
      , slug = "sebastien-cazenove"
      , titre = "M."
      }
    , { circonscription = "Gard (3e circonscription)"
      , code = "OMC_PA719440"
      , collaborateurs = [ "laurent-savary" ]
      , commission = "Commission des affaires économiques"
      , courriels = [ "anthony.cellier@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Cellier"
      , nomComplet = "Anthony Cellier"
      , particule = Nothing
      , photo = "/img/photos/719440.jpg"
      , prenom = "Anthony"
      , sitesWeb = []
      , slug = "anthony-cellier"
      , titre = "M."
      }
    , { circonscription = "Vaucluse (1re circonscription)"
      , code = "OMC_PA721860"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "jean-francois.cesarini@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Cesarini"
      , nomComplet = "Jean-François Cesarini"
      , particule = Nothing
      , photo = "/img/photos/721860.jpg"
      , prenom = "Jean-François"
      , sitesWeb = []
      , slug = "jean-francois-cesarini"
      , titre = "M."
      }
    , { circonscription = "Isère (3e circonscription)"
      , code = "OMC_PA719748"
      , collaborateurs = [ "salma-el-ayyadi" ]
      , commission = "Commission des lois"
      , courriels = [ "emilie.chalas@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Chalas"
      , nomComplet = "Émilie Chalas"
      , particule = Nothing
      , photo = "/img/photos/719748.jpg"
      , prenom = "Émilie"
      , sitesWeb = []
      , slug = "emilie-chalas"
      , titre = "Mme"
      }
    , { circonscription = "Indre-et-Loire (1re circonscription)"
      , code = "OMC_PA719790"
      , collaborateurs =
            [ "yann-boulay"
            , "arnaud-roy"
            ]
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "philippe.chalumeau@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Chalumeau"
      , nomComplet = "Philippe Chalumeau"
      , particule = Nothing
      , photo = "/img/photos/719790.jpg"
      , prenom = "Philippe"
      , sitesWeb = [ "http://www.philippechalumeau2017.fr" ]
      , slug = "philippe-chalumeau"
      , titre = "M."
      }
    , { circonscription = "Gard (4e circonscription)"
      , code = "OMC_PA719448"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "annie.chapelier@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Chapelier"
      , nomComplet = "Annie Chapelier"
      , particule = Nothing
      , photo = "/img/photos/719448.jpg"
      , prenom = "Annie"
      , sitesWeb = []
      , slug = "annie-chapelier"
      , titre = "Mme"
      }
    , { circonscription = "Seine-Saint-Denis (8e circonscription)"
      , code = "OMC_PA721218"
      , collaborateurs =
            [ "sam-dautrevaux"
            , "maxime-vesselinoff"
            , "barbara-stanisavljevic"
            ]
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "sylvie.charriere@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "La République en Marche"
      , nom = "Charrière"
      , nomComplet = "Sylvie Charrière"
      , particule = Nothing
      , photo = "/img/photos/721218.jpg"
      , prenom = "Sylvie"
      , sitesWeb = [ "http://sylviecharriere.fr" ]
      , slug = "sylvie-charriere"
      , titre = "Mme"
      }
    , { circonscription = "Doubs (1re circonscription)"
      , code = "OMC_PA719170"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "fannette.charvier@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Charvier"
      , nomComplet = "Fannette Charvier"
      , particule = Nothing
      , photo = "/img/photos/719170.jpg"
      , prenom = "Fannette"
      , sitesWeb = []
      , slug = "fannette-charvier"
      , titre = "Mme"
      }
    , { circonscription = "Puy-de-Dôme (5e circonscription)"
      , code = "OMC_PA267306"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "andre.chassaigne@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Gauche démocrate et républicaine"
      , nom = "Chassaigne"
      , nomComplet = "André Chassaigne"
      , particule = Nothing
      , photo = "/img/photos/267306.jpg"
      , prenom = "André"
      , sitesWeb = [ "http://www.andrechassaigne.org" ]
      , slug = "andre-chassaigne"
      , titre = "M."
      }
    , { circonscription = "Dordogne (1re circonscription)"
      , code = "OMC_PA719250"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "philippe.chassaing@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Chassaing"
      , nomComplet = "Philippe Chassaing"
      , particule = Nothing
      , photo = "/img/photos/719250.jpg"
      , prenom = "Philippe"
      , sitesWeb = []
      , slug = "philippe-chassaing"
      , titre = "M."
      }
    , { circonscription = "Nord (19e circonscription)"
      , code = "OMC_PA720468"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "sebastien.chenu@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Non inscrit"
      , nom = "Chenu"
      , nomComplet = "Sébastien Chenu"
      , particule = Nothing
      , photo = "/img/photos/720468.jpg"
      , prenom = "Sébastien"
      , sitesWeb = []
      , slug = "sebastien-chenu"
      , titre = "M."
      }
    , { circonscription = "Vosges (2e circonscription)"
      , code = "OMC_PA856"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "gerard.cherpion@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Cherpion"
      , nomComplet = "Gérard Cherpion"
      , particule = Nothing
      , photo = "/img/photos/856.jpg"
      , prenom = "Gérard"
      , sitesWeb = []
      , slug = "gerard-cherpion"
      , titre = "M."
      }
    , { circonscription = "Deux-Sèvres (1re circonscription)"
      , code = "OMC_PA722126"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "guillaume.chiche@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Chiche"
      , nomComplet = "Guillaume Chiche"
      , particule = Nothing
      , photo = "/img/photos/722126.jpg"
      , prenom = "Guillaume"
      , sitesWeb = []
      , slug = "guillaume-chiche"
      , titre = "M."
      }
    , { circonscription = "Nord (14e circonscription)"
      , code = "OMC_PA642868"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "paul.christophe@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Christophe"
      , nomComplet = "Paul Christophe"
      , particule = Nothing
      , photo = "/img/photos/642868.jpg"
      , prenom = "Paul"
      , sitesWeb = []
      , slug = "paul-christophe"
      , titre = "M."
      }
    , { circonscription = "Loire (4e circonscription)"
      , code = "OMC_PA267429"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "dino.cinieri@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Cinieri"
      , nomComplet = "Dino Cinieri"
      , particule = Nothing
      , photo = "/img/photos/267429.jpg"
      , prenom = "Dino"
      , sitesWeb = []
      , slug = "dino-cinieri"
      , titre = "M."
      }
    , { circonscription = "Alpes-Maritimes (1re circonscription)"
      , code = "OMC_PA330240"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "eric.ciotti@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Ciotti"
      , nomComplet = "Éric Ciotti"
      , particule = Nothing
      , photo = "/img/photos/330240.jpg"
      , prenom = "Éric"
      , sitesWeb = [ "http://www.eric-ciotti.com" ]
      , slug = "eric-ciotti"
      , titre = "M."
      }
    , { circonscription = "Saint-Pierre-et-Miquelon (1re circonscription)"
      , code = "OMC_PA702054"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "stephane.claireaux@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Claireaux"
      , nomComplet = "Stéphane Claireaux"
      , particule = Nothing
      , photo = "/img/photos/702054.jpg"
      , prenom = "Stéphane"
      , sitesWeb = [ "http://depute975.net" ]
      , slug = "stephane-claireaux"
      , titre = "M."
      }
    , { circonscription = "Drôme (1re circonscription)"
      , code = "OMC_PA719294"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "mireille.clapot@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "La République en Marche"
      , nom = "Clapot"
      , nomComplet = "Mireille Clapot"
      , particule = Nothing
      , photo = "/img/photos/719294.jpg"
      , prenom = "Mireille"
      , sitesWeb = [ "http://www.mireilleclapot.fr" ]
      , slug = "mireille-clapot"
      , titre = "Mme"
      }
    , { circonscription = "Ille-et-Vilaine (5e circonscription)"
      , code = "OMC_PA719756"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "christine.cloarec@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Cloarec"
      , nomComplet = "Christine Cloarec"
      , particule = Nothing
      , photo = "/img/photos/719756.jpg"
      , prenom = "Christine"
      , sitesWeb = []
      , slug = "christine-cloarec"
      , titre = "Mme"
      }
    , { circonscription = "Vienne (3e circonscription)"
      , code = "OMC_PA336439"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "jean-michel.clement@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Clément"
      , nomComplet = "Jean-Michel Clément"
      , particule = Nothing
      , photo = "/img/photos/336439.jpg"
      , prenom = "Jean-Michel"
      , sitesWeb = [ "http://www.jeanmichelclement.com/" ]
      , slug = "jean-michel-clement"
      , titre = "M."
      }
    , { circonscription = "Isère (2e circonscription)"
      , code = "OMC_PA719740"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "jean-charles.colas-roy@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Colas-Roy"
      , nomComplet = "Jean-Charles Colas-Roy"
      , particule = Nothing
      , photo = "/img/photos/719740.jpg"
      , prenom = "Jean-Charles"
      , sitesWeb = []
      , slug = "jean-charles-colas-roy"
      , titre = "M."
      }
    , { circonscription = "Indre-et-Loire (4e circonscription)"
      , code = "OMC_PA719814"
      , collaborateurs = [ "mathilde-ayral" ]
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "fabienne.colboc@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Colboc"
      , nomComplet = "Fabienne Colboc"
      , particule = Nothing
      , photo = "/img/photos/719814.jpg"
      , prenom = "Fabienne"
      , sitesWeb = []
      , slug = "fabienne-colboc"
      , titre = "Mme"
      }
    , { circonscription = "Gard (2e circonscription)"
      , code = "OMC_PA606212"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "gilbert.collard@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Non inscrit"
      , nom = "Collard"
      , nomComplet = "Gilbert Collard"
      , particule = Nothing
      , photo = "/img/photos/606212.jpg"
      , prenom = "Gilbert"
      , sitesWeb = []
      , slug = "gilbert-collard"
      , titre = "M."
      }
    , { circonscription = "Corse-du-Sud (2e circonscription)"
      , code = "OMC_PA719286"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "paul-andre.colombani@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Non inscrit"
      , nom = "Colombani"
      , nomComplet = "Paul-André Colombani"
      , particule = Nothing
      , photo = "/img/photos/719286.jpg"
      , prenom = "Paul-André"
      , sitesWeb = []
      , slug = "paul-andre-colombani"
      , titre = "M."
      }
    , { circonscription = "Seine-Saint-Denis (1re circonscription)"
      , code = "OMC_PA721202"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "eric.coquerel@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La France insoumise"
      , nom = "Coquerel"
      , nomComplet = "Éric Coquerel"
      , particule = Nothing
      , photo = "/img/photos/721202.jpg"
      , prenom = "Éric"
      , sitesWeb = [ "http://www.eric-coquerel.fr" ]
      , slug = "eric-coquerel"
      , titre = "M."
      }
    , { circonscription = "Seine-Saint-Denis (7e circonscription)"
      , code = "OMC_PA721210"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "alexis.corbiere@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La France insoumise"
      , nom = "Corbière"
      , nomComplet = "Alexis Corbière"
      , particule = Nothing
      , photo = "/img/photos/721210.jpg"
      , prenom = "Alexis"
      , sitesWeb = [ "http://www.alexis-corbiere.com" ]
      , slug = "alexis-corbiere"
      , titre = "M."
      }
    , { circonscription = "Ardennes (2e circonscription)"
      , code = "OMC_PA718850"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "pierre.cordier@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Cordier"
      , nomComplet = "Pierre Cordier"
      , particule = Nothing
      , photo = "/img/photos/718850.jpg"
      , prenom = "Pierre"
      , sitesWeb = []
      , slug = "pierre-cordier"
      , titre = "M."
      }
    , { circonscription = "Cher (1re circonscription)"
      , code = "OMC_PA719118"
      , collaborateurs = [ "frederic-sardin" ]
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "francois.cormier-bouligeon@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Cormier-Bouligeon"
      , nomComplet = "François Cormier-Bouligeon"
      , particule = Nothing
      , photo = "/img/photos/719118.jpg"
      , prenom = "François"
      , sitesWeb = []
      , slug = "francois-cormier-bouligeon"
      , titre = "M."
      }
    , { circonscription = "Saône-et-Loire (2e circonscription)"
      , code = "OMC_PA722390"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "josiane.corneloup@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Corneloup"
      , nomComplet = "Josiane Corneloup"
      , particule = Nothing
      , photo = "/img/photos/722390.jpg"
      , prenom = "Josiane"
      , sitesWeb = [ "http://josianecorneloup.fr" ]
      , slug = "josiane-corneloup"
      , titre = "Mme"
      }
    , { circonscription = "Haute-Marne (2e circonscription)"
      , code = "OMC_PA923"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "francois.cornut-gentille@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Cornut-Gentille"
      , nomComplet = "François Cornut-Gentille"
      , particule = Nothing
      , photo = "/img/photos/923.jpg"
      , prenom = "François"
      , sitesWeb = []
      , slug = "francois-cornut-gentille"
      , titre = "M."
      }
    , { circonscription = "Gironde (7e circonscription)"
      , code = "OMC_PA719624"
      , collaborateurs = [ "clement-pinte" ]
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "berangere.couillard@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Couillard"
      , nomComplet = "Bérangère Couillard"
      , particule = Nothing
      , photo = "/img/photos/719624.jpg"
      , prenom = "Bérangère"
      , sitesWeb = [ "http://berangerecouillard.en-marche.fr" ]
      , slug = "berangere-couillard"
      , titre = "Mme"
      }
    , { circonscription = "Marne (5e circonscription)"
      , code = "OMC_PA942"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "charles.decourson@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Courson"
      , nomComplet = "Charles de Courson"
      , particule = Just "de"
      , photo = "/img/photos/942.jpg"
      , prenom = "Charles"
      , sitesWeb = []
      , slug = "charles-de-courson"
      , titre = "M."
      }
    , { circonscription = "Côte-d'Or (4e circonscription)"
      , code = "OMC_PA719194"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "yolaine.decourson@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Courson"
      , nomComplet = "Yolaine de Courson"
      , particule = Just "de"
      , photo = "/img/photos/719194.jpg"
      , prenom = "Yolaine"
      , sitesWeb = []
      , slug = "yolaine-de-courson"
      , titre = "Mme"
      }
    , { circonscription = "Yonne (3e circonscription)"
      , code = "OMC_PA721808"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "michele.crouzet@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Crouzet"
      , nomComplet = "Michèle Crouzet"
      , particule = Nothing
      , photo = "/img/photos/721808.jpg"
      , prenom = "Michèle"
      , sitesWeb = []
      , slug = "michele-crouzet"
      , titre = "Mme"
      }
    , { circonscription = "Dordogne (3e circonscription)"
      , code = "OMC_PA719266"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "jean-pierre.cubertafon@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Cubertafon"
      , nomComplet = "Jean-Pierre Cubertafon"
      , particule = Nothing
      , photo = "/img/photos/719266.jpg"
      , prenom = "Jean-Pierre"
      , sitesWeb = []
      , slug = "jean-pierre-cubertafon"
      , titre = "M."
      }
    , { circonscription = "Val-d'Oise (7e circonscription)"
      , code = "OMC_PA720932"
      , collaborateurs = [ "pauline-parisot" ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "dominique.dasilva@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Da Silva"
      , nomComplet = "Dominique Da Silva"
      , particule = Nothing
      , photo = "/img/photos/720932.jpg"
      , prenom = "Dominique"
      , sitesWeb = []
      , slug = "dominique-da-silva"
      , titre = "M."
      }
    , { circonscription = "Jura (2e circonscription)"
      , code = "OMC_PA332523"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "marie-christine.dalloz@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "Les Républicains"
      , nom = "Dalloz"
      , nomComplet = "Marie-Christine Dalloz"
      , particule = Nothing
      , photo = "/img/photos/332523.jpg"
      , prenom = "Marie-Christine"
      , sitesWeb = [ "http://www.mcdalloz.fr/" ]
      , slug = "marie-christine-dalloz"
      , titre = "Mme"
      }
    , { circonscription = "Lot-et-Garonne (3e circonscription)"
      , code = "OMC_PA720268"
      , collaborateurs = [ "stephane-mazeaufroid-boulestin" ]
      , commission = "Commission des finances"
      , courriels = [ "olivier.damaisin@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Damaisin"
      , nomComplet = "Olivier Damaisin"
      , particule = Nothing
      , photo = "/img/photos/720268.jpg"
      , prenom = "Olivier"
      , sitesWeb = []
      , slug = "olivier-damaisin"
      , titre = "M."
      }
    , { circonscription = "Loire-Atlantique (6e circonscription)"
      , code = "OMC_PA607155"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "yves.daniel@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Daniel"
      , nomComplet = "Yves Daniel"
      , particule = Nothing
      , photo = "/img/photos/607155.jpg"
      , prenom = "Yves"
      , sitesWeb = [ "http://www.yves-daniel.org" ]
      , slug = "yves-daniel"
      , titre = "M."
      }
    , { circonscription = "Oise (1re circonscription)"
      , code = "OMC_PA998"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "olivier.dassault@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Dassault"
      , nomComplet = "Olivier Dassault"
      , particule = Nothing
      , photo = "/img/photos/998.jpg"
      , prenom = "Olivier"
      , sitesWeb = [ "http://www.olivierdassault.fr" ]
      , slug = "olivier-dassault"
      , titre = "M."
      }
    , { circonscription = "Gironde (4e circonscription)"
      , code = "OMC_PA1008"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "alain.david@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Nouvelle Gauche"
      , nom = "David"
      , nomComplet = "Alain David"
      , particule = Nothing
      , photo = "/img/photos/1008.jpg"
      , prenom = "Alain"
      , sitesWeb = []
      , slug = "alain-david"
      , titre = "M."
      }
    , { circonscription = "Gironde (1re circonscription)"
      , code = "OMC_PA719710"
      , collaborateurs = [ "benoit-rudinger" ]
      , commission = "Commission des finances"
      , courriels = [ "dominique.david@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "David"
      , nomComplet = "Dominique David"
      , particule = Nothing
      , photo = "/img/photos/719710.jpg"
      , prenom = "Dominique"
      , sitesWeb = [ "http://dominique-david.fr" ]
      , slug = "dominique-david"
      , titre = "Mme"
      }
    , { circonscription = "Nord (15e circonscription)"
      , code = "OMC_PA720538"
      , collaborateurs =
            [ "remi-loison"
            , "sabrina-blondel"
            ]
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "jennifer.detemmerman@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "La République en Marche"
      , nom = "De Temmerman"
      , nomComplet = "Jennifer De Temmerman"
      , particule = Nothing
      , photo = "/img/photos/720538.jpg"
      , prenom = "Jennifer"
      , sitesWeb = []
      , slug = "jennifer-de-temmerman"
      , titre = "Mme"
      }
    , { circonscription = "Bouches-du-Rhône (9e circonscription)"
      , code = "OMC_PA1029"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "bernard.deflesselles@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Deflesselles"
      , nomComplet = "Bernard Deflesselles"
      , particule = Nothing
      , photo = "/img/photos/1029.jpg"
      , prenom = "Bernard"
      , sitesWeb = [ "http://www.bernarddeflesselles.com" ]
      , slug = "bernard-deflesselles"
      , titre = "M."
      }
    , { circonscription = "Savoie (1re circonscription)"
      , code = "OMC_PA721398"
      , collaborateurs = [ "david-petit" ]
      , commission = "Commission des lois"
      , courriels = [ "typhanie.degois@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Degois"
      , nomComplet = "Typhanie Degois"
      , particule = Nothing
      , photo = "/img/photos/721398.jpg"
      , prenom = "Typhanie"
      , sitesWeb = [ "http://www.typhaniedegois.fr" ]
      , slug = "typhanie-degois"
      , titre = "Mme"
      }
    , { circonscription = "Aisne (4e circonscription)"
      , code = "OMC_PA718694"
      , collaborateurs = [ "karim-chafi" ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "marc.delatte@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Delatte"
      , nomComplet = "Marc Delatte"
      , particule = Nothing
      , photo = "/img/photos/718694.jpg"
      , prenom = "Marc"
      , sitesWeb = []
      , slug = "marc-delatte"
      , titre = "M."
      }
    , { circonscription = "Côte-d'Or (2e circonscription)"
      , code = "OMC_PA267450"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "remi.delatte@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Delatte"
      , nomComplet = "Rémi Delatte"
      , particule = Nothing
      , photo = "/img/photos/267450.jpg"
      , prenom = "Rémi"
      , sitesWeb = []
      , slug = "remi-delatte"
      , titre = "M."
      }
    , { circonscription = "Dordogne (2e circonscription)"
      , code = "OMC_PA719258"
      , collaborateurs = [ "isabelle-verot-besnault" ]
      , commission = "Commission des affaires économiques"
      , courriels = [ "michel.delpon@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Delpon"
      , nomComplet = "Michel Delpon"
      , particule = Nothing
      , photo = "/img/photos/719258.jpg"
      , prenom = "Michel"
      , sitesWeb = []
      , slug = "michel-delpon"
      , titre = "M."
      }
    , { circonscription = "Somme (5e circonscription)"
      , code = "OMC_PA267241"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "stephane.demilly@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Demilly"
      , nomComplet = "Stéphane Demilly"
      , particule = Nothing
      , photo = "/img/photos/267241.jpg"
      , prenom = "Stéphane"
      , sitesWeb = [ "http://www.stephane-demilly.org" ]
      , slug = "stephane-demilly"
      , titre = "M."
      }
    , { circonscription = "Pas-de-Calais (9e circonscription)"
      , code = "OMC_PA720598"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "marguerite.deprez-audebert@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Deprez-Audebert"
      , nomComplet = "Marguerite Deprez-Audebert"
      , particule = Nothing
      , photo = "/img/photos/720598.jpg"
      , prenom = "Marguerite"
      , sitesWeb = []
      , slug = "marguerite-deprez-audebert"
      , titre = "Mme"
      }
    , { circonscription = "Nord (21e circonscription)"
      , code = "OMC_PA720696"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "beatrice.descamps@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Descamps"
      , nomComplet = "Béatrice Descamps"
      , particule = Nothing
      , photo = "/img/photos/720696.jpg"
      , prenom = "Béatrice"
      , sitesWeb = []
      , slug = "beatrice-descamps"
      , titre = "Mme"
      }
    , { circonscription = "Cantal (1re circonscription)"
      , code = "OMC_PA330909"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "vincent.descoeur@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Descoeur"
      , nomComplet = "Vincent Descoeur"
      , particule = Nothing
      , photo = "/img/photos/330909.jpg"
      , prenom = "Vincent"
      , sitesWeb = [ "http://www.descoeur.com" ]
      , slug = "vincent-descoeur"
      , titre = "M."
      }
    , { circonscription = "Val-de-Marne (1re circonscription)"
      , code = "OMC_PA720862"
      , collaborateurs =
            [ "emmanuel-chaumery"
            , "mehmet-ceylan"
            , "khadija-chatouani"
            ]
      , commission = "Commission des affaires étrangères"
      , courriels = [ "frederic.descrozaille@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Descrozaille"
      , nomComplet = "Frédéric Descrozaille"
      , particule = Nothing
      , photo = "/img/photos/720862.jpg"
      , prenom = "Frédéric"
      , sitesWeb = [ "http://descrozaille.fr/" ]
      , slug = "frederic-descrozaille"
      , titre = "M."
      }
    , { circonscription = "Bouches-du-Rhône (13e circonscription)"
      , code = "OMC_PA718926"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "pierre.dharreville@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Gauche démocrate et républicaine"
      , nom = "Dharréville"
      , nomComplet = "Pierre Dharréville"
      , particule = Nothing
      , photo = "/img/photos/718926.jpg"
      , prenom = "Pierre"
      , sitesWeb = []
      , slug = "pierre-dharreville"
      , titre = "M."
      }
    , { circonscription = "Moselle (4e circonscription)"
      , code = "OMC_PA720386"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "fabien.difilippo@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Di Filippo"
      , nomComplet = "Fabien Di Filippo"
      , particule = Nothing
      , photo = "/img/photos/720386.jpg"
      , prenom = "Fabien"
      , sitesWeb = []
      , slug = "fabien-di-filippo"
      , titre = "M."
      }
    , { circonscription = "Nord (3e circonscription)"
      , code = "OMC_PA720438"
      , collaborateurs = [ "layla-imoula" ]
      , commission = "Commission des affaires étrangères"
      , courriels = [ "christophe.dipompeo@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Di Pompeo"
      , nomComplet = "Christophe Di Pompeo"
      , particule = Nothing
      , photo = "/img/photos/720438.jpg"
      , prenom = "Christophe"
      , sitesWeb = []
      , slug = "christophe-di-pompeo"
      , titre = "M."
      }
    , { circonscription = "Bouches-du-Rhône (12e circonscription)"
      , code = "OMC_PA267440"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "eric.diard@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Diard"
      , nomComplet = "Éric Diard"
      , particule = Nothing
      , photo = "/img/photos/267440.jpg"
      , prenom = "Éric"
      , sitesWeb = [ "http://www.ericdiard.fr" ]
      , slug = "eric-diard"
      , titre = "M."
      }
    , { circonscription = "Saône-et-Loire (1re circonscription)"
      , code = "OMC_PA722382"
      , collaborateurs = [ "maxim-plat" ]
      , commission = "Commission des affaires étrangères"
      , courriels = [ "benjamin.dirx@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Dirx"
      , nomComplet = "Benjamin Dirx"
      , particule = Nothing
      , photo = "/img/photos/722382.jpg"
      , prenom = "Benjamin"
      , sitesWeb = []
      , slug = "benjamin-dirx"
      , titre = "M."
      }
    , { circonscription = "Aisne (2e circonscription)"
      , code = "OMC_PA712015"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "julien.dive@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Dive"
      , nomComplet = "Julien Dive"
      , particule = Nothing
      , photo = "/img/photos/712015.jpg"
      , prenom = "Julien"
      , sitesWeb = []
      , slug = "julien-dive"
      , titre = "M."
      }
    , { circonscription = "Haute-Vienne (2e circonscription)"
      , code = "OMC_PA722236"
      , collaborateurs = [ "alexia-soucaze" ]
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "jean-baptiste.djebbari@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Djebbari"
      , nomComplet = "Jean-Baptiste Djebbari"
      , particule = Nothing
      , photo = "/img/photos/722236.jpg"
      , prenom = "Jean-Baptiste"
      , sitesWeb = []
      , slug = "jean-baptiste-djebbari"
      , titre = "M."
      }
    , { circonscription = "Seine-et-Marne (10e circonscription)"
      , code = "OMC_PA721718"
      , collaborateurs =
            [ "barbara-chancrin"
            , "alizata-diallo"
            ]
      , commission = "Commission des affaires économiques"
      , courriels = [ "stephanie.do@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Do"
      , nomComplet = "Stéphanie Do"
      , particule = Nothing
      , photo = "/img/photos/721718.jpg"
      , prenom = "Stéphanie"
      , sitesWeb = [ "http://stephaniedo.en-marche.fr" ]
      , slug = "stephanie-do"
      , titre = "Mme"
      }
    , { circonscription = "Alpes-Maritimes (2e circonscription)"
      , code = "OMC_PA718894"
      , collaborateurs = [ "aline-hubert" ]
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "loic.dombreval@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Dombreval"
      , nomComplet = "Loïc Dombreval"
      , particule = Nothing
      , photo = "/img/photos/718894.jpg"
      , prenom = "Loïc"
      , sitesWeb = []
      , slug = "loic-dombreval"
      , titre = "M."
      }
    , { circonscription = "Loiret (4e circonscription)"
      , code = "OMC_PA267289"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "jean-pierre.door@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "Les Républicains"
      , nom = "Door"
      , nomComplet = "Jean-Pierre Door"
      , particule = Nothing
      , photo = "/img/photos/267289.jpg"
      , prenom = "Jean-Pierre"
      , sitesWeb = []
      , slug = "jean-pierre-door"
      , titre = "M."
      }
    , { circonscription = "Hautes-Pyrénées (2e circonscription)"
      , code = "OMC_PA608292"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "jeanine.dubie@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Non inscrit"
      , nom = "Dubié"
      , nomComplet = "Jeanine Dubié"
      , particule = Nothing
      , photo = "/img/photos/608292.jpg"
      , prenom = "Jeanine"
      , sitesWeb = [ "http://www.jeaninedubie.fr" ]
      , slug = "jeanine-dubie"
      , titre = "Mme"
      }
    , { circonscription = "Dordogne (4e circonscription)"
      , code = "OMC_PA719162"
      , collaborateurs = [ "jacqueline-ghirardelli" ]
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "jacqueline.dubois@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Dubois"
      , nomComplet = "Jacqueline Dubois"
      , particule = Nothing
      , photo = "/img/photos/719162.jpg"
      , prenom = "Jacqueline"
      , sitesWeb = []
      , slug = "jacqueline-dubois"
      , titre = "Mme"
      }
    , { circonscription = "Loiret (5e circonscription)"
      , code = "OMC_PA342935"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "marianne.dubois@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "Les Républicains"
      , nom = "Dubois"
      , nomComplet = "Marianne Dubois"
      , particule = Nothing
      , photo = "/img/photos/342935.jpg"
      , prenom = "Marianne"
      , sitesWeb = [ "http://www.marianne-dubois.fr/" ]
      , slug = "marianne-dubois"
      , titre = "Mme"
      }
    , { circonscription = "Gironde (12e circonscription)"
      , code = "OMC_PA719660"
      , collaborateurs = [ "xavier-giraud" ]
      , commission = "Commission des affaires économiques"
      , courriels = [ "christelle.dubos@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "La République en Marche"
      , nom = "Dubos"
      , nomComplet = "Christelle Dubos"
      , particule = Nothing
      , photo = "/img/photos/719660.jpg"
      , prenom = "Christelle"
      , sitesWeb = [ "http://christelledubos.fr" ]
      , slug = "christelle-dubos"
      , titre = "Mme"
      }
    , { circonscription = "Hérault (3e circonscription)"
      , code = "OMC_PA719684"
      , collaborateurs =
            [ "lionel-bianquis"
            , "philippe-cheron"
            ]
      , commission = "Commission des lois"
      , courriels = [ "coralie.dubost@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Dubost"
      , nomComplet = "Coralie Dubost"
      , particule = Nothing
      , photo = "/img/photos/719684.jpg"
      , prenom = "Coralie"
      , sitesWeb = []
      , slug = "coralie-dubost"
      , titre = "Mme"
      }
    , { circonscription = "Maine-et-Loire (6e circonscription)"
      , code = "OMC_PA720154"
      , collaborateurs = [ "romain-variot" ]
      , commission = "Commission des lois"
      , courriels = [ "nicole.dubre-chirat@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Dubre-Chirat"
      , nomComplet = "Nicole Dubre-Chirat"
      , particule = Nothing
      , photo = "/img/photos/720154.jpg"
      , prenom = "Nicole"
      , sitesWeb = []
      , slug = "nicole-dubre-chirat"
      , titre = "Mme"
      }
    , { circonscription = "Haute-Savoie (4e circonscription)"
      , code = "OMC_PA608826"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "virginie.duby-muller@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Duby-Muller"
      , nomComplet = "Virginie Duby-Muller"
      , particule = Nothing
      , photo = "/img/photos/608826.jpg"
      , prenom = "Virginie"
      , sitesWeb = []
      , slug = "virginie-duby-muller"
      , titre = "Mme"
      }
    , { circonscription = "Loire-Atlantique (8e circonscription)"
      , code = "OMC_PA720046"
      , collaborateurs = [ "william-benaissa" ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "audrey.dufeuschubert@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Dufeu Schubert"
      , nomComplet = "Audrey Dufeu Schubert"
      , particule = Nothing
      , photo = "/img/photos/720046.jpg"
      , prenom = "Audrey"
      , sitesWeb = []
      , slug = "audrey-dufeu-schubert"
      , titre = "Mme"
      }
    , { circonscription = "Allier (1re circonscription)"
      , code = "OMC_PA718720"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "jean-paul.dufregne@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Gauche démocrate et républicaine"
      , nom = "Dufrègne"
      , nomComplet = "Jean-Paul Dufrègne"
      , particule = Nothing
      , photo = "/img/photos/718720.jpg"
      , prenom = "Jean-Paul"
      , sitesWeb = [ "http://www.dufregne2017.fr" ]
      , slug = "jean-paul-dufregne"
      , titre = "M."
      }
    , { circonscription = "Gard (1re circonscription)"
      , code = "OMC_PA606202"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "francoise.dumas@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "La République en Marche"
      , nom = "Dumas"
      , nomComplet = "Françoise Dumas"
      , particule = Nothing
      , photo = "/img/photos/606202.jpg"
      , prenom = "Françoise"
      , sitesWeb = []
      , slug = "francoise-dumas"
      , titre = "Mme"
      }
    , { circonscription = "Hauts-de-Seine (13e circonscription)"
      , code = "OMC_PA721194"
      , collaborateurs =
            [ "mickael-burlot"
            , "etienne-lesoeur"
            ]
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "frederique.dumas@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "La République en Marche"
      , nom = "Dumas"
      , nomComplet = "Frédérique Dumas"
      , particule = Nothing
      , photo = "/img/photos/721194.jpg"
      , prenom = "Frédérique"
      , sitesWeb = [ "http://www.frederiquedumas2017.fr" ]
      , slug = "frederique-dumas"
      , titre = "Mme"
      }
    , { circonscription = "Calvados (2e circonscription)"
      , code = "OMC_PA1198"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "laurence.dumont@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "Nouvelle Gauche"
      , nom = "Dumont"
      , nomComplet = "Laurence Dumont"
      , particule = Nothing
      , photo = "/img/photos/1198.jpg"
      , prenom = "Laurence"
      , sitesWeb = []
      , slug = "laurence-dumont"
      , titre = "Mme"
      }
    , { circonscription = "Pas-de-Calais (7e circonscription)"
      , code = "OMC_PA720684"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "pierre-henri.dumont@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Dumont"
      , nomComplet = "Pierre-Henri Dumont"
      , particule = Nothing
      , photo = "/img/photos/720684.jpg"
      , prenom = "Pierre-Henri"
      , sitesWeb = [ "http://phdumont.fr" ]
      , slug = "pierre-henri-dumont"
      , titre = "M."
      }
    , { circonscription = "Nouvelle-Calédonie (1re circonscription)"
      , code = "OMC_PA721094"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "philippe.dunoyer@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Dunoyer"
      , nomComplet = "Philippe Dunoyer"
      , particule = Nothing
      , photo = "/img/photos/721094.jpg"
      , prenom = "Philippe"
      , sitesWeb = []
      , slug = "philippe-dunoyer"
      , titre = "M."
      }
    , { circonscription = "Maine-et-Loire (2e circonscription)"
      , code = "OMC_PA643175"
      , collaborateurs = [ "sebastien-boussion" ]
      , commission = "Commission des finances"
      , courriels = [ "stella.dupont@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Dupont"
      , nomComplet = "Stella Dupont"
      , particule = Nothing
      , photo = "/img/photos/643175.jpg"
      , prenom = "Stella"
      , sitesWeb = []
      , slug = "stella-dupont"
      , titre = "Mme"
      }
    , { circonscription = "Essonne (8e circonscription)"
      , code = "OMC_PA1206"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "nicolas.dupont-aignan@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Non inscrit"
      , nom = "Dupont-Aignan"
      , nomComplet = "Nicolas Dupont-Aignan"
      , particule = Nothing
      , photo = "/img/photos/1206.jpg"
      , prenom = "Nicolas"
      , sitesWeb = []
      , slug = "nicolas-dupont-aignan"
      , titre = "M."
      }
    , { circonscription = "Ardèche (2e circonscription)"
      , code = "OMC_PA330357"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "olivier.dussopt@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Nouvelle Gauche"
      , nom = "Dussopt"
      , nomComplet = "Olivier Dussopt"
      , particule = Nothing
      , photo = "/img/photos/330357.jpg"
      , prenom = "Olivier"
      , sitesWeb = [ "http://www.olivierdussopt.fr" ]
      , slug = "olivier-dussopt"
      , titre = "M."
      }
    , { circonscription = "Pas-de-Calais (1re circonscription)"
      , code = "OMC_PA720652"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "bruno.duverge@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Duvergé"
      , nomComplet = "Bruno Duvergé"
      , particule = Nothing
      , photo = "/img/photos/720652.jpg"
      , prenom = "Bruno"
      , sitesWeb = []
      , slug = "bruno-duverge"
      , titre = "M."
      }
    , { circonscription = "Hérault (8e circonscription)"
      , code = "OMC_PA719830"
      , collaborateurs =
            [ "arnaud-knobloch"
            , "olivier-boudet"
            ]
      , commission = "Commission des affaires économiques"
      , courriels = [ "nicolas.demoulin@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Démoulin"
      , nomComplet = "Nicolas Démoulin"
      , particule = Nothing
      , photo = "/img/photos/719830.jpg"
      , prenom = "Nicolas"
      , sitesWeb = [ "http://nicolasdemoulin.fr" ]
      , slug = "nicolas-demoulin"
      , titre = "M."
      }
    , { circonscription = "Français établis hors de France (9e circonscription)"
      , code = "OMC_PA720996"
      , collaborateurs = [ "elsa-carneiro" ]
      , commission = "Commission des finances"
      , courriels = [ "mjid.elguerrab@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "El Guerrab"
      , nomComplet = "M'jid El Guerrab"
      , particule = Nothing
      , photo = "/img/photos/720996.jpg"
      , prenom = "M'jid"
      , sitesWeb = [ "http://mjidelguerrab.fr" ]
      , slug = "m-jid-el-guerrab"
      , titre = "M."
      }
    , { circonscription = "Loire-Atlantique (5e circonscription)"
      , code = "OMC_PA720002"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "sarah.elhairy@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "El Haïry"
      , nomComplet = "Sarah El Haïry"
      , particule = Nothing
      , photo = "/img/photos/720002.jpg"
      , prenom = "Sarah"
      , sitesWeb = [ "http://www.elhairy.fr" ]
      , slug = "sarah-el-hairy"
      , titre = "Mme"
      }
    , { circonscription = "Hérault (4e circonscription)"
      , code = "OMC_PA719692"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "jean-francois.eliaou@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Eliaou"
      , nomComplet = "Jean-François Eliaou"
      , particule = Nothing
      , photo = "/img/photos/719692.jpg"
      , prenom = "Jean-François"
      , sitesWeb = []
      , slug = "jean-francois-eliaou"
      , titre = "M."
      }
    , { circonscription = "Val-d'Oise (6e circonscription)"
      , code = "OMC_PA720924"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "nathalie.elimas@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Elimas"
      , nomComplet = "Nathalie Elimas"
      , particule = Nothing
      , photo = "/img/photos/720924.jpg"
      , prenom = "Nathalie"
      , sitesWeb = []
      , slug = "nathalie-elimas"
      , titre = "Mme"
      }
    , { circonscription = "Loire-Atlantique (10e circonscription)"
      , code = "OMC_PA607193"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "sophie.errante@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "La République en Marche"
      , nom = "Errante"
      , nomComplet = "Sophie Errante"
      , particule = Nothing
      , photo = "/img/photos/607193.jpg"
      , prenom = "Sophie"
      , sitesWeb = []
      , slug = "sophie-errante"
      , titre = "Mme"
      }
    , { circonscription = "Cher (2e circonscription)"
      , code = "OMC_PA719044"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "nadia.essayan@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Essayan"
      , nomComplet = "Nadia Essayan"
      , particule = Nothing
      , photo = "/img/photos/719044.jpg"
      , prenom = "Nadia"
      , sitesWeb = []
      , slug = "nadia-essayan"
      , titre = "Mme"
      }
    , { circonscription = "Hérault (7e circonscription)"
      , code = "OMC_PA719616"
      , collaborateurs = [ "thibaut-coussens" ]
      , commission = "Commission des lois"
      , courriels = [ "christophe.euzet@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Euzet"
      , nomComplet = "Christophe Euzet"
      , particule = Nothing
      , photo = "/img/photos/719616.jpg"
      , prenom = "Christophe"
      , sitesWeb = []
      , slug = "christophe-euzet"
      , titre = "M."
      }
    , { circonscription = "Pas-de-Calais (3e circonscription)"
      , code = "OMC_PA720664"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "jose.evrard@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Non inscrit"
      , nom = "Evrard"
      , nomComplet = "José Evrard"
      , particule = Nothing
      , photo = "/img/photos/720664.jpg"
      , prenom = "José"
      , sitesWeb = []
      , slug = "jose-evrard"
      , titre = "M."
      }
    , { circonscription = "Gironde (2e circonscription)"
      , code = "OMC_PA719570"
      , collaborateurs = [ "margaux-lyprendi" ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "catherine.fabre@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Fabre"
      , nomComplet = "Catherine Fabre"
      , particule = Nothing
      , photo = "/img/photos/719570.jpg"
      , prenom = "Catherine"
      , sitesWeb = [ "http://catherinefabre.fr" ]
      , slug = "catherine-fabre"
      , titre = "Mme"
      }
    , { circonscription = "Paris (5e circonscription)"
      , code = "OMC_PA721564"
      , collaborateurs = [ "constantin-de-salvatore" ]
      , commission = "Commission des lois"
      , courriels = [ "Elise.Fajgeles@assemblee-nationale.fr " ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Fajgeles"
      , nomComplet = "Elise Fajgeles"
      , particule = Nothing
      , photo = "/img/photos/721564.jpg"
      , prenom = "Elise"
      , sitesWeb = []
      , slug = "elise-fajgeles"
      , titre = "Mme"
      }
    , { circonscription = "Charente-Maritime (1re circonscription)"
      , code = "OMC_PA605694"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "olivier.falorni@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Non inscrit"
      , nom = "Falorni"
      , nomComplet = "Olivier Falorni"
      , particule = Nothing
      , photo = "/img/photos/605694.jpg"
      , prenom = "Olivier"
      , sitesWeb = [ "http://www.olivierfalorni.com" ]
      , slug = "olivier-falorni"
      , titre = "M."
      }
    , { circonscription = "Puy-de-Dôme (4e circonscription)"
      , code = "OMC_PA1276"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "michel.fanget@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Fanget"
      , nomComplet = "Michel Fanget"
      , particule = Nothing
      , photo = "/img/photos/1276.jpg"
      , prenom = "Michel"
      , sitesWeb = []
      , slug = "michel-fanget"
      , titre = "M."
      }
    , { circonscription = "Pas-de-Calais (4e circonscription)"
      , code = "OMC_PA334149"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "daniel.fasquelle@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "Les Républicains"
      , nom = "Fasquelle"
      , nomComplet = "Daniel Fasquelle"
      , particule = Nothing
      , photo = "/img/photos/334149.jpg"
      , prenom = "Daniel"
      , sitesWeb = []
      , slug = "daniel-fasquelle"
      , titre = "M."
      }
    , { circonscription = "Hauts-de-Seine (1re circonscription)"
      , code = "OMC_PA721896"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "elsa.faucillon@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Gauche démocrate et républicaine"
      , nom = "Faucillon"
      , nomComplet = "Elsa Faucillon"
      , particule = Nothing
      , photo = "/img/photos/721896.jpg"
      , prenom = "Elsa"
      , sitesWeb = [ "http://www.elsa-faucillon.org" ]
      , slug = "elsa-faucillon"
      , titre = "Mme"
      }
    ]


deputes2 : Deputes
deputes2 =
    [ { circonscription = "Seine-et-Marne (11e circonscription)"
      , code = "OMC_PA609332"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "ofaure@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Nouvelle Gauche"
      , nom = "Faure"
      , nomComplet = "Olivier Faure"
      , particule = Nothing
      , photo = "/img/photos/609332.jpg"
      , prenom = "Olivier"
      , sitesWeb = [ "http://www.olivierfaure.fr" ]
      , slug = "olivier-faure"
      , titre = "M."
      }
    , { circonscription = "Loire (3e circonscription)"
      , code = "OMC_PA719960"
      , collaborateurs =
            [ "sarah-jalade"
            , "julien-ranc"
            ]
      , commission = "Commission des affaires économiques"
      , courriels = [ "valeria.faure-muntian@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Faure-Muntian"
      , nomComplet = "Valéria Faure-Muntian"
      , particule = Nothing
      , photo = "/img/photos/719960.jpg"
      , prenom = "Valéria"
      , sitesWeb = [ "http://www.valeriafm.fr" ]
      , slug = "valeria-faure-muntian"
      , titre = "Mme"
      }
    , { circonscription = "Seine-et-Marne (8e circonscription)"
      , code = "OMC_PA721702"
      , collaborateurs = [ "antoine-coutant" ]
      , commission = "Commission des lois"
      , courriels = [ "jean-michel.fauvergue@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Fauvergue"
      , nomComplet = "Jean-Michel Fauvergue"
      , particule = Nothing
      , photo = "/img/photos/721702.jpg"
      , prenom = "Jean-Michel"
      , sitesWeb = []
      , slug = "jean-michel-fauvergue"
      , titre = "M."
      }
    , { circonscription = "Mayenne (3e circonscription)"
      , code = "OMC_PA267042"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "yannick.favennec@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Favennec Becot"
      , nomComplet = "Yannick Favennec Becot"
      , particule = Nothing
      , photo = "/img/photos/267042.jpg"
      , prenom = "Yannick"
      , sitesWeb = [ "http://www.favennecactualite.com" ]
      , slug = "yannick-favennec-becot"
      , titre = "M."
      }
    , { circonscription = "Finistère (6e circonscription)"
      , code = "OMC_PA606171"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "richard.ferrand@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Ferrand"
      , nomComplet = "Richard Ferrand"
      , particule = Nothing
      , photo = "/img/photos/606171.jpg"
      , prenom = "Richard"
      , sitesWeb = [ "http://www.richardferrand.fr" ]
      , slug = "richard-ferrand"
      , titre = "M."
      }
    , { circonscription = "Corse-du-Sud (1re circonscription)"
      , code = "OMC_PA643103"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "jean-jacques.ferrara@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Ferrara"
      , nomComplet = "Jean-Jacques Ferrara"
      , particule = Nothing
      , photo = "/img/photos/643103.jpg"
      , prenom = "Jean-Jacques"
      , sitesWeb = []
      , slug = "jean-jacques-ferrara"
      , titre = "M."
      }
    , { circonscription = "Loir-et-Cher (1re circonscription)"
      , code = "OMC_PA719938"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "marc.fesneau@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Fesneau"
      , nomComplet = "Marc Fesneau"
      , particule = Nothing
      , photo = "/img/photos/719938.jpg"
      , prenom = "Marc"
      , sitesWeb = []
      , slug = "marc-fesneau"
      , titre = "M."
      }
    , { circonscription = "Meurthe-et-Moselle (6e circonscription)"
      , code = "OMC_PA720286"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "caroline.fiat@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La France insoumise"
      , nom = "Fiat"
      , nomComplet = "Caroline Fiat"
      , particule = Nothing
      , photo = "/img/photos/720286.jpg"
      , prenom = "Caroline"
      , sitesWeb = []
      , slug = "caroline-fiat"
      , titre = "Mme"
      }
    , { circonscription = "Seine-Maritime (7e circonscription)"
      , code = "OMC_PA267780"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "agnes.firminlebodo@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Firmin Le Bodo"
      , nomComplet = "Agnès Firmin Le Bodo"
      , particule = Nothing
      , photo = "/img/photos/267780.jpg"
      , prenom = "Agnès"
      , sitesWeb = []
      , slug = "agnes-firmin-le-bodo"
      , titre = "Mme"
      }
    , { circonscription = "Deux-Sèvres (3e circonscription)"
      , code = "OMC_PA722134"
      , collaborateurs = [ "benjamin-sayag" ]
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "jean-marie.fievet@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Fiévet"
      , nomComplet = "Jean-Marie Fiévet"
      , particule = Nothing
      , photo = "/img/photos/722134.jpg"
      , prenom = "Jean-Marie"
      , sitesWeb = []
      , slug = "jean-marie-fievet"
      , titre = "M."
      }
    , { circonscription = "Hauts-de-Seine (4e circonscription)"
      , code = "OMC_PA721742"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "isabelle.florennes@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Florennes"
      , nomComplet = "Isabelle Florennes"
      , particule = Nothing
      , photo = "/img/photos/721742.jpg"
      , prenom = "Isabelle"
      , sitesWeb = []
      , slug = "isabelle-florennes"
      , titre = "Mme"
      }
    , { circonscription = "Tarn (1re circonscription)"
      , code = "OMC_PA267673"
      , collaborateurs = [ "jeremy-haddad" ]
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "philippe.folliot@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Folliot"
      , nomComplet = "Philippe Folliot"
      , particule = Nothing
      , photo = "/img/photos/267673.jpg"
      , prenom = "Philippe"
      , sitesWeb = [ "http://www.philippe-folliot.fr" ]
      , slug = "philippe-folliot"
      , titre = "M."
      }
    , { circonscription = "Alpes-de-Haute-Provence (2e circonscription)"
      , code = "OMC_PA718706"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "Emmanuelle.Fontaine-Domeizel@assemblee-nationale.fr " ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Fontaine-Domeizel"
      , nomComplet = "Emmanuelle Fontaine-Domeizel"
      , particule = Nothing
      , photo = "/img/photos/718706.jpg"
      , prenom = "Emmanuelle"
      , sitesWeb = []
      , slug = "emmanuelle-fontaine-domeizel"
      , titre = "Mme"
      }
    , { circonscription = "Sarthe (3e circonscription)"
      , code = "OMC_PA721384"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "pascale.fontenel-personne@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Fontenel-Personne"
      , nomComplet = "Pascale Fontenel-Personne"
      , particule = Nothing
      , photo = "/img/photos/721384.jpg"
      , prenom = "Pascale"
      , sitesWeb = []
      , slug = "pascale-fontenel-personne"
      , titre = "Mme"
      }
    , { circonscription = "Indre (2e circonscription)"
      , code = "OMC_PA1327"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "nicolas.forissier@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Forissier"
      , nomComplet = "Nicolas Forissier"
      , particule = Nothing
      , photo = "/img/photos/1327.jpg"
      , prenom = "Nicolas"
      , sitesWeb = []
      , slug = "nicolas-forissier"
      , titre = "M."
      }
    , { circonscription = "Français établis hors de France (2e circonscription)"
      , code = "OMC_PA721142"
      , collaborateurs =
            [ "marianne-billard"
            , "emmanuel-raviart"
            , "mauricio-meija"
            ]
      , commission = "Commission des lois"
      , courriels = [ "paula.forteza@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Forteza"
      , nomComplet = "Paula Forteza"
      , particule = Nothing
      , photo = "/img/photos/721142.jpg"
      , prenom = "Paula"
      , sitesWeb = [ "http://www.forteza.fr" ]
      , slug = "paula-forteza"
      , titre = "Mme"
      }
    , { circonscription = "Val-de-Marne (2e circonscription)"
      , code = "OMC_PA720870"
      , collaborateurs =
            [ "jacqueline-eude-durler"
            , "eleonore-delannoy"
            ]
      , commission = "Commission des affaires étrangères"
      , courriels = [ "jeanfrancois.mbaye@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "François Mbaye"
      , nomComplet = "Jean François Mbaye"
      , particule = Nothing
      , photo = "/img/photos/720870.jpg"
      , prenom = "Jean"
      , sitesWeb = []
      , slug = "jean-francois-mbaye"
      , titre = "M."
      }
    , { circonscription = "Lot-et-Garonne (2e circonscription)"
      , code = "OMC_PA720030"
      , collaborateurs = [ "charlotte-pautet" ]
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "alexandre.freschi@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Freschi"
      , nomComplet = "Alexandre Freschi"
      , particule = Nothing
      , photo = "/img/photos/720030.jpg"
      , prenom = "Alexandre"
      , sitesWeb = []
      , slug = "alexandre-freschi"
      , titre = "M."
      }
    , { circonscription = "Haut-Rhin (6e circonscription)"
      , code = "OMC_PA722284"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "bruno.fuchs@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Fuchs"
      , nomComplet = "Bruno Fuchs"
      , particule = Nothing
      , photo = "/img/photos/722284.jpg"
      , prenom = "Bruno"
      , sitesWeb = []
      , slug = "bruno-fuchs"
      , titre = "M."
      }
    , { circonscription = "Rhône (11e circonscription)"
      , code = "OMC_PA722366"
      , collaborateurs = [ "joelle-argot" ]
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "jean-luc.fugit@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Fugit"
      , nomComplet = "Jean-Luc Fugit"
      , particule = Nothing
      , photo = "/img/photos/722366.jpg"
      , prenom = "Jean-Luc"
      , sitesWeb = []
      , slug = "jean-luc-fugit"
      , titre = "M."
      }
    , { circonscription = "Bas-Rhin (6e circonscription)"
      , code = "OMC_PA266774"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "laurent.furst@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Furst"
      , nomComplet = "Laurent Furst"
      , particule = Nothing
      , photo = "/img/photos/266774.jpg"
      , prenom = "Laurent"
      , sitesWeb = [ "http://www.laurentfurst.fr" ]
      , slug = "laurent-furst"
      , titre = "M."
      }
    , { circonscription = "Gard (5e circonscription)"
      , code = "OMC_PA719480"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "olivier.gaillard@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Gaillard"
      , nomComplet = "Olivier Gaillard"
      , particule = Nothing
      , photo = "/img/photos/719480.jpg"
      , prenom = "Olivier"
      , sitesWeb = []
      , slug = "olivier-gaillard"
      , titre = "M."
      }
    , { circonscription = "Val-de-Marne (11e circonscription)"
      , code = "OMC_PA721246"
      , collaborateurs =
            [ "sebastien-lavandon"
            , "leonor-brucker"
            ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "albane.gaillot@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Gaillot"
      , nomComplet = "Albane Gaillot"
      , particule = Nothing
      , photo = "/img/photos/721246.jpg"
      , prenom = "Albane"
      , sitesWeb = [ "http://www.albanegaillot-2017.fr" ]
      , slug = "albane-gaillot"
      , titre = "Mme"
      }
    , { circonscription = "Manche (3e circonscription)"
      , code = "OMC_PA720198"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "Gregory.Galbadon@assemblee-nationale.fr " ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Galbadon"
      , nomComplet = "Grégory Galbadon"
      , particule = Nothing
      , photo = "/img/photos/720198.jpg"
      , prenom = "Grégory"
      , sitesWeb = []
      , slug = "gregory-galbadon"
      , titre = "M."
      }
    , { circonscription = "Vendée (2e circonscription)"
      , code = "OMC_PA721992"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "patricia.gallerneau@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Gallerneau"
      , nomComplet = "Patricia Gallerneau"
      , particule = Nothing
      , photo = "/img/photos/721992.jpg"
      , prenom = "Patricia"
      , sitesWeb = []
      , slug = "patricia-gallerneau"
      , titre = "Mme"
      }
    , { circonscription = "Loiret (3e circonscription)"
      , code = "OMC_PA267735"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "claude.deganay@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Ganay"
      , nomComplet = "Claude de Ganay"
      , particule = Just "de"
      , photo = "/img/photos/267735.jpg"
      , prenom = "Claude"
      , sitesWeb = [ "http://www.claudedeganay.fr" ]
      , slug = "claude-de-ganay"
      , titre = "M."
      }
    , { circonscription = "Meurthe-et-Moselle (2e circonscription)"
      , code = "OMC_PA720178"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "laurent.garcia@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Garcia"
      , nomComplet = "Laurent Garcia"
      , particule = Nothing
      , photo = "/img/photos/720178.jpg"
      , prenom = "Laurent"
      , sitesWeb = [ "http://www.laurentgarcia2017.com" ]
      , slug = "laurent-garcia"
      , titre = "M."
      }
    , { circonscription = "Mayenne (1re circonscription)"
      , code = "OMC_PA333285"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "guillaume.garot@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "Nouvelle Gauche"
      , nom = "Garot"
      , nomComplet = "Guillaume Garot"
      , particule = Nothing
      , photo = "/img/photos/333285.jpg"
      , prenom = "Guillaume"
      , sitesWeb = [ "http://www.guillaume-garot.fr/" ]
      , slug = "guillaume-garot"
      , titre = "M."
      }
    , { circonscription = "Rhône (10e circonscription)"
      , code = "OMC_PA722358"
      , collaborateurs =
            [ "romain-gerardi"
            , "sylvie-thouvenin"
            ]
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "thomas.gassilloud@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Gassilloud"
      , nomComplet = "Thomas Gassilloud"
      , particule = Nothing
      , photo = "/img/photos/722358.jpg"
      , prenom = "Thomas"
      , sitesWeb = []
      , slug = "thomas-gassilloud"
      , titre = "M."
      }
    , { circonscription = "Vosges (4e circonscription)"
      , code = "OMC_PA267324"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "jean-jacques.gaultier@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Gaultier"
      , nomComplet = "Jean-Jacques Gaultier"
      , particule = Nothing
      , photo = "/img/photos/267324.jpg"
      , prenom = "Jean-Jacques"
      , sitesWeb = []
      , slug = "jean-jacques-gaultier"
      , titre = "M."
      }
    , { circonscription = "Saône-et-Loire (5e circonscription)"
      , code = "OMC_PA721364"
      , collaborateurs = [ "berenice-mottelay" ]
      , commission = "Commission des lois"
      , courriels = [ "raphael.gauvain@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Gauvain"
      , nomComplet = "Raphaël Gauvain"
      , particule = Nothing
      , photo = "/img/photos/721364.jpg"
      , prenom = "Raphaël"
      , sitesWeb = []
      , slug = "raphael-gauvain"
      , titre = "M."
      }
    , { circonscription = "Pyrénées-Orientales (3e circonscription)"
      , code = "OMC_PA720806"
      , collaborateurs = [ "camille-aspar" ]
      , commission = "Commission des affaires étrangères"
      , courriels = [ "laurence.gayte@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Gayte"
      , nomComplet = "Laurence Gayte"
      , particule = Nothing
      , photo = "/img/photos/720806.jpg"
      , prenom = "Laurence"
      , sitesWeb = []
      , slug = "laurence-gayte"
      , titre = "Mme"
      }
    , { circonscription = "Français établis hors de France (11e circonscription)"
      , code = "OMC_PA721024"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "anne.genetet@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Genetet"
      , nomComplet = "Anne Genetet"
      , particule = Nothing
      , photo = "/img/photos/721024.jpg"
      , prenom = "Anne"
      , sitesWeb = []
      , slug = "anne-genetet"
      , titre = "Mme"
      }
    , { circonscription = "Doubs (5e circonscription)"
      , code = "OMC_PA605991"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "annie.genevard@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Genevard"
      , nomComplet = "Annie Genevard"
      , particule = Nothing
      , photo = "/img/photos/605991.jpg"
      , prenom = "Annie"
      , sitesWeb = []
      , slug = "annie-genevard"
      , titre = "Mme"
      }
    , { circonscription = "Eure (1re circonscription)"
      , code = "OMC_PA719326"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "Severine.Gipson@assemblee-nationale.fr " ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Gipson"
      , nomComplet = "Séverine Gipson"
      , particule = Nothing
      , photo = "/img/photos/719326.jpg"
      , prenom = "Séverine"
      , sitesWeb = []
      , slug = "severine-gipson"
      , titre = "Mme"
      }
    , { circonscription = "Marne (3e circonscription)"
      , code = "OMC_PA720222"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "eric.girardin@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Girardin"
      , nomComplet = "Éric Girardin"
      , particule = Nothing
      , photo = "/img/photos/720222.jpg"
      , prenom = "Éric"
      , sitesWeb = []
      , slug = "eric-girardin"
      , titre = "M."
      }
    , { circonscription = "Hautes-Alpes (2e circonscription)"
      , code = "OMC_PA267336"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "joel.giraud@assemblee-nationale.fr" ]
      , fonctionCommission = "Rapporteur"
      , groupe = "La République en Marche"
      , nom = "Giraud"
      , nomComplet = "Joël Giraud"
      , particule = Nothing
      , photo = "/img/photos/267336.jpg"
      , prenom = "Joël"
      , sitesWeb = [ "http://joelgirauddepute.fr" ]
      , slug = "joel-giraud"
      , titre = "M."
      }
    , { circonscription = "Ain (3e circonscription)"
      , code = "OMC_PA718674"
      , collaborateurs =
            [ "arthur-cheul"
            , "yael-yavanovitch"
            ]
      , commission = "Commission des affaires étrangères"
      , courriels = [ "olga.givernet@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Givernet"
      , nomComplet = "Olga Givernet"
      , particule = Nothing
      , photo = "/img/photos/718674.jpg"
      , prenom = "Olga"
      , sitesWeb = []
      , slug = "olga-givernet"
      , titre = "Mme"
      }
    , { circonscription = "Paris (14e circonscription)"
      , code = "OMC_PA1498"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "claude.goasguen@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Goasguen"
      , nomComplet = "Claude Goasguen"
      , particule = Nothing
      , photo = "/img/photos/1498.jpg"
      , prenom = "Claude"
      , sitesWeb = []
      , slug = "claude-goasguen"
      , titre = "M."
      }
    , { circonscription = "Var (6e circonscription)"
      , code = "OMC_PA721784"
      , collaborateurs = [ "loris-gaudin" ]
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "valerie.gomez-bassac@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Gomez-Bassac"
      , nomComplet = "Valérie Gomez-Bassac"
      , particule = Nothing
      , photo = "/img/photos/721784.jpg"
      , prenom = "Valérie"
      , sitesWeb = []
      , slug = "valerie-gomez-bassac"
      , titre = "Mme"
      }
    , { circonscription = "Nouvelle-Calédonie (2e circonscription)"
      , code = "OMC_PA610775"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "philippe.gomes@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Gomès"
      , nomComplet = "Philippe Gomès"
      , particule = Nothing
      , photo = "/img/photos/610775.jpg"
      , prenom = "Philippe"
      , sitesWeb = []
      , slug = "philippe-gomes"
      , titre = "M."
      }
    , { circonscription = "Manche (1re circonscription)"
      , code = "OMC_PA266797"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "philippe.gosselin@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "Les Républicains"
      , nom = "Gosselin"
      , nomComplet = "Philippe Gosselin"
      , particule = Nothing
      , photo = "/img/photos/266797.jpg"
      , prenom = "Philippe"
      , sitesWeb = [ "http://www.philippegosselin.fr" ]
      , slug = "philippe-gosselin"
      , titre = "M."
      }
    , { circonscription = "Val-de-Marne (6e circonscription)"
      , code = "OMC_PA721296"
      , collaborateurs =
            [ "florence-gall"
            , "emile-stefani"
            , "paula-doumerg"
            ]
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "guillaume.gouffier-cha@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Gouffier-Cha"
      , nomComplet = "Guillaume Gouffier-Cha"
      , particule = Nothing
      , photo = "/img/photos/721296.jpg"
      , prenom = "Guillaume"
      , sitesWeb = []
      , slug = "guillaume-gouffier-cha"
      , titre = "M."
      }
    , { circonscription = "Nièvre (1re circonscription)"
      , code = "OMC_PA720560"
      , collaborateurs =
            [ "gwenaelle-tschudin"
            , "severine-arbaut"
            , "raphael-saidi"
            , "celine-devoise"
            ]
      , commission = "Commission des finances"
      , courriels = [ "perrine.goulet@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Goulet"
      , nomComplet = "Perrine Goulet"
      , particule = Nothing
      , photo = "/img/photos/720560.jpg"
      , prenom = "Perrine"
      , sitesWeb = []
      , slug = "perrine-goulet"
      , titre = "Mme"
      }
    , { circonscription = "Eure (2e circonscription)"
      , code = "OMC_PA719330"
      , collaborateurs =
            [ "vincent-debas"
            , "cedric-brout"
            ]
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "fabien.gouttefarde@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Gouttefarde"
      , nomComplet = "Fabien Gouttefarde"
      , particule = Nothing
      , photo = "/img/photos/719330.jpg"
      , prenom = "Fabien"
      , sitesWeb = []
      , slug = "fabien-gouttefarde"
      , titre = "M."
      }
    , { circonscription = "Meurthe-et-Moselle (1re circonscription)"
      , code = "OMC_PA720170"
      , collaborateurs =
            [ "anne-pauline-butault"
            , "axelle-corosine"
            , "celine-brockly"
            ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "carole.grandjean@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Grandjean"
      , nomComplet = "Carole Grandjean"
      , particule = Nothing
      , photo = "/img/photos/720170.jpg"
      , prenom = "Carole"
      , sitesWeb = [ "http://www.carolegrandjean.fr" ]
      , slug = "carole-grandjean"
      , titre = "Mme"
      }
    , { circonscription = "Yvelines (12e circonscription)"
      , code = "OMC_PA722062"
      , collaborateurs = [ "emeric-vallespi" ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "florence.granjus@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Granjus"
      , nomComplet = "Florence Granjus"
      , particule = Nothing
      , photo = "/img/photos/722062.jpg"
      , prenom = "Florence"
      , sitesWeb = []
      , slug = "florence-granjus"
      , titre = "Mme"
      }
    , { circonscription = "Pyrénées-Orientales (1re circonscription)"
      , code = "OMC_PA720790"
      , collaborateurs = [ "jean-bernard-gaillot" ]
      , commission = "Commission des finances"
      , courriels = [ "romain.grau@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Grau"
      , nomComplet = "Romain Grau"
      , particule = Nothing
      , photo = "/img/photos/720790.jpg"
      , prenom = "Romain"
      , sitesWeb = []
      , slug = "romain-grau"
      , titre = "M."
      }
    , { circonscription = "Paris (12e circonscription)"
      , code = "OMC_PA721764"
      , collaborateurs =
            [ "gaspard-brochard"
            , "baltis-mejanes"
            ]
      , commission = "Commission des finances"
      , courriels = [ "olivia.gregoire@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Gregoire"
      , nomComplet = "Olivia Gregoire"
      , particule = Nothing
      , photo = "/img/photos/721764.jpg"
      , prenom = "Olivia"
      , sitesWeb = [ "http://www.oliviagregoire.fr" ]
      , slug = "olivia-gregoire"
      , titre = "Mme"
      }
    , { circonscription = "Sarthe (5e circonscription)"
      , code = "OMC_PA267318"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "jean-carles.grelier@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Grelier"
      , nomComplet = "Jean-Carles Grelier"
      , particule = Nothing
      , photo = "/img/photos/267318.jpg"
      , prenom = "Jean-Carles"
      , sitesWeb = []
      , slug = "jean-carles-grelier"
      , titre = "M."
      }
    , { circonscription = "Var (7e circonscription)"
      , code = "OMC_PA721792"
      , collaborateurs =
            [ "claire-andre"
            , "pierre-de-la-touche"
            ]
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "emilie.guerel@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Guerel"
      , nomComplet = "Émilie Guerel"
      , particule = Nothing
      , photo = "/img/photos/721792.jpg"
      , prenom = "Émilie"
      , sitesWeb = [ "http://www.emilieguerel2017.com/" ]
      , slug = "emilie-guerel"
      , titre = "Mme"
      }
    , { circonscription = "Paris (3e circonscription)"
      , code = "OMC_PA721498"
      , collaborateurs =
            [ "maelle-charreau"
            , "paul-fleurance"
            , "lea-soldermann"
            ]
      , commission = "Commission des finances"
      , courriels = [ "stanislas.guerini@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Guerini"
      , nomComplet = "Stanislas Guerini"
      , particule = Nothing
      , photo = "/img/photos/721498.jpg"
      , prenom = "Stanislas"
      , sitesWeb = [ "http://www.stanislasguerini.fr" ]
      , slug = "stanislas-guerini"
      , titre = "M."
      }
    , { circonscription = "Saint-Barthélemy et Saint-Martin (1re circonscription)"
      , code = "OMC_PA721126"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "claire.javois@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Guion-Firmin"
      , nomComplet = "Claire Guion-Firmin"
      , particule = Nothing
      , photo = "/img/photos/721126.jpg"
      , prenom = "Claire"
      , sitesWeb = []
      , slug = "claire-guion-firmin"
      , titre = "Mme"
      }
    , { circonscription = "Essonne (9e circonscription)"
      , code = "OMC_PA721880"
      , collaborateurs =
            [ "chris-chenebault"
            , "sofiane-seridji"
            ]
      , commission = "Commission des lois"
      , courriels = [ "marie.guevenoux@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Guévenoux"
      , nomComplet = "Marie Guévenoux"
      , particule = Nothing
      , photo = "/img/photos/721880.jpg"
      , prenom = "Marie"
      , sitesWeb = [ "http://marie-guevenoux.fr" ]
      , slug = "marie-guevenoux"
      , titre = "Mme"
      }
    , { circonscription = "Charente-Maritime (4e circonscription)"
      , code = "OMC_PA719108"
      , collaborateurs =
            [ "maeva-dargaud-tarquin"
            , "kevin-baudy"
            ]
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "raphael.gerard@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Gérard"
      , nomComplet = "Raphaël Gérard"
      , particule = Nothing
      , photo = "/img/photos/719108.jpg"
      , prenom = "Raphaël"
      , sitesWeb = [ "http://www.raphaelgerard.fr" ]
      , slug = "raphael-gerard"
      , titre = "M."
      }
    , { circonscription = "Pyrénées-Atlantiques (3e circonscription)"
      , code = "OMC_PA1592"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "david.habib@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Nouvelle Gauche"
      , nom = "Habib"
      , nomComplet = "David Habib"
      , particule = Nothing
      , photo = "/img/photos/1592.jpg"
      , prenom = "David"
      , sitesWeb = [ "http://www.david-habib.fr" ]
      , slug = "david-habib"
      , titre = "M."
      }
    , { circonscription = "Français établis hors de France (8e circonscription)"
      , code = "OMC_PA695100"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "meyer.habib@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Habib"
      , nomComplet = "Meyer Habib"
      , particule = Nothing
      , photo = "/img/photos/695100.jpg"
      , prenom = "Meyer"
      , sitesWeb = []
      , slug = "meyer-habib"
      , titre = "M."
      }
    , { circonscription = "Yvelines (11e circonscription)"
      , code = "OMC_PA722054"
      , collaborateurs = [ "moussa-ouarouss" ]
      , commission = "Commission des finances"
      , courriels = [ "nadia.hai@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Hai"
      , nomComplet = "Nadia Hai"
      , particule = Nothing
      , photo = "/img/photos/722054.jpg"
      , prenom = "Nadia"
      , sitesWeb = []
      , slug = "nadia-hai"
      , titre = "Mme"
      }
    , { circonscription = "Gironde (11e circonscription)"
      , code = "OMC_PA719652"
      , collaborateurs = [ "nina-halimi" ]
      , commission = "Commission des affaires économiques"
      , courriels = [ "veronique.hammerer@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Hammerer"
      , nomComplet = "Véronique Hammerer"
      , particule = Nothing
      , photo = "/img/photos/719652.jpg"
      , prenom = "Véronique"
      , sitesWeb = []
      , slug = "veronique-hammerer"
      , titre = "Mme"
      }
    , { circonscription = "Moselle (8e circonscription)"
      , code = "OMC_PA720326"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "brahim.hammouche@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Hammouche"
      , nomComplet = "Brahim Hammouche"
      , particule = Nothing
      , photo = "/img/photos/720326.jpg"
      , prenom = "Brahim"
      , sitesWeb = []
      , slug = "brahim-hammouche"
      , titre = "M."
      }
    , { circonscription = "Loire-Atlantique (9e circonscription)"
      , code = "OMC_PA720054"
      , collaborateurs = [ "lucie-voisin" ]
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "yannick.haury@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Haury"
      , nomComplet = "Yannick Haury"
      , particule = Nothing
      , photo = "/img/photos/720054.jpg"
      , prenom = "Yannick"
      , sitesWeb = [ "http://yannick-haury.fr" ]
      , slug = "yannick-haury"
      , titre = "M."
      }
    , { circonscription = "Hauts-de-Seine (3e circonscription)"
      , code = "OMC_PA722094"
      , collaborateurs = [ "selim-denoyelle" ]
      , commission = "Commission des affaires économiques"
      , courriels = [ "christine.hennion@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Hennion"
      , nomComplet = "Christine Hennion"
      , particule = Nothing
      , photo = "/img/photos/722094.jpg"
      , prenom = "Christine"
      , sitesWeb = []
      , slug = "christine-hennion"
      , titre = "Mme"
      }
    , { circonscription = "Vendée (5e circonscription)"
      , code = "OMC_PA722070"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "pierre.henriet@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Henriet"
      , nomComplet = "Pierre Henriet"
      , particule = Nothing
      , photo = "/img/photos/722070.jpg"
      , prenom = "Pierre"
      , sitesWeb = []
      , slug = "pierre-henriet"
      , titre = "M."
      }
    , { circonscription = "Val-de-Marne (8e circonscription)"
      , code = "OMC_PA1630"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "michel.herbillon@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "Les Républicains"
      , nom = "Herbillon"
      , nomComplet = "Michel Herbillon"
      , particule = Nothing
      , photo = "/img/photos/1630.jpg"
      , prenom = "Michel"
      , sitesWeb = []
      , slug = "michel-herbillon"
      , titre = "M."
      }
    , { circonscription = "Bas-Rhin (5e circonscription)"
      , code = "OMC_PA267355"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "antoine.herth@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Herth"
      , nomComplet = "Antoine Herth"
      , particule = Nothing
      , photo = "/img/photos/267355.jpg"
      , prenom = "Antoine"
      , sitesWeb = [ "http://www.antoine-herth.fr/" ]
      , slug = "antoine-herth"
      , titre = "M."
      }
    , { circonscription = "Bas-Rhin (7e circonscription)"
      , code = "OMC_PA608416"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "patrick.hetzel@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Hetzel"
      , nomComplet = "Patrick Hetzel"
      , particule = Nothing
      , photo = "/img/photos/608416.jpg"
      , prenom = "Patrick"
      , sitesWeb = [ "http://www.patrick-hetzel.fr" ]
      , slug = "patrick-hetzel"
      , titre = "M."
      }
    , { circonscription = "Français établis hors de France (3e circonscription)"
      , code = "OMC_PA721150"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "alexandre.holroyd@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Holroyd"
      , nomComplet = "Alexandre Holroyd"
      , particule = Nothing
      , photo = "/img/photos/721150.jpg"
      , prenom = "Alexandre"
      , sitesWeb = []
      , slug = "alexandre-holroyd"
      , titre = "M."
      }
    , { circonscription = "Nord (17e circonscription)"
      , code = "OMC_PA720454"
      , collaborateurs = [ "pierre-pavy" ]
      , commission = "Commission des lois"
      , courriels = [ "dimitri.houbron@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Houbron"
      , nomComplet = "Dimitri Houbron"
      , particule = Nothing
      , photo = "/img/photos/720454.jpg"
      , prenom = "Dimitri"
      , sitesWeb = []
      , slug = "dimitri-houbron"
      , titre = "M."
      }
    , { circonscription = "Vienne (2e circonscription)"
      , code = "OMC_PA722150"
      , collaborateurs =
            [ "emma-ettlinger"
            , "flavie-philipon"
            ]
      , commission = "Commission des lois"
      , courriels = [ "sacha.houlie@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Houlié"
      , nomComplet = "Sacha Houlié"
      , particule = Nothing
      , photo = "/img/photos/722150.jpg"
      , prenom = "Sacha"
      , sitesWeb = []
      , slug = "sacha-houlie"
      , titre = "M."
      }
    , { circonscription = "Hérault (5e circonscription)"
      , code = "OMC_PA719700"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "philippe.huppe@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Huppé"
      , nomComplet = "Philippe Huppé"
      , particule = Nothing
      , photo = "/img/photos/719700.jpg"
      , prenom = "Philippe"
      , sitesWeb = []
      , slug = "philippe-huppe"
      , titre = "M."
      }
    , { circonscription = "Nord (13e circonscription)"
      , code = "OMC_PA333818"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "christian.hutin@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Nouvelle Gauche"
      , nom = "Hutin"
      , nomComplet = "Christian Hutin"
      , particule = Nothing
      , photo = "/img/photos/333818.jpg"
      , prenom = "Christian"
      , sitesWeb = []
      , slug = "christian-hutin"
      , titre = "M."
      }
    , { circonscription = "Nord (5e circonscription)"
      , code = "OMC_PA267200"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "sebastien.huyghe@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Huyghe"
      , nomComplet = "Sébastien Huyghe"
      , particule = Nothing
      , photo = "/img/photos/267200.jpg"
      , prenom = "Sébastien"
      , sitesWeb = []
      , slug = "sebastien-huyghe"
      , titre = "M."
      }
    , { circonscription = "Aude (1re circonscription)"
      , code = "OMC_PA718794"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "daniele.herin@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Hérin"
      , nomComplet = "Danièle Hérin"
      , particule = Nothing
      , photo = "/img/photos/718794.jpg"
      , prenom = "Danièle"
      , sitesWeb = []
      , slug = "daniele-herin"
      , titre = "Mme"
      }
    , { circonscription = "Haute-Garonne (6e circonscription)"
      , code = "OMC_PA331835"
      , collaborateurs = [ "marie-vanderchmitt" ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "monique.iborra@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "La République en Marche"
      , nom = "Iborra"
      , nomComplet = "Monique Iborra"
      , particule = Nothing
      , photo = "/img/photos/331835.jpg"
      , prenom = "Monique"
      , sitesWeb = []
      , slug = "monique-iborra"
      , titre = "Mme"
      }
    , { circonscription = "Rhône (12e circonscription)"
      , code = "OMC_PA722374"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "cyrille.isaac-sibille@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Isaac-Sibille"
      , nomComplet = "Cyrille Isaac-Sibille"
      , particule = Nothing
      , photo = "/img/photos/722374.jpg"
      , prenom = "Cyrille"
      , sitesWeb = []
      , slug = "cyrille-isaac-sibille"
      , titre = "M."
      }
    , { circonscription = "Seine-et-Marne (4e circonscription)"
      , code = "OMC_PA1695"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "christian.jacob@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Jacob"
      , nomComplet = "Christian Jacob"
      , particule = Nothing
      , photo = "/img/photos/1695.jpg"
      , prenom = "Christian"
      , sitesWeb = []
      , slug = "christian-jacob"
      , titre = "M."
      }
    , { circonscription = "Morbihan (6e circonscription)"
      , code = "OMC_PA720354"
      , collaborateurs = [ "nadia-souffoy" ]
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "jean-michel.jacques@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "La République en Marche"
      , nom = "Jacques"
      , nomComplet = "Jean-Michel Jacques"
      , particule = Nothing
      , photo = "/img/photos/720354.jpg"
      , prenom = "Jean-Michel"
      , sitesWeb = []
      , slug = "jean-michel-jacques"
      , titre = "M."
      }
    , { circonscription = "Isère (9e circonscription)"
      , code = "OMC_PA719874"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "elodie.jacquier-laforge@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Jacquier-Laforge"
      , nomComplet = "Élodie Jacquier-Laforge"
      , particule = Nothing
      , photo = "/img/photos/719874.jpg"
      , prenom = "Élodie"
      , sitesWeb = []
      , slug = "elodie-jacquier-laforge"
      , titre = "Mme"
      }
    , { circonscription = "Loiret (2e circonscription)"
      , code = "OMC_PA720074"
      , collaborateurs = [ "robin-troutot" ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "caroline.janvier@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Janvier"
      , nomComplet = "Caroline Janvier"
      , particule = Nothing
      , photo = "/img/photos/720074.jpg"
      , prenom = "Caroline"
      , sitesWeb = []
      , slug = "caroline-janvier"
      , titre = "Mme"
      }
    , { circonscription = "Corrèze (1re circonscription)"
      , code = "OMC_PA719060"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "christophe.jerretie@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Jerretie"
      , nomComplet = "Christophe Jerretie"
      , particule = Nothing
      , photo = "/img/photos/719060.jpg"
      , prenom = "Christophe"
      , sitesWeb = []
      , slug = "christophe-jerretie"
      , titre = "M."
      }
    , { circonscription = "Indre (1re circonscription)"
      , code = "OMC_PA719778"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "francois.jolivet@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Jolivet"
      , nomComplet = "François Jolivet"
      , particule = Nothing
      , photo = "/img/photos/719778.jpg"
      , prenom = "François"
      , sitesWeb = [ "http://www.francoisjolivet.fr" ]
      , slug = "francois-jolivet"
      , titre = "M."
      }
    , { circonscription = "Côtes-d'Armor (1re circonscription)"
      , code = "OMC_PA719210"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "bruno.joncour@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Joncour"
      , nomComplet = "Bruno Joncour"
      , particule = Nothing
      , photo = "/img/photos/719210.jpg"
      , prenom = "Bruno"
      , sitesWeb = []
      , slug = "bruno-joncour"
      , titre = "M."
      }
    , { circonscription = "Loire-Atlantique (7e circonscription)"
      , code = "OMC_PA720038"
      , collaborateurs = [ "nadege-moury-gadiot" ]
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "sandrine.josso@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Josso"
      , nomComplet = "Sandrine Josso"
      , particule = Nothing
      , photo = "/img/photos/720038.jpg"
      , prenom = "Sandrine"
      , sitesWeb = []
      , slug = "sandrine-josso"
      , titre = "Mme"
      }
    , { circonscription = "Loire (1re circonscription)"
      , code = "OMC_PA332614"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "regis.juanico@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "Nouvelle Gauche"
      , nom = "Juanico"
      , nomComplet = "Régis Juanico"
      , particule = Nothing
      , photo = "/img/photos/332614.jpg"
      , prenom = "Régis"
      , sitesWeb = [ "http://www.juanico.fr" ]
      , slug = "regis-juanico"
      , titre = "M."
      }
    , { circonscription = "Rhône (2e circonscription)"
      , code = "OMC_PA722344"
      , collaborateurs =
            [ "simon-virlogeux"
            , "chloe-prudhomme"
            ]
      , commission = "Commission des affaires étrangères"
      , courriels = [ "hubert.julien-laferriere@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Julien-Laferriere"
      , nomComplet = "Hubert Julien-Laferriere"
      , particule = Nothing
      , photo = "/img/photos/722344.jpg"
      , prenom = "Hubert"
      , sitesWeb = []
      , slug = "hubert-julien-laferriere"
      , titre = "M."
      }
    , { circonscription = "Seine-Maritime (6e circonscription)"
      , code = "OMC_PA722202"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "sebastien.jumel@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Gauche démocrate et républicaine"
      , nom = "Jumel"
      , nomComplet = "Sébastien Jumel"
      , particule = Nothing
      , photo = "/img/photos/722202.jpg"
      , prenom = "Sébastien"
      , sitesWeb = []
      , slug = "sebastien-jumel"
      , titre = "M."
      }
    , { circonscription = "Seine-et-Marne (3e circonscription)"
      , code = "OMC_PA267801"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "yves.jego@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Jégo"
      , nomComplet = "Yves Jégo"
      , particule = Nothing
      , photo = "/img/photos/267801.jpg"
      , prenom = "Yves"
      , sitesWeb = []
      , slug = "yves-jego"
      , titre = "M."
      }
    , { circonscription = "Mayotte (2e circonscription)"
      , code = "OMC_PA267901"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "mansour.kamardine@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Kamardine"
      , nomComplet = "Mansour Kamardine"
      , particule = Nothing
      , photo = "/img/photos/267901.jpg"
      , prenom = "Mansour"
      , sitesWeb = [ "http://www.mansour-kamardine.net" ]
      , slug = "mansour-kamardine"
      , titre = "M."
      }
    , { circonscription = "Isère (5e circonscription)"
      , code = "OMC_PA719994"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "catherine.kamowski@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Kamowski"
      , nomComplet = "Catherine Kamowski"
      , particule = Nothing
      , photo = "/img/photos/719994.jpg"
      , prenom = "Catherine"
      , sitesWeb = []
      , slug = "catherine-kamowski"
      , titre = "Mme"
      }
    , { circonscription = "Sarthe (2e circonscription)"
      , code = "OMC_PA335054"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "marietta.karamanli@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Nouvelle Gauche"
      , nom = "Karamanli"
      , nomComplet = "Marietta Karamanli"
      , particule = Nothing
      , photo = "/img/photos/335054.jpg"
      , prenom = "Marietta"
      , sitesWeb = [ "http://www.mariettakaramanli.fr  " ]
      , slug = "marietta-karamanli"
      , titre = "Mme"
      }
    , { circonscription = "Eure-et-Loir (1re circonscription)"
      , code = "OMC_PA719372"
      , collaborateurs =
            [ "gael-garreau"
            , "alphonse-corone"
            , "barbara-gombert"
            ]
      , commission = "Commission des affaires économiques"
      , courriels = [ "guillaume.kasbarian@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Kasbarian"
      , nomComplet = "Guillaume Kasbarian"
      , particule = Nothing
      , photo = "/img/photos/719372.jpg"
      , prenom = "Guillaume"
      , sitesWeb = []
      , slug = "guillaume-kasbarian"
      , titre = "M."
      }
    , { circonscription = "Seine-Maritime (9e circonscription)"
      , code = "OMC_PA721514"
      , collaborateurs = [ "samuel-menager" ]
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "stephanie.kerbarh@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Kerbarh"
      , nomComplet = "Stéphanie Kerbarh"
      , particule = Nothing
      , photo = "/img/photos/721514.jpg"
      , prenom = "Stéphanie"
      , sitesWeb = []
      , slug = "stephanie-kerbarh"
      , titre = "Mme"
      }
    , { circonscription = "Côtes-d'Armor (4e circonscription)"
      , code = "OMC_PA719230"
      , collaborateurs = [ "julie-guyot" ]
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "yannick.kerlogot@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Kerlogot"
      , nomComplet = "Yannick Kerlogot"
      , particule = Nothing
      , photo = "/img/photos/719230.jpg"
      , prenom = "Yannick"
      , sitesWeb = []
      , slug = "yannick-kerlogot"
      , titre = "M."
      }
    , { circonscription = "Cher (3e circonscription)"
      , code = "OMC_PA719052"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "loic.kervran@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Kervran"
      , nomComplet = "Loïc Kervran"
      , particule = Nothing
      , photo = "/img/photos/719052.jpg"
      , prenom = "Loïc"
      , sitesWeb = []
      , slug = "loic-kervran"
      , titre = "M."
      }
    , { circonscription = "Côte-d'Or (3e circonscription)"
      , code = "OMC_PA719186"
      , collaborateurs = [ "andrea-koshkhou" ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "fadila.khattabi@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Khattabi"
      , nomComplet = "Fadila Khattabi"
      , particule = Nothing
      , photo = "/img/photos/719186.jpg"
      , prenom = "Fadila"
      , sitesWeb = []
      , slug = "fadila-khattabi"
      , titre = "Mme"
      }
    , { circonscription = "Rhône (7e circonscription)"
      , code = "OMC_PA722300"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "anissa.khedher@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Khedher"
      , nomComplet = "Anissa Khedher"
      , particule = Nothing
      , photo = "/img/photos/722300.jpg"
      , prenom = "Anissa"
      , sitesWeb = []
      , slug = "anissa-khedher"
      , titre = "Mme"
      }
    , { circonscription = "Seine-et-Marne (7e circonscription)"
      , code = "OMC_PA721644"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "rodrigue.kokouendo@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Kokouendo"
      , nomComplet = "Rodrigue Kokouendo"
      , particule = Nothing
      , photo = "/img/photos/721644.jpg"
      , prenom = "Rodrigue"
      , sitesWeb = []
      , slug = "rodrigue-kokouendo"
      , titre = "M."
      }
    , { circonscription = "Aisne (5e circonscription)"
      , code = "OMC_PA605084"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "jacques.krabal@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "La République en Marche"
      , nom = "Krabal"
      , nomComplet = "Jacques Krabal"
      , particule = Nothing
      , photo = "/img/photos/605084.jpg"
      , prenom = "Jacques"
      , sitesWeb = []
      , slug = "jacques-krabal"
      , titre = "M."
      }
    , { circonscription = "Manche (4e circonscription)"
      , code = "OMC_PA720202"
      , collaborateurs = [ "boris-restier" ]
      , commission = "Commission des affaires étrangères"
      , courriels = [ "sonia.krimi@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Krimi"
      , nomComplet = "Sonia Krimi"
      , particule = Nothing
      , photo = "/img/photos/720202.jpg"
      , prenom = "Sonia"
      , sitesWeb = [ "http://www.soniakrimi.fr" ]
      , slug = "sonia-krimi"
      , titre = "Mme"
      }
    , { circonscription = "Marne (2e circonscription)"
      , code = "OMC_PA720214"
      , collaborateurs =
            [ "louise-sita"
            , "quentin-spooner"
            ]
      , commission = "Commission des finances"
      , courriels = [ "aina.kuric@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Kuric"
      , nomComplet = "Aina Kuric"
      , particule = Nothing
      , photo = "/img/photos/720214.jpg"
      , prenom = "Aina"
      , sitesWeb = []
      , slug = "aina-kuric"
      , titre = "Mme"
      }
    ]


deputes3 : Deputes
deputes3 =
    [ { circonscription = "Paris (4e circonscription)"
      , code = "OMC_PA721506"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "brigitte.kuster@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Kuster"
      , nomComplet = "Brigitte Kuster"
      , particule = Nothing
      , photo = "/img/photos/721506.jpg"
      , prenom = "Brigitte"
      , sitesWeb = [ "http://www.brigittekuster.fr" ]
      , slug = "brigitte-kuster"
      , titre = "Mme"
      }
    , { circonscription = "Eure-et-Loir (3e circonscription)"
      , code = "OMC_PA331567"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "laure.delaraudiere@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "La Raudière"
      , nomComplet = "Laure de La Raudière"
      , particule = Just "de"
      , photo = "/img/photos/331567.jpg"
      , prenom = "Laure"
      , sitesWeb = [ "http://www.la-raudiere.com" ]
      , slug = "laure-de-la-raudiere"
      , titre = "Mme"
      }
    , { circonscription = "Ille-et-Vilaine (1re circonscription)"
      , code = "OMC_PA719842"
      , collaborateurs = [ "jean-philippe-calmus" ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "mustapha.laabid@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Laabid"
      , nomComplet = "Mustapha Laabid"
      , particule = Nothing
      , photo = "/img/photos/719842.jpg"
      , prenom = "Mustapha"
      , sitesWeb = []
      , slug = "mustapha-laabid"
      , titre = "M."
      }
    , { circonscription = "Indre-et-Loire (2e circonscription)"
      , code = "OMC_PA719798"
      , collaborateurs =
            [ "coline-roux"
            , "stephane-merceron"
            ]
      , commission = "Commission des finances"
      , courriels = [ "daniel.labaronne@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Labaronne"
      , nomComplet = "Daniel Labaronne"
      , particule = Nothing
      , photo = "/img/photos/719798.jpg"
      , prenom = "Daniel"
      , sitesWeb = []
      , slug = "daniel-labaronne"
      , titre = "M."
      }
    , { circonscription = "Seine-Saint-Denis (6e circonscription)"
      , code = "OMC_PA720846"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "bastien.lachaud@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La France insoumise"
      , nom = "Lachaud"
      , nomComplet = "Bastien Lachaud"
      , particule = Nothing
      , photo = "/img/photos/720846.jpg"
      , prenom = "Bastien"
      , sitesWeb = []
      , slug = "bastien-lachaud"
      , titre = "M."
      }
    , { circonscription = "Seine-et-Marne (2e circonscription)"
      , code = "OMC_PA609245"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "valerie.lacroute@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Lacroute"
      , nomComplet = "Valérie Lacroute"
      , particule = Nothing
      , photo = "/img/photos/609245.jpg"
      , prenom = "Valérie"
      , sitesWeb = [ "http://www.valerie-lacroute.fr" ]
      , slug = "valerie-lacroute"
      , titre = "Mme"
      }
    , { circonscription = "Seine-Saint-Denis (5e circonscription)"
      , code = "OMC_PA268019"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "jean-christophe.lagarde@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Lagarde"
      , nomComplet = "Jean-Christophe Lagarde"
      , particule = Nothing
      , photo = "/img/photos/268019.jpg"
      , prenom = "Jean-Christophe"
      , sitesWeb = [ "http://www.jclagarde.fr" ]
      , slug = "jean-christophe-lagarde"
      , titre = "M."
      }
    , { circonscription = "Haute-Garonne (2e circonscription)"
      , code = "OMC_PA719504"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "jean-luc.lagleize@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Lagleize"
      , nomComplet = "Jean-Luc Lagleize"
      , particule = Nothing
      , photo = "/img/photos/719504.jpg"
      , prenom = "Jean-Luc"
      , sitesWeb = []
      , slug = "jean-luc-lagleize"
      , titre = "M."
      }
    , { circonscription = "Landes (1re circonscription)"
      , code = "OMC_PA719918"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "Fabien.Laine@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Lainé"
      , nomComplet = "Fabien Lainé"
      , particule = Nothing
      , photo = "/img/photos/719918.jpg"
      , prenom = "Fabien"
      , sitesWeb = []
      , slug = "fabien-laine"
      , titre = "M."
      }
    , { circonscription = "Français établis hors de France (10e circonscription)"
      , code = "OMC_PA721004"
      , collaborateurs =
            [ "marion-hugues"
            , "gabriel-dumont"
            , "amel-labbas"
            ]
      , commission = "Commission des affaires étrangères"
      , courriels = [ "amelia.lakrafi@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Lakrafi"
      , nomComplet = "Amal-Amélia Lakrafi"
      , particule = Nothing
      , photo = "/img/photos/721004.jpg"
      , prenom = "Amal-Amélia"
      , sitesWeb = [ "http://amelia2017.fr" ]
      , slug = "amal-amelia-lakrafi"
      , titre = "Mme"
      }
    , { circonscription = "Bouches-du-Rhône (10e circonscription)"
      , code = "OMC_PA605518"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "francois-michel.lambert@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Lambert"
      , nomComplet = "François-Michel Lambert"
      , particule = Nothing
      , photo = "/img/photos/605518.jpg"
      , prenom = "François-Michel"
      , sitesWeb = [ "http://fmlambert.fr" ]
      , slug = "francois-michel-lambert"
      , titre = "M."
      }
    , { circonscription = "Charente (3e circonscription)"
      , code = "OMC_PA1809"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "jerome.lambert@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Nouvelle Gauche"
      , nom = "Lambert"
      , nomComplet = "Jérôme Lambert"
      , particule = Nothing
      , photo = "/img/photos/1809.jpg"
      , prenom = "Jérôme"
      , sitesWeb = []
      , slug = "jerome-lambert"
      , titre = "M."
      }
    , { circonscription = "Paris (10e circonscription)"
      , code = "OMC_PA643205"
      , collaborateurs =
            [ "theo-tedeschi"
            , "benjamin-weinberger"
            , "john-samingo"
            ]
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "anne-christine.lang@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Lang"
      , nomComplet = "Anne-Christine Lang"
      , particule = Nothing
      , photo = "/img/photos/643205.jpg"
      , prenom = "Anne-Christine"
      , sitesWeb = [ "http://www.annechristinelang.fr" ]
      , slug = "anne-christine-lang"
      , titre = "Mme"
      }
    , { circonscription = "Bouches-du-Rhône (11e circonscription)"
      , code = "OMC_PA718978"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "mohamed.laqhila@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Laqhila"
      , nomComplet = "Mohamed Laqhila"
      , particule = Nothing
      , photo = "/img/photos/718978.jpg"
      , prenom = "Mohamed"
      , sitesWeb = [ "http://laqhila11.com" ]
      , slug = "mohamed-laqhila"
      , titre = "M."
      }
    , { circonscription = "Haute-Savoie (2e circonscription)"
      , code = "OMC_PA721434"
      , collaborateurs =
            [ "lea-thabot"
            , "jordane-wodli"
            ]
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "frederique.lardet@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Lardet"
      , nomComplet = "Frédérique Lardet"
      , particule = Nothing
      , photo = "/img/photos/721434.jpg"
      , prenom = "Frédérique"
      , sitesWeb = [ "http://www.frederiquelardet.fr/" ]
      , slug = "frederique-lardet"
      , titre = "Mme"
      }
    , { circonscription = "Ariège (2e circonscription)"
      , code = "OMC_PA718868"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "michel.larive@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La France insoumise"
      , nom = "Larive"
      , nomComplet = "Michel Larive"
      , particule = Nothing
      , photo = "/img/photos/718868.jpg"
      , prenom = "Michel"
      , sitesWeb = [ "http://www.michellarive2017.com" ]
      , slug = "michel-larive"
      , titre = "M."
      }
    , { circonscription = "Yonne (1re circonscription)"
      , code = "OMC_PA267785"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "guillaume.larrive@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Larrivé"
      , nomComplet = "Guillaume Larrivé"
      , particule = Nothing
      , photo = "/img/photos/267785.jpg"
      , prenom = "Guillaume"
      , sitesWeb = [ "http://www.larrive.com" ]
      , slug = "guillaume-larrive"
      , titre = "M."
      }
    , { circonscription = "Finistère (2e circonscription)"
      , code = "OMC_PA719396"
      , collaborateurs = [ "pierre-tissier" ]
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "jean-charles.larsonneur@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Larsonneur"
      , nomComplet = "Jean-Charles Larsonneur"
      , particule = Nothing
      , photo = "/img/photos/719396.jpg"
      , prenom = "Jean-Charles"
      , sitesWeb = [ "http://www.jeancharleslarsonneur.fr" ]
      , slug = "jean-charles-larsonneur"
      , titre = "M."
      }
    , { circonscription = "Pyrénées-Atlantiques (4e circonscription)"
      , code = "OMC_PA1838"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "jean.lassalle@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Non inscrit"
      , nom = "Lassalle"
      , nomComplet = "Jean Lassalle"
      , particule = Nothing
      , photo = "/img/photos/1838.jpg"
      , prenom = "Jean"
      , sitesWeb = []
      , slug = "jean-lassalle"
      , titre = "M."
      }
    , { circonscription = "Pyrénées-Atlantiques (5e circonscription)"
      , code = "OMC_PA720764"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "florence.lasserre-david@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Lasserre-David"
      , nomComplet = "Florence Lasserre-David"
      , particule = Nothing
      , photo = "/img/photos/720764.jpg"
      , prenom = "Florence"
      , sitesWeb = []
      , slug = "florence-lasserre-david"
      , titre = "Mme"
      }
    , { circonscription = "Vendée (1re circonscription)"
      , code = "OMC_PA721984"
      , collaborateurs = [ "victoria-jolly" ]
      , commission = "Commission des lois"
      , courriels = [ "philippe.latombe@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Latombe"
      , nomComplet = "Philippe Latombe"
      , particule = Nothing
      , photo = "/img/photos/721984.jpg"
      , prenom = "Philippe"
      , sitesWeb = []
      , slug = "philippe-latombe"
      , titre = "M."
      }
    , { circonscription = "Lot-et-Garonne (1re circonscription)"
      , code = "OMC_PA720022"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "michel.lauzzana@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Lauzzana"
      , nomComplet = "Michel Lauzzana"
      , particule = Nothing
      , photo = "/img/photos/720022.jpg"
      , prenom = "Michel"
      , sitesWeb = []
      , slug = "michel-lauzzana"
      , titre = "M."
      }
    , { circonscription = "Drôme (3e circonscription)"
      , code = "OMC_PA719310"
      , collaborateurs =
            [ "aurore-feuer"
            , "hugo-basset"
            ]
      , commission = "Commission des affaires économiques"
      , courriels = [ "celia.delavergne@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Lavergne"
      , nomComplet = "Célia de Lavergne"
      , particule = Just "de"
      , photo = "/img/photos/719310.jpg"
      , prenom = "Célia"
      , sitesWeb = [ "http://www.celiadelavergne2017.fr" ]
      , slug = "celia-de-lavergne"
      , titre = "Mme"
      }
    , { circonscription = "Val-d'Oise (5e circonscription)"
      , code = "OMC_PA720916"
      , collaborateurs =
            [ "damien-horn"
            , "roman-boumlih"
            , "karin-riviere"
            ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "fiona.lazaar@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Lazaar"
      , nomComplet = "Fiona Lazaar"
      , particule = Nothing
      , photo = "/img/photos/720916.jpg"
      , prenom = "Fiona"
      , sitesWeb = [ "http://fionalazaar.fr" ]
      , slug = "fiona-lazaar"
      , titre = "Mme"
      }
    , { circonscription = "Ille-et-Vilaine (4e circonscription)"
      , code = "OMC_PA719728"
      , collaborateurs = [ "nicolas-elleouet" ]
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "gael.lebohec@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Le Bohec"
      , nomComplet = "Gaël Le Bohec"
      , particule = Nothing
      , photo = "/img/photos/719728.jpg"
      , prenom = "Gaël"
      , sitesWeb = [ "http://lebohecgael.simplesite.com" ]
      , slug = "gael-le-bohec"
      , titre = "M."
      }
    , { circonscription = "Finistère (4e circonscription)"
      , code = "OMC_PA719412"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "sandrine.lefeur@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Le Feur"
      , nomComplet = "Sandrine Le Feur"
      , particule = Nothing
      , photo = "/img/photos/719412.jpg"
      , prenom = "Sandrine"
      , sitesWeb = []
      , slug = "sandrine-le-feur"
      , titre = "Mme"
      }
    , { circonscription = "Sarthe (4e circonscription)"
      , code = "OMC_PA588886"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "stephane.lefoll@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Nouvelle Gauche"
      , nom = "Le Foll"
      , nomComplet = "Stéphane Le Foll"
      , particule = Nothing
      , photo = "/img/photos/588886.jpg"
      , prenom = "Stéphane"
      , sitesWeb = []
      , slug = "stephane-le-foll"
      , titre = "M."
      }
    , { circonscription = "Côtes-d'Armor (3e circonscription)"
      , code = "OMC_PA1874"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "marc.lefur@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Le Fur"
      , nomComplet = "Marc Le Fur"
      , particule = Nothing
      , photo = "/img/photos/1874.jpg"
      , prenom = "Marc"
      , sitesWeb = [ "http://www.marclefur.bzh" ]
      , slug = "marc-le-fur"
      , titre = "M."
      }
    , { circonscription = "Finistère (3e circonscription)"
      , code = "OMC_PA719404"
      , collaborateurs = [ "jerome-hebert" ]
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "didier.legac@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Le Gac"
      , nomComplet = "Didier Le Gac"
      , particule = Nothing
      , photo = "/img/photos/719404.jpg"
      , prenom = "Didier"
      , sitesWeb = []
      , slug = "didier-le-gac"
      , titre = "M."
      }
    , { circonscription = "Paris (2e circonscription)"
      , code = "OMC_PA721466"
      , collaborateurs =
            [ "theo-dardonville"
            , "sonia-de-maigret"
            ]
      , commission = "Commission des finances"
      , courriels = [ "gilles.le.gendre@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Le Gendre"
      , nomComplet = "Gilles Le Gendre"
      , particule = Nothing
      , photo = "/img/photos/721466.jpg"
      , prenom = "Gilles"
      , sitesWeb = [ "http://glg2017.fr" ]
      , slug = "gilles-le-gendre"
      , titre = "M."
      }
    , { circonscription = "Hauts-de-Seine (6e circonscription)"
      , code = "OMC_PA345722"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "constance.legrip@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "Les Républicains"
      , nom = "Le Grip"
      , nomComplet = "Constance Le Grip"
      , particule = Nothing
      , photo = "/img/photos/345722.jpg"
      , prenom = "Constance"
      , sitesWeb = []
      , slug = "constance-le-grip"
      , titre = "Mme"
      }
    , { circonscription = "Finistère (1re circonscription)"
      , code = "OMC_PA719388"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "annaig.lemeur@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Le Meur"
      , nomComplet = "Annaïg Le Meur"
      , particule = Nothing
      , photo = "/img/photos/719388.jpg"
      , prenom = "Annaïg"
      , sitesWeb = []
      , slug = "annaig-le-meur"
      , titre = "Mme"
      }
    , { circonscription = "Morbihan (3e circonscription)"
      , code = "OMC_PA720342"
      , collaborateurs = [ "paola-cloe-sola" ]
      , commission = "Commission des affaires étrangères"
      , courriels = [ "nicole.lepeih@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Le Peih"
      , nomComplet = "Nicole Le Peih"
      , particule = Nothing
      , photo = "/img/photos/720342.jpg"
      , prenom = "Nicole"
      , sitesWeb = []
      , slug = "nicole-le-peih"
      , titre = "Mme"
      }
    , { circonscription = "Pas-de-Calais (11e circonscription)"
      , code = "OMC_PA720614"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "marine.lepen@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Non inscrit"
      , nom = "Le Pen"
      , nomComplet = "Marine Le Pen"
      , particule = Nothing
      , photo = "/img/photos/720614.jpg"
      , prenom = "Marine"
      , sitesWeb = []
      , slug = "marine-le-pen"
      , titre = "Mme"
      }
    , { circonscription = "Calvados (1re circonscription)"
      , code = "OMC_PA719006"
      , collaborateurs = [ "alicia-gerber" ]
      , commission = "Commission des finances"
      , courriels = [ "fabrice.levigoureux@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Le Vigoureux"
      , nomComplet = "Fabrice Le Vigoureux"
      , particule = Nothing
      , photo = "/img/photos/719006.jpg"
      , prenom = "Fabrice"
      , sitesWeb = []
      , slug = "fabrice-le-vigoureux"
      , titre = "M."
      }
    , { circonscription = "Yvelines (4e circonscription)"
      , code = "OMC_PA721852"
      , collaborateurs =
            [ "thomas-philippe"
            , "jean-baptiste-ogoundele"
            ]
      , commission = "Commission des affaires économiques"
      , courriels = [ "marie.lebec@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Lebec"
      , nomComplet = "Marie Lebec"
      , particule = Nothing
      , photo = "/img/photos/721852.jpg"
      , prenom = "Marie"
      , sitesWeb = [ "http://marielebec.en-marche.fr/" ]
      , slug = "marie-lebec"
      , titre = "Mme"
      }
    , { circonscription = "Somme (4e circonscription)"
      , code = "OMC_PA722228"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "jean-claude.leclabart@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Leclabart"
      , nomComplet = "Jean-Claude Leclabart"
      , particule = Nothing
      , photo = "/img/photos/722228.jpg"
      , prenom = "Jean-Claude"
      , sitesWeb = []
      , slug = "jean-claude-leclabart"
      , titre = "M."
      }
    , { circonscription = "Calvados (3e circonscription)"
      , code = "OMC_PA340853"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "sebastien.leclerc@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Leclerc"
      , nomComplet = "Sébastien Leclerc"
      , particule = Nothing
      , photo = "/img/photos/340853.jpg"
      , prenom = "Sébastien"
      , sitesWeb = [ "http://www.Sebastienleclerc2017.fr" ]
      , slug = "sebastien-leclerc"
      , titre = "M."
      }
    , { circonscription = "Nord (6e circonscription)"
      , code = "OMC_PA720480"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "charlotte.lecocq@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Lecocq"
      , nomComplet = "Charlotte Lecocq"
      , particule = Nothing
      , photo = "/img/photos/720480.jpg"
      , prenom = "Charlotte"
      , sitesWeb = []
      , slug = "charlotte-lecocq"
      , titre = "Mme"
      }
    , { circonscription = "Seine-Maritime (8e circonscription)"
      , code = "OMC_PA335612"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "jean-paul.lecoq@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Gauche démocrate et républicaine"
      , nom = "Lecoq"
      , nomComplet = "Jean-Paul Lecoq"
      , particule = Nothing
      , photo = "/img/photos/335612.jpg"
      , prenom = "Jean-Paul"
      , sitesWeb = [ "http://jeanpaul-lecoq.fr" ]
      , slug = "jean-paul-lecoq"
      , titre = "M."
      }
    , { circonscription = "Nord (10e circonscription)"
      , code = "OMC_PA712014"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "vincent.ledoux@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Ledoux"
      , nomComplet = "Vincent Ledoux"
      , particule = Nothing
      , photo = "/img/photos/712014.jpg"
      , prenom = "Vincent"
      , sitesWeb = [ "http://www.vincent ledoux.com" ]
      , slug = "vincent-ledoux"
      , titre = "M."
      }
    , { circonscription = "Vendée (4e circonscription)"
      , code = "OMC_PA722008"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "martine.leguille-balloy@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Leguille-Balloy"
      , nomComplet = "Martine Leguille-Balloy"
      , particule = Nothing
      , photo = "/img/photos/722008.jpg"
      , prenom = "Martine"
      , sitesWeb = []
      , slug = "martine-leguille-balloy"
      , titre = "Mme"
      }
    , { circonscription = "Haute-Saône (2e circonscription)"
      , code = "OMC_PA722320"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "christophe.lejeune@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Lejeune"
      , nomComplet = "Christophe Lejeune"
      , particule = Nothing
      , photo = "/img/photos/722320.jpg"
      , prenom = "Christophe"
      , sitesWeb = []
      , slug = "christophe-lejeune"
      , titre = "M."
      }
    , { circonscription = "Haute-Savoie (5e circonscription)"
      , code = "OMC_PA721450"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "marion.lenne@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Lenne"
      , nomComplet = "Marion Lenne"
      , particule = Nothing
      , photo = "/img/photos/721450.jpg"
      , prenom = "Marion"
      , sitesWeb = []
      , slug = "marion-lenne"
      , titre = "Mme"
      }
    , { circonscription = "Loir-et-Cher (3e circonscription)"
      , code = "OMC_PA1960"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "maurice.leroy@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Leroy"
      , nomComplet = "Maurice Leroy"
      , particule = Nothing
      , photo = "/img/photos/1960.jpg"
      , prenom = "Maurice"
      , sitesWeb = []
      , slug = "maurice-leroy"
      , titre = "M."
      }
    , { circonscription = "Français établis hors de France (1re circonscription)"
      , code = "OMC_PA721134"
      , collaborateurs = [ "christophe-weissberg" ]
      , commission = "Commission des affaires économiques"
      , courriels = [ "roland.lescure@assemblee-nationale.fr" ]
      , fonctionCommission = "Président"
      , groupe = "La République en Marche"
      , nom = "Lescure"
      , nomComplet = "Roland Lescure"
      , particule = Nothing
      , photo = "/img/photos/721134.jpg"
      , prenom = "Roland"
      , sitesWeb = [ "http://rolandlescure.org" ]
      , slug = "roland-lescure"
      , titre = "M."
      }
    , { circonscription = "Martinique (3e circonscription)"
      , code = "OMC_PA337483"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "serge.letchimy@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Nouvelle Gauche"
      , nom = "Letchimy"
      , nomComplet = "Serge Letchimy"
      , particule = Nothing
      , photo = "/img/photos/337483.jpg"
      , prenom = "Serge"
      , sitesWeb = []
      , slug = "serge-letchimy"
      , titre = "M."
      }
    , { circonscription = "Var (1re circonscription)"
      , code = "OMC_PA267378"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "genevieve.levy@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Levy"
      , nomComplet = "Geneviève Levy"
      , particule = Nothing
      , photo = "/img/photos/267378.jpg"
      , prenom = "Geneviève"
      , sitesWeb = []
      , slug = "genevieve-levy"
      , titre = "Mme"
      }
    , { circonscription = "Isère (7e circonscription)"
      , code = "OMC_PA719858"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "monique.limon@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Limon"
      , nomComplet = "Monique Limon"
      , particule = Nothing
      , photo = "/img/photos/719858.jpg"
      , prenom = "Monique"
      , sitesWeb = [ "http://www.monique-limon-deputee.fr/" ]
      , slug = "monique-limon"
      , titre = "Mme"
      }
    , { circonscription = "Moselle (3e circonscription)"
      , code = "OMC_PA720378"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "richard.lioger@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Lioger"
      , nomComplet = "Richard Lioger"
      , particule = Nothing
      , photo = "/img/photos/720378.jpg"
      , prenom = "Richard"
      , sitesWeb = []
      , slug = "richard-lioger"
      , titre = "M."
      }
    , { circonscription = "Nord (4e circonscription)"
      , code = "OMC_PA720446"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "brigitte.liso@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Liso"
      , nomComplet = "Brigitte Liso"
      , particule = Nothing
      , photo = "/img/photos/720446.jpg"
      , prenom = "Brigitte"
      , sitesWeb = []
      , slug = "brigitte-liso"
      , titre = "Mme"
      }
    , { circonscription = "Réunion (4e circonscription)"
      , code = "OMC_PA721054"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "david.lorion@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Lorion"
      , nomComplet = "David Lorion"
      , particule = Nothing
      , photo = "/img/photos/721054.jpg"
      , prenom = "David"
      , sitesWeb = [ "http://www.davidlorion.re" ]
      , slug = "david-lorion"
      , titre = "M."
      }
    , { circonscription = "Bouches-du-Rhône (3e circonscription)"
      , code = "OMC_PA718918"
      , collaborateurs = [ "yassine-medjani" ]
      , commission = "Commission des lois"
      , courriels = [ "alexandra.louis@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Louis"
      , nomComplet = "Alexandra Louis"
      , particule = Nothing
      , photo = "/img/photos/718918.jpg"
      , prenom = "Alexandra"
      , sitesWeb = []
      , slug = "alexandra-louis"
      , titre = "Mme"
      }
    , { circonscription = "Orne (2e circonscription)"
      , code = "OMC_PA608016"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "veronique.louwagie@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Louwagie"
      , nomComplet = "Véronique Louwagie"
      , particule = Nothing
      , photo = "/img/photos/608016.jpg"
      , prenom = "Véronique"
      , sitesWeb = [ "http://www.veroniquelouwagie.fr" ]
      , slug = "veronique-louwagie"
      , titre = "Mme"
      }
    , { circonscription = "Seine-et-Marne (1re circonscription)"
      , code = "OMC_PA721530"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "aude.luquet@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Luquet"
      , nomComplet = "Aude Luquet"
      , particule = Nothing
      , photo = "/img/photos/721530.jpg"
      , prenom = "Aude"
      , sitesWeb = []
      , slug = "aude-luquet"
      , titre = "Mme"
      }
    , { circonscription = "Ille-et-Vilaine (7e circonscription)"
      , code = "OMC_PA606712"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "gilles.lurton@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "Les Républicains"
      , nom = "Lurton"
      , nomComplet = "Gilles Lurton"
      , particule = Nothing
      , photo = "/img/photos/606712.jpg"
      , prenom = "Gilles"
      , sitesWeb = [ "http://www.gilleslurton.fr/" ]
      , slug = "gilles-lurton"
      , titre = "M."
      }
    , { circonscription = "Haute-Vienne (3e circonscription)"
      , code = "OMC_PA722244"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "marie-ange.magne@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Magne"
      , nomComplet = "Marie-Ange Magne"
      , particule = Nothing
      , photo = "/img/photos/722244.jpg"
      , prenom = "Marie-Ange"
      , sitesWeb = [ "http://www.marie-ange-en-marche.fr" ]
      , slug = "marie-ange-magne"
      , titre = "Mme"
      }
    , { circonscription = "Marne (4e circonscription)"
      , code = "OMC_PA720230"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "lise.magnier@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Magnier"
      , nomComplet = "Lise Magnier"
      , particule = Nothing
      , photo = "/img/photos/720230.jpg"
      , prenom = "Lise"
      , sitesWeb = []
      , slug = "lise-magnier"
      , titre = "Mme"
      }
    , { circonscription = "Paris (1re circonscription)"
      , code = "OMC_PA717379"
      , collaborateurs = [ "aude-vrillet" ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "sylvain.maillard@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Maillard"
      , nomComplet = "Sylvain Maillard"
      , particule = Nothing
      , photo = "/img/photos/717379.jpg"
      , prenom = "Sylvain"
      , sitesWeb = [ "http://sylvainmaillard.fr" ]
      , slug = "sylvain-maillard"
      , titre = "M."
      }
    , { circonscription = "Ille-et-Vilaine (2e circonscription)"
      , code = "OMC_PA719718"
      , collaborateurs =
            [ "lorene-kloster"
            , "dylan-bonfils"
            ]
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "laurence.maillart-mehaignerie@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Maillart-Méhaignerie"
      , nomComplet = "Laurence Maillart-Méhaignerie"
      , particule = Nothing
      , photo = "/img/photos/719718.jpg"
      , prenom = "Laurence"
      , sitesWeb = []
      , slug = "laurence-maillart-mehaignerie"
      , titre = "Mme"
      }
    , { circonscription = "Hauts-de-Seine (8e circonscription)"
      , code = "OMC_PA722178"
      , collaborateurs =
            [ "quentin-michael"
            , "isabelle-sotto"
            ]
      , commission = "Commission des affaires étrangères"
      , courriels = [ "jacques.maire@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "La République en Marche"
      , nom = "Maire"
      , nomComplet = "Jacques Maire"
      , particule = Nothing
      , photo = "/img/photos/722178.jpg"
      , prenom = "Jacques"
      , sitesWeb = [ "http://www.jacquesmaire.fr" ]
      , slug = "jacques-maire"
      , titre = "M."
      }
    , { circonscription = "Martinique (1re circonscription)"
      , code = "OMC_PA720988"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "josette.manin@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Nouvelle Gauche"
      , nom = "Manin"
      , nomComplet = "Josette Manin"
      , particule = Nothing
      , photo = "/img/photos/720988.jpg"
      , prenom = "Josette"
      , sitesWeb = []
      , slug = "josette-manin"
      , titre = "Mme"
      }
    , { circonscription = "Somme (3e circonscription)"
      , code = "OMC_PA346054"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "emmanuel.maquet@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Maquet"
      , nomComplet = "Emmanuel Maquet"
      , particule = Nothing
      , photo = "/img/photos/346054.jpg"
      , prenom = "Emmanuel"
      , sitesWeb = []
      , slug = "emmanuel-maquet"
      , titre = "M."
      }
    , { circonscription = "Pas-de-Calais (2e circonscription)"
      , code = "OMC_PA334116"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "jacqueline.maquet@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Maquet"
      , nomComplet = "Jacqueline Maquet"
      , particule = Nothing
      , photo = "/img/photos/334116.jpg"
      , prenom = "Jacqueline"
      , sitesWeb = [ "http://jacquelinemaquet2017.fr/" ]
      , slug = "jacqueline-maquet"
      , titre = "Mme"
      }
    , { circonscription = "Hauts-de-Seine (7e circonscription)"
      , code = "OMC_PA721946"
      , collaborateurs =
            [ "okan-germiyan"
            , "barbara-wallon"
            , "jeremy-rojon"
            , "valerie-cordon"
            ]
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "jacques.marilossian@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Marilossian"
      , nomComplet = "Jacques Marilossian"
      , particule = Nothing
      , photo = "/img/photos/721946.jpg"
      , prenom = "Jacques"
      , sitesWeb = []
      , slug = "jacques-marilossian"
      , titre = "M."
      }
    , { circonscription = "Eure-et-Loir (2e circonscription)"
      , code = "OMC_PA606098"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "olivier.marleix@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Marleix"
      , nomComplet = "Olivier Marleix"
      , particule = Nothing
      , photo = "/img/photos/606098.jpg"
      , prenom = "Olivier"
      , sitesWeb = []
      , slug = "olivier-marleix"
      , titre = "M."
      }
    , { circonscription = "Essonne (2e circonscription)"
      , code = "OMC_PA2086"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "franck.marlin@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Marlin"
      , nomComplet = "Franck Marlin"
      , particule = Nothing
      , photo = "/img/photos/2086.jpg"
      , prenom = "Franck"
      , sitesWeb = []
      , slug = "franck-marlin"
      , titre = "M."
      }
    , { circonscription = "Charente (2e circonscription)"
      , code = "OMC_PA719080"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "sandra.marsaud@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Marsaud"
      , nomComplet = "Sandra Marsaud"
      , particule = Nothing
      , photo = "/img/photos/719080.jpg"
      , prenom = "Sandra"
      , sitesWeb = []
      , slug = "sandra-marsaud"
      , titre = "Mme"
      }
    , { circonscription = "Côte-d'Or (1re circonscription)"
      , code = "OMC_PA719154"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "didier.martin@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Martin"
      , nomComplet = "Didier Martin"
      , particule = Nothing
      , photo = "/img/photos/719154.jpg"
      , prenom = "Didier"
      , sitesWeb = []
      , slug = "didier-martin"
      , titre = "M."
      }
    , { circonscription = "Var (3e circonscription)"
      , code = "OMC_PA346218"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "jean-louis.masson@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Masson"
      , nomComplet = "Jean-Louis Masson"
      , particule = Nothing
      , photo = "/img/photos/346218.jpg"
      , prenom = "Jean-Louis"
      , sitesWeb = []
      , slug = "jean-louis-masson"
      , titre = "M."
      }
    , { circonscription = "Maine-et-Loire (5e circonscription)"
      , code = "OMC_PA720146"
      , collaborateurs =
            [ "marc-lerouge"
            , "carole-bossard-gautier"
            ]
      , commission = "Commission des affaires étrangères"
      , courriels = [ "denis.masseglia@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "La République en Marche"
      , nom = "Masséglia"
      , nomComplet = "Denis Masséglia"
      , particule = Nothing
      , photo = "/img/photos/720146.jpg"
      , prenom = "Denis"
      , sitesWeb = []
      , slug = "denis-masseglia"
      , titre = "M."
      }
    , { circonscription = "Guadeloupe (3e circonscription)"
      , code = "OMC_PA720976"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "max.mathiasin@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Mathiasin"
      , nomComplet = "Max Mathiasin"
      , particule = Nothing
      , photo = "/img/photos/720976.jpg"
      , prenom = "Max"
      , sitesWeb = []
      , slug = "max-mathiasin"
      , titre = "M."
      }
    , { circonscription = "Var (8e circonscription)"
      , code = "OMC_PA721800"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "fabien.matras@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Matras"
      , nomComplet = "Fabien Matras"
      , particule = Nothing
      , photo = "/img/photos/721800.jpg"
      , prenom = "Fabien"
      , sitesWeb = []
      , slug = "fabien-matras"
      , titre = "M."
      }
    , { circonscription = "Pyrénées-Atlantiques (2e circonscription)"
      , code = "OMC_PA720728"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "jean-paul.mattei@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Mattei"
      , nomComplet = "Jean-Paul Mattei"
      , particule = Nothing
      , photo = "/img/photos/720728.jpg"
      , prenom = "Jean-Paul"
      , sitesWeb = []
      , slug = "jean-paul-mattei"
      , titre = "M."
      }
    , { circonscription = "Var (4e circonscription)"
      , code = "OMC_PA721726"
      , collaborateurs = [ "sebstien-masteau" ]
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "sereine.mauborgne@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Mauborgne"
      , nomComplet = "Sereine Mauborgne"
      , particule = Nothing
      , photo = "/img/photos/721726.jpg"
      , prenom = "Sereine"
      , sitesWeb = []
      , slug = "sereine-mauborgne"
      , titre = "Mme"
      }
    , { circonscription = "Aveyron (1re circonscription)"
      , code = "OMC_PA677483"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "stephane.mazars@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "La République en Marche"
      , nom = "Mazars"
      , nomComplet = "Stéphane Mazars"
      , particule = Nothing
      , photo = "/img/photos/677483.jpg"
      , prenom = "Stéphane"
      , sitesWeb = []
      , slug = "stephane-mazars"
      , titre = "M."
      }
    , { circonscription = "Finistère (5e circonscription)"
      , code = "OMC_PA719338"
      , collaborateurs = [ "gautier-vernerey" ]
      , commission = "Commission des affaires économiques"
      , courriels = [ "graziella.melchior@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Melchior"
      , nomComplet = "Graziella Melchior"
      , particule = Nothing
      , photo = "/img/photos/719338.jpg"
      , prenom = "Graziella"
      , sitesWeb = []
      , slug = "graziella-melchior"
      , titre = "Mme"
      }
    , { circonscription = "Moselle (2e circonscription)"
      , code = "OMC_PA720370"
      , collaborateurs =
            [ "arthur-vidal"
            , "alice-genco"
            ]
      , commission = "Commission des affaires étrangères"
      , courriels = [ "ludovic.mendes@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Mendes"
      , nomComplet = "Ludovic Mendes"
      , particule = Nothing
      , photo = "/img/photos/720370.jpg"
      , prenom = "Ludovic"
      , sitesWeb = []
      , slug = "ludovic-mendes"
      , titre = "M."
      }
    , { circonscription = "Aube (3e circonscription)"
      , code = "OMC_PA2155"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "gerard.menuel@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Menuel"
      , nomComplet = "Gérard Menuel"
      , particule = Nothing
      , photo = "/img/photos/2155.jpg"
      , prenom = "Gérard"
      , sitesWeb = []
      , slug = "gerard-menuel"
      , titre = "M."
      }
    , { circonscription = "Charente (1re circonscription)"
      , code = "OMC_PA719072"
      , collaborateurs =
            [ "elise-haffen"
            , "fabien-buffeteau"
            ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "thomas.mesnier@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Mesnier"
      , nomComplet = "Thomas Mesnier"
      , particule = Nothing
      , photo = "/img/photos/719072.jpg"
      , prenom = "Thomas"
      , sitesWeb = []
      , slug = "thomas-mesnier"
      , titre = "M."
      }
    , { circonscription = "Gironde (9e circonscription)"
      , code = "OMC_PA719640"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "sophie.mette@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Mette"
      , nomComplet = "Sophie Mette"
      , particule = Nothing
      , photo = "/img/photos/719640.jpg"
      , prenom = "Sophie"
      , sitesWeb = []
      , slug = "sophie-mette"
      , titre = "Mme"
      }
    , { circonscription = "Corrèze (2e circonscription)"
      , code = "OMC_PA719272"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "frederique.meunier@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Meunier"
      , nomComplet = "Frédérique Meunier"
      , particule = Nothing
      , photo = "/img/photos/719272.jpg"
      , prenom = "Frédérique"
      , sitesWeb = []
      , slug = "frederique-meunier"
      , titre = "Mme"
      }
    , { circonscription = "Isère (10e circonscription)"
      , code = "OMC_PA719882"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "marjolaine.meynier-millefert@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Meynier-Millefert"
      , nomComplet = "Marjolaine Meynier-Millefert"
      , particule = Nothing
      , photo = "/img/photos/719882.jpg"
      , prenom = "Marjolaine"
      , sitesWeb = []
      , slug = "marjolaine-meynier-millefert"
      , titre = "Mme"
      }
    , { circonscription = "Bouches-du-Rhône (16e circonscription)"
      , code = "OMC_PA719130"
      , collaborateurs = [ "flore-hennion" ]
      , commission = "Commission des affaires étrangères"
      , courriels = [ "monica.michel@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Michel"
      , nomComplet = "Monica Michel"
      , particule = Nothing
      , photo = "/img/photos/719130.jpg"
      , prenom = "Monica"
      , sitesWeb = []
      , slug = "monica-michel"
      , titre = "Mme"
      }
    , { circonscription = "Var (5e circonscription)"
      , code = "OMC_PA721734"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "Philippe.Michel-Kleisbauer@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Michel-Kleisbauer"
      , nomComplet = "Philippe Michel-Kleisbauer"
      , particule = Nothing
      , photo = "/img/photos/721734.jpg"
      , prenom = "Philippe"
      , sitesWeb = [ "http://www.philippe-michel.fr" ]
      , slug = "philippe-michel-kleisbauer"
      , titre = "M."
      }
    , { circonscription = "Bas-Rhin (1re circonscription)"
      , code = "OMC_PA720738"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "thierry.michels@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Michels"
      , nomComplet = "Thierry Michels"
      , particule = Nothing
      , photo = "/img/photos/720738.jpg"
      , prenom = "Thierry"
      , sitesWeb = [ "http://www.thierrymichels2017.eu" ]
      , slug = "thierry-michels"
      , titre = "M."
      }
    , { circonscription = "Savoie (4e circonscription)"
      , code = "OMC_PA721418"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "patrick.mignola@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Mignola"
      , nomComplet = "Patrick Mignola"
      , particule = Nothing
      , photo = "/img/photos/721418.jpg"
      , prenom = "Patrick"
      , sitesWeb = []
      , slug = "patrick-mignola"
      , titre = "M."
      }
    , { circonscription = "Yvelines (9e circonscription)"
      , code = "OMC_PA721976"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "bruno.millienne@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Millienne"
      , nomComplet = "Bruno Millienne"
      , particule = Nothing
      , photo = "/img/photos/721976.jpg"
      , prenom = "Bruno"
      , sitesWeb = []
      , slug = "bruno-millienne"
      , titre = "M."
      }
    , { circonscription = "Oise (7e circonscription)"
      , code = "OMC_PA720630"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "maxime.minot@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "Les Républicains"
      , nom = "Minot"
      , nomComplet = "Maxime Minot"
      , particule = Nothing
      , photo = "/img/photos/720630.jpg"
      , prenom = "Maxime"
      , sitesWeb = []
      , slug = "maxime-minot"
      , titre = "M."
      }
    , { circonscription = "Hérault (1re circonscription)"
      , code = "OMC_PA719668"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "patricia.miralles@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Mirallès"
      , nomComplet = "Patricia Mirallès"
      , particule = Nothing
      , photo = "/img/photos/719668.jpg"
      , prenom = "Patricia"
      , sitesWeb = []
      , slug = "patricia-miralles"
      , titre = "Mme"
      }
    , { circonscription = "Loire (2e circonscription)"
      , code = "OMC_PA719952"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "jean-michel.mis@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Mis"
      , nomComplet = "Jean-Michel Mis"
      , particule = Nothing
      , photo = "/img/photos/719952.jpg"
      , prenom = "Jean-Michel"
      , sitesWeb = []
      , slug = "jean-michel-mis"
      , titre = "M."
      }
    , { circonscription = "Morbihan (4e circonscription)"
      , code = "OMC_PA607619"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "paul.molac@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Molac"
      , nomComplet = "Paul Molac"
      , particule = Nothing
      , photo = "/img/photos/607619.jpg"
      , prenom = "Paul"
      , sitesWeb = [ "http://paulmolac.bzh" ]
      , slug = "paul-molac"
      , titre = "M."
      }
    , { circonscription = "Essonne (6e circonscription)"
      , code = "OMC_PA721670"
      , collaborateurs =
            [ "ludovic-balzeau"
            , "laurence-marty"
            ]
      , commission = "Commission des finances"
      , courriels = [ "amelie.demontchalin@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Montchalin"
      , nomComplet = "Amélie de Montchalin"
      , particule = Just "de"
      , photo = "/img/photos/721670.jpg"
      , prenom = "Amélie"
      , sitesWeb = [ "http://ameliedemontchalin.en-marche.fr/" ]
      , slug = "amelie-de-montchalin"
      , titre = "Mme"
      }
    , { circonscription = "Creuse (1re circonscription)"
      , code = "OMC_PA719242"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "jean-baptiste.moreau@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Moreau"
      , nomComplet = "Jean-Baptiste Moreau"
      , particule = Nothing
      , photo = "/img/photos/719242.jpg"
      , prenom = "Jean-Baptiste"
      , sitesWeb = [ "http://www.jeanbaptistemoreau.fr" ]
      , slug = "jean-baptiste-moreau"
      , titre = "M."
      }
    , { circonscription = "Lozère (1re circonscription)"
      , code = "OMC_PA266788"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "pierre.morel-a-lhuissier@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Morel-À-L'Huissier"
      , nomComplet = "Pierre Morel-À-L'Huissier"
      , particule = Nothing
      , photo = "/img/photos/266788.jpg"
      , prenom = "Pierre"
      , sitesWeb = [ "http://www.pierre-morel.fr" ]
      , slug = "pierre-morel-a-l-huissier"
      , titre = "M."
      }
    , { circonscription = "Vaucluse (3e circonscription)"
      , code = "OMC_PA721876"
      , collaborateurs = [ "maurice-moise-houzard" ]
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "Adrien.Morenas@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Morenas"
      , nomComplet = "Adrien Morenas"
      , particule = Nothing
      , photo = "/img/photos/721876.jpg"
      , prenom = "Adrien"
      , sitesWeb = []
      , slug = "adrien-morenas"
      , titre = "M."
      }
    , { circonscription = "Isère (6e circonscription)"
      , code = "OMC_PA719850"
      , collaborateurs = [ "yannick-loustau" ]
      , commission = "Commission des finances"
      , courriels = [ "cendra.motin@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Motin"
      , nomComplet = "Cendra Motin"
      , particule = Nothing
      , photo = "/img/photos/719850.jpg"
      , prenom = "Cendra"
      , sitesWeb = []
      , slug = "cendra-motin"
      , titre = "Mme"
      }
    , { circonscription = "Val-d'Oise (4e circonscription)"
      , code = "OMC_PA720908"
      , collaborateurs = [ "juan-diaz-del-cano" ]
      , commission = "Commission des lois"
      , courriels = [ "naima.moutchou@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Moutchou"
      , nomComplet = "Naïma Moutchou"
      , particule = Nothing
      , photo = "/img/photos/720908.jpg"
      , prenom = "Naïma"
      , sitesWeb = []
      , slug = "naima-moutchou"
      , titre = "Mme"
      }
    , { circonscription = "Val-d'Oise (1re circonscription)"
      , code = "OMC_PA721254"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "isabelle.muller-quoy@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Muller-Quoy"
      , nomComplet = "Isabelle Muller-Quoy"
      , particule = Nothing
      , photo = "/img/photos/721254.jpg"
      , prenom = "Isabelle"
      , sitesWeb = []
      , slug = "isabelle-muller-quoy"
      , titre = "Mme"
      }
    , { circonscription = "Var (2e circonscription)"
      , code = "OMC_PA721656"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "cecile.muschotti@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Muschotti"
      , nomComplet = "Cécile Muschotti"
      , particule = Nothing
      , photo = "/img/photos/721656.jpg"
      , prenom = "Cécile"
      , sitesWeb = [ "http://muschotti.fr" ]
      , slug = "cecile-muschotti"
      , titre = "Mme"
      }
    ]


deputes4 : Deputes
deputes4 =
    [ { circonscription = "Bouches-du-Rhône (4e circonscription)"
      , code = "OMC_PA2150"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "jean-luc.melenchon@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La France insoumise"
      , nom = "Mélenchon"
      , nomComplet = "Jean-Luc Mélenchon"
      , particule = Nothing
      , photo = "/img/photos/2150.jpg"
      , prenom = "Jean-Luc"
      , sitesWeb = [ "http://www.melenchon.fr" ]
      , slug = "jean-luc-melenchon"
      , titre = "M."
      }
    , { circonscription = "Hérault (6e circonscription)"
      , code = "OMC_PA719608"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "emmanuelle.menard@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Non inscrit"
      , nom = "Ménard"
      , nomComplet = "Emmanuelle Ménard"
      , particule = Nothing
      , photo = "/img/photos/719608.jpg"
      , prenom = "Emmanuelle"
      , sitesWeb = []
      , slug = "emmanuelle-menard"
      , titre = "Mme"
      }
    , { circonscription = "Haute-Garonne (9e circonscription)"
      , code = "OMC_PA719456"
      , collaborateurs = [ "anne-marie-ho-dinh" ]
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "sandrine.morch@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Mörch"
      , nomComplet = "Sandrine Mörch"
      , particule = Nothing
      , photo = "/img/photos/719456.jpg"
      , prenom = "Sandrine"
      , sitesWeb = []
      , slug = "sandrine-morch"
      , titre = "Mme"
      }
    , { circonscription = "Haute-Garonne (10e circonscription)"
      , code = "OMC_PA719464"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "sebastien.nadot@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Nadot"
      , nomComplet = "Sébastien Nadot"
      , particule = Nothing
      , photo = "/img/photos/719464.jpg"
      , prenom = "Sébastien"
      , sitesWeb = []
      , slug = "sebastien-nadot"
      , titre = "M."
      }
    , { circonscription = "Vosges (3e circonscription)"
      , code = "OMC_PA721486"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "christophe.naegelen@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Naegelen"
      , nomComplet = "Christophe Naegelen"
      , particule = Nothing
      , photo = "/img/photos/721486.jpg"
      , prenom = "Christophe"
      , sitesWeb = []
      , slug = "christophe-naegelen"
      , titre = "M."
      }
    , { circonscription = "Martinique (2e circonscription)"
      , code = "OMC_PA610634"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "brunonestor.azerot@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "Gauche démocrate et républicaine"
      , nom = "Nestor Azerot"
      , nomComplet = "Bruno Nestor Azerot"
      , particule = Nothing
      , photo = "/img/photos/610634.jpg"
      , prenom = "Bruno"
      , sitesWeb = []
      , slug = "bruno-nestor-azerot"
      , titre = "M."
      }
    , { circonscription = "Martinique (4e circonscription)"
      , code = "OMC_PA610654"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "jean-philippe.nilor@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Gauche démocrate et républicaine"
      , nom = "Nilor"
      , nomComplet = "Jean-Philippe Nilor"
      , particule = Nothing
      , photo = "/img/photos/610654.jpg"
      , prenom = "Jean-Philippe"
      , sitesWeb = []
      , slug = "jean-philippe-nilor"
      , titre = "M."
      }
    , { circonscription = "Haute-Garonne (4e circonscription)"
      , code = "OMC_PA719520"
      , collaborateurs =
            [ "amandine-pasquier"
            , "marie-claire-constans"
            ]
      , commission = "Commission des affaires économiques"
      , courriels = [ "mickael.nogal@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "La République en Marche"
      , nom = "Nogal"
      , nomComplet = "Mickaël Nogal"
      , particule = Nothing
      , photo = "/img/photos/719520.jpg"
      , prenom = "Mickaël"
      , sitesWeb = [ "http://www.mickaelnogal.fr" ]
      , slug = "mickael-nogal"
      , titre = "M."
      }
    , { circonscription = "Orne (3e circonscription)"
      , code = "OMC_PA720644"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "jerome.nury@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Nury"
      , nomComplet = "Jérôme Nury"
      , particule = Nothing
      , photo = "/img/photos/720644.jpg"
      , prenom = "Jérôme"
      , sitesWeb = []
      , slug = "jerome-nury"
      , titre = "M."
      }
    , { circonscription = "Paris (16e circonscription)"
      , code = "OMC_PA721956"
      , collaborateurs =
            [ "samy-lemedy"
            , "benjamin-baudry"
            , "rhizlane-bouachra"
            ]
      , commission = "Commission des affaires étrangères"
      , courriels = [ "Delphine.O@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "O"
      , nomComplet = "Delphine O"
      , particule = Nothing
      , photo = "/img/photos/721956.jpg"
      , prenom = "Delphine"
      , sitesWeb = []
      , slug = "delphine-o"
      , titre = "Mme"
      }
    , { circonscription = "Eure (5e circonscription)"
      , code = "OMC_PA719364"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "claire.opetit@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "O'Petit"
      , nomComplet = "Claire O'Petit"
      , particule = Nothing
      , photo = "/img/photos/719364.jpg"
      , prenom = "Claire"
      , sitesWeb = [ "http://www.claireopetit.fr" ]
      , slug = "claire-o-petit"
      , titre = "Mme"
      }
    , { circonscription = "Paris (17e circonscription)"
      , code = "OMC_PA721960"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "daniele.obono@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La France insoumise"
      , nom = "Obono"
      , nomComplet = "Danièle Obono"
      , particule = Nothing
      , photo = "/img/photos/721960.jpg"
      , prenom = "Danièle"
      , sitesWeb = []
      , slug = "daniele-obono"
      , titre = "Mme"
      }
    , { circonscription = "Loire-Atlantique (2e circonscription)"
      , code = "OMC_PA720108"
      , collaborateurs = [ "morgan-simon" ]
      , commission = "Commission des affaires économiques"
      , courriels = [ "valerie.oppelt@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Oppelt"
      , nomComplet = "Valérie Oppelt"
      , particule = Nothing
      , photo = "/img/photos/720108.jpg"
      , prenom = "Valérie"
      , sitesWeb = []
      , slug = "valerie-oppelt"
      , titre = "Mme"
      }
    , { circonscription = "Maine-et-Loire (1re circonscription)"
      , code = "OMC_PA720278"
      , collaborateurs =
            [ "eleonore-cartillier"
            , "isabelle-prime"
            , "emeline-robert"
            ]
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels =
            [ "bureau-m-orphelin@assemblee-nationale.fr"
            , "matthieu.orphelin@assemblee-nationale.fr"
            ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Orphelin"
      , nomComplet = "Matthieu Orphelin"
      , particule = Nothing
      , photo = "/img/photos/720278.jpg"
      , prenom = "Matthieu"
      , sitesWeb = [ "http://matthieuorphelin.org" ]
      , slug = "matthieu-orphelin"
      , titre = "M."
      }
    , { circonscription = "Nord (8e circonscription)"
      , code = "OMC_PA720492"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "catherine.osson@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Osson"
      , nomComplet = "Catherine Osson"
      , particule = Nothing
      , photo = "/img/photos/720492.jpg"
      , prenom = "Catherine"
      , sitesWeb = []
      , slug = "catherine-osson"
      , titre = "Mme"
      }
    , { circonscription = "Morbihan (2e circonscription)"
      , code = "OMC_PA720334"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "jimmy.pahun@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Pahun"
      , nomComplet = "Jimmy Pahun"
      , particule = Nothing
      , photo = "/img/photos/720334.jpg"
      , prenom = "Jimmy"
      , sitesWeb = []
      , slug = "jimmy-pahun"
      , titre = "M."
      }
    , { circonscription = "Pas-de-Calais (10e circonscription)"
      , code = "OMC_PA720606"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "ludovic.pajot@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Non inscrit"
      , nom = "Pajot"
      , nomComplet = "Ludovic Pajot"
      , particule = Nothing
      , photo = "/img/photos/720606.jpg"
      , prenom = "Ludovic"
      , sitesWeb = []
      , slug = "ludovic-pajot"
      , titre = "M."
      }
    , { circonscription = "Meurthe-et-Moselle (3e circonscription)"
      , code = "OMC_PA720402"
      , collaborateurs =
            [ "francois-plaine"
            , "florent-poindron"
            ]
      , commission = "Commission des finances"
      , courriels = [ "xavier.paluszkiewicz@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Paluszkiewicz"
      , nomComplet = "Xavier Paluszkiewicz"
      , particule = Nothing
      , photo = "/img/photos/720402.jpg"
      , prenom = "Xavier"
      , sitesWeb = []
      , slug = "xavier-paluszkiewicz"
      , titre = "M."
      }
    , { circonscription = "Meuse (1re circonscription)"
      , code = "OMC_PA333421"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "bertrand.pancher@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Pancher"
      , nomComplet = "Bertrand Pancher"
      , particule = Nothing
      , photo = "/img/photos/333421.jpg"
      , prenom = "Bertrand"
      , sitesWeb = [ "http://www.bertrandpancher.fr/" ]
      , slug = "bertrand-pancher"
      , titre = "M."
      }
    , { circonscription = "Gironde (8e circonscription)"
      , code = "OMC_PA719632"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "sophie.panonacle@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Panonacle"
      , nomComplet = "Sophie Panonacle"
      , particule = Nothing
      , photo = "/img/photos/719632.jpg"
      , prenom = "Sophie"
      , sitesWeb = []
      , slug = "sophie-panonacle"
      , titre = "Mme"
      }
    , { circonscription = "Val-de-Marne (10e circonscription)"
      , code = "OMC_PA720892"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "mathilde.panot@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La France insoumise"
      , nom = "Panot"
      , nomComplet = "Mathilde Panot"
      , particule = Nothing
      , photo = "/img/photos/720892.jpg"
      , prenom = "Mathilde"
      , sitesWeb = []
      , slug = "mathilde-panot"
      , titre = "Mme"
      }
    , { circonscription = "Seine-et-Marne (6e circonscription)"
      , code = "OMC_PA721636"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "jean-francois.parigi@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Parigi"
      , nomComplet = "Jean-François Parigi"
      , particule = Nothing
      , photo = "/img/photos/721636.jpg"
      , prenom = "Jean-François"
      , sitesWeb = []
      , slug = "jean-francois-parigi"
      , titre = "M."
      }
    , { circonscription = "Côte-d'Or (5e circonscription)"
      , code = "OMC_PA719202"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "didier.paris@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "La République en Marche"
      , nom = "Paris"
      , nomComplet = "Didier Paris"
      , particule = Nothing
      , photo = "/img/photos/719202.jpg"
      , prenom = "Didier"
      , sitesWeb = []
      , slug = "didier-paris"
      , titre = "M."
      }
    , { circonscription = "Val-d'Oise (9e circonscription)"
      , code = "OMC_PA720944"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "zivka.park@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Park"
      , nomComplet = "Zivka Park"
      , particule = Nothing
      , photo = "/img/photos/720944.jpg"
      , prenom = "Zivka"
      , sitesWeb = []
      , slug = "zivka-park"
      , titre = "Mme"
      }
    , { circonscription = "Paris (15e circonscription)"
      , code = "OMC_PA335532"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "george.pau-langevin@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Nouvelle Gauche"
      , nom = "Pau-Langevin"
      , nomComplet = "George Pau-Langevin"
      , particule = Nothing
      , photo = "/img/photos/335532.jpg"
      , prenom = "George"
      , sitesWeb = [ "http://www.georgepaulangevin.fr" ]
      , slug = "george-pau-langevin"
      , titre = "Mme"
      }
    , { circonscription = "Alpes-Maritimes (7e circonscription)"
      , code = "OMC_PA718784"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "eric.pauget@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Pauget"
      , nomComplet = "Éric Pauget"
      , particule = Nothing
      , photo = "/img/photos/718784.jpg"
      , prenom = "Éric"
      , sitesWeb = []
      , slug = "eric-pauget"
      , titre = "M."
      }
    , { circonscription = "Morbihan (1re circonscription)"
      , code = "OMC_PA607595"
      , collaborateurs = [ "charline-birault" ]
      , commission = "Commission des finances"
      , courriels = [ "herve.pellois@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Pellois"
      , nomComplet = "Hervé Pellois"
      , particule = Nothing
      , photo = "/img/photos/607595.jpg"
      , prenom = "Hervé"
      , sitesWeb = []
      , slug = "herve-pellois"
      , titre = "M."
      }
    , { circonscription = "Loir-et-Cher (2e circonscription)"
      , code = "OMC_PA719946"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "guillaume.peltier@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Peltier"
      , nomComplet = "Guillaume Peltier"
      , particule = Nothing
      , photo = "/img/photos/719946.jpg"
      , prenom = "Guillaume"
      , sitesWeb = [ "http://www.guillaumepeltier.fr" ]
      , slug = "guillaume-peltier"
      , titre = "M."
      }
    , { circonscription = "Aude (2e circonscription)"
      , code = "OMC_PA718802"
      , collaborateurs = [ "gabrielle-pollet" ]
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "alain.perea@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "La République en Marche"
      , nom = "Perea"
      , nomComplet = "Alain Perea"
      , particule = Nothing
      , photo = "/img/photos/718802.jpg"
      , prenom = "Alain"
      , sitesWeb = [ "http://www.alainperea.fr" ]
      , slug = "alain-perea"
      , titre = "M."
      }
    , { circonscription = "Nièvre (2e circonscription)"
      , code = "OMC_PA720414"
      , collaborateurs = [ "catherine-maligne" ]
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "patrice.perrot@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Perrot"
      , nomComplet = "Patrice Perrot"
      , particule = Nothing
      , photo = "/img/photos/720414.jpg"
      , prenom = "Patrice"
      , sitesWeb = []
      , slug = "patrice-perrot"
      , titre = "M."
      }
    , { circonscription = "Rhône (9e circonscription)"
      , code = "OMC_PA2377"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "bernard.perrut@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "Les Républicains"
      , nom = "Perrut"
      , nomComplet = "Bernard Perrut"
      , particule = Nothing
      , photo = "/img/photos/2377.jpg"
      , prenom = "Bernard"
      , sitesWeb = [ "http://www.bperrut.fr" ]
      , slug = "bernard-perrut"
      , titre = "M."
      }
    , { circonscription = "Paris (6e circonscription)"
      , code = "OMC_PA721568"
      , collaborateurs =
            [ "victor-lailler"
            , "nam-hoang"
            , "melodie-ambroise"
            ]
      , commission = "Commission des finances"
      , courriels = [ "pierre.person@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "La République en Marche"
      , nom = "Person"
      , nomComplet = "Pierre Person"
      , particule = Nothing
      , photo = "/img/photos/721568.jpg"
      , prenom = "Pierre"
      , sitesWeb = []
      , slug = "pierre-person"
      , titre = "M."
      }
    , { circonscription = "Bouches-du-Rhône (14e circonscription)"
      , code = "OMC_PA718930"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "anne-laurence.petel@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "La République en Marche"
      , nom = "Petel"
      , nomComplet = "Anne-Laurence Petel"
      , particule = Nothing
      , photo = "/img/photos/718930.jpg"
      , prenom = "Anne-Laurence"
      , sitesWeb = []
      , slug = "anne-laurence-petel"
      , titre = "Mme"
      }
    , { circonscription = "Français établis hors de France (7e circonscription)"
      , code = "OMC_PA721182"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "frederic.petit@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Petit"
      , nomComplet = "Frédéric Petit"
      , particule = Nothing
      , photo = "/img/photos/721182.jpg"
      , prenom = "Frédéric"
      , sitesWeb = []
      , slug = "frederic-petit"
      , titre = "M."
      }
    , { circonscription = "Val-de-Marne (4e circonscription)"
      , code = "OMC_PA721234"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "maud.petit@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Petit"
      , nomComplet = "Maud Petit"
      , particule = Nothing
      , photo = "/img/photos/721234.jpg"
      , prenom = "Maud"
      , sitesWeb = [ "http://www.maudpetit2017.fr" ]
      , slug = "maud-petit"
      , titre = "Mme"
      }
    , { circonscription = "Nord (9e circonscription)"
      , code = "OMC_PA720500"
      , collaborateurs =
            [ "cedric-pruvost"
            , "philippe-henry-person-fauqueu"
            , "raphael-martinez"
            ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "valerie.petit@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Petit"
      , nomComplet = "Valérie Petit"
      , particule = Nothing
      , photo = "/img/photos/720500.jpg"
      , prenom = "Valérie"
      , sitesWeb = []
      , slug = "valerie-petit"
      , titre = "Mme"
      }
    , { circonscription = "Seine-Saint-Denis (2e circonscription)"
      , code = "OMC_PA721270"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "stephane.peu@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Gauche démocrate et républicaine"
      , nom = "Peu"
      , nomComplet = "Stéphane Peu"
      , particule = Nothing
      , photo = "/img/photos/721270.jpg"
      , prenom = "Stéphane"
      , sitesWeb =
            [ "http://www.stephanepeu2017.fr"
            , "http://www.stephanepeu.com"
            ]
      , slug = "stephane-peu"
      , titre = "M."
      }
    , { circonscription = "Allier (3e circonscription)"
      , code = "OMC_PA718736"
      , collaborateurs =
            [ "thibaud-roblin"
            , "valentin-framont"
            ]
      , commission = "Commission des finances"
      , courriels = [ "benedicte.peyrol@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Peyrol"
      , nomComplet = "Bénédicte Peyrol"
      , particule = Nothing
      , photo = "/img/photos/718736.jpg"
      , prenom = "Bénédicte"
      , sitesWeb = [ "http://benedictepeyrol.en-marche.fr/" ]
      , slug = "benedicte-peyrol"
      , titre = "Mme"
      }
    , { circonscription = "Seine-et-Marne (9e circonscription)"
      , code = "OMC_PA721710"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "michele.peyron@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Peyron"
      , nomComplet = "Michèle Peyron"
      , particule = Nothing
      , photo = "/img/photos/721710.jpg"
      , prenom = "Michèle"
      , sitesWeb = []
      , slug = "michele-peyron"
      , titre = "Mme"
      }
    , { circonscription = "Sarthe (1re circonscription)"
      , code = "OMC_PA721372"
      , collaborateurs = [ "myriam-souami" ]
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "damien.pichereau@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "La République en Marche"
      , nom = "Pichereau"
      , nomComplet = "Damien Pichereau"
      , particule = Nothing
      , photo = "/img/photos/721372.jpg"
      , prenom = "Damien"
      , sitesWeb = [ "http://www.damien-pichereau.fr" ]
      , slug = "damien-pichereau"
      , titre = "M."
      }
    , { circonscription = "Nord (11e circonscription)"
      , code = "OMC_PA720512"
      , collaborateurs = [ "eric-van-roy" ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "laurent.pietraszewski@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Pietraszewski"
      , nomComplet = "Laurent Pietraszewski"
      , particule = Nothing
      , photo = "/img/photos/720512.jpg"
      , prenom = "Laurent"
      , sitesWeb = [ "http://www.laurentpietraszewski.fr" ]
      , slug = "laurent-pietraszewski"
      , titre = "M."
      }
    , { circonscription = "Tarn-et-Garonne (2e circonscription)"
      , code = "OMC_PA336175"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "sylvia.pinel@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Non inscrit"
      , nom = "Pinel"
      , nomComplet = "Sylvia Pinel"
      , particule = Nothing
      , photo = "/img/photos/336175.jpg"
      , prenom = "Sylvia"
      , sitesWeb = []
      , slug = "sylvia-pinel"
      , titre = "Mme"
      }
    , { circonscription = "Puy-de-Dôme (2e circonscription)"
      , code = "OMC_PA608172"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "christine.piresbeaune@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Nouvelle Gauche"
      , nom = "Pires Beaune"
      , nomComplet = "Christine Pires Beaune"
      , particule = Nothing
      , photo = "/img/photos/608172.jpg"
      , prenom = "Christine"
      , sitesWeb = [ "http://www.christinepiresbeaune.fr" ]
      , slug = "christine-pires-beaune"
      , titre = "Mme"
      }
    , { circonscription = "Yvelines (3e circonscription)"
      , code = "OMC_PA721844"
      , collaborateurs = [ "isadora-hugo-provost" ]
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "beatrice.piron@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Piron"
      , nomComplet = "Béatrice Piron"
      , particule = Nothing
      , photo = "/img/photos/721844.jpg"
      , prenom = "Béatrice"
      , sitesWeb = [ "http://www.beatricepiron.fr" ]
      , slug = "beatrice-piron"
      , titre = "Mme"
      }
    , { circonscription = "Bouches-du-Rhône (2e circonscription)"
      , code = "OMC_PA718910"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "claire.pitollat@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Pitollat"
      , nomComplet = "Claire Pitollat"
      , particule = Nothing
      , photo = "/img/photos/718910.jpg"
      , prenom = "Claire"
      , sitesWeb = [ "http://www.claire-pitollat.fr/" ]
      , slug = "claire-pitollat"
      , titre = "Mme"
      }
    , { circonscription = "Ardennes (1re circonscription)"
      , code = "OMC_PA267260"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "berengere.poletti@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "Les Républicains"
      , nom = "Poletti"
      , nomComplet = "Bérengère Poletti"
      , particule = Nothing
      , photo = "/img/photos/267260.jpg"
      , prenom = "Bérengère"
      , sitesWeb = []
      , slug = "berengere-poletti"
      , titre = "Mme"
      }
    , { circonscription = "Wallis-et-Futuna (1re circonscription)"
      , code = "OMC_PA693008"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "napole.polutele@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Polutele"
      , nomComplet = "Napole Polutele"
      , particule = Nothing
      , photo = "/img/photos/693008.jpg"
      , prenom = "Napole"
      , sitesWeb = []
      , slug = "napole-polutele"
      , titre = "M."
      }
    , { circonscription = "Somme (2e circonscription)"
      , code = "OMC_PA609520"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "barbara.pompili@assemblee-nationale.fr" ]
      , fonctionCommission = "Président"
      , groupe = "La République en Marche"
      , nom = "Pompili"
      , nomComplet = "Barbara Pompili"
      , particule = Nothing
      , photo = "/img/photos/609520.jpg"
      , prenom = "Barbara"
      , sitesWeb =
            [ "http://barbarapompili.fr"
            , "http://www.barbarapompili.fr"
            ]
      , slug = "barbara-pompili"
      , titre = "Mme"
      }
    , { circonscription = "Pas-de-Calais (5e circonscription)"
      , code = "OMC_PA2449"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "jean-pierre.pont@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Pont"
      , nomComplet = "Jean-Pierre Pont"
      , particule = Nothing
      , photo = "/img/photos/2449.jpg"
      , prenom = "Jean-Pierre"
      , sitesWeb = []
      , slug = "jean-pierre-pont"
      , titre = "M."
      }
    , { circonscription = "Haute-Garonne (5e circonscription)"
      , code = "OMC_PA719528"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "jean-francois.portarrieu@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Portarrieu"
      , nomComplet = "Jean-François Portarrieu"
      , particule = Nothing
      , photo = "/img/photos/719528.jpg"
      , prenom = "Jean-François"
      , sitesWeb = [ "http://portarrieu.fr" ]
      , slug = "jean-francois-portarrieu"
      , titre = "M."
      }
    , { circonscription = "Meurthe-et-Moselle (5e circonscription)"
      , code = "OMC_PA607553"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "dominique.potier@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Nouvelle Gauche"
      , nom = "Potier"
      , nomComplet = "Dominique Potier"
      , particule = Nothing
      , photo = "/img/photos/607553.jpg"
      , prenom = "Dominique"
      , sitesWeb = [ "http://www.dominiquepotier.com" ]
      , slug = "dominique-potier"
      , titre = "M."
      }
    , { circonscription = "Pas-de-Calais (8e circonscription)"
      , code = "OMC_PA720590"
      , collaborateurs = [ "maxime-lelievre" ]
      , commission = "Commission des affaires économiques"
      , courriels = [ "benoit.potterie@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Potterie"
      , nomComplet = "Benoit Potterie"
      , particule = Nothing
      , photo = "/img/photos/720590.jpg"
      , prenom = "Benoit"
      , sitesWeb = [ "http://benoitpotterie.en-marche.fr" ]
      , slug = "benoit-potterie"
      , titre = "M."
      }
    , { circonscription = "Pyrénées-Atlantiques (1re circonscription)"
      , code = "OMC_PA720720"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "josy.poueyto@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Poueyto"
      , nomComplet = "Josy Poueyto"
      , particule = Nothing
      , photo = "/img/photos/720720.jpg"
      , prenom = "Josy"
      , sitesWeb = []
      , slug = "josy-poueyto"
      , titre = "Mme"
      }
    , { circonscription = "Gironde (6e circonscription)"
      , code = "OMC_PA719600"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "eric.poulliat@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Poulliat"
      , nomComplet = "Éric Poulliat"
      , particule = Nothing
      , photo = "/img/photos/719600.jpg"
      , prenom = "Éric"
      , sitesWeb = []
      , slug = "eric-poulliat"
      , titre = "M."
      }
    , { circonscription = "Yvelines (6e circonscription)"
      , code = "OMC_PA721916"
      , collaborateurs = [ "nicolas-turmeau" ]
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "natalia.pouzyreff@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "La République en Marche"
      , nom = "Pouzyreff"
      , nomComplet = "Natalia Pouzyreff"
      , particule = Nothing
      , photo = "/img/photos/721916.jpg"
      , prenom = "Natalia"
      , sitesWeb = []
      , slug = "natalia-pouzyreff"
      , titre = "Mme"
      }
    , { circonscription = "Lot (1re circonscription)"
      , code = "OMC_PA720100"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "aurelien.pradie@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Pradié"
      , nomComplet = "Aurélien Pradié"
      , particule = Nothing
      , photo = "/img/photos/720100.jpg"
      , prenom = "Aurélien"
      , sitesWeb = []
      , slug = "aurelien-pradie"
      , titre = "M."
      }
    , { circonscription = "Gironde (3e circonscription)"
      , code = "OMC_PA719578"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "loic.prudhomme@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La France insoumise"
      , nom = "Prud'homme"
      , nomComplet = "Loïc Prud'homme"
      , particule = Nothing
      , photo = "/img/photos/719578.jpg"
      , prenom = "Loïc"
      , sitesWeb = []
      , slug = "loic-prud-homme"
      , titre = "M."
      }
    , { circonscription = "Orne (1re circonscription)"
      , code = "OMC_PA608011"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "joaquim.pueyo@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "Nouvelle Gauche"
      , nom = "Pueyo"
      , nomComplet = "Joaquim Pueyo"
      , particule = Nothing
      , photo = "/img/photos/608011.jpg"
      , prenom = "Joaquim"
      , sitesWeb = []
      , slug = "joaquim-pueyo"
      , titre = "M."
      }
    , { circonscription = "Val-d'Oise (8e circonscription)"
      , code = "OMC_PA405480"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "francois.pupponi@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Nouvelle Gauche"
      , nom = "Pupponi"
      , nomComplet = "François Pupponi"
      , particule = Nothing
      , photo = "/img/photos/405480.jpg"
      , prenom = "François"
      , sitesWeb = []
      , slug = "francois-pupponi"
      , titre = "M."
      }
    , { circonscription = "Nord (1re circonscription)"
      , code = "OMC_PA720422"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "adrien.quatennens@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La France insoumise"
      , nom = "Quatennens"
      , nomComplet = "Adrien Quatennens"
      , particule = Nothing
      , photo = "/img/photos/720422.jpg"
      , prenom = "Adrien"
      , sitesWeb = []
      , slug = "adrien-quatennens"
      , titre = "M."
      }
    , { circonscription = "Charente-Maritime (5e circonscription)"
      , code = "OMC_PA2492"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "didier.quentin@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Quentin"
      , nomComplet = "Didier Quentin"
      , particule = Nothing
      , photo = "/img/photos/2492.jpg"
      , prenom = "Didier"
      , sitesWeb = [ "http://www.didierquentin.com" ]
      , slug = "didier-quentin"
      , titre = "M."
      }
    , { circonscription = "Eure (4e circonscription)"
      , code = "OMC_PA643127"
      , collaborateurs =
            [ "irving-verneuil"
            , "jacob-celik"
            ]
      , commission = "Commission des lois"
      , courriels = [ "bruno.questel@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Questel"
      , nomComplet = "Bruno Questel"
      , particule = Nothing
      , photo = "/img/photos/643127.jpg"
      , prenom = "Bruno"
      , sitesWeb = [ "http://www.brunoquestel.fr" ]
      , slug = "bruno-questel"
      , titre = "M."
      }
    , { circonscription = "Tarn-et-Garonne (1re circonscription)"
      , code = "OMC_PA609590"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "valerie.rabault@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "Nouvelle Gauche"
      , nom = "Rabault"
      , nomComplet = "Valérie Rabault"
      , particule = Nothing
      , photo = "/img/photos/609590.jpg"
      , prenom = "Valérie"
      , sitesWeb = [ "http://www.valerierabault.fr" ]
      , slug = "valerie-rabault"
      , titre = "Mme"
      }
    , { circonscription = "Bouches-du-Rhône (5e circonscription)"
      , code = "OMC_PA718944"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "cathy.racon-bouzon@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Racon-Bouzon"
      , nomComplet = "Cathy Racon-Bouzon"
      , particule = Nothing
      , photo = "/img/photos/718944.jpg"
      , prenom = "Cathy"
      , sitesWeb = []
      , slug = "cathy-racon-bouzon"
      , titre = "Mme"
      }
    , { circonscription = "Seine-Saint-Denis (10e circonscription)"
      , code = "OMC_PA721286"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "alain.ramadier@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Ramadier"
      , nomComplet = "Alain Ramadier"
      , particule = Nothing
      , photo = "/img/photos/721286.jpg"
      , prenom = "Alain"
      , sitesWeb = []
      , slug = "alain-ramadier"
      , titre = "M."
      }
    , { circonscription = "Réunion (6e circonscription)"
      , code = "OMC_PA721070"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "nadia.ramassamy@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Ramassamy"
      , nomComplet = "Nadia Ramassamy"
      , particule = Nothing
      , photo = "/img/photos/721070.jpg"
      , prenom = "Nadia"
      , sitesWeb = []
      , slug = "nadia-ramassamy"
      , titre = "Mme"
      }
    , { circonscription = "Loiret (6e circonscription)"
      , code = "OMC_PA720092"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "richard.ramos@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Ramos"
      , nomComplet = "Richard Ramos"
      , particule = Nothing
      , photo = "/img/photos/720092.jpg"
      , prenom = "Richard"
      , sitesWeb = []
      , slug = "richard-ramos"
      , titre = "M."
      }
    , { circonscription = "Essonne (10e circonscription)"
      , code = "OMC_PA721888"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "pierre-alain.raphan@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "La République en Marche"
      , nom = "Raphan"
      , nomComplet = "Pierre-Alain Raphan"
      , particule = Nothing
      , photo = "/img/photos/721888.jpg"
      , prenom = "Pierre-Alain"
      , sitesWeb = []
      , slug = "pierre-alain-raphan"
      , titre = "M."
      }
    , { circonscription = "Réunion (5e circonscription)"
      , code = "OMC_PA721062"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "jean-hugues.ratenon@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La France insoumise"
      , nom = "Ratenon"
      , nomComplet = "Jean-Hugues Ratenon"
      , particule = Nothing
      , photo = "/img/photos/721062.jpg"
      , prenom = "Jean-Hugues"
      , sitesWeb = []
      , slug = "jean-hugues-ratenon"
      , titre = "M."
      }
    , { circonscription = "Moselle (9e circonscription)"
      , code = "OMC_PA720552"
      , collaborateurs = [ "catherine-hurstel" ]
      , commission = "Commission des affaires étrangères"
      , courriels = [ "isabelle.rauch@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "La République en Marche"
      , nom = "Rauch"
      , nomComplet = "Isabelle Rauch"
      , particule = Nothing
      , photo = "/img/photos/720552.jpg"
      , prenom = "Isabelle"
      , sitesWeb = []
      , slug = "isabelle-rauch"
      , titre = "Mme"
      }
    , { circonscription = "Saône-et-Loire (3e circonscription)"
      , code = "OMC_PA722398"
      , collaborateurs = [ "benjamin-gauthey" ]
      , commission = "Commission des lois"
      , courriels = [ "remy.rebeyrotte@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Rebeyrotte"
      , nomComplet = "Rémy Rebeyrotte"
      , particule = Nothing
      , photo = "/img/photos/722398.jpg"
      , prenom = "Rémy"
      , sitesWeb = []
      , slug = "remy-rebeyrotte"
      , titre = "M."
      }
    , { circonscription = "Essonne (7e circonscription)"
      , code = "OMC_PA721678"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "robin.reda@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Reda"
      , nomComplet = "Robin Reda"
      , particule = Nothing
      , photo = "/img/photos/721678.jpg"
      , prenom = "Robin"
      , sitesWeb = []
      , slug = "robin-reda"
      , titre = "M."
      }
    , { circonscription = "Bas-Rhin (8e circonscription)"
      , code = "OMC_PA266776"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "frederic.reiss@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Reiss"
      , nomComplet = "Frédéric Reiss"
      , particule = Nothing
      , photo = "/img/photos/266776.jpg"
      , prenom = "Frédéric"
      , sitesWeb = [ "http://www.fredericreiss.net" ]
      , slug = "frederic-reiss"
      , titre = "M."
      }
    , { circonscription = "Haut-Rhin (3e circonscription)"
      , code = "OMC_PA2529"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "jean-luc.reitzer@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Reitzer"
      , nomComplet = "Jean-Luc Reitzer"
      , particule = Nothing
      , photo = "/img/photos/2529.jpg"
      , prenom = "Jean-Luc"
      , sitesWeb = [ "http://www.depute-reitzer.net" ]
      , slug = "jean-luc-reitzer"
      , titre = "M."
      }
    , { circonscription = "Paris (13e circonscription)"
      , code = "OMC_PA721824"
      , collaborateurs =
            [ "laure-bourdon"
            , "severine-templet"
            ]
      , commission = "Commission des affaires étrangères"
      , courriels = [ "hugues.renson@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Renson"
      , nomComplet = "Hugues Renson"
      , particule = Nothing
      , photo = "/img/photos/721824.jpg"
      , prenom = "Hugues"
      , sitesWeb = []
      , slug = "hugues-renson"
      , titre = "M."
      }
    , { circonscription = "Hérault (2e circonscription)"
      , code = "OMC_PA719676"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "muriel.ressiguier@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La France insoumise"
      , nom = "Ressiguier"
      , nomComplet = "Muriel Ressiguier"
      , particule = Nothing
      , photo = "/img/photos/719676.jpg"
      , prenom = "Muriel"
      , sitesWeb = []
      , slug = "muriel-ressiguier"
      , titre = "Mme"
      }
    , { circonscription = "Bouches-du-Rhône (15e circonscription)"
      , code = "OMC_PA330788"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "bernard.reynes@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Reynès"
      , nomComplet = "Bernard Reynès"
      , particule = Nothing
      , photo = "/img/photos/330788.jpg"
      , prenom = "Bernard"
      , sitesWeb = [ "http://www.bernard-reynes.fr" ]
      , slug = "bernard-reynes"
      , titre = "M."
      }
    , { circonscription = "Seine-et-Marne (5e circonscription)"
      , code = "OMC_PA335758"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "franck.riester@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Riester"
      , nomComplet = "Franck Riester"
      , particule = Nothing
      , photo = "/img/photos/335758.jpg"
      , prenom = "Franck"
      , sitesWeb = []
      , slug = "franck-riester"
      , titre = "M."
      }
    , { circonscription = "Val-d'Oise (3e circonscription)"
      , code = "OMC_PA720900"
      , collaborateurs = [ "karim-boulkhoubz" ]
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "cecile.rilhac@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Rilhac"
      , nomComplet = "Cécile Rilhac"
      , particule = Nothing
      , photo = "/img/photos/720900.jpg"
      , prenom = "Cécile"
      , sitesWeb = []
      , slug = "cecile-rilhac"
      , titre = "Mme"
      }
    , { circonscription = "Haute-Savoie (1re circonscription)"
      , code = "OMC_PA721426"
      , collaborateurs =
            [ "francis-feugnet"
            , "alexis-cintrat"
            ]
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "veronique.riotton@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Riotton"
      , nomComplet = "Véronique Riotton"
      , particule = Nothing
      , photo = "/img/photos/721426.jpg"
      , prenom = "Véronique"
      , sitesWeb = [ "http://riotton.fr" ]
      , slug = "veronique-riotton"
      , titre = "Mme"
      }
    , { circonscription = "Loiret (1re circonscription)"
      , code = "OMC_PA720066"
      , collaborateurs = [ "brice-lacourieux" ]
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "stephanie.rist@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Rist"
      , nomComplet = "Stéphanie Rist"
      , particule = Nothing
      , photo = "/img/photos/720066.jpg"
      , prenom = "Stéphanie"
      , sitesWeb = [ "http://stephanierist.en-marche.fr/" ]
      , slug = "stephanie-rist"
      , titre = "Mme"
      }
    , { circonscription = "Essonne (4e circonscription)"
      , code = "OMC_PA722252"
      , collaborateurs =
            [ "fabrice-garau"
            , "amelie-marques"
            ]
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "marie-pierre.rixain@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Rixain"
      , nomComplet = "Marie-Pierre Rixain"
      , particule = Nothing
      , photo = "/img/photos/722252.jpg"
      , prenom = "Marie-Pierre"
      , sitesWeb = []
      , slug = "marie-pierre-rixain"
      , titre = "Mme"
      }
    , { circonscription = "Aude (3e circonscription)"
      , code = "OMC_PA718810"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "mireille.robert@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Robert"
      , nomComplet = "Mireille Robert"
      , particule = Nothing
      , photo = "/img/photos/718810.jpg"
      , prenom = "Mireille"
      , sitesWeb = []
      , slug = "mireille-robert"
      , titre = "Mme"
      }
    , { circonscription = "Réunion (7e circonscription)"
      , code = "OMC_PA610733"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "thierry.robert@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Robert"
      , nomComplet = "Thierry Robert"
      , particule = Nothing
      , photo = "/img/photos/610733.jpg"
      , prenom = "Thierry"
      , sitesWeb = []
      , slug = "thierry-robert"
      , titre = "M."
      }
    , { circonscription = "Savoie (2e circonscription)"
      , code = "OMC_PA266808"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "vincent.rolland@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Rolland"
      , nomComplet = "Vincent Rolland"
      , particule = Nothing
      , photo = "/img/photos/266808.jpg"
      , prenom = "Vincent"
      , sitesWeb = []
      , slug = "vincent-rolland"
      , titre = "M."
      }
    , { circonscription = "Essonne (3e circonscription)"
      , code = "OMC_PA722022"
      , collaborateurs = [ "florent-de-gigord" ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "laetitia.romeirodias@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Romeiro Dias"
      , nomComplet = "Laëtitia Romeiro Dias"
      , particule = Nothing
      , photo = "/img/photos/722022.jpg"
      , prenom = "Laëtitia"
      , sitesWeb = []
      , slug = "laetitia-romeiro-dias"
      , titre = "Mme"
      }
    , { circonscription = "Haute-Savoie (6e circonscription)"
      , code = "OMC_PA721458"
      , collaborateurs = [ "charlotte-dewitte" ]
      , commission = "Commission des finances"
      , courriels = [ "xavier.roseren@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Roseren"
      , nomComplet = "Xavier Roseren"
      , particule = Nothing
      , photo = "/img/photos/721458.jpg"
      , prenom = "Xavier"
      , sitesWeb = [ "http://roseren.com" ]
      , slug = "xavier-roseren"
      , titre = "M."
      }
    , { circonscription = "Hauts-de-Seine (11e circonscription)"
      , code = "OMC_PA721600"
      , collaborateurs =
            [ "marine-gossa"
            , "alexandre-fernandez"
            , "nadia-desbois"
            ]
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "laurianne.rossi@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Rossi"
      , nomComplet = "Laurianne Rossi"
      , particule = Nothing
      , photo = "/img/photos/721600.jpg"
      , prenom = "Laurianne"
      , sitesWeb = [ "http://www.LaurianneRossi2017.fr" ]
      , slug = "laurianne-rossi"
      , titre = "Mme"
      }
    , { circonscription = "Morbihan (5e circonscription)"
      , code = "OMC_PA343493"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "gwendal.rouillard@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Rouillard"
      , nomComplet = "Gwendal Rouillard"
      , particule = Nothing
      , photo = "/img/photos/343493.jpg"
      , prenom = "Gwendal"
      , sitesWeb = [ "http://www.gwendal-rouillard.fr/" ]
      , slug = "gwendal-rouillard"
      , titre = "M."
      }
    , { circonscription = "Alpes-Maritimes (3e circonscription)"
      , code = "OMC_PA718902"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "cedric.roussel@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Roussel"
      , nomComplet = "Cédric Roussel"
      , particule = Nothing
      , photo = "/img/photos/718902.jpg"
      , prenom = "Cédric"
      , sitesWeb = [ "http://cedricroussel.en-marche.fr" ]
      , slug = "cedric-roussel"
      , titre = "M."
      }
    , { circonscription = "Nord (20e circonscription)"
      , code = "OMC_PA720692"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "fabien.roussel@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Gauche démocrate et républicaine"
      , nom = "Roussel"
      , nomComplet = "Fabien Roussel"
      , particule = Nothing
      , photo = "/img/photos/720692.jpg"
      , prenom = "Fabien"
      , sitesWeb = [ "http://rousselfabien.com" ]
      , slug = "fabien-roussel"
      , titre = "M."
      }
    , { circonscription = "Seine-Saint-Denis (9e circonscription)"
      , code = "OMC_PA721226"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "sabine.rubin@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La France insoumise"
      , nom = "Rubin"
      , nomComplet = "Sabine Rubin"
      , particule = Nothing
      , photo = "/img/photos/721226.jpg"
      , prenom = "Sabine"
      , sitesWeb = []
      , slug = "sabine-rubin"
      , titre = "Mme"
      }
    , { circonscription = "Rhône (1re circonscription)"
      , code = "OMC_PA722292"
      , collaborateurs =
            [ "thomas-montmessin"
            , "eleonore-branchy"
            ]
      , commission = "Commission des lois"
      , courriels = [ "thomas.rudigoz@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Rudigoz"
      , nomComplet = "Thomas Rudigoz"
      , particule = Nothing
      , photo = "/img/photos/722292.jpg"
      , prenom = "Thomas"
      , sitesWeb = []
      , slug = "thomas-rudigoz"
      , titre = "M."
      }
    , { circonscription = "Somme (1re circonscription)"
      , code = "OMC_PA722142"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "francois.ruffin@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "La France insoumise"
      , nom = "Ruffin"
      , nomComplet = "François Ruffin"
      , particule = Nothing
      , photo = "/img/photos/722142.jpg"
      , prenom = "François"
      , sitesWeb = [ "http://www.francoisruffin.fr" ]
      , slug = "francois-ruffin"
      , titre = "M."
      }
    , { circonscription = "Loire-Atlantique (1re circonscription)"
      , code = "OMC_PA332747"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "francois.derugy@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Rugy"
      , nomComplet = "François de Rugy"
      , particule = Just "de"
      , photo = "/img/photos/332747.jpg"
      , prenom = "François"
      , sitesWeb = [ "http://www.francoisderugy.fr" ]
      , slug = "francois-de-rugy"
      , titre = "M."
      }
    , { circonscription = "Paris (7e circonscription)"
      , code = "OMC_PA721616"
      , collaborateurs =
            [ "alexandre-bianco"
            , "baptiste-vaugoyeau"
            ]
      , commission = "Commission des lois"
      , courriels = [ "pacome.rupin@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Rupin"
      , nomComplet = "Pacôme Rupin"
      , particule = Nothing
      , photo = "/img/photos/721616.jpg"
      , prenom = "Pacôme"
      , sitesWeb = []
      , slug = "pacome-rupin"
      , titre = "M."
      }
    , { circonscription = "Haute-Savoie (3e circonscription)"
      , code = "OMC_PA267527"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "martial.saddier@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Saddier"
      , nomComplet = "Martial Saddier"
      , particule = Nothing
      , photo = "/img/photos/267527.jpg"
      , prenom = "Martial"
      , sitesWeb = [ "http://www.martial-saddier.fr" ]
      , slug = "martial-saddier"
      , titre = "M."
      }
    , { circonscription = "Polynésie Française (1re circonscription)"
      , code = "OMC_PA702052"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "maina.sage@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Sage"
      , nomComplet = "Maina Sage"
      , particule = Nothing
      , photo = "/img/photos/702052.jpg"
      , prenom = "Maina"
      , sitesWeb = [ "http://www.msage-an.fr" ]
      , slug = "maina-sage"
      , titre = "Mme"
      }
    , { circonscription = "Val-de-Marne (3e circonscription)"
      , code = "OMC_PA720878"
      , collaborateurs =
            [ "etienne-barraud"
            , "frederic-geney"
            , "amandine-mackako"
            ]
      , commission = "Commission des finances"
      , courriels = [ "laurent.saint-martin@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "La République en Marche"
      , nom = "Saint-Martin"
      , nomComplet = "Laurent Saint-Martin"
      , particule = Nothing
      , photo = "/img/photos/720878.jpg"
      , prenom = "Laurent"
      , sitesWeb = []
      , slug = "laurent-saint-martin"
      , titre = "M."
      }
    , { circonscription = "Maine-et-Loire (4e circonscription)"
      , code = "OMC_PA720138"
      , collaborateurs = [ "clemence-billot" ]
      , commission = "Commission des affaires étrangères"
      , courriels = [ "laetitia.saint-paul@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Saint-Paul"
      , nomComplet = "Laetitia Saint-Paul"
      , particule = Nothing
      , photo = "/img/photos/720138.jpg"
      , prenom = "Laetitia"
      , sitesWeb = [ "http://laetitiasaintpaul.enmarche.fr" ]
      , slug = "laetitia-saint-paul"
      , titre = "Mme"
      }
    ]


deputes5 : Deputes
deputes5 =
    [ { circonscription = "Polynésie Française (2e circonscription)"
      , code = "OMC_PA721110"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "nicole.sanquer@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Sanquer"
      , nomComplet = "Nicole Sanquer"
      , particule = Nothing
      , photo = "/img/photos/721110.jpg"
      , prenom = "Nicole"
      , sitesWeb = []
      , slug = "nicole-sanquer"
      , titre = "Mme"
      }
    , { circonscription = "Loire (5e circonscription)"
      , code = "OMC_PA719972"
      , collaborateurs = [ "laure-deroche" ]
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "nathalie.sarles@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Sarles"
      , nomComplet = "Nathalie Sarles"
      , particule = Nothing
      , photo = "/img/photos/719972.jpg"
      , prenom = "Nathalie"
      , sitesWeb = []
      , slug = "nathalie-sarles"
      , titre = "Mme"
      }
    , { circonscription = "Paris (11e circonscription)"
      , code = "OMC_PA717151"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "marielle.desarnez@assemblee-nationale.fr" ]
      , fonctionCommission = "Président"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Sarnez"
      , nomComplet = "Marielle de Sarnez"
      , particule = Just "de"
      , photo = "/img/photos/717151.jpg"
      , prenom = "Marielle"
      , sitesWeb = []
      , slug = "marielle-de-sarnez"
      , titre = "Mme"
      }
    , { circonscription = "Ardèche (1re circonscription)"
      , code = "OMC_PA340343"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "herve.saulignac@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Nouvelle Gauche"
      , nom = "Saulignac"
      , nomComplet = "Hervé Saulignac"
      , particule = Nothing
      , photo = "/img/photos/340343.jpg"
      , prenom = "Hervé"
      , sitesWeb = [ "http://www.hervesaulignac.fr" ]
      , slug = "herve-saulignac"
      , titre = "M."
      }
    , { circonscription = "Vienne (1re circonscription)"
      , code = "OMC_PA722078"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "jacques.savatier@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Savatier"
      , nomComplet = "Jacques Savatier"
      , particule = Nothing
      , photo = "/img/photos/722078.jpg"
      , prenom = "Jacques"
      , sitesWeb = []
      , slug = "jacques-savatier"
      , titre = "M."
      }
    , { circonscription = "Haut-Rhin (4e circonscription)"
      , code = "OMC_PA721314"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "raphael.schellenberger@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Schellenberger"
      , nomComplet = "Raphaël Schellenberger"
      , particule = Nothing
      , photo = "/img/photos/721314.jpg"
      , prenom = "Raphaël"
      , sitesWeb = []
      , slug = "raphael-schellenberger"
      , titre = "M."
      }
    , { circonscription = "Hautes-Pyrénées (1re circonscription)"
      , code = "OMC_PA720780"
      , collaborateurs =
            [ "leila-morch"
            , "charlene-then"
            ]
      , commission = "Commission des affaires économiques"
      , courriels = [ "jean-bernard.sempastous@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Sempastous"
      , nomComplet = "Jean-Bernard Sempastous"
      , particule = Nothing
      , photo = "/img/photos/720780.jpg"
      , prenom = "Jean-Bernard"
      , sitesWeb = []
      , slug = "jean-bernard-sempastous"
      , titre = "M."
      }
    , { circonscription = "Jura (3e circonscription)"
      , code = "OMC_PA267204"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "jean-marie.sermier@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "Les Républicains"
      , nom = "Sermier"
      , nomComplet = "Jean-Marie Sermier"
      , particule = Nothing
      , photo = "/img/photos/267204.jpg"
      , prenom = "Jean-Marie"
      , sitesWeb = []
      , slug = "jean-marie-sermier"
      , titre = "M."
      }
    , { circonscription = "Guadeloupe (1re circonscription)"
      , code = "OMC_PA720960"
      , collaborateurs = [ "raphael-lapin" ]
      , commission = "Commission des finances"
      , courriels = [ "olivier.serva@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Serva"
      , nomComplet = "Olivier Serva"
      , particule = Nothing
      , photo = "/img/photos/720960.jpg"
      , prenom = "Olivier"
      , sitesWeb = [ "http://www.olivierserva.fr" ]
      , slug = "olivier-serva"
      , titre = "M."
      }
    , { circonscription = "Guyane (1re circonscription)"
      , code = "OMC_PA610667"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "gabriel.serville@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Gauche démocrate et républicaine"
      , nom = "Serville"
      , nomComplet = "Gabriel Serville"
      , particule = Nothing
      , photo = "/img/photos/610667.jpg"
      , prenom = "Gabriel"
      , sitesWeb = [ "http://www.gabrielserville.com" ]
      , slug = "gabriel-serville"
      , titre = "M."
      }
    , { circonscription = "Gironde (5e circonscription)"
      , code = "OMC_PA719592"
      , collaborateurs = [ "camille-reboul" ]
      , commission = "Commission des finances"
      , courriels = [ "benoit.simian@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Simian"
      , nomComplet = "Benoit Simian"
      , particule = Nothing
      , photo = "/img/photos/719592.jpg"
      , prenom = "Benoit"
      , sitesWeb = [ "http://benoitsimian.en-marche.fr" ]
      , slug = "benoit-simian"
      , titre = "M."
      }
    , { circonscription = "Hauts-de-Seine (9e circonscription)"
      , code = "OMC_PA346876"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "tsolere@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Solère"
      , nomComplet = "Thierry Solère"
      , particule = Nothing
      , photo = "/img/photos/346876.jpg"
      , prenom = "Thierry"
      , sitesWeb =
            [ "http://thierrysolere.fr"
            , "http://solere.blogs.com"
            ]
      , slug = "thierry-solere"
      , titre = "M."
      }
    , { circonscription = "Doubs (3e circonscription)"
      , code = "OMC_PA719420"
      , collaborateurs = [ "trixie-meyssonnier" ]
      , commission = "Commission des affaires économiques"
      , courriels = [ "denis.sommer@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Sommer"
      , nomComplet = "Denis Sommer"
      , particule = Nothing
      , photo = "/img/photos/719420.jpg"
      , prenom = "Denis"
      , sitesWeb = []
      , slug = "denis-sommer"
      , titre = "M."
      }
    , { circonscription = "Français établis hors de France (6e circonscription)"
      , code = "OMC_PA721174"
      , collaborateurs = [ "marie-ange-rousselot" ]
      , commission = "Commission des affaires étrangères"
      , courriels = [ "joachim.son-forget@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Son-Forget"
      , nomComplet = "Joachim Son-Forget"
      , particule = Nothing
      , photo = "/img/photos/721174.jpg"
      , prenom = "Joachim"
      , sitesWeb = []
      , slug = "joachim-son-forget"
      , titre = "M."
      }
    , { circonscription = "Manche (2e circonscription)"
      , code = "OMC_PA720190"
      , collaborateurs = [ "benjamin-bailleul" ]
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "bertrand.sorre@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Sorre"
      , nomComplet = "Bertrand Sorre"
      , particule = Nothing
      , photo = "/img/photos/720190.jpg"
      , prenom = "Bertrand"
      , sitesWeb = []
      , slug = "bertrand-sorre"
      , titre = "M."
      }
    , { circonscription = "Haut-Rhin (1re circonscription)"
      , code = "OMC_PA334654"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "eric.straumann@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Straumann"
      , nomComplet = "Éric Straumann"
      , particule = Nothing
      , photo = "/img/photos/334654.jpg"
      , prenom = "Éric"
      , sitesWeb = [ "http://ericstraumann.over-blog.com" ]
      , slug = "eric-straumann"
      , titre = "M."
      }
    , { circonscription = "Bas-Rhin (3e circonscription)"
      , code = "OMC_PA720754"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "bruno.studer@assemblee-nationale.fr" ]
      , fonctionCommission = "Président"
      , groupe = "La République en Marche"
      , nom = "Studer"
      , nomComplet = "Bruno Studer"
      , particule = Nothing
      , photo = "/img/photos/720754.jpg"
      , prenom = "Bruno"
      , sitesWeb = [ "http://www.studer2017.fr" ]
      , slug = "bruno-studer"
      , titre = "M."
      }
    , { circonscription = "Seine-Maritime (4e circonscription)"
      , code = "OMC_PA722118"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "sira.sylla@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Sylla"
      , nomComplet = "Sira Sylla"
      , particule = Nothing
      , photo = "/img/photos/722118.jpg"
      , prenom = "Sira"
      , sitesWeb = []
      , slug = "sira-sylla"
      , titre = "Mme"
      }
    , { circonscription = "Alpes-Maritimes (9e circonscription)"
      , code = "OMC_PA267794"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "michele.tabarot@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Tabarot"
      , nomComplet = "Michèle Tabarot"
      , particule = Nothing
      , photo = "/img/photos/267794.jpg"
      , prenom = "Michèle"
      , sitesWeb = []
      , slug = "michele-tabarot"
      , titre = "Mme"
      }
    , { circonscription = "Val-d'Oise (10e circonscription)"
      , code = "OMC_PA720952"
      , collaborateurs =
            [ "deborah-asseraf"
            , "romain-chevrey"
            , "mathurin-laurin"
            ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "aurelien.tache@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Taché"
      , nomComplet = "Aurélien Taché"
      , particule = Nothing
      , photo = "/img/photos/720952.jpg"
      , prenom = "Aurélien"
      , sitesWeb = []
      , slug = "aurelien-tache"
      , titre = "M."
      }
    , { circonscription = "Eure (3e circonscription)"
      , code = "OMC_PA719350"
      , collaborateurs =
            [ "antonin-violette"
            , "arnaud-vidal"
            ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "marie.tamarelle@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Tamarelle-Verhaeghe"
      , nomComplet = "Marie Tamarelle-Verhaeghe"
      , particule = Nothing
      , photo = "/img/photos/719350.jpg"
      , prenom = "Marie"
      , sitesWeb = []
      , slug = "marie-tamarelle-verhaeghe"
      , titre = "Mme"
      }
    , { circonscription = "Paris (9e circonscription)"
      , code = "OMC_PA721690"
      , collaborateurs = [ "gregoire-le-taillandier" ]
      , commission = "Commission des affaires étrangères"
      , courriels = [ "buon.tan@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Tan"
      , nomComplet = "Buon Tan"
      , particule = Nothing
      , photo = "/img/photos/721690.jpg"
      , prenom = "Buon"
      , sitesWeb = [ "http://www.buon-tan.fr" ]
      , slug = "buon-tan"
      , titre = "M."
      }
    , { circonscription = "Finistère (7e circonscription)"
      , code = "OMC_PA719550"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "liliana.tanguy@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Tanguy"
      , nomComplet = "Liliana Tanguy"
      , particule = Nothing
      , photo = "/img/photos/719550.jpg"
      , prenom = "Liliana"
      , sitesWeb = [ "http://www.lilianetanguy.en-marche.fr" ]
      , slug = "liliana-tanguy"
      , titre = "Mme"
      }
    , { circonscription = "Hauts-de-Seine (2e circonscription)"
      , code = "OMC_PA722086"
      , collaborateurs =
            [ "gwennaelle-pierre"
            , "mathilde-laurent"
            ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "adrien.taquet@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Taquet"
      , nomComplet = "Adrien Taquet"
      , particule = Nothing
      , photo = "/img/photos/722086.jpg"
      , prenom = "Adrien"
      , sitesWeb = [ "http://taquet2017.fr" ]
      , slug = "adrien-taquet"
      , titre = "M."
      }
    , { circonscription = "Maine-et-Loire (3e circonscription)"
      , code = "OMC_PA2792"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "jean-charles.taugourdeau@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Taugourdeau"
      , nomComplet = "Jean-Charles Taugourdeau"
      , particule = Nothing
      , photo = "/img/photos/2792.jpg"
      , prenom = "Jean-Charles"
      , sitesWeb = [ "http://jctaugourdeau.fr/" ]
      , slug = "jean-charles-taugourdeau"
      , titre = "M."
      }
    , { circonscription = "Ariège (1re circonscription)"
      , code = "OMC_PA718860"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "benedicte.taurine@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La France insoumise"
      , nom = "Taurine"
      , nomComplet = "Bénédicte Taurine"
      , particule = Nothing
      , photo = "/img/photos/718860.jpg"
      , prenom = "Bénédicte"
      , sitesWeb = []
      , slug = "benedicte-taurine"
      , titre = "Mme"
      }
    , { circonscription = "Bouches-du-Rhône (6e circonscription)"
      , code = "OMC_PA2796"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "guy.teissier@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Teissier"
      , nomComplet = "Guy Teissier"
      , particule = Nothing
      , photo = "/img/photos/2796.jpg"
      , prenom = "Guy"
      , sitesWeb = []
      , slug = "guy-teissier"
      , titre = "M."
      }
    , { circonscription = "Tarn (3e circonscription)"
      , code = "OMC_PA721584"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "jean.terlier@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "La République en Marche"
      , nom = "Terlier"
      , nomComplet = "Jean Terlier"
      , particule = Nothing
      , photo = "/img/photos/721584.jpg"
      , prenom = "Jean"
      , sitesWeb = []
      , slug = "jean-terlier"
      , titre = "M."
      }
    , { circonscription = "Seine-Saint-Denis (12e circonscription)"
      , code = "OMC_PA720854"
      , collaborateurs = [ "jerome-meunier" ]
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "stephane.teste@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Testé"
      , nomComplet = "Stéphane Testé"
      , particule = Nothing
      , photo = "/img/photos/720854.jpg"
      , prenom = "Stéphane"
      , sitesWeb = []
      , slug = "stephane-teste"
      , titre = "M."
      }
    , { circonscription = "Oise (2e circonscription)"
      , code = "OMC_PA720568"
      , collaborateurs = [ "charles-meiller" ]
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "agnes.thill@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Thill"
      , nomComplet = "Agnès Thill"
      , particule = Nothing
      , photo = "/img/photos/720568.jpg"
      , prenom = "Agnès"
      , sitesWeb = []
      , slug = "agnes-thill"
      , titre = "Mme"
      }
    , { circonscription = "Indre-et-Loire (5e circonscription)"
      , code = "OMC_PA719822"
      , collaborateurs = [ "benjamin-bousquet" ]
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "sabine.thillaye@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Thillaye"
      , nomComplet = "Sabine Thillaye"
      , particule = Nothing
      , photo = "/img/photos/719822.jpg"
      , prenom = "Sabine"
      , sitesWeb = [ "http://www.sabinethillaye2017.fr" ]
      , slug = "sabine-thillaye"
      , titre = "Mme"
      }
    , { circonscription = "Bas-Rhin (9e circonscription)"
      , code = "OMC_PA722336"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "vincent.thiebaut@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Thiébaut"
      , nomComplet = "Vincent Thiébaut"
      , particule = Nothing
      , photo = "/img/photos/722336.jpg"
      , prenom = "Vincent"
      , sitesWeb = []
      , slug = "vincent-thiebaut"
      , titre = "M."
      }
    , { circonscription = "Puy-de-Dôme (1re circonscription)"
      , code = "OMC_PA720830"
      , collaborateurs = [ "etienne-le-norcy" ]
      , commission = "Commission des affaires étrangères"
      , courriels = [ "valerie.thomas@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Thomas"
      , nomComplet = "Valérie Thomas"
      , particule = Nothing
      , photo = "/img/photos/720830.jpg"
      , prenom = "Valérie"
      , sitesWeb = []
      , slug = "valerie-thomas"
      , titre = "Mme"
      }
    , { circonscription = "Drôme (2e circonscription)"
      , code = "OMC_PA719302"
      , collaborateurs = [ "emilie-gapaillard" ]
      , commission = "Commission des lois"
      , courriels = [ "alice.thourot@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Thourot"
      , nomComplet = "Alice Thourot"
      , particule = Nothing
      , photo = "/img/photos/719302.jpg"
      , prenom = "Alice"
      , sitesWeb = []
      , slug = "alice-thourot"
      , titre = "Mme"
      }
    , { circonscription = "Lot (2e circonscription)"
      , code = "OMC_PA720014"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "huguette.tiegna@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Tiegna"
      , nomComplet = "Huguette Tiegna"
      , particule = Nothing
      , photo = "/img/photos/720014.jpg"
      , prenom = "Huguette"
      , sitesWeb = []
      , slug = "huguette-tiegna"
      , titre = "Mme"
      }
    , { circonscription = "Rhône (3e circonscription)"
      , code = "OMC_PA334768"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "jean-louis.touraine@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "La République en Marche"
      , nom = "Touraine"
      , nomComplet = "Jean-Louis Touraine"
      , particule = Nothing
      , photo = "/img/photos/334768.jpg"
      , prenom = "Jean-Louis"
      , sitesWeb = [ "http://www.jeanlouistouraine.fr" ]
      , slug = "jean-louis-touraine"
      , titre = "M."
      }
    , { circonscription = "Calvados (6e circonscription)"
      , code = "OMC_PA2828"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "alain.tourret@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Tourret"
      , nomComplet = "Alain Tourret"
      , particule = Nothing
      , photo = "/img/photos/2828.jpg"
      , prenom = "Alain"
      , sitesWeb = []
      , slug = "alain-tourret"
      , titre = "M."
      }
    , { circonscription = "Haute-Garonne (7e circonscription)"
      , code = "OMC_PA719540"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "elisabeth.toutut-picard@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "La République en Marche"
      , nom = "Toutut-Picard"
      , nomComplet = "Élisabeth Toutut-Picard"
      , particule = Nothing
      , photo = "/img/photos/719540.jpg"
      , prenom = "Élisabeth"
      , sitesWeb = [ "http://www.elisabeth-toutut-picard.fr" ]
      , slug = "elisabeth-toutut-picard"
      , titre = "Mme"
      }
    , { circonscription = "Alpes-Maritimes (6e circonscription)"
      , code = "OMC_PA718780"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "laurence.trastour-isnart@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Trastour-Isnart"
      , nomComplet = "Laurence Trastour-Isnart"
      , particule = Nothing
      , photo = "/img/photos/718780.jpg"
      , prenom = "Laurence"
      , sitesWeb = [ "http://www.laurencetrastour2017.fr" ]
      , slug = "laurence-trastour-isnart"
      , titre = "Mme"
      }
    , { circonscription = "Moselle (5e circonscription)"
      , code = "OMC_PA720394"
      , collaborateurs = [ "clemence-taillan" ]
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "nicole.gries-trisse@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Trisse"
      , nomComplet = "Nicole Trisse"
      , particule = Nothing
      , photo = "/img/photos/720394.jpg"
      , prenom = "Nicole"
      , sitesWeb = []
      , slug = "nicole-trisse"
      , titre = "Mme"
      }
    , { circonscription = "Ain (4e circonscription)"
      , code = "OMC_PA718682"
      , collaborateurs = [ "amine-kaabeche" ]
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "stephane.trompille@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Trompille"
      , nomComplet = "Stéphane Trompille"
      , particule = Nothing
      , photo = "/img/photos/718682.jpg"
      , prenom = "Stéphane"
      , sitesWeb = []
      , slug = "stephane-trompille"
      , titre = "M."
      }
    , { circonscription = "Charente-Maritime (2e circonscription)"
      , code = "OMC_PA719092"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "frederique.tuffnell@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Tuffnell"
      , nomComplet = "Frédérique Tuffnell"
      , particule = Nothing
      , photo = "/img/photos/719092.jpg"
      , prenom = "Frédérique"
      , sitesWeb = []
      , slug = "frederique-tuffnell"
      , titre = "Mme"
      }
    , { circonscription = "Vienne (4e circonscription)"
      , code = "OMC_PA722162"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "nicolas.turquois@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Turquois"
      , nomComplet = "Nicolas Turquois"
      , particule = Nothing
      , photo = "/img/photos/722162.jpg"
      , prenom = "Nicolas"
      , sitesWeb = [ "http://www.nicolas-turquois.fr/" ]
      , slug = "nicolas-turquois"
      , titre = "M."
      }
    , { circonscription = "Saône-et-Loire (4e circonscription)"
      , code = "OMC_PA608695"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "cecile.untermaier@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Nouvelle Gauche"
      , nom = "Untermaier"
      , nomComplet = "Cécile Untermaier"
      , particule = Nothing
      , photo = "/img/photos/608695.jpg"
      , prenom = "Cécile"
      , sitesWeb = [ "http://www.cecileuntermaier.fr" ]
      , slug = "cecile-untermaier"
      , titre = "Mme"
      }
    , { circonscription = "Guadeloupe (4e circonscription)"
      , code = "OMC_PA643145"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "helene.vainqueur@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Nouvelle Gauche"
      , nom = "Vainqueur-Christophe"
      , nomComplet = "Hélène Vainqueur-Christophe"
      , particule = Nothing
      , photo = "/img/photos/643145.jpg"
      , prenom = "Hélène"
      , sitesWeb = []
      , slug = "helene-vainqueur-christophe"
      , titre = "Mme"
      }
    , { circonscription = "Haute-Loire (1re circonscription)"
      , code = "OMC_PA643134"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "isabelle.valentin@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Valentin"
      , nomComplet = "Isabelle Valentin"
      , particule = Nothing
      , photo = "/img/photos/643134.jpg"
      , prenom = "Isabelle"
      , sitesWeb = []
      , slug = "isabelle-valentin"
      , titre = "Mme"
      }
    , { circonscription = "Alpes-Maritimes (4e circonscription)"
      , code = "OMC_PA718768"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "alexandra.ardisson@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Valetta Ardisson"
      , nomComplet = "Alexandra Valetta Ardisson"
      , particule = Nothing
      , photo = "/img/photos/718768.jpg"
      , prenom = "Alexandra"
      , sitesWeb = []
      , slug = "alexandra-valetta-ardisson"
      , titre = "Mme"
      }
    , { circonscription = "Landes (3e circonscription)"
      , code = "OMC_PA719930"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "boris.vallaud@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Nouvelle Gauche"
      , nom = "Vallaud"
      , nomComplet = "Boris Vallaud"
      , particule = Nothing
      , photo = "/img/photos/719930.jpg"
      , prenom = "Boris"
      , sitesWeb = []
      , slug = "boris-vallaud"
      , titre = "M."
      }
    , { circonscription = "Essonne (1re circonscription)"
      , code = "OMC_PA267622"
      , collaborateurs =
            [ "margot-antoniazzi"
            , "juliette-nuti"
            ]
      , commission = "Commission des lois"
      , courriels = [ "manuel.valls@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Valls"
      , nomComplet = "Manuel Valls"
      , particule = Nothing
      , photo = "/img/photos/267622.jpg"
      , prenom = "Manuel"
      , sitesWeb = []
      , slug = "manuel-valls"
      , titre = "M."
      }
    , { circonscription = "Allier (2e circonscription)"
      , code = "OMC_PA718728"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "laurence.vanceunebrock-mialon@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Vanceunebrock-Mialon"
      , nomComplet = "Laurence Vanceunebrock-Mialon"
      , particule = Nothing
      , photo = "/img/photos/718728.jpg"
      , prenom = "Laurence"
      , sitesWeb = []
      , slug = "laurence-vanceunebrock-mialon"
      , titre = "Mme"
      }
    , { circonscription = "Oise (5e circonscription)"
      , code = "OMC_PA720586"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "pierre.vatin@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Vatin"
      , nomComplet = "Pierre Vatin"
      , particule = Nothing
      , photo = "/img/photos/720586.jpg"
      , prenom = "Pierre"
      , sitesWeb = []
      , slug = "pierre-vatin"
      , titre = "M."
      }
    , { circonscription = "Yvelines (7e circonscription)"
      , code = "OMC_PA721924"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "michele.devaucouleurs@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Vaucouleurs"
      , nomComplet = "Michèle de Vaucouleurs"
      , particule = Just "de"
      , photo = "/img/photos/721924.jpg"
      , prenom = "Michèle"
      , sitesWeb = []
      , slug = "michele-de-vaucouleurs"
      , titre = "Mme"
      }
    , { circonscription = "Nord (7e circonscription)"
      , code = "OMC_PA267585"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "francis.vercamer@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Vercamer"
      , nomComplet = "Francis Vercamer"
      , particule = Nothing
      , photo = "/img/photos/267585.jpg"
      , prenom = "Francis"
      , sitesWeb = [ "http://www.vercamer.fr" ]
      , slug = "francis-vercamer"
      , titre = "M."
      }
    , { circonscription = "Rhône (8e circonscription)"
      , code = "OMC_PA334843"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "patrice.verchere@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Verchère"
      , nomComplet = "Patrice Verchère"
      , particule = Nothing
      , photo = "/img/photos/334843.jpg"
      , prenom = "Patrice"
      , sitesWeb = [ "http://www.patriceverchere.fr" ]
      , slug = "patrice-verchere"
      , titre = "M."
      }
    , { circonscription = "Tarn (2e circonscription)"
      , code = "OMC_PA721542"
      , collaborateurs = [ "sabrine-sassi" ]
      , commission = "Commission des finances"
      , courriels = [ "marie-christine.verdier-jouclas@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "La République en Marche"
      , nom = "Verdier-Jouclas"
      , nomComplet = "Marie-Christine Verdier-Jouclas"
      , particule = Nothing
      , photo = "/img/photos/721542.jpg"
      , prenom = "Marie-Christine"
      , sitesWeb = [ "http://www.verdier-jouclas.fr" ]
      , slug = "marie-christine-verdier-jouclas"
      , titre = "Mme"
      }
    , { circonscription = "Aveyron (3e circonscription)"
      , code = "OMC_PA709315"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "arnaud.viala@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Viala"
      , nomComplet = "Arnaud Viala"
      , particule = Nothing
      , photo = "/img/photos/709315.jpg"
      , prenom = "Arnaud"
      , sitesWeb = [ "http://www.arnaudviala.fr" ]
      , slug = "arnaud-viala"
      , titre = "M."
      }
    , { circonscription = "Yvelines (8e circonscription)"
      , code = "OMC_PA721968"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "michel.vialay@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Vialay"
      , nomComplet = "Michel Vialay"
      , particule = Nothing
      , photo = "/img/photos/721968.jpg"
      , prenom = "Michel"
      , sitesWeb = []
      , slug = "michel-vialay"
      , titre = "M."
      }
    , { circonscription = "Puy-de-Dôme (3e circonscription)"
      , code = "OMC_PA720704"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "laurence.vichnievsky@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Vichnievsky"
      , nomComplet = "Laurence Vichnievsky"
      , particule = Nothing
      , photo = "/img/photos/720704.jpg"
      , prenom = "Laurence"
      , sitesWeb = []
      , slug = "laurence-vichnievsky"
      , titre = "Mme"
      }
    , { circonscription = "Seine-Maritime (2e circonscription)"
      , code = "OMC_PA722102"
      , collaborateurs = [ "lucie-loncle-duda" ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "annie.vidal@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Vidal"
      , nomComplet = "Annie Vidal"
      , particule = Nothing
      , photo = "/img/photos/722102.jpg"
      , prenom = "Annie"
      , sitesWeb = []
      , slug = "annie-vidal"
      , titre = "Mme"
      }
    , { circonscription = "Haute-Loire (2e circonscription)"
      , code = "OMC_PA607090"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "jean-pierre.vigier@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Vigier"
      , nomComplet = "Jean-Pierre Vigier"
      , particule = Nothing
      , photo = "/img/photos/607090.jpg"
      , prenom = "Jean-Pierre"
      , sitesWeb = [ "http://www.jpvigier.fr" ]
      , slug = "jean-pierre-vigier"
      , titre = "M."
      }
    , { circonscription = "Eure-et-Loir (4e circonscription)"
      , code = "OMC_PA331582"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "philippe.vigier@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Vigier"
      , nomComplet = "Philippe Vigier"
      , particule = Nothing
      , photo = "/img/photos/331582.jpg"
      , prenom = "Philippe"
      , sitesWeb = []
      , slug = "philippe-vigier"
      , titre = "M."
      }
    , { circonscription = "Hérault (9e circonscription)"
      , code = "OMC_PA606639"
      , collaborateurs = []
      , commission = "Commission des affaires culturelles et de l'éducation"
      , courriels = [ "patrick.vignal@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Vignal"
      , nomComplet = "Patrick Vignal"
      , particule = Nothing
      , photo = "/img/photos/606639.jpg"
      , prenom = "Patrick"
      , sitesWeb = []
      , slug = "patrick-vignal"
      , titre = "M."
      }
    , { circonscription = "Haute-Garonne (3e circonscription)"
      , code = "OMC_PA719512"
      , collaborateurs = [ "julien-coquenas" ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "corinne.vignon@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Vignon"
      , nomComplet = "Corinne Vignon"
      , particule = Nothing
      , photo = "/img/photos/719512.jpg"
      , prenom = "Corinne"
      , sitesWeb = [ "http://www.corinnevignon.fr" ]
      , slug = "corinne-vignon"
      , titre = "Mme"
      }
    , { circonscription = "Essonne (5e circonscription)"
      , code = "OMC_PA722260"
      , collaborateurs =
            [ "david-saussol"
            , "thomas-friang"
            , "juliette-marceaux"
            , "candice-foehrenbach"
            ]
      , commission = "Commission des lois"
      , courriels = [ "cedric.villani@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Villani"
      , nomComplet = "Cédric Villani"
      , particule = Nothing
      , photo = "/img/photos/722260.jpg"
      , prenom = "Cédric"
      , sitesWeb = [ "http://www.villani2017.eu" ]
      , slug = "cedric-villani"
      , titre = "M."
      }
    , { circonscription = "Yonne (2e circonscription)"
      , code = "OMC_PA421348"
      , collaborateurs = []
      , commission = "Commission des affaires économiques"
      , courriels = [ "andre.villiers@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Villiers"
      , nomComplet = "André Villiers"
      , particule = Nothing
      , photo = "/img/photos/421348.jpg"
      , prenom = "André"
      , sitesWeb = []
      , slug = "andre-villiers"
      , titre = "M."
      }
    , { circonscription = "Vosges (1re circonscription)"
      , code = "OMC_PA721474"
      , collaborateurs = []
      , commission = "Commission des affaires sociales"
      , courriels = [ "stephane.viry@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Républicains"
      , nom = "Viry"
      , nomComplet = "Stéphane Viry"
      , particule = Nothing
      , photo = "/img/photos/721474.jpg"
      , prenom = "Stéphane"
      , sitesWeb = []
      , slug = "stephane-viry"
      , titre = "M."
      }
    , { circonscription = "Val-d'Oise (2e circonscription)"
      , code = "OMC_PA721262"
      , collaborateurs =
            [ "isabelle-mercier"
            , "arthur-nowicki"
            ]
      , commission = "Commission des lois"
      , courriels = [ "guillaume.vuilletet@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Vuilletet"
      , nomComplet = "Guillaume Vuilletet"
      , particule = Nothing
      , photo = "/img/photos/721262.jpg"
      , prenom = "Guillaume"
      , sitesWeb = []
      , slug = "guillaume-vuilletet"
      , titre = "M."
      }
    , { circonscription = "Isère (1re circonscription)"
      , code = "OMC_PA642788"
      , collaborateurs =
            [ "sacha-benhamou"
            , "loic-terrenes"
            , "julie-dulcire"
            ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "olivier.veran@assemblee-nationale.fr" ]
      , fonctionCommission = "Rapporteur"
      , groupe = "La République en Marche"
      , nom = "Véran"
      , nomComplet = "Olivier Véran"
      , particule = Nothing
      , photo = "/img/photos/642788.jpg"
      , prenom = "Olivier"
      , sitesWeb = [ "http://www.olivier-veran.fr" ]
      , slug = "olivier-veran"
      , titre = "M."
      }
    , { circonscription = "Ardennes (3e circonscription)"
      , code = "OMC_PA2952"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "jean-luc.warsmann@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Warsmann"
      , nomComplet = "Jean-Luc Warsmann"
      , particule = Nothing
      , photo = "/img/photos/2952.jpg"
      , prenom = "Jean-Luc"
      , sitesWeb = []
      , slug = "jean-luc-warsmann"
      , titre = "M."
      }
    , { circonscription = "Bas-Rhin (2e circonscription)"
      , code = "OMC_PA720746"
      , collaborateurs = []
      , commission = "Commission des affaires étrangères"
      , courriels = [ "sylvain.waserman@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Mouvement Démocrate et apparentés"
      , nom = "Waserman"
      , nomComplet = "Sylvain Waserman"
      , particule = Nothing
      , photo = "/img/photos/720746.jpg"
      , prenom = "Sylvain"
      , sitesWeb = [ "http://www.sylvainwaserman.com" ]
      , slug = "sylvain-waserman"
      , titre = "M."
      }
    , { circonscription = "Oise (4e circonscription)"
      , code = "OMC_PA2960"
      , collaborateurs = []
      , commission = "Commission des finances"
      , courriels = [ "eric.woerth@assemblee-nationale.fr" ]
      , fonctionCommission = "Président"
      , groupe = "Les Républicains"
      , nom = "Woerth"
      , nomComplet = "Éric Woerth"
      , particule = Nothing
      , photo = "/img/photos/2960.jpg"
      , prenom = "Éric"
      , sitesWeb = []
      , slug = "eric-woerth"
      , titre = "M."
      }
    , { circonscription = "Bas-Rhin (4e circonscription)"
      , code = "OMC_PA722268"
      , collaborateurs = [ "christophe-brunelle" ]
      , commission = "Commission des affaires sociales"
      , courriels = [ "martine.wonner@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Wonner"
      , nomComplet = "Martine Wonner"
      , particule = Nothing
      , photo = "/img/photos/722268.jpg"
      , prenom = "Martine"
      , sitesWeb = []
      , slug = "martine-wonner"
      , titre = "Mme"
      }
    , { circonscription = "Seine-Maritime (3e circonscription)"
      , code = "OMC_PA722110"
      , collaborateurs = []
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "hubert.wulfranc@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Gauche démocrate et républicaine"
      , nom = "Wulfranc"
      , nomComplet = "Hubert Wulfranc"
      , particule = Nothing
      , photo = "/img/photos/722110.jpg"
      , prenom = "Hubert"
      , sitesWeb = []
      , slug = "hubert-wulfranc"
      , titre = "M."
      }
    , { circonscription = "Moselle (7e circonscription)"
      , code = "OMC_PA720318"
      , collaborateurs =
            [ "umit-yildrim"
            , "benjamin-fargeaud"
            ]
      , commission = "Commission des lois"
      , courriels = [ "helene.zannier@assemblee-nationale.fr" ]
      , fonctionCommission = "Secrétaire"
      , groupe = "La République en Marche"
      , nom = "Zannier"
      , nomComplet = "Hélène Zannier"
      , particule = Nothing
      , photo = "/img/photos/720318.jpg"
      , prenom = "Hélène"
      , sitesWeb = []
      , slug = "helene-zannier"
      , titre = "Mme"
      }
    , { circonscription = "Bouches-du-Rhône (8e circonscription)"
      , code = "OMC_PA718962"
      , collaborateurs = [ "mehdi-thibaut-affes" ]
      , commission = "Commission du développement durable et de l'aménagement du territoire"
      , courriels = [ "jean-marc.zulesi@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "La République en Marche"
      , nom = "Zulesi"
      , nomComplet = "Jean-Marc Zulesi"
      , particule = Nothing
      , photo = "/img/photos/718962.jpg"
      , prenom = "Jean-Marc"
      , sitesWeb = [ "http://www.jeanmarczulesi.fr" ]
      , slug = "jean-marc-zulesi"
      , titre = "M."
      }
    , { circonscription = "Territoire de Belfort (2e circonscription)"
      , code = "OMC_PA267330"
      , collaborateurs = []
      , commission = "Commission des lois"
      , courriels = [ "michel.zumkeller@assemblee-nationale.fr" ]
      , fonctionCommission = "Membre"
      , groupe = "Les Constructifs : républicains, UDI, indépendants"
      , nom = "Zumkeller"
      , nomComplet = "Michel Zumkeller"
      , particule = Nothing
      , photo = "/img/photos/267330.jpg"
      , prenom = "Michel"
      , sitesWeb = []
      , slug = "michel-zumkeller"
      , titre = "M."
      }
    , { circonscription = "Ain (2e circonscription)"
      , code = "OMC_PA1012"
      , collaborateurs = []
      , commission = "Commission de la défense nationale et des forces armées"
      , courriels = [ "charles.delaverpilliere@assemblee-nationale.fr" ]
      , fonctionCommission = "Vice-président"
      , groupe = "Les Républicains"
      , nom = "la Verpillière"
      , nomComplet = "Charles de la Verpillière"
      , particule = Just "de"
      , photo = "/img/photos/1012.jpg"
      , prenom = "Charles"
      , sitesWeb = [ "http://www.charlesdelaverpilliere.com" ]
      , slug = "charles-de-la-verpilliere"
      , titre = "M."
      }
    ]


deputes : Deputes
deputes =
    deputes0 ++ deputes1 ++ deputes2 ++ deputes3 ++ deputes4 ++ deputes5


collaborateurBySlug : Dict String Collaborateur
collaborateurBySlug =
    collaborateurs
        |> List.map (\collaborateur -> ( collaborateur.slug, collaborateur ))
        |> Dict.fromList


commissions : List String
commissions =
    deputes
        |> List.map .commission
        |> List.Extra.unique
        |> List.sort


deputeBySlug : Dict String Depute
deputeBySlug =
    deputes
        |> List.map (\depute -> ( depute.slug, depute ))
        |> Dict.fromList


fonctionsCommissions : List String
fonctionsCommissions =
    deputes
        |> List.map .fonctionCommission
        |> List.Extra.unique
        |> List.sort


groupes : List String
groupes =
    deputes
        |> List.map .groupe
        |> List.Extra.unique
        |> List.sort


( textSearchIndex, errors ) =
    ElmTextSearch.addDocs
        deputes
    <|
        ElmTextSearch.new
            { ref = .slug
            , fields = [ ( .slug, 2 ) ]
            , listFields = [ ( .collaborateurs, 1 ) ]
            }
_ =
    List.map
        (\number message -> Debug.log message (toString number))
        errors
