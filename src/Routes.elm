module Routes exposing (..)

import Data exposing (deputeBySlug)
import Dict exposing (Dict)
import Navigation
import Types exposing (Depute)
import UrlParser exposing ((</>), custom, map, oneOf, parsePath, Parser, remaining, s, string, top)


type DeputeRoute
    = DeputeIndexRoute


type DeputesRoute
    = DeputeRoute Depute DeputeRoute
    | DeputesIndexRoute


type Route
    = AboutRoute
    | DeputesRoute DeputesRoute
    | HomeRoute
    | NotFoundRoute (List String)


deputeSlugParser : Parser (Depute -> a) a
deputeSlugParser =
    custom "DEPUTE_CODE" <|
        \segment ->
            case Dict.get segment deputeBySlug of
                Just depute ->
                    Ok depute

                Nothing ->
                    Err "Ce n'est pas un slug de député"


deputeRouteParser : Parser (DeputeRoute -> a) a
deputeRouteParser =
    oneOf
        [ map DeputeIndexRoute top
        ]


deputesRouteParser : Parser (DeputesRoute -> a) a
deputesRouteParser =
    oneOf
        [ map DeputesIndexRoute top
        , map DeputeRoute (deputeSlugParser </> deputeRouteParser)
        ]


routeParser : Parser (Route -> a) a
routeParser =
    oneOf
        [ map HomeRoute top
        , map AboutRoute (s "a-propos")
        , map DeputesRoute (s "deputes" </> deputesRouteParser)
        , map NotFoundRoute remaining
        ]


parseLocation : Navigation.Location -> Maybe Route
parseLocation location =
    UrlParser.parsePath routeParser location
