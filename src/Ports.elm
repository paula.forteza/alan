port module Ports exposing (..)

import Configuration
import Urls


-- DOCUMENT METADATA


type alias DocumentMetadata =
    { description : String
    , imageUrl : String
    , title : String
    }


type alias DocumentMetatags =
    { description : String
    , imageUrl : String
    , title : String
    , twitterName : String
    }


setDocumentMetadata : DocumentMetadata -> Cmd msg
setDocumentMetadata metadata =
    setDocumentMetatags
        { description = metadata.description
        , imageUrl = Urls.fullApiUrl metadata.imageUrl ++ "?dim=500"
        , title = metadata.title
        , twitterName = Configuration.twitterName
        }


port setDocumentMetatags : DocumentMetatags -> Cmd msg
