module Deputes.ViewsHelpers exposing (..)

import Data exposing (collaborateurBySlug)
import Deputes.Item.Types exposing (..)
import Dict exposing (Dict)
import Html exposing (..)
import Html.Attributes exposing (..)
import Types exposing (Depute)


viewDepute : Depute -> Html msg
viewDepute depute =
    div [ class "card text-center mt-3" ]
        ([ img
            [ alt
                ("Photographie de "
                    ++ depute.titre
                    ++ " "
                    ++ depute.nomComplet
                )
            , class "card-img-top"
            , height 192
            , src (depute.photo)
            , style [ ( "align-self", "center" ) ]
            , width 150
            ]
            []
         , h4 [ class "card-title" ]
            [ text depute.nomComplet ]
         , p [ class "card-text" ]
            [ text depute.circonscription ]
         , p [ class "card-text" ]
            [ i [] [ text depute.groupe ] ]
         , p [ class "card-text" ]
            [ text depute.commission
            , br [] []
            , text depute.fonctionCommission
            ]
         , case depute.sitesWeb of
            [] ->
                text ""

            [ siteWeb ] ->
                p [ class "card-text" ]
                    [ a [ href siteWeb, target "_blank" ] [ text siteWeb ]
                    ]

            sitesWeb ->
                ul [] <|
                    List.map
                        (\siteWeb -> li [] [ a [ href siteWeb, target "_blank" ] [ text siteWeb ] ])
                        sitesWeb
         , case depute.courriels of
            [] ->
                text ""

            [ courriel ] ->
                p [ class "card-text" ]
                    [ a [ href <| "mailto:" ++ courriel ] [ text courriel ]
                    ]

            courriels ->
                ul [] <|
                    List.map
                        (\courriel -> li [] [ a [ href <| "mailto:" ++ courriel ] [ text courriel ] ])
                        courriels
         ]
            ++ (let
                    collaborateurs =
                        List.filterMap
                            (\slug -> Dict.get slug collaborateurBySlug)
                            depute.collaborateurs
                in
                    if List.isEmpty collaborateurs then
                        []
                    else
                        [ hr [] []
                        , h5 [ class "card-title", style [ ( "text-align", "left" ) ] ]
                            [ text "Collaborateurs" ]
                        , div [ class "row" ]
                            (List.map
                                (\collaborateur ->
                                    div [ class "col-xs-12 col-md-6" ]
                                        [ div [ class "card text-center mt-3" ]
                                            [ h5 [ class "card-title" ]
                                                [ text collaborateur.nomComplet ]
                                            , p [ class "card-text" ]
                                                [ text
                                                    (if collaborateur.enCirconscription then
                                                        "Présent en circonscription"
                                                     else
                                                        "Présent à l'Assemblée nationale"
                                                    )
                                                ]
                                            , case collaborateur.courriels of
                                                [] ->
                                                    text ""

                                                [ courriel ] ->
                                                    p [ class "card-text" ]
                                                        [ a [ href <| "mailto:" ++ courriel ] [ text courriel ]
                                                        ]

                                                courriels ->
                                                    ul [] <|
                                                        List.map
                                                            (\courriel -> li [] [ a [ href <| "mailto:" ++ courriel ] [ text courriel ] ])
                                                            courriels
                                            ]
                                        ]
                                )
                                collaborateurs
                            )
                        ]
               )
        )
