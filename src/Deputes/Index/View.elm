module Deputes.Index.View exposing (..)

import Data exposing (collaborateurBySlug)
import Deputes.Index.Types exposing (..)
import Dict exposing (Dict)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Attributes.Aria exposing (..)
import Html.Events exposing (onClick, onSubmit)
import Html.Helpers exposing (aForPath)
import ViewsHelpers


view : Model -> Html Msg
view model =
    div []
        [ viewSearchForm False model
        , if List.isEmpty model.filteredDeputes then
            div
                [ class "alert alert-warningr"
                , role "alert"
                ]
                [ strong [] [ text "Aucun député trouvé." ] ]
          else
            div [ class "row" ]
                (List.map
                    (\depute ->
                        div [ class "col-xs-12 col-sm-6 col-md-4 col-lg-3 col-xl-2" ]
                            [ div [ class "card text-center mt-3" ]
                                ([ img
                                    [ alt
                                        ("Photographie de "
                                            ++ depute.titre
                                            ++ " "
                                            ++ depute.nomComplet
                                        )
                                    , class "card-img-top"
                                    , height 192
                                    , src (depute.photo)
                                    , style [ ( "align-self", "center" ) ]
                                    , width 150
                                    ]
                                    []
                                 , aForPath
                                    (ForParent << Navigate)
                                    ("/deputes/" ++ depute.slug)
                                    []
                                    [ h4 [ class "card-title" ]
                                        [ text depute.nomComplet ]
                                    ]
                                 , p [ class "card-text" ]
                                    [ i [] [ text depute.groupe ] ]
                                 , p [ class "card-text" ]
                                    (if depute.fonctionCommission == "Membre" then
                                        [ text depute.commission ]
                                     else
                                        [ text depute.commission
                                        , br [] []
                                        , text depute.fonctionCommission
                                        ]
                                    )
                                 ]
                                    ++ (let
                                            collaborateurs =
                                                List.filterMap
                                                    (\slug -> Dict.get slug collaborateurBySlug)
                                                    depute.collaborateurs
                                        in
                                            if List.isEmpty collaborateurs then
                                                []
                                            else
                                                [ p
                                                    [ style
                                                        [ ( "margin-bottom", "0" )
                                                        , ( "text-align", "left" )
                                                        ]
                                                    ]
                                                    [ text "Collaborateurs :" ]
                                                , ul
                                                    [ style
                                                        [ ( "margin-bottom", "0" )
                                                        , ( "text-align", "left" )
                                                        ]
                                                    ]
                                                    (List.map
                                                        (\collaborateur ->
                                                            li []
                                                                [ aForPath
                                                                    (ForParent << Navigate)
                                                                    ("/deputes/" ++ depute.slug)
                                                                    []
                                                                    [ text collaborateur.nomComplet ]
                                                                ]
                                                        )
                                                        collaborateurs
                                                    )
                                                ]
                                       )
                                )
                            ]
                    )
                    model.filteredDeputes
                )
        ]


viewSearchForm : Bool -> Model -> Html Msg
viewSearchForm collapsed model =
    let
        submitButton =
            button
                [ class "btn btn-primary btn-lg btn-block"
                , onClick (ForSelf Submit)
                , type_ "button"
                ]
                [ span [ class "fa fa-search" ] []
                , text " "
                , text "Rechercher"
                ]
    in
        if collapsed then
            nav
                [ class "navbar navbar-light navbar-toggleable bg-faded" ]
                [ div [ class "navbar-collapse" ]
                    [ Html.form [ class "form-inline mr-auto", onSubmit (ForSelf Submit) ]
                        [ ViewsHelpers.viewSearchTerm
                            True
                            model.searchTerm
                            (Dict.get "searchTerm" model.errors)
                            (ForSelf << SearchTermChanged)
                        , submitButton
                        ]
                    ]
                ]
        else
            Html.form [ onSubmit (ForSelf Submit) ]
                [ ViewsHelpers.viewSearchTerm
                    True
                    model.searchTerm
                    (Dict.get "searchTerm" model.errors)
                    (ForSelf << SearchTermChanged)
                , ViewsHelpers.viewSearchGroupe
                    True
                    model.searchGroupe
                    (Dict.get "searchGroupe" model.errors)
                    (ForSelf << SearchGroupeChanged)
                , ViewsHelpers.viewSearchCommission
                    True
                    model.searchCommission
                    (Dict.get "searchCommission" model.errors)
                    (ForSelf << SearchCommissionChanged)
                , submitButton
                ]
