module Deputes.Index.State exposing (..)

import Data exposing (commissions, deputeBySlug, deputes, groupes, textSearchIndex)
import Deputes.Index.Types exposing (..)
import Dict exposing (Dict)
import ElmTextSearch
import Navigation
import Ports
import Urls


convertControlsToSearchCriteria : Model -> Result FormErrors SearchCriteria
convertControlsToSearchCriteria model =
    let
        errorsList =
            (List.filterMap
                (\( name, errorMaybe ) ->
                    case errorMaybe of
                        Just error ->
                            Just ( name, error )

                        Nothing ->
                            Nothing
                )
                [ ( "searchCommission"
                  , if String.isEmpty model.searchCommission || List.member model.searchCommission commissions then
                        Nothing
                    else
                        Just "Commission incorrecte"
                  )
                , ( "searchGroupe"
                  , if String.isEmpty model.searchGroupe || List.member model.searchGroupe groupes then
                        Nothing
                    else
                        Just "Groupe incorrect"
                  )
                , ( "searchTerm"
                  , Nothing
                  )
                ]
            )
    in
        if List.isEmpty errorsList then
            Ok
                { commission =
                    if String.isEmpty model.searchCommission then
                        Nothing
                    else
                        Just model.searchCommission
                , groupe =
                    if String.isEmpty model.searchGroupe then
                        Nothing
                    else
                        Just model.searchGroupe
                , term =
                    if String.isEmpty model.searchTerm then
                        Nothing
                    else
                        Just model.searchTerm
                }
        else
            Err (Dict.fromList errorsList)


doSearch : Model -> Model
doSearch model =
    case (convertControlsToSearchCriteria model) of
        Err errors ->
            { model | errors = errors }

        Ok searchCriteria ->
            let
                ( textSearchIndex, foundDeputes ) =
                    case searchCriteria.term of
                        Just term ->
                            let
                                ( textSearchIndex, found ) =
                                    case ElmTextSearch.search term model.textSearchIndex of
                                        Err error ->
                                            let
                                                _ =
                                                    Debug.log "Search error" error
                                            in
                                                ( model.textSearchIndex, [] )

                                        Ok result ->
                                            result
                            in
                                ( textSearchIndex
                                , List.filterMap (\( code, score ) -> Dict.get code deputeBySlug) found
                                )

                        Nothing ->
                            ( model.textSearchIndex, deputes )

                filteredDeputesByCommission =
                    case searchCriteria.commission of
                        Just commission ->
                            List.filter
                                (\depute -> depute.commission == commission)
                                foundDeputes

                        Nothing ->
                            foundDeputes

                filteredDeputes =
                    case searchCriteria.groupe of
                        Just groupe ->
                            List.filter
                                (\depute -> depute.groupe == groupe)
                                filteredDeputesByCommission

                        Nothing ->
                            filteredDeputesByCommission
            in
                { model
                    | errors = Dict.empty
                    , filteredDeputes = filteredDeputes
                    , searchCriteria = searchCriteria
                    , textSearchIndex = textSearchIndex
                }


init : Model
init =
    { errors = Dict.empty
    , filteredDeputes = deputes
    , searchCriteria =
        { commission = Nothing
        , groupe = Nothing
        , term = Nothing
        }
    , searchCommission = ""
    , searchGroupe = ""
    , searchTerm = ""
    , textSearchIndex = textSearchIndex
    }
        |> doSearch


update : InternalMsg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SearchCommissionChanged searchCommission ->
            ( doSearch { model | searchCommission = searchCommission }, Cmd.none )

        SearchGroupeChanged searchGroupe ->
            ( doSearch { model | searchGroupe = searchGroupe }, Cmd.none )

        SearchTermChanged searchTerm ->
            ( { model | searchTerm = searchTerm }, Cmd.none )

        Submit ->
            ( doSearch model, Cmd.none )


urlUpdate : Navigation.Location -> Model -> ( Model, Cmd Msg )
urlUpdate location model =
    ( model
    , Ports.setDocumentMetadata
        { description = "Index des députés"
        , imageUrl = Urls.appLogoFullUrl
        , title = "Deputes"
        }
    )
