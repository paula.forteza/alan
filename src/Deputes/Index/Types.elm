module Deputes.Index.Types exposing (..)

import Dict exposing (Dict)
import ElmTextSearch
import Types exposing (Depute, Deputes)


type ExternalMsg
    = Navigate String


type alias FormErrors =
    Dict String String


type InternalMsg
    = SearchCommissionChanged String
    | SearchGroupeChanged String
    | SearchTermChanged String
    | Submit


type alias Model =
    { errors : FormErrors
    , filteredDeputes : Deputes
    , searchCriteria : SearchCriteria
    , searchCommission : String
    , searchGroupe : String
    , searchTerm : String
    , textSearchIndex : ElmTextSearch.Index Depute
    }


type Msg
    = ForParent ExternalMsg
    | ForSelf InternalMsg


type alias MsgTranslation parentMsg =
    { onInternalMsg : InternalMsg -> parentMsg
    , onNavigate : String -> parentMsg
    }


type alias MsgTranslator parentMsg =
    Msg -> parentMsg


type alias SearchCriteria =
    { commission : Maybe String
    , groupe : Maybe String
    , term : Maybe String
    }


translateMsg : MsgTranslation parentMsg -> MsgTranslator parentMsg
translateMsg { onInternalMsg, onNavigate } msg =
    case msg of
        ForParent (Navigate path) ->
            onNavigate path

        ForSelf internalMsg ->
            onInternalMsg internalMsg
