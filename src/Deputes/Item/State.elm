module Deputes.Item.State exposing (..)

import Deputes.Item.Types exposing (..)
import Navigation
import Ports
import Types exposing (Depute)


init : Depute -> Model
init depute =
    { depute = depute }


setContext : Depute -> Model -> Model
setContext depute model =
    { model
        | depute = depute
    }


update : InternalMsg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )


urlUpdate : Navigation.Location -> Model -> ( Model, Cmd Msg )
urlUpdate location model =
    let
        depute =
            model.depute
    in
        ( model
        , Ports.setDocumentMetadata
            { description =
                "Fiche descriptive de"
                    ++ depute.titre
                    ++ " "
                    ++ depute.nomComplet
            , imageUrl = depute.photo
            , title = depute.nomComplet
            }
        )
