module Deputes.Item.View exposing (..)

import Deputes.Item.Types exposing (..)
import Deputes.ViewsHelpers exposing (viewDepute)
import Html exposing (..)


view : Model -> Html Msg
view model =
    viewDepute model.depute
