module Root.Types exposing (..)

import About.Types
import Deputes.Index.Types
import Deputes.Item.Types
import Navigation
import Routes


type alias Flags =
    {}


type alias Model =
    { aboutModel : Maybe About.Types.Model
    , deputeModel : Maybe Deputes.Item.Types.Model
    , deputesModel : Maybe Deputes.Index.Types.Model
    , location : Navigation.Location
    , route : Routes.Route
    }


type Msg
    = AboutMsg About.Types.InternalMsg
    | DeputeMsg Deputes.Item.Types.InternalMsg
    | DeputesMsg Deputes.Index.Types.InternalMsg
    | LocationChanged Navigation.Location
    | Navigate String
    | NoOp


translateAboutMsg : About.Types.MsgTranslator Msg
translateAboutMsg =
    About.Types.translateMsg
        { onInternalMsg = AboutMsg
        , onNavigate = Navigate
        }


translateDeputeMsg : Deputes.Item.Types.MsgTranslator Msg
translateDeputeMsg =
    Deputes.Item.Types.translateMsg
        { onInternalMsg = DeputeMsg
        , onNavigate = Navigate
        }


translateDeputesMsg : Deputes.Index.Types.MsgTranslator Msg
translateDeputesMsg =
    Deputes.Index.Types.translateMsg
        { onInternalMsg = DeputesMsg
        , onNavigate = Navigate
        }
