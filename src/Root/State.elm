module Root.State exposing (..)

import About.State
import Deputes.Index.State
import Deputes.Item.State
import Dom.Scroll
import Navigation
import Root.Types exposing (..)
import Routes exposing (..)
import Task


init : Flags -> Navigation.Location -> ( Model, Cmd Msg )
init flags location =
    { aboutModel = Nothing
    , deputeModel = Nothing
    , deputesModel = Nothing
    , location = location
    , route = HomeRoute
    }
        |> update (LocationChanged location)


navigate : Model -> String -> Cmd msg
navigate model path =
    -- TODO: Better handle when currentUrLPath contains a scheme, domain name, etc and not path.
    if model.location.href /= path then
        Navigation.newUrl path
    else
        Cmd.none


subscriptions : Model -> Sub Msg
subscriptions model =
    -- TODO Fix duplicate messages with port "fileContentRead", that was worked around by a "ImageSelectedStatus"
    -- constructor.
    List.filterMap identity
        [--  case model.deputeModel of
         -- Just deputeModel ->
         --     Just <| Sub.map DeputeMsg (Deputes.Item.State.subscriptions deputeModel)
         -- Nothing ->
         --     Nothing
        ]
        |> Sub.batch


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        AboutMsg childMsg ->
            case model.aboutModel of
                Just aboutModel ->
                    let
                        ( updatedAboutModel, childCmd ) =
                            About.State.update childMsg aboutModel
                    in
                        ( { model | aboutModel = Just updatedAboutModel }
                        , Cmd.map translateAboutMsg childCmd
                        )

                Nothing ->
                    ( model, Cmd.none )

        DeputeMsg childMsg ->
            case model.deputeModel of
                Just deputeModel ->
                    let
                        ( updatedDeputeModel, childCmd ) =
                            Deputes.Item.State.update childMsg deputeModel
                    in
                        ( { model | deputeModel = Just updatedDeputeModel }
                        , Cmd.map translateDeputeMsg childCmd
                        )

                Nothing ->
                    ( model, Cmd.none )

        DeputesMsg childMsg ->
            case model.deputesModel of
                Just deputesModel ->
                    let
                        ( updatedDeputesModel, childCmd ) =
                            Deputes.Index.State.update childMsg deputesModel
                    in
                        ( { model | deputesModel = Just updatedDeputesModel }
                        , Cmd.map translateDeputesMsg childCmd
                        )

                Nothing ->
                    ( model, Cmd.none )

        LocationChanged location ->
            urlUpdate location model

        Navigate path ->
            ( model, navigate model path )

        NoOp ->
            ( model, Cmd.none )


urlUpdate : Navigation.Location -> Model -> ( Model, Cmd Msg )
urlUpdate location model =
    let
        ( newModel, cmd ) =
            case parseLocation location of
                Just route ->
                    let
                        ( newModel, newCmd ) =
                            case route of
                                AboutRoute ->
                                    let
                                        aboutModel =
                                            case model.aboutModel of
                                                Just existingAboutModel ->
                                                    existingAboutModel

                                                Nothing ->
                                                    About.State.init

                                        ( updatedAboutModel, updatedAboutCmd ) =
                                            About.State.urlUpdate location aboutModel
                                    in
                                        ( { model | aboutModel = Just updatedAboutModel }
                                        , Cmd.map translateAboutMsg updatedAboutCmd
                                        )

                                DeputesRoute childRoute ->
                                    case childRoute of
                                        DeputeRoute depute deputeRoute ->
                                            let
                                                deputeModel =
                                                    case model.deputeModel of
                                                        Just existingDeputeModel ->
                                                            Deputes.Item.State.setContext depute existingDeputeModel

                                                        Nothing ->
                                                            Deputes.Item.State.init depute

                                                ( updatedDeputeModel, updatedDeputeCmd ) =
                                                    Deputes.Item.State.urlUpdate
                                                        location
                                                        -- deputeRoute
                                                        deputeModel
                                            in
                                                ( { model | deputeModel = Just updatedDeputeModel }
                                                , Cmd.map translateDeputeMsg updatedDeputeCmd
                                                )

                                        DeputesIndexRoute ->
                                            let
                                                deputesModel =
                                                    case model.deputesModel of
                                                        Just existingDeputesModel ->
                                                            existingDeputesModel

                                                        Nothing ->
                                                            Deputes.Index.State.init

                                                ( updatedDeputesModel, updatedDeputesCmd ) =
                                                    Deputes.Index.State.urlUpdate location deputesModel
                                            in
                                                ( { model | deputesModel = Just updatedDeputesModel }
                                                , Cmd.map translateDeputesMsg updatedDeputesCmd
                                                )

                                HomeRoute ->
                                    ( model, Navigation.newUrl "/deputes" )

                                NotFoundRoute path ->
                                    ( model, Cmd.none )
                    in
                        ( { newModel | route = route }, newCmd )

                Nothing ->
                    -- let
                    --     url =
                    --         location.href
                    --             |> Erl.parse
                    --     newUrl =
                    --         { url | path = url.path }
                    -- in
                    --     ( model, Navigation.modifyUrl (Erl.toString newUrl) )
                    ( model, Cmd.none )
    in
        { newModel | location = location }
            ! [ Task.attempt
                    (\result ->
                        case result of
                            Result.Err err ->
                                Debug.crash ("Dom.Scroll.toTop \"html-element\": " ++ toString err)

                            Result.Ok _ ->
                                NoOp
                    )
                    (Dom.Scroll.toTop "html-element")
              , cmd
              ]
