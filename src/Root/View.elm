module Root.View exposing (..)

import About.View
import Deputes.Index.View
import Deputes.Item.View
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Helpers exposing (aForPath)
import Root.Types exposing (..)
import Routes exposing (..)
import ViewsHelpers exposing (viewNotFound)


view : Model -> Html Msg
view model =
    div
        [ class "container-fluid" ]
        [ nav [ class "navbar navbar-fixed-top navbar-inverse navbar-toggleable-md bg-inverse" ]
            [ aForPath Navigate
                "/"
                [ class "navbar-brand", title "À l'Assemblée" ]
                [ img
                    [ src "/img/icone-96x96.png"
                    , width 30
                    , height 30
                    , class "d-inline-block align-top"
                    , alt ""
                    ]
                    []
                , text " "
                , text "À l'AN"
                ]
            , button
                [ attribute "aria-controls" "collapsable-navbar"
                , attribute "aria-expanded" "false"
                , attribute "aria-label" "Toggle navigation"
                , class "navbar-toggler navbar-toggler-right"
                , attribute "data-target" "#collapsable-navbar"
                , attribute "data-toggle" "collapse"
                , type_ "button"
                ]
                [ span [ class "navbar-toggler-icon" ] [] ]
            , div [ class "collapse navbar-collapse", id "collapsable-navbar" ]
                [ ul [ class "navbar-nav mr-auto" ]
                    -- [ li [ class "nav-item" ]
                    --     [ aForPath Navigate
                    --         "/"
                    --         [ class "nav-link" ]
                    --         [ span [ class "fa fa-search" ] []
                    --         , text " "
                    --         , text "Rechercher"
                    --         ]
                    --     ]
                    [ li [ class "nav-item" ]
                        [ aForPath Navigate
                            "/deputes"
                            [ class "nav-link" ]
                            [ span [ class "fa fa-fw fa-user-o" ] [], text " ", text "Députés" ]
                        ]
                    ]
                , ul [ class "navbar-nav" ]
                    [ li [ class "nav-item" ]
                        [ aForPath Navigate
                            "/a-propos"
                            [ class "nav-link" ]
                            [ span [ class "fa fa-fw fa-info-circle" ] [] ]
                        ]
                    ]
                ]
            ]
        , case model.route of
            AboutRoute ->
                case model.aboutModel of
                    Just aboutModel ->
                        About.View.view aboutModel
                            |> Html.map translateAboutMsg

                    Nothing ->
                        text "This should not occur: aboutModel == Nothing!"

            DeputesRoute childRoute ->
                case childRoute of
                    DeputeRoute _ _ ->
                        case model.deputeModel of
                            Just deputeModel ->
                                Deputes.Item.View.view deputeModel
                                    |> Html.map translateDeputeMsg

                            Nothing ->
                                text "This should not occur: deputeModel == Nothing!"

                    DeputesIndexRoute ->
                        case model.deputesModel of
                            Just deputesModel ->
                                Deputes.Index.View.view deputesModel
                                    |> Html.map translateDeputesMsg

                            Nothing ->
                                text "This should not occur: deputesModel == Nothing!"

            HomeRoute ->
                p
                    []
                    [ img [ src "/img/icone-192x192.png" ] []
                    , text "Home"
                    ]

            NotFoundRoute _ ->
                viewNotFound
        ]
