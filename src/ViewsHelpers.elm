module ViewsHelpers exposing (..)

import Data exposing (commissions, groupes)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Attributes.Aria exposing (..)
import Html.Events exposing (on, onInput, targetValue)
import Json.Decode


viewNotFound : Html msg
viewNotFound =
    div
        []
        [ h1 []
            [ text "Page non trouvée" ]
        , p
            -- [ style
            --     [ ( "color", "rgb(136, 136, 136)" )
            --     , ( "margin-top", "3em" )
            --     ]
            -- ]
            []
            [ text "La page demandée n'a pas été trouvée." ]
        ]


viewOption : a -> ( a, String ) -> Html msg
viewOption selectedItem ( item, label ) =
    let
        itemString =
            (toString item)

        itemString_ =
            if String.left 1 itemString == "\"" && String.right 1 itemString == "\"" then
                String.slice 1 -1 itemString
            else
                itemString
    in
        option
            [ selected (item == selectedItem)
            , value itemString_
            ]
            [ text label ]


viewSearchCommission : Bool -> String -> Maybe String -> (String -> msg) -> Html msg
viewSearchCommission inline fieldValue errorMaybe changed =
    let
        ( errorClass, errorAttributes, errorBlock ) =
            case errorMaybe of
                Just error ->
                    ( " has-danger"
                    , [ ariaDescribedby "search-commission-error" ]
                    , [ div
                            [ class "form-control-feedback"
                            , id "search-commission-error"
                            ]
                            [ text error ]
                      ]
                    )

                Nothing ->
                    ( "", [], [] )
    in
        div [ class ("form-group" ++ errorClass) ]
            ([ label
                [ class <|
                    if inline then
                        "sr-only"
                    else
                        "form-control-label"
                , for "search-commission"
                ]
                [ text "Commission" ]
             , select
                ([ class "form-control form-control-lg"
                 , id "search-commission"
                 , on "change" (Json.Decode.map changed targetValue)
                 ]
                    ++ errorAttributes
                )
                ([ viewOption fieldValue ( "", "Toutes les commissions" ) ]
                    ++ List.map
                        (\commission -> viewOption fieldValue ( commission, commission ))
                        commissions
                )
             ]
                ++ errorBlock
            )


viewSearchGroupe : Bool -> String -> Maybe String -> (String -> msg) -> Html msg
viewSearchGroupe inline fieldValue errorMaybe changed =
    let
        ( errorClass, errorAttributes, errorBlock ) =
            case errorMaybe of
                Just error ->
                    ( " has-danger"
                    , [ ariaDescribedby "search-groupe-error" ]
                    , [ div
                            [ class "form-control-feedback"
                            , id "search-groupe-error"
                            ]
                            [ text error ]
                      ]
                    )

                Nothing ->
                    ( "", [], [] )
    in
        div [ class ("form-group" ++ errorClass) ]
            ([ label
                [ class <|
                    if inline then
                        "sr-only"
                    else
                        "form-control-label"
                , for "search-groupe"
                ]
                [ text "Groupe" ]
             , select
                ([ class "form-control form-control-lg"
                 , id "search-groupe"
                 , on "change" (Json.Decode.map changed targetValue)
                 ]
                    ++ errorAttributes
                )
                ([ viewOption fieldValue ( "", "Tous les groupes" ) ]
                    ++ List.map
                        (\groupe -> viewOption fieldValue ( groupe, groupe ))
                        groupes
                )
             ]
                ++ errorBlock
            )


viewSearchTerm : Bool -> String -> Maybe String -> (String -> msg) -> Html msg
viewSearchTerm inline fieldValue errorMaybe changed =
    let
        ( errorClass, errorAttributes, errorBlock ) =
            case errorMaybe of
                Just error ->
                    ( " has-danger"
                    , [ ariaDescribedby "search-term-error" ]
                    , [ div
                            [ class "form-control-feedback"
                            , id "search-term-error"
                            ]
                            [ text error ]
                      ]
                    )

                Nothing ->
                    ( "", [], [] )
    in
        div [ class ("form-group" ++ errorClass) ]
            ([ label
                [ class <|
                    if inline then
                        "sr-only"
                    else
                        "form-control-label"
                , for "search-term"
                ]
                [ text "Texte" ]
             , input
                ([ class "form-control form-control-lg"
                 , id "search-term"
                 , placeholder "Nom de la personne recherchée"
                 , type_ "text"
                 , value fieldValue
                 , onInput changed
                 ]
                    ++ errorAttributes
                )
                []
             ]
                ++ errorBlock
            )
