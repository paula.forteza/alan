# À l'Assemblée

Trombinoscope de l'Assemblée Nationale

## Installation

    npm install

Cela installera les dépendences JavaScript dand `node_modules` et les dépendences Elm dans `elm-stuff`.

## Developpement

Lancer le serveur de développement Webpack en mode rechargement à chaud :

    npm start

Avec votre navigateur, aller sur <http://localhost:3011>.

## Production

Quand vous êtes prêts à déployer :

    npm run build

Pour tester le serveur de production :

    npm run serve

Avec votre navigateur, aller sur <http://localhost:3012>.
