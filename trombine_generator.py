#! /usr/bin/env python3


# À l'Assemblée -- Trombinoscope de l'Assemblée nationale
# By: Paula Forteza <paula@forteza.fr>
#     Emmanuel Raviart <eraviart@forteza.fr>
#
# Copyright (C) 2017 Paula Forteza & Emmanuel Raviart
# https://framagit.org/paula.forteza/alan
#
# À l'Assemblée is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# À l'Assemblée is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http:>www.gnu.org/licenses/>.


"""Generate À l'Assemblée web site from fetched pages."""


import argparse
import csv
import json
import os
import shutil
import sys
import urllib.parse

from lxml import html
from slugify import slugify


pages_repository_url = 'git@framagit.org:paula.forteza/alan-pages.git'
site_url = 'http://www2.assemblee-nationale.fr/'


data_elm_header = '''\
module Data exposing (..)

import Dict exposing (Dict)
import ElmTextSearch
import List.Extra
import Types exposing (Collaborateur, Collaborateurs, Depute, Deputes)
'''

data_elm_footer = '''\


collaborateurBySlug : Dict String Collaborateur
collaborateurBySlug =
    collaborateurs
        |> List.map (\\collaborateur -> ( collaborateur.slug, collaborateur ))
        |> Dict.fromList


commissions : List String
commissions =
    deputes
        |> List.map .commission
        |> List.Extra.unique
        |> List.sort


deputeBySlug : Dict String Depute
deputeBySlug =
    deputes
        |> List.map (\\depute -> ( depute.slug, depute ))
        |> Dict.fromList


fonctionsCommissions : List String
fonctionsCommissions =
    deputes
        |> List.map .fonctionCommission
        |> List.Extra.unique
        |> List.sort


groupes : List String
groupes =
    deputes
        |> List.map .groupe
        |> List.Extra.unique
        |> List.sort


( textSearchIndex, errors ) =
    ElmTextSearch.addDocs
        deputes
    <|
        ElmTextSearch.new
            { ref = .slug
            , fields = [ ( .slug, 2 ) ]
            , listFields = [ ( .collaborateurs, 1 ) ]
            }
_ =
    List.map
        (\\number message -> Debug.log message (toString number))
        errors
'''


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'source_dir',
        help='path of source directory HTML pages fetched from Assemblée nationale web site',
        )
    parser.add_argument(
        'assistants',
        help='path of CSV file containing a list of assistants',
        )
    parser.add_argument(
        'target_dir',
        help="path of target directory containing À l'Assemblée project source code",
        )
    args = parser.parse_args()

    assert os.path.exists(args.source_dir)
    assert os.path.exists(args.assistants)
    assert os.path.exists(args.target_dir)

    deputes_html_dir = os.path.join(args.source_dir, 'deputes', 'fiche')
    picture_source_path_by_name = {
        picture_name: os.path.join(parent_dir, picture_name)
        for parent_dir, directories_names, pictures_names in os.walk(os.path.join(args.source_dir, 'static', 'tribun'))
        for picture_name in pictures_names
        }
    pictures_target_dir = os.path.join(args.target_dir, 'static', 'img', 'photos')
    if os.path.exists(pictures_target_dir):
        shutil.rmtree(pictures_target_dir)
    os.makedirs(pictures_target_dir)
    depute_by_slug = {}
    pictures_paths = set()
    for filename in os.listdir(deputes_html_dir):
        if filename.startswith('.'):
            continue
        filename_core, extension = os.path.splitext(filename)
        if extension != '.html':
            continue
        page_root = html.parse(os.path.join(deputes_html_dir, filename))
        full_name = page_root.find('//h1').text
        if "- Président de l'Assemblée nationale" in full_name:
            full_name = full_name.replace("- Président de l'Assemblée nationale", '')
        print(filename_core, full_name)
        assert full_name.startswith(('M.', 'Mme'))
        titre, prenom, nom = full_name.split(' ', 2)
        if nom.startswith('de '):
            particule, nom = nom.split(None, 1)
        else:
            particule = None
        nom_complet = ' '.join(
            fragment
            for fragment in (prenom, particule, nom)
            if fragment is not None
            )
        circonscription = html.tostring(
            page_root.find('//p[@class="deputy-healine-sub-title"]'),
            encoding=str,
            method="text",
            with_tail=False,
            ).strip().replace('&nbsp', ' ')
        picture_name = urllib.parse.urljoin(
            site_url,
            page_root.xpath('//div[@class="deputes-image"]/img')[0].attrib['src'],
            )
        picture_name = picture_name.rsplit('/', 1)[-1]
        photo = '/img/photos/{}'.format(picture_name)
        groupe = page_root.xpath('//a[@title="Accédez à la composition du groupe"]')[0].text

        commission_elements = page_root.xpath(
            '//dl[@class="deputes-liste-attributs"]/dt[text()="Commission"]/following-sibling::dd[1]/ul/li')
        assert len(commission_elements) == 1, len(commission_elements)
        commission_element = commission_elements[0]
        fonction_commission = commission_element.text.split(None, 1)[0].strip()
        assert fonction_commission in (
            'Membre',
            'Président',
            'Présidente',
            'Rapporteur',
            'Secrétaire',
            'Vice-président',
            'Vice-présidente',
            ), fonction_commission
        fonction_commission = {
            'Présidente': 'Président',
            'Vice-présidente': 'Vice-président',
            }.get(fonction_commission, fonction_commission)
        commission = commission_element.find('a').text.strip()
        assert commission.startswith('de la '), commission
        commission = commission[len('de la '):].capitalize()
        assert commission in (
            'Commission de la défense nationale et des forces armées',
            "Commission des affaires culturelles et de l'éducation",
            'Commission des affaires économiques',
            'Commission des affaires étrangères',
            'Commission des affaires sociales',
            'Commission des finances',
            'Commission des lois',
            "Commission du développement durable et de l'aménagement du territoire",
            ), commission

        biographie_elements = page_root.xpath(
            '//dl[@class="deputes-liste-attributs"]/dt[text()="Biographie"]/following-sibling::dd[1]/ul/li')
        assert len(biographie_elements) == 2, len(biographie_elements)
        naissance = ' '.join(biographie_elements[0].text.strip().split())
        profession = biographie_elements[1].text.strip()

        suppleant_elements = page_root.xpath(
            '//dl[@class="deputes-liste-attributs"]/dt[text()=" Suppléant" or text()=" Suppléante"]'
            '/following-sibling::dd[1]/ul/li')
        assert len(suppleant_elements) <= 1, len(suppleant_elements)
        if len(suppleant_elements) == 0:
            nom_suppleant = None
            prenom_suppleant = None
            particule_suppleant = None
        else:
            nom_suppleant = suppleant_elements[0].text
            assert nom_suppleant.startswith(('M.', 'Mme'))
            titre_suppleant, prenom_suppleant, nom_suppleant = nom_suppleant.split(' ', 2)
            if nom_suppleant.startswith('de '):
                particule_suppleant, nom_suppleant = nom_suppleant.split(None, 1)
            else:
                particule_suppleant = None
        nom_complet_suppleant = ' '.join(
            fragment
            for fragment in (prenom_suppleant, particule_suppleant, nom_suppleant)
            if fragment is not None
            )

        contact_elements = page_root.xpath(
            '//dl[@class="deputes-liste-attributs"]/dt[text()="Contact"]/following-sibling::dd[1]/ul/li')
        assert len(contact_elements) >= 1, len(contact_elements)
        courriels = []
        sites_web = []
        for contact_element in contact_elements:
            href = contact_element.find('a').attrib['href']
            if href.startswith('mailto:'):
                courriels.append(href.split(':', 1)[1])
            else:
                assert contact_element.find('a').text.startswith('Visiter le site ')
                sites_web.append(href)

        suppleant = None if nom_suppleant is None else dict(
            nom=nom_suppleant,
            nomComplet=nom_complet_suppleant,
            particule=particule_suppleant,
            prenom=prenom_suppleant,
            slug=slugify(nom_complet_suppleant),
            titre=titre_suppleant,
            )

        depute = dict(
            circonscription=circonscription,
            code=filename_core,
            collaborateurs=[],
            commission=commission,
            courriels=courriels or None,
            fonctionCommission=fonction_commission,
            groupe=groupe,
            naissance=naissance,
            nom=nom,
            nomComplet=nom_complet,
            particule=particule,
            photo=photo,
            prenom=prenom,
            profession=profession,
            sitesWeb=sites_web or None,
            slug=slugify(nom_complet),
            suppleant=suppleant,
            titre=titre,
            )
        depute = {
            key: value
            for key, value in depute.items()
            if value is not None
            }
        depute_by_slug[depute['slug']] = depute
        shutil.copyfile(picture_source_path_by_name[picture_name], os.path.join(pictures_target_dir, picture_name))
        pictures_paths.add(photo)

    collaborateur_by_slug = {}
    with open(args.assistants) as csv_file:
        csv_reader = csv.reader(csv_file)
        headers = next(csv_reader)
        current_slug = None
        for row in csv_reader:
            entry = dict(zip(headers, row))
            depute_slug = slugify(entry['DÉPUTÉ'])
            if depute_slug:
                current_slug = depute_slug
            else:
                depute_slug = current_slug
            if depute_slug in ('christophe-castaner', 'bruno-le-maire', 'francois-de-rugy', 'stephane-travert'):
                # TODO
                continue
            depute = depute_by_slug.get(depute_slug)
            assert depute is not None, (entry['DÉPUTÉ'], depute_slug)
            nom_complet = entry['COLLABORATEUR(S)'].strip()
            if not nom_complet:
                continue
            prenom, nom = nom_complet.title().split(None, 1)
            if nom.lower().startswith('de '):
                particule, nom = nom.split(None, 1)
                particule = particule.lower()
            else:
                particule = None
            nom_complet = ' '.join(
                fragment
                for fragment in (prenom, particule, nom)
                if fragment is not None
                )
            collaborateur = {
                key: value
                for key, value in dict(
                    courriels=[
                        courriel
                        for courriel in [entry['MAIL'].strip()]
                        if courriel
                        ] or None,
                    deputes=[depute_slug],
                    enCirconscription=bool(entry['CIRCO ?'].strip()),
                    nom=nom,
                    nomComplet=nom_complet,
                    particule=particule,
                    prenom=prenom,
                    slug=slugify(nom_complet),
                    ).items()
                if value is not None
                }
            collaborateur_by_slug[collaborateur['slug']] = collaborateur
            depute['collaborateurs'].append(collaborateur['slug'])

    with open(os.path.join(pictures_target_dir, 'index.json'), 'w') as json_file:
        json.dump(sorted(pictures_paths), json_file, ensure_ascii=False, indent=2, sort_keys=True)

    collaborateurs = sorted(
        collaborateur_by_slug.values(),
        key=lambda collaborateur: (collaborateur['nom'], collaborateur['prenom']),
        )
    deputes = sorted(depute_by_slug.values(), key=lambda depute: (depute['nom'], depute['prenom']))

    collaborateur_fields = [
        ('courriels', write_strings_list_required),
        ('deputes', write_strings_list_required),
        ('enCirconscription', write_bool_required),
        ('nom', write_string_required),
        ('nomComplet', write_string_required),
        ('particule', write_string_optional),
        ('prenom', write_string_required),
        ('slug', write_string_required),
        ]
    depute_fields = [
        ('circonscription', write_string_required),
        ('code', write_string_required),
        ('collaborateurs', write_strings_list_required),
        ('commission', write_string_required),
        ('courriels', write_strings_list_required),
        ('fonctionCommission', write_string_required),
        ('groupe', write_string_required),
        ('nom', write_string_required),
        ('nomComplet', write_string_required),
        ('particule', write_string_optional),
        ('photo', write_string_required),
        ('prenom', write_string_required),
        ('sitesWeb', write_strings_list_required),
        ('slug', write_string_required),
        ('titre', write_string_required),
        ]
    with open(os.path.join(args.target_dir, 'src', 'Data.elm'), 'w') as elm_file:
        elm_file.write(data_elm_header)

        for collaborateur_index, collaborateur in enumerate(collaborateurs):
            variable_index, collaborateur_modulo = divmod(collaborateur_index, 100)
            if collaborateur_modulo == 0:
                if variable_index > 0:
                    elm_file.write('    ]\n')
                elm_file.write('\n\ncollaborateurs{0} : Collaborateurs\ncollaborateurs{0} =\n'.format(variable_index))
                collaborateur_indent = '    [ '
            field_indent = collaborateur_indent + '{ '
            for name, field_writer in collaborateur_fields:
                value = collaborateur.get(name)
                field_writer(elm_file, field_indent, name, value)
                field_indent = ' ' * len(collaborateur_indent) + ', '
            elm_file.write(' ' * len(collaborateur_indent) + '}\n')
            collaborateur_indent = '    , '
        elm_file.write('    ]\n')
        elm_file.write('\n\ncollaborateurs : Collaborateurs\ncollaborateurs =\n    {}\n'.format(' ++ '.join(
            'collaborateurs{}'.format(i)
            for i in range((collaborateur_index - 1) // 100 + 1)
            )))

        for depute_index, depute in enumerate(deputes):
            variable_index, depute_modulo = divmod(depute_index, 100)
            if depute_modulo == 0:
                if variable_index > 0:
                    elm_file.write('    ]\n')
                elm_file.write('\n\ndeputes{0} : Deputes\ndeputes{0} =\n'.format(variable_index))
                depute_indent = '    [ '
            field_indent = depute_indent + '{ '
            for name, field_writer in depute_fields:
                value = depute.get(name)
                field_writer(elm_file, field_indent, name, value)
                field_indent = ' ' * len(depute_indent) + ', '
            elm_file.write(' ' * len(depute_indent) + '}\n')
            depute_indent = '    , '
        elm_file.write('    ]\n')
        elm_file.write('\n\ndeputes : Deputes\ndeputes =\n    {}\n'.format(' ++ '.join(
            'deputes{}'.format(i)
            for i in range((depute_index - 1) // 100 + 1)
            )))

        elm_file.write(data_elm_footer)

    sys.exit(0)

    return 0


def write_bool_required(elm_file, indent, name, value):
    if value:
        elm_file.write('{}{} = True\n'.format(indent, name))
    else:
        elm_file.write('{}{} = False\n'.format(indent, name))


def write_string_optional(elm_file, indent, name, value):
    if value is None:
        elm_file.write('{}{} = Nothing\n'.format(indent, name))
    else:
        elm_file.write('{}{} = Just {}\n'.format(indent, name, json.dumps(value, ensure_ascii=False)))


def write_string_required(elm_file, indent, name, value):
    assert value is not None
    elm_file.write('{}{} = {}\n'.format(indent, name, json.dumps(value, ensure_ascii=False)))


def write_strings_list_required(elm_file, indent, name, values):
    if not values:
        elm_file.write('{}{} = []\n'.format(indent, name))
    elif len(values) == 1:
        elm_file.write('{}{} = [ {} ]\n'.format(indent, name, json.dumps(values[0], ensure_ascii=False)))
    else:
        elm_file.write('{}{} =\n'.format(indent, name))
        item_indent = '    [ '
        for value in values:
            elm_file.write('{}{}{}\n'.format(' ' * len(indent), item_indent, json.dumps(value, ensure_ascii=False)))
            item_indent = '    , '
        elm_file.write(' ' * len(indent) + '    ]\n')


if __name__ == '__main__':
    sys.exit(main())
