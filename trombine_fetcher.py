#! /usr/bin/env python3


# À l'Assemblée -- Trombinoscope de l'Assemblée nationale
# By: Paula Forteza <paula@forteza.fr>
#     Emmanuel Raviart <eraviart@forteza.fr>
#
# Copyright (C) 2017 Paula Forteza & Emmanuel Raviart
# https://framagit.org/paula.forteza/alan
#
# À l'Assemblée is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# À l'Assemblée is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http:>www.gnu.org/licenses/>.


"""Fetch members of Parliament from Assemblée Nationale web site.

http://www2.assemblee-nationale.fr/qui
"""


import argparse
import os
import shutil
import subprocess
import sys
import urllib.parse

from lxml import html
import requests


pages_repository_url = 'git@framagit.org:paula.forteza/alan-pages.git'
site_url = 'http://www2.assemblee-nationale.fr/'


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'target_dir',
        help='path of target directory containing members of parliament pages',
        )
    parser.add_argument(
        '-p',
        '--push',
        action='store_true',
        help='push commits to remote Git server',
        )
    parser.add_argument(
        '-r',
        '--reset',
        action='store_true',
        help='Reset the whole history of the repository instead of adding the latest version',
        )
    args = parser.parse_args()

    if not os.path.exists(args.target_dir):
        if args.reset:
            os.makedirs(args.target_dir)
        else:
            subprocess.check_call(
                ['git', 'clone', pages_repository_url, args.target_dir],
                )

    target_git_dir = os.path.join(args.target_dir, '.git')
    if os.path.exists(target_git_dir):
        assert not args.reset
    else:
        subprocess.check_call(
            ['git', 'init'],
            cwd=args.target_dir,
            )
        subprocess.check_call(
            ['git', 'remote', 'add', 'origin', pages_repository_url],
            cwd=args.target_dir,
            )

    # Fetch list of members of parliament.
    url_path = '/deputes/liste/alphabetique'
    url = urllib.parse.urljoin(site_url, url_path)
    print('Fetching alphabetical index {}'.format(url))
    response = requests.get(url)
    html_element = html.fromstring(response.content)
    html_file_path = os.path.join(args.target_dir, *url_path.strip('/').split('/')) + '.html'
    html_dir = os.path.dirname(html_file_path)
    os.makedirs(html_dir, exist_ok=True)
    with open(html_file_path, 'wb') as html_file:
        html_file.write(response.content)

    # Fetch each member of parliament.
    deputies_dir = os.path.join(args.target_dir, 'deputes', 'fiche')
    if os.path.exists(deputies_dir):
        shutil.rmtree(deputies_dir)
    pictures_urls = set()
    for a_element in html_element.xpath('.//div[@id="deputes-list"]//ul/li/a'):
        url_path = a_element.attrib['href']
        url = urllib.parse.urljoin(site_url, url_path)
        print('Fetching parliament member {}: {}'.format(a_element.text, url))
        response = requests.get(url)
        html_element = html.fromstring(response.content)
        html_file_path = os.path.join(args.target_dir, *url_path.strip('/').split('/')) + '.html'
        html_dir = os.path.dirname(html_file_path)
        os.makedirs(html_dir, exist_ok=True)
        with open(html_file_path, 'wb') as html_file:
            html_file.write(response.content)
        for img_element in html_element.xpath('.//div[@class="deputes-image"]/img'):
            pictures_urls.add(img_element.attrib['src'])

    # Fetch each picture.
    pictures_dir = os.path.join(args.target_dir, 'static', 'tribun')
    if os.path.exists(pictures_dir):
        shutil.rmtree(pictures_dir)
    for picture_url in sorted(pictures_urls):
        url = urllib.parse.urljoin(site_url, picture_url)
        print('Fetching picture: {}'.format(url))
        response = requests.get(url)
        picture_file_path = os.path.join(args.target_dir, *urllib.parse.urlsplit(url).path.strip('/').split('/'))
        picture_dir = os.path.dirname(picture_file_path)
        os.makedirs(picture_dir, exist_ok=True)
        with open(picture_file_path, 'wb') as picture_file:
            picture_file.write(response.content)

    # Update git repository.

    subprocess.check_call(
        ['git', 'add', '.'],
        cwd=args.target_dir,
        )
    # Note: Don't fail when there is nothing to commit.
    subprocess.call(
        ['git', 'commit', '-m', 'New fetch'],
        cwd=args.target_dir,
        )
    if args.push:
        # Note: Don't fail when there is nothing to push.
        if args.reset:
            subprocess.call(
                ['git', 'push', '-f', 'origin', 'master'],
                cwd=args.target_dir,
                )
        else:
            subprocess.call(
                ['git', 'push', 'origin', 'master'],
                cwd=args.target_dir,
                )

    return 0


if __name__ == '__main__':
    sys.exit(main())
